'''
Checking output: example

@author: Natalia Vale Asari@Krakow - 08/Feb/2017
'''

# ** Standard python libraries
import os

# **  Sci/astro python libraries
import numpy as np
import matplotlib.pyplot as plt

# ** BOND libraries

# Clever reader
import bond.strongLinesReader as slr

# Some other useful functions
from bond.plotsPDFs import plot_NOvsOH_all, plot_PDFs_source

# Config
from bond.config import get_config, default_config_path

#=========================================================================
# ==> Read config
cfg = get_config()

outDir     = cfg.get('output', 'outDir')
outPreffix = cfg.get('output', 'outPreffix')

#=========================================================================
# ===> Read output file

# Read hdf5 table with results
fitFile = os.path.join(outDir, '%s.hdf5' % outPreffix)
f = slr.readFit(outPreffix, fileName = fitFile, forceRead = False)

#=========================================================================
# ===> Examples of data access

# Plot the joint O/H, N/O PDF for source #3 (in python the counting starts from 0)
iSource = 2
jPDF__j = f.jPDF__csj['all'][iSource]
Xgrid = f.jPDF_Xgrid

plt.figure(1)
plt.clf()
im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap='Blues')

plt.xlabel(r'12 + $\log O/H$')
plt.ylabel(r'$\log N/O$')


# FIXME not working
# Plot the marginalized PDF for N/O for source #3 (in python the counting starts from 0)
iSource = 2
param = 'OXYGEN'  # To see other parameters: print f.parName__p

x = f.bParCen__pb[param]
y = f.PDF__cpsb['all'][param][iSource]

plt.figure(1)
plt.clf()
plt.plot(12+x, y/y.max())

#=========================================================================
# EOF


