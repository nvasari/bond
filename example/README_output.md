The output starts with:

    id
    name
    
If you are interested in both O/H and N/O:
    
    logO/H_jmod       Joint-PDF mode, i.e. the maximum a posteriori value for log O/H.
    logN/O_jmod       Joint-PDF mode, i.e. the maximum a posteriori value for log N/O.
    
And id you want to draw the covariance uncertainty ellipse in those values:    
    
    logO/H_jc68_cen   Centre of the 1-sigma uncertainty elipse in log O/H.
    logO/H_jc68_sig   Size of the 1-sigma uncertainty elipse in log O/H.
    logN/O_jc68_cen   Centre of the 1-sigma uncertainty elipse in log N/O.
    logN/O_jc68_sig   Size of the 1-sigma uncertainty elipse in log N/O.
    jc68_cov          Covariance of the 1-sigma uncertainty elipse in log O/H.
    jc68_scale        Scaling for the 1-sigma uncertainty elipse in log O/H.

If you want to use either O/H or N/O by itself:

    logO/H_mmed       Median of the marginalized log O/H PDF.
    logN/O_mmed       Median of the marginalized log N/O PDF.
    logO/H_mp68_low   The 16th percetile of the marginalized log O/H PDF.
    logO/H_mp68_upp   The 84th percetile of the marginalized log O/H PDF.
    logN/O_mp68_low   The 16th percetile of the marginalized log N/O PDF.
    logN/O_mp68_upp   The 84th percetile of the marginalized log N/O PDF.


See the file checking.py for a few useful parameters in the hdf5 files and how to access them in python:
    
--
08/Feb/2017
