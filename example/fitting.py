'''
Example of source fitting

@author: Natalia Vale Asari@UFSC - 25/Apr/2016
'''

# ** Standard python libraries
import os
import sys
import time
import re

# **  Sci/astro python libraries
import numpy as np
import matplotlib.pyplot as plt
import astropy.table
from astropy import log

# ** BOND libraries

# BOND main
import bond.strongLinesHelper as sl

# Clever reader
import bond.strongLinesReader as slr

# Fitting helper
import bond.fittingHelper as fh

# Some other useful functions
from bond.natastro import utils
import bond.plotHelpers as pl
from bond.saveTableTxt import saveTableTxt
from bond.plotsPDFs import plot_NOvsOH_summary

# Config
from bond.config import get_config, default_config_path

#=========================================================================
# ==> Read config
cfg = get_config()

gridDir = cfg.get('data', 'gridDir')
srcDir  = cfg.get('data', 'srcDir')
srcFile = cfg.get('data', 'srcFile')
srcName = cfg.get('data', 'srcName')
tabInLog = cfg.getboolean('data', 'tabInLog')
 
outDir     = cfg.get('output', 'outDir')
outPreffix = cfg.get('output', 'outPreffix')

sourcesToFit = cfg.get('fitting', 'sourcesToFit')
nProc        = cfg.getint(    'fitting', 'nProc')
saveFile     = cfg.getboolean('fitting', 'saveFile')
recalc       = cfg.getboolean('fitting', 'recalc')
overwrite    = cfg.getboolean('fitting', 'overwrite')
debug        = cfg.getboolean('fitting', 'debug')
reAddSources = cfg.getboolean('fitting', 'reAddSources')
smallFiles   = cfg.getboolean('fitting', 'smallFiles')



# Fit all sources
if (sourcesToFit == '') | (sourcesToFit == 'all'):
    fitAll = True

# Fit only a few sources
else:
    fitAll = False

    # Convert the string sourcesToTxt into an index
    aux1 = sourcesToFit.split(',')

    inds = []
    for r in aux1:
        ii = [int(x) for x in r.split(':')]
        if (len(ii) == 2):
            ii = range(ii[0], ii[1])
        inds.extend(ii)
    
    inds = np.unique(inds)
    
#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
#S = slr.readSources(sources, debug = False, forceRead = False)
S = slr.readSources(srcName, fileName = os.path.join(srcDir, srcFile), tableIsInLog = tabInLog, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]
gScenario = 'Goctree'

#=========================================================================
# ===> Fitting

# ===> Naming fit
G = slr.f[gScenario]
G.selectScenarios(findBimodality=True)
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
log.info("@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario))

# ===> Output file & dir
dirName = outDir
baseName = outPreffix

# ===> Choose the sources to be fitted
flag_to_fit = (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['F6584'] > -999) & (S.tabData['F7135'] > -999)  & (S.tabData['F3869'] > -999) & (S.tabData['F5876'] > -999)
iSourcesAll = np.where(flag_to_fit)[0]
# Select just a few sources to fit
if fitAll:
    iSources = iSourcesAll
else:
    iSources = [ind for ind in inds if ind in iSourcesAll]

log.info("@@> Fitting N = %i sources: %s (%i selected / %i in original table)" % (len(iSources), iSources, len(iSourcesAll), len(flag_to_fit)))

# ===> Fit sources 
log.info("@@> Starting %s" % baseName)

# Start fit
f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = iSources, setupParBins = True, debug = debug)

# Run for each source
fh.run(nProc = nProc, saveFile = saveFile, recalc = recalc, overwrite = overwrite, smallFiles = smallFiles)

# Consolidate results into a single hdf5 file
f.combineSources_singleFile(iSources = iSources, reAddSources = reAddSources)

# Load results from hdf5 file
fitFile = os.path.join(dirName, '%s.hdf5' % baseName)
f2 = slr.readFit(baseName, fileName = fitFile, forceRead = True)
f2.sources = f.sources
f2.grid = f.grid
f2.flagSourcesFitted()

# Save results in a txt file
txtFile = os.path.join(dirName, '%s.txt' % baseName)
f2.saveToTXT(txtFile, overwrite = overwrite)

# Plot PDFs for each source and for all sources
plot_NOvsOH_summary(f2, outDir=outDir)

#=========================================================================
# EOF
