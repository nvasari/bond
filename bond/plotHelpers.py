# External
from astropy import log
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import matplotlib.colors as mplcolors
import seaborn as sns

# External / my libraries
import bond.natastro.plotutils as nplot

# Internal
from bond.natastro import utils

#=========================================================================
def colour(category, adjust = None):

    catColours = {
        'BOND' : sns.xkcd_rgb["cobalt"],
        'Te'   : sns.xkcd_rgb["black"],
        'SL'   : sns.xkcd_rgb["orange"],
        'obs'  : sns.xkcd_rgb["purple"],
        'fake' : sns.xkcd_rgb["grass green"]
    }
    
    if category in catColours.keys():
        colour = catColours[category]
    else:
        colour = 'k'

    if adjust is not None:
        cs = sns.light_palette(colour, 100)
        colour = cs[np.int(adjust*100)]

    return colour
#=========================================================================

#=========================================================================
def lims(key):

    lims = {
        'logN2O' : (-1.9, 0.1),
        'OXYGEN' : (6.91, 9.3),
    }
    
    if key in lims.keys():
        lim = lims[key]
    else:
        lim = (None, None)

    return lim
#=========================================================================
    
#=========================================================================
def label(key, short = False):
    labels = {
        'ewHb'   : r'$W_{\mathrm{H\beta}}$ [$\AA$]' ,
        'T_OXYGEN_vol_1'     : r'$T_\mathrm{[N\,\textsc{ii}]}$ [K]',
        'T_OXYGEN_vol_2'     : r'$T_\mathrm{[O\,\textsc{iii}]}$ [K]',
        'T_OXYGEN_vol_1/1e3' : r'$T_\mathrm{[N\,\textsc{ii}]}$ [$10^3$ K]',
        'T_OXYGEN_vol_2/1e3' : r'$T_\mathrm{[O\,\textsc{iii}]}$ [$10^3$ K]',
        'A_OXYGEN_vol_1'     : r'$\mathrm{O^+/O}$',
        'A_NITROGEN_vol_1'   : r'$\mathrm{N^+/N}$',
        'lgA_OXYGEN_vol_1'   : r'$\log \, \mathrm{O^+/O}$',
        'lgA_NITROGEN_vol_1' : r'$\log \, \mathrm{N^+/N}$',
        'tln2'   : r'T[NII] [kK]'  ,
        'trn2'   : r'T[NII] [kK]'  ,
        'tlo2'   : r'T[OII] [kK]'  ,
        'tro2'   : r'T[OII] [kK]'  ,
        'tlo3'   : r'T[OIII] [kK]' ,
        'tro3'   : r'T[OIII] [kK]' ,
        'tls3'   : r'T[SIII] [kK]' ,
        'trs3'   : r'T[SIII] [kK]' ,
        'des2'   : r'log n[SII] [cm$^3$]' ,
        'OXYGEN' : r'log O/H' ,
        'logN2O' : r'log N/O' ,
        'logUin' : r'$\log U$' ,
        'QHe_QH': r'$Q_{\mathrm{He}^0} / Q_{\mathrm{H}^0}$',
        'F3727'  : r'log [O {\scshape ii}]3727/H$\beta$'  ,
        'F3869'  : r'log [Ne {\scshape iii}]3869/H$\beta$'    ,
        'F4363'  : r'log [O {\scshape iii}]4363/H$\beta$' ,
        'F5007'  : r'log [O {\scshape iii}]5007/H$\beta$' ,
        'F5755'  : r'log [N {\scshape ii}]5755/H$\beta$'  ,
        'F5876'  : r'log He {\scshape i} 5876/H$\beta$'  ,
        'linF5876' : r'He {\scshape i} 5876/H$\beta$'  ,
        'F6312'  : r'log [SIII]6312/H$\beta$' ,
        'F6584'  : r'log [N {\scshape ii}]6584/H$\beta$'  ,
        'F7135'  : r'log [Ar {\scshape iii}]7135/H$\beta$'    ,
        'N2Ha'   : r'log [N {\scshape ii}]6584/H$\alpha$'  ,
        'O3O2'   : r'log [O {\scshape iii}]$_\mathrm{S}$/[O {\scshape ii}]'   ,
        'N2O2'   : r'log [N {\scshape ii}]6584/[O {\scshape ii}]3727'    ,
        'Ne3O2'  : r'log [Ne {\scshape iii}]/[O {\scshape ii}]'   ,
        'O3N2'   : r'log [O {\scshape iii}]5007/[N {\scshape ii}]6584'   ,
        'RO3'    : r'log [O {\scshape iii}]4363/[O {\scshape iii}]5007'  ,
        'RN2'    : r'log [N {\scshape ii}]5755/[N {\scshape ii}]6584'   ,
        'Ar3O3'  : r'log [Ar {\scshape iii}]7135/[O {\scshape iii}]5007' ,
        'Ar3Ne3' : r'log [Ar {\scshape iii}]7135/[Ne {\scshape iii}]3869'    ,
        'O23'    : r'log ([O {\scshape ii}]+[O {\scshape iii}]$_\mathrm{S}$)/H$\beta$' ,
        'r/r25'  : r'$r/r_{25}$' ,
        'M91_OH' : r'log O/H' ,
        'AO_ON'  : r'log O/H' ,
        'AN_ON'  : r'log N/O' ,
        'd_OH_2m6' : r'$\Delta$ log O/H [2$-$6Myr]',
        'd_OH2_0.03m3.00' : r'$\Delta$ log O/H [sp$-$sh]',
        'd_OH6_0.03m3.00' : r'$\Delta$ log O/H [sp$-$sh]',
        'd_NO_2m6' : r'$\Delta$ log N/O [2$-$6Myr]',
        'd_NO2_0.03m3.00' : r'$\Delta$ log N/O [sp$-$sh]',
        'd_NO6_0.03m3.00' : r'$\Delta$ log N/O [sp$-$sh]',
    }

    
    short_labels = {
        'F3727'  : r'log [O {\scshape ii}]/H$\beta$'  ,
        'F5007'  : r'log [O {\scshape iii}]/H$\beta$' ,
        'F5755'  : r'log [N {\scshape ii}]/H$\beta$'  ,
        'F5876'  : r'log He {\scshape i}/H$\beta$'  ,
        'F6584'  : r'log [N {\scshape ii}]/H$\beta$'  ,
        'N2Ha'   : r'log [N {\scshape ii}]/H$\alpha$'  ,
        'Ar3Ne3' : r'log [Ar {\scshape iii}]/[Ne {\scshape iii}]'    ,        
        'RO3'    : r'log [O {\scshape iii}]4363/5007'  ,
    }
    
    if short:
        for k, v in short_labels.items():
            labels[k] = v

    if key in labels.keys():
        label = labels[key]
    else:
        label = key.replace('_', ' ')

    return label
#=========================================================================

#=========================================================================
def adjust_subplots(fig, desired_box_ratioN = 1., exclude_subp = []):    
    # http://stackoverflow.com/questions/14907062/matplotlib-aspect-ratio-in-subplots-with-various-y-axes
    for i, ax in enumerate(fig.get_axes(), start=1):
        if i not in exclude_subp:
            temp_inverse_axis_ratioN = abs( (ax.get_xlim()[1] - ax.get_xlim()[0])/(ax.get_ylim()[1] - ax.get_ylim()[0]) )
            ax.set(aspect = desired_box_ratioN * temp_inverse_axis_ratioN, adjustable='box-forced')
#=========================================================================

#=========================================================================
def save_figs_paper(fig, fileName, raster=True, dpi=300, savePDF = True, saveEPS = True,
                    pdftocairo = True, epsFromPdf = True):
    if raster == True:

        for ax in fig.get_axes():
            ax.set_rasterization_zorder(1)
        if savePDF:
            nplot.save(fig, '%s.pdf' % fileName, rasterized=True, dpi=dpi, pdftocairo=pdftocairo)
        if saveEPS:
            nplot.save(fig, '%s.eps' % fileName, rasterized=True, dpi=dpi, epsFromPdf=epsFromPdf)    
        
    else:
        
        for ax in fig.get_axes():
            ax.set_rasterization_zorder(-10)
        nplot.save(fig, '%s.pdf' % fileName, rasterized=False, dpi=dpi, pdftocairo=True)
#=========================================================================

#=========================================================================
def label_panels(fig, x = 0.05, y = 0.95, labels = None, addToLabel = None, va='top', **kwargs):
    import string
    if labels is None:
        labels = list(string.ascii_lowercase)
        labels = ['(%s)' % a for a in labels]
        
    if addToLabel is not None:
        labels = ['%s %s' % (a, b) for a, b in zip(labels, addToLabel)]

    for i, ax in enumerate(fig.get_axes()):
        ax.text(x, y, labels[i], transform=ax.transAxes, va = va, **kwargs)
#=========================================================================

#=========================================================================
def label_Npoi(ax, N, xNpoi = 0.95, yNpoi = 0.95, fontsize = 'medium'):
    ax.text(xNpoi, yNpoi, N, transform=ax.transAxes, va='top', ha='right', fontdict = {'size': fontsize})
#=========================================================================

#=========================================================================
def title_Npoi(N, addToTitle = None, title = None):
    if title is None:
        title = r"N = %s" % N
    else:
        title = title
        
    if addToTitle is not None:
        title = r"%s %s" % (addToTitle, title)

    plt.title(title)
#=========================================================================

#=========================================================================
def plot_table_xy(table, xlab, ylab, table_y = None, colours = None, **kwargs):
    
    if colours is None:
        colours = 'k'
        if 'colour' in table.keys():
            colours = table['colour']

    if table_y is None:
        table_y = table

    return plot_xy(table[xlab], table_y[ylab], xlab = xlab, ylab = ylab, colours = colours, **kwargs)
#=========================================================================

#=========================================================================
def plot_xy(x, y, xlab = None, ylab = None, dx = 0, dy = 0, xerr = None, yerr = None, flag = None, colours = None, 
            addToTitle = None, title = None, pltTitle = True, pltXlab = True, pltYlab = True, 
            pltNpoi = False, labSize = 'medium', xNpoi = 0.95, yNpoi = 0.95, ax = None,
            s = 4, marker = ',', lw = 0, set_facecolors=True, set_edgecolor=True, **kwargs):

    f = (x > -999) & (y > -999)
    if flag is None:
        flag = f
    else:
        flag = flag & f

    if ax is not None:
        plt.sca(ax)
        
    if colours is None:
        colours = 'k'
    elif (np.asarray([colours]).shape[-1] > 1):
        if len(colours) == len(flag):
            colours = colours[flag]

    facecolors, edgecolor = 'none', 'none'
    if set_facecolors:
        facecolors = colours
    if set_edgecolor:
        edgecolor = colours
            
    aux = plt.scatter(dx+x[flag], dy+y[flag], facecolors = facecolors, edgecolor = edgecolor, marker = marker, lw = lw, s = s, **kwargs)

    if xerr is not None:
        plt.errorbar(dx+x[flag], dy+y[flag], xerr=xerr[..., flag], fmt='none', ecolor=facecolors, elinewidth=1, capsize=0, alpha=0.5)
        
    if yerr is not None:
        plt.errorbar(dx+x[flag], dy+y[flag], yerr=yerr[..., flag], fmt='none', ecolor=facecolors, elinewidth=1, capsize=0, alpha=0.5)

    if pltTitle:
        title_Npoi(np.ma.sum(flag), addToTitle, title)

    if pltNpoi:
        label_Npoi(ax, np.ma.sum(flag), xNpoi=xNpoi, yNpoi=yNpoi, fontsize = labSize)

    if pltXlab:
        plt.xlabel(label(xlab))
        if dx != 0:
            plt.xlabel('%s + %s' % (dx, label(xlab)))
    if pltYlab:
        plt.ylabel(label(ylab))
        if dy != 0:
            plt.ylabel('%s + %s' % (dy, label(ylab)))
            
    return aux
#=========================================================================

#=========================================================================
def plot_fit(fit, x = None, y = None, xlab = None, ylab = None, ax = None,
             ftype = 'jpdf', branch = 'all', flag = None, 
             colours = None, pltLegend = True,  pltUncer = False,
             annotatePts = False, dx = 0, dy = 0, **kwargs):

    f = fit

    # Choosing pdf solution
    fresult = f.PDFSummaries__tcps[ftype][branch]

    if (x is None) & (xlab is not None):
        x = fresult[xlab][..., 0]
        if ftype == 'ave':
            xerr = f.PDFSummaries__tcps['sig'][branch][xlab]
        if ftype == 'med':
            p16 = f.PDFSummaries__tcps['p16'][branch][xlab]
            p84 = f.PDFSummaries__tcps['p84'][branch][xlab]
            xerr = np.array((x-p16, p84-x))
        
    if (y is None) & (ylab is not None):
        y = fresult[ylab][..., 0]
        if ftype == 'ave':
            yerr = f.PDFSummaries__tcps['sig'][branch][ylab]
        if ftype == 'med':
            p16 = f.PDFSummaries__tcps['p16'][branch][ylab]
            p84 = f.PDFSummaries__tcps['p84'][branch][ylab]
            yerr = np.array((y-p16, p84-y))
            
    if flag is None:
        flag = f.flagSources
    else:
        flag = flag & f.flagSources

    if not pltUncer:
        xerr, yerr = None, None

    plot_xy(x, y, xlab = xlab, ylab = ylab, xerr = xerr, yerr = yerr, flag = flag, colours = colours, label = label, ax = ax, dx = dx, dy = dy, **kwargs)

    if annotatePts:
        annotatePoints(x+dx, y+dy, flag = flag, ax = ax)
        
    if pltLegend:
        plt.legend(loc = 'lower left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.05, 0.05), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')
#=========================================================================

#=========================================================================
def annotatePoints(x, y, flag = None, ax = None):

    if ax is not None:
        plt.sca(ax)

    if flag is None:
        flag = np.ones_like(x, 'bool')

    markers = np.where(flag)[0]
    
    for i in markers:
        plt.annotate( i, xy=(x[i], y[i]) )
#=========================================================================

#=========================================================================
def plot_fit_confEllipse(fit, flag = None, jtype = 'jc05', branch = 'all', dx = 12, pars = None, pltAve = False,
                         marker='.', s=30, colours='b', alpha = 1, facecolor = 'none', **kwargs):

    if pars is None:
        pars = fit.parsJointPDF

    if flag is None:
        iSources = fit.sourcesRowsFitted
    else:
        iSources = np.where(flag)[0]
        
    j = fit.PDFSummaries__tcps[jtype][branch]

    for iSrc in iSources:
        
        ave = [0] * len(pars)
        sig = [0] * len(pars)
        
        for i, P in enumerate(pars):
            ave[i] = j[P][iSrc][0]
            sig[i], rho_xy, scale = j[P][iSrc][1:]
            
        cov__xy = utils.calcCovMatrix(sig[0], sig[1], rho_xy)

        if not np.any(np.isnan(cov__xy)):
            plot_ellipse_covMatrix(ave, cov__xy, dx = dx, scale = scale, alpha = alpha, facecolor = facecolor, **kwargs)

        if pltAve:
            plt.scatter(ave[0]+dx, ave[1], marker=marker, s=s, c=colours, lw = 0)            
#=========================================================================

#=========================================================================
def plot_fit_obsAndFit(fit, xlab, ylab, x_data = True, y_data = False, diff_y = True, pltZero = True,
                       labelObs = True, labelFit = True,
                       ftype = 'jmod', branch = 'all', flag = None, **kwargs):

    # Choosing branch
    f = fit
    fresult = f.PDFSummaries__tcps[ftype][branch]

    if x_data:
        x = f.sources.tabData[xlab]
        xlabel = label(xlab)
        if labelObs:
            xlabel = r'%s [obs]' % xlabel
    else:
        x = fresult[xlab][..., 0]
        xlabel = label(xlab)
        if labelFit:
            xlabel = r'%s [fit]' % xlabel

    if y_data:
        y = f.sources.tabData[ylab]
        ylabel = label(ylab)
        if labelObs:
            ylabel = r'%s [obs]' % ylabel
    else:
        y = fresult[ylab][..., 0]
        ylabel = label(ylab)
        if labelFit:
            ylabel = r'%s [obs]' % ylabel

    if diff_y:
        y = utils.safe_sub(y, fit.sources.tabData[ylab])
        #ylabel = r'$\Delta$%s [fit$-$obs]' % label(ylab)
        ylabel = r'$\Delta$%s' % label(ylab)
        
    plot_fit(fit, x, y, xlab = xlabel, ylab = ylabel, flag = flag, ftype = ftype, branch = branch, **kwargs)

    
    if (diff_y) & (pltZero):
        plt.axhline(0, color = '#666666', lw = 1)
#=========================================================================

#=========================================================================
def plot_grid_3D(grid, sources = None, iSource = None, d = None, 
                 fignumber=1, branch='all', 
                 x1 = 'F3727', x2 = 'F6584', x3 = 'F6584', 
                 color = 'OXYGEN', size = 'logN2O',
                 cmap=None, **kwargs):
    
    if cmap is None:
        palette = sns.color_palette("hls", 256)[::-1][30:]
        ccentre = palette[len(palette)/2]
        cmap = mplcolors.ListedColormap(palette)
        
    try:
        f_log_chi2 = utils.safe_log10(fit.chi2__gs[:, iSource])[flag]
    except:
        f_log_chi2 = np.full(grid.nGrid, 3)
        
    flag = np.ones(grid.nGrid, 'bool')
    if branch != 'all':
        flag = grid.branch[branch].copy()
    
    tabGrid = grid.tabGrid[flag]
    if sources is not None:
        tabData = sources.tabData
        
    # Redefine variable names for grid
    gcolor = tabGrid[color]
    g1     = tabGrid[x1]
    g2     = tabGrid[x2]
    g3     = tabGrid[x3]
    cmin, cmax = gcolor.min(), gcolor.max()

    # Redefine variable names for sources
    if iSource is not None:
        scolor = tabData[color][iSource]
        s1     = tabData[x1][iSource]
        s2     = tabData[x1][iSource]
        s3     = tabData[x1][iSource]
        cmin, cmax = scolor - 0.5, scolor + 0.5
    
    # Min anx max oxygen values
    #omin, omax = 12+fit.grid.inputVars__u['OXYGEN'].min(), 12+fit.grid.inputVars__u['OXYGEN'].max()
    
    # Define how far from the source we want to plot models. If you want to plot the whole grid, set d to an absurdly large number.
    if iSource is not None:
        ff = (abs(g1 - s1) <= d) & (abs(g2 - s2) <= d) &  (abs(g3 - s3) <= d)
    else:
        ff = np.ones_like(g1, 'bool')
    
    # Plot grid in 3D
    fig = plt.figure(fignumber)
    plt.clf()
    ax = fig.add_subplot(111, projection='3d')
    
    if (ff.sum() > 0):
        g = ax.scatter(g1[ff], g2[ff], g3[ff], s=5, c=gcolor, lw = 0, vmin=cmin, vmax=cmax, cmap=cmap, **kwargs)
    
    # Plot source
    if iSource is not None:
        s = ax.scatter(s1, s2, s3, c=scolor, lw = 1, s=200, marker="*", vmin=cmin, vmax=cmax, cmap=cmap)
    
    ax.set_xlabel(label(x1))
    ax.set_ylabel(label(x2))
    ax.set_zlabel(label(x3))
    
    cb = fig.colorbar(g)
    cb.set_label(label(color))

    return ax
#=========================================================================

#=========================================================================
def plot_grid_3D_old(d, fit, iSource, fignumber=1, branch='all', x3 = 'F6584', cmap=None, **kwargs):
    
    if cmap is None:
        palette = sns.color_palette("hls", 256)[::-1][30:]
        ccentre = palette[len(palette)/2]
        cmap = mplcolors.ListedColormap(palette)
        
    flag = np.ones(fit.grid.nGrid, 'bool')
    if branch != 'all':
        flag = fit.grid.branch[branch].copy()
    
    tabData = fit.sources.tabData
    tabGrid = fit.grid.tabGrid[flag]
    try:
        f_log_chi2 = utils.safe_log10(fit.chi2__gs[:, iSource])[flag]
    except:
        f_log_chi2 = np.full(fit.grid.nGrid, 3)
        
    # Redefine variable names for grid
    g_OH   = tabGrid['OXYGEN'] + 12
    g_NO   = tabGrid['logN2O']
    g_4363 = tabGrid['F4363' ]
    g_O2Hb = tabGrid['F3727' ]
    g_O3Hb = tabGrid['F5007' ]
    g_N2Hb = tabGrid['F6584' ]

    # Redefine variable names for sources
    s_4363 = tabData['F4363' ][iSource]
    s_O2Hb = tabData['F3727' ][iSource]
    s_O3Hb = tabData['F5007' ][iSource]
    s_N2Hb = tabData['F6584' ][iSource]
    s_OH   = tabData['OXYGEN'][iSource] + 12

    if (x3 == 'F4363'):
        g_x3, s_x3 = g_4363, s_4363
    else:
        g_x3, s_x3 = g_N2Hb, s_N2Hb
        
    
    # Min anx max oxygen values
    #omin, omax = 12+fit.grid.inputVars__u['OXYGEN'].min(), 12+fit.grid.inputVars__u['OXYGEN'].max()
    omin, omax = s_OH - 0.5, s_OH + 0.5
    
    # Define how far from the source we want to plot models. If you want to plot the whole grid, set d to an absurdly large number.
    ff = (abs(g_O2Hb - s_O2Hb) <= d) & (abs(g_O3Hb - s_O3Hb) <= d) &  (abs(g_N2Hb - s_N2Hb) <= d)
    
    # Plot grid in 3D
    fig = plt.figure(fignumber)
    plt.clf()
    ax = fig.add_subplot(111, projection='3d')
    
    if (ff.sum() > 0):
        #g = ax.scatter(g_O2Hb[ff], g_O3Hb[ff], g_N2Hb[ff], s=(g_NO[ff] + 4)**3, c=f_log_chi2[ff], lw = 0)
        g = ax.scatter(g_O2Hb[ff], g_O3Hb[ff], g_x3[ff], s=(5-f_log_chi2[ff])**3, c=g_OH[ff], lw = 0, vmin=omin, vmax=omax, cmap=cmap, **kwargs)
    
    # Plot source
    s = ax.scatter(s_O2Hb, s_O3Hb, s_x3, c=s_OH, lw = 1, s=200, marker="*", vmin=omin, vmax=omax, cmap=cmap)
    
    ax.set_xlabel(label('F3727'))
    ax.set_ylabel(label('F5007'))
    ax.set_zlabel(label(x3))
    
    cb = fig.colorbar(s)
    cb.set_label('O/H')
    #cb.set_label('$\log \chi^2$')

    return ax
#=========================================================================

#=========================================================================
def allPlotsSources(sources, figNumber = 1, figsize=(11,13), saveFigs = False):

    nplot.plotSetup(300, lw = 2., fontsize=10, override_params={'text.usetex': False, 'font.weight': 'normal'})

    # First plot
    fig = plt.figure(figNumber, figsize=figsize)
    fig.clf()
    nplot.title_date()
    

    subp = (5, 4)
    
    ax = plt.subplot2grid( subp, (0, 0) )
    plot_table_xy(sources.tabData, 'O23', 'F5755')
    plt.xlim(0.2, 1.2)
    plt.ylim(-3.5, 0)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (0, 1) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'RO3', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-4, 0)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (0, 2) )
    plot_table_xy(sources.tabData, 'O23', 'F4363')
    plt.xlim(0.2, 1.2)
    plt.ylim(-3.5, 0)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (0, 3) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'O3N2', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 3)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (1, 0) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'O23', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 2)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (1, 1) )
    plot_table_xy(sources.tabData, 'O3N2', 'Ar3O3')
    plt.ylim(-2, 3)
    plt.ylim(-3, 1)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (1, 2) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'logN2O', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 1)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (1, 3) )
    plot_table_xy(sources.tabData, 'O23', 'F6312')
    plt.xlim(0.2, 1.2)
    plt.ylim(-3.5, 0)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (2, 0) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'O3O2', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 2)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (2, 1) )
    plot_table_xy(sources.tabData, 'F6584', 'F5007')
    plt.xlim(-2.5, 0.5)
    plt.ylim(-1.9, 1.1)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (2, 2) )
    plot_table_xy(sources.tabData, 'F3727', 'F5007')
    plt.xlim(-2, 1)
    plt.ylim(-1.9, 1.1)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (3, 0) )
    plot_table_xy(sources.tabData, 'r/r25', 'OXYGEN', dy = 12)
    plt.xlim(0, 1.4)
    plt.ylim(6.5, 9.5)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (3, 1) )
    plot_table_xy(sources.tabData, 'r/r25', 'logN2O')
    plt.xlim(0, 1.4)
    plt.ylim(-2, 0.5)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (3, 2) )
    plot_table_xy(sources.tabData, 'tro3', 'trn2')
    plt.xlim(5, 20)
    plt.ylim(5, 20)
    nplot.fix_ticks(ax, 4, 3)

    
    ax = plt.subplot2grid( subp, (4, 3) )
    plot_table_xy(sources.tabData, 'O23', 'N2O2')
    plt.xlim(-1, 1.5)
    plt.ylim(-2, 1)
    nplot.fix_ticks(ax, 4, 3)
    
    fig.set_tight_layout(True)
    if saveFigs:
        fig.savefig('%s_plot1.pdf' % sources.tabDataName)


    # Second figure
    fig = plt.figure(figNumber+1, figsize=(11,13))
    fig.clf()
    nplot.title_date()

    subp = (5, 4)
    
    ax = plt.subplot2grid( subp, (0, 0) )
    plot_table_xy(sources.tabData, 'r/r25', 'logN2O')
    plt.xlim(0, 1.4)
    plt.ylim(-2, 0.5)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (0, 1) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'des2', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(1.5, 3.5)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (0, 2) )
    plot_table_xy(sources.tabData, 'trn2', 'trs3')
    plt.xlim(5, 20)
    plt.ylim(5, 20)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (0, 3) )
    plot_table_xy(sources.tabData, 'tro3', 'tro2')
    plt.xlim(5, 20)
    plt.ylim(5, 20)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (1, 0) )
    plot_table_xy(sources.tabData, 'tro3', 'trs2')
    plt.xlim(5, 20)
    plt.ylim(5, 20)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = plt.subplot2grid( subp, (1, 1) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'Ar3O3', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-3, 1)
    nplot.fix_ticks(ax, 4, 3)
        
    ax = plt.subplot2grid( subp, (1, 2) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'N2O2', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 1)
    nplot.fix_ticks(ax, 4, 3)

    ax = plt.subplot2grid( subp, (1, 3) )
    plot_table_xy(sources.tabData, 'N2O2', 'Ar3O3')
    plt.xlim(-2, 1)
    plt.ylim(-3, 1)
    nplot.fix_ticks(ax, 4, 3)
        
    ax = plt.subplot2grid( subp, (2, 0) )
    plot_table_xy(sources.tabData, 'OXYGEN', 'F6584', dx = 12)
    plt.xlim(6.5, 9.5)
    plt.ylim(-3, 1)
    nplot.fix_ticks(ax, 4, 3)
    
    fig.set_tight_layout(True)
    if saveFigs:
        fig.savefig('%s_plot2.pdf' % sources.tabDataName)

    
    # Third figure
    nplot.plotSetup(300, lw = 2., fontsize=10, override_params={'text.usetex': False, 'font.weight': 'normal'})
    fig = plt.figure(figNumber+2, figsize=(10,10))
    fig.clf()
    nplot.title_date()

    ax = plt.subplot(111)
    plot_table_xy(sources.tabData, 'r/r25', 'OXYGEN', dy = 12, s = 20.)
    plt.xlim(0, 1.4)
    plt.ylim(6.5, 9.5)
    nplot.fix_ticks(ax, 2, 3)
    plt.grid()
#=========================================================================

#=========================================================================
def allPlots_gridSourceFit_Delta(fit, iSource, Z='OXYGEN', cmap = None, gridName = None, branch = 'low',
                                 delta=0.1, zoom=True, deltaPars=['F3727', 'F6584', 'F6584'], 
                                 **kwargs):

    if cmap is None:
        palette = sns.color_palette("hls", 256)[::-1][30:]
        ccentre = palette[len(palette)/2]
        cmap = mplcolors.ListedColormap(palette)
    
    ax = plt.subplot(4, 5, 1)
    plot_table_xy(fit.sources.tabData, 'OXYGEN', 'logN2O', dx = 12, colours = 'k', pltTitle = False)
    ax.scatter(12+fit.sources.tabData['OXYGEN'][iSource], fit.sources.tabData['logN2O'][iSource], c=ccentre, lw = 1, s=100, marker="*")
    plt.xlim(6.5, 9.5)
    plt.ylim(-2, 1)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 2) 
    plot_gridSourceFit(fit, iSource, 'F6584', 'F5007', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars,  branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 3)
    plot_gridSourceFit(fit, iSource, 'F3727', 'F5007', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 4)
    plot_gridSourceFit(fit, iSource, 'F6584', 'F3727', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    
    ax = plt.subplot(4, 5, 5)
    plot_gridSourceFit(fit, iSource, 'F5007', 'F4363', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)
    
    ax = plt.subplot(4, 5, 6)
    plot_gridSourceFit(fit, iSource, 'F3727', 'F4363', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 7)
    plot_gridSourceFit(fit, iSource, 'F6584', 'F4363', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    
    ax = plt.subplot(4, 5, 8)
    plot_gridSourceFit(fit, iSource, 'O23', 'N2O2', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)
    
    ax = plt.subplot(4, 5, 9)
    plot_gridSourceFit(fit, iSource, 'O23', 'O3O2', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)
    
    ax = plt.subplot(4, 5, 10)
    plot_gridSourceFit(fit, iSource, 'O23', 'RO3', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    
    ax = plt.subplot(4, 5, 11)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'RO3', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 12)
    plot_gridSourceFit(fit, iSource, 'logN2O', 'N2O2', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    ax = plt.subplot(4, 5, 13)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'logUin', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, **kwargs)
    nplot.fix_ticks(ax, 2, 3)
    
    ax = plt.subplot(4, 5, 14)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'N2O2', ax=ax, delta=delta, zoom=zoom, Z=Z, cmap=cmap, deltaPars=deltaPars, branch=branch, cbar=True, **kwargs)
    nplot.fix_ticks(ax, 2, 3)

    nplot.title_date("iSrc = %s | deltaPars = %s | $\Delta$ = %s | branch %s | grid %s | " % (iSource, delta, deltaPars, branch, gridName))

#=========================================================================

#=========================================================================
def allPlots_gridSourceFit(fit, iSource, psize=10, branch='all'):

    ax = plt.subplot(341)
    plot_gridSourceFit(fit, iSource, 'F6584', 'F5007', ax=ax, psize=psize, branch=branch)

    ax = plt.subplot(342)
    plot_gridSourceFit(fit, iSource, 'F3727', 'F5007', ax=ax, psize=psize, branch=branch)

    ax = plt.subplot(343)
    plot_gridSourceFit(fit, iSource, 'F4363', 'F5007', ax=ax, psize=psize, branch=branch)
    
    ax = plt.subplot(345)
    plot_gridSourceFit(fit, iSource, 'O23', 'N2O2', ax=ax, psize=psize, branch=branch)
    
    ax = plt.subplot(346)
    plot_gridSourceFit(fit, iSource, 'O23', 'O3O2', ax=ax, psize=psize, branch=branch)
    
    ax = plt.subplot(347)
    plot_gridSourceFit(fit, iSource, 'O23', 'RO3', ax=ax, psize=psize, branch=branch)
    
    ax = plt.subplot(3,4,9)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'RO3', ax=ax, psize=psize, branch=branch)

    ax = plt.subplot(3,4,10)
    plot_gridSourceFit(fit, iSource, 'logN2O', 'N2O2', ax=ax, psize=psize, branch=branch)

    ax = plt.subplot(3,4,11)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'logUin', ax=ax, psize=psize, branch=branch)
    
    ax = plt.subplot(3,4,12)
    plot_gridSourceFit(fit, iSource, 'OXYGEN', 'N2O2', ax=ax, psize=psize, branch=branch)
#=========================================================================
    
#=========================================================================
def plot_gridSourceFit(fit, iSource, X, Y, grid=None, sources=None,
                       dx = 0, psize=10, ax = None, legend=False, 
                       branch='all', delta=1000, deltaPars=None, zoom=False, 
                       Z = 'chi2', cmap='autumn', cbar=False, debug = False):

    if grid is None:
        grid = fit.grid
    if sources is None:
        sources = fit.sources
    
    flag = np.ones(grid.nGrid, 'bool')
    if branch != 'all':
        flag = grid.branch[branch].copy()

    # Select part of the grid at +- delta from some parameters 
    if deltaPars is None:
        if fit is not None:
            deltaPars = fit.obsFit__f
        else:
            deltaPars = ['F3727', 'F5007', 'F6584']
            
    for par in deltaPars:
        mod = grid.tabGrid[par]
        obs = sources.tabData[par][iSource]
        flag &= (abs(mod - obs) <= delta)

        
    # Get x, y, z from grid
    x = dx + grid.tabGrid[X][flag]
    y = grid.tabGrid[Y][flag]
    if (Z == 'chi2'):
        if fit is not None:
            z = utils.safe_log10(fit.chi2__gs[..., iSource])[flag]
            sz = 0
            zmin, zmax = z.min(), z.max()
        else:
            log.warn("No fit, no chi2!")
            sys.exit()
    else:
        z = grid.tabGrid[Z][flag]
        sz = sources.tabData[Z][iSource]
        zmin, zmax = sz - 0.5, sz + 0.5
        
    if ax is None:
        ax = plt.subplot(111)    
        
    # Plot grid
    if (flag.sum() > 0):
        plt.scatter(x, y, c = z, lw = 0, s = psize, vmin=zmin, vmax=zmax, cmap=cmap)
        cb = plt.colorbar()
        cb.set_label(X)

    plt.xlabel(label(X))
    plt.ylabel(label(Y))
    nplot.fix_ticks(ax, 4, 4)

    # Lims
    xx = dx + grid.tabGrid[X]
    yy = grid.tabGrid[Y]
    xmin, xmax = xx.min(), xx.max()
    ymin, ymax = yy.min(), yy.max()

    # Plot source
    if (X in sources.tabData.keys()) and (Y in sources.tabData.keys()):
        #plt.plot(dx+sources.tabData[X][iSource], sources.tabData[Y][iSource], 'w*', ms = 10)
        s = ax.scatter(dx+sources.tabData[X][iSource], sources.tabData[Y][iSource], c=sz, lw = 1, s=100, marker="*", vmin=zmin, vmax=zmax, cmap=cmap)
        xx = dx+sources.tabData[X][iSource]
        yy = sources.tabData[Y][iSource]
        xmin, xmax = min(xx, xmin), max(xx, xmax)
        ymin, ymax = min(yy, ymin), max(yy, ymax)
        
    if not zoom:
        plt.xlim(xmin, xmax)
        plt.ylim(ymin, ymax)
    
    # Plot fit results
    for branch in grid.branch.keys():

        alpha = 0.2
        if fit.branch__s[iSource] == branch:
            alpha = 1.

        #plt.plot(dx+fit.PDFSummaries__tcps['best'][branch][X][iSource], fit.PDFSummaries__tcps['best'][branch][Y][iSource], 'ro', alpha = alpha, label = 'best')
        #plt.plot(dx+fit.PDFSummaries__tcps['peak'][branch][X][iSource], fit.PDFSummaries__tcps['peak'][branch][Y][iSource], 'co', alpha = alpha, label = 'peak')
        #plt.plot(dx+ fit.PDFSummaries__tcps['ave'][branch][X][iSource],  fit.PDFSummaries__tcps['ave'][branch][Y][iSource], 'bo', alpha = alpha, label = 'ave' )
        #plt.plot(dx+ fit.PDFSummaries__tcps['med'][branch][X][iSource],  fit.PDFSummaries__tcps['med'][branch][Y][iSource], 'bo', alpha = alpha, label = 'med' )

    if debug:
        log.debug(branch, 'best', X, dx+fit.parBest__cps[branch][X][iSource], Y, fit.parBest__cps[branch][Y][iSource])
        log.debug(branch, 'peak', X, dx+fit.parPeak__cps[branch][X][iSource], Y, fit.parPeak__cps[branch][Y][iSource])
        log.debug(branch, 'ave',  X, dx+fit.parAve__cps[branch][X][iSource],  Y, fit.parAve__cps[branch][Y][iSource])
        log.debug(branch, 'med',  X, dx+fit.parMed__cps[branch][X][iSource], Y, fit.parMed__cps[branch][Y][iSource])
        log.debug('obs', X, dx+sources.tabData[X][iSource], Y, sources.tabData[Y][iSource])
#=========================================================================

#=========================================================================
def plots_fittingPDFs_example(fit, iSource, zoom = False, drow = 0, icol = 0, subp = [1, 4], pltSource = True, title = None, **kwargs):

    ax = plt.subplot2grid(subp, (drow, 0))
    plot_jointPDF(fit, iSource, 'OXYGEN', 'logN2O', dx = 12, zoom=zoom, ax=ax, pltSource = False, xlab_short = True, ylab_short = True, **kwargs)

    ax = plt.subplot2grid(subp, (drow, 1))
    plot_jointPDF(fit, iSource, 'F6584',  'F5007',  zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)
    
    ax = plt.subplot2grid(subp, (drow, 2))
    plot_jointPDF(fit, iSource, 'F3727',  'F5007',  zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)
    
    ax = plt.subplot2grid(subp, (drow, 3))
    plot_jointPDF(fit, iSource, 'F5876',  'Ar3Ne3', zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)

    if title is not None:
        ax = plt.subplot2grid(subp, (drow, 0))
        ax.set_axis_off()
        ax.text(-0.3, 1.05, title, ha='left', va='top', transform=ax.transAxes) 
#=========================================================================

#=========================================================================
def plots_fittingPDFs_paper1(fit, iSource, zoom = False, drow = 0, subp = [5, 5], pltSource = True, title = None, **kwargs):

    ax = plt.subplot2grid(subp, (drow, 1))
    plot_jointPDF(fit, iSource, 'OXYGEN', 'logN2O', dx = 12, zoom=zoom, xlim=(7.5, 9.4), ylim=(-1.5, -0.4), ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)

    ax = plt.subplot2grid(subp, (drow, 2))
    plot_jointPDF(fit, iSource, 'F6584',  'F5007',  zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)
    
    ax = plt.subplot2grid(subp, (drow, 3))
    plot_jointPDF(fit, iSource, 'F3727',  'F5007',  zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)
    ##plot_jointPDF(fit, iSource, 'OXYGEN',  'age', dx = 12, zoom=zoom, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, **kwargs)
    
    ax = plt.subplot2grid(subp, (drow, 4))
    plot_jointPDF(fit, iSource, 'F5876',  'Ar3Ne3', zoom=True, ax=ax, pltSource = pltSource, xlab_short = True, ylab_short = True, addDelta = [0.05, 0.05], **kwargs)

    if title is not None:
        ax = plt.subplot2grid(subp, (drow, 0))
        ax.set_axis_off()
        ax.text(-0.3, 1.05, title, ha='left', va='top', transform=ax.transAxes) 
#=========================================================================

#=========================================================================
def plots_fittingPDFs_check(fit, iSource, zoom = False, drow = 0, title = None, recalc=False, nrow = 4, ncol=6, **kwargs):

    outer_grid = gridspec.GridSpec(nrow, ncol, wspace=0.6, hspace=0.6)
    diG = drow * ncol

    if recalc:
        fit.calcPosterior()
        fit.priorSolutions()
        fit.calcPDFsAll()
        fit.calcPDFSummaries()
        log.info('nSolutions = ', fit.nSolutions__s[iSource])

    ax = plot_jointMargPDF(fit, iSource, 'OXYGEN', 'logN2O', dx=12, zoom=zoom, outer_grid=outer_grid, iGs=0+diG, **kwargs)
    plot_jointMargPDF(fit, iSource, 'OXYGEN', 'logUin', dx=12, zoom=zoom, outer_grid=outer_grid, iGs=1+diG, **kwargs)
    plot_jointMargPDF(fit, iSource, 'fr',     'age'   ,        zoom=zoom, outer_grid=outer_grid, iGs=2+diG, **kwargs)
    plot_jointMargPDF(fit, iSource, 'F6584',  'F5007' ,        zoom=True, outer_grid=outer_grid, iGs=3+diG, **kwargs)
    plot_jointMargPDF(fit, iSource, 'F3727',  'F5007' ,        zoom=True, outer_grid=outer_grid, iGs=4+diG, **kwargs)
    plot_jointMargPDF(fit, iSource, 'F5876',  'Ar3Ne3',        zoom=True, outer_grid=outer_grid, iGs=5+diG, **kwargs)
    
    if title is not None:
        ax.text(0.08, 1.5, title, ha='left', va='center', transform=ax.transAxes)
        
    #plot_margPDF( fit, iSource, 'OXYGEN',           dx=12, zoom=zoom, outer_grid=outer_grid, iGs=4, **kwargs)
#=========================================================================


#=========================================================================
def allPlots_jointPDF(fit, iSource, zoom = False):

    outer_grid = gridspec.GridSpec(3, 4, wspace=0.4, hspace=0.4)
    
    #plot_jointMargPDF(fit, iSource, 'F6584' , 'F5007', zoom=zoom)
    #plot_jointMargPDF(fit, iSource, 'OXYGEN', 'RO3'  , zoom=zoom)
    #plot_jointMargPDF(fit, iSource, 'OXYGEN', 'RO3'   , zoom=zoom)
    #def bla():
    
    outer_grid = gridspec.GridSpec(3, 4, wspace=0.4, hspace=0.4)

    plot_jointMargPDF(fit, iSource, 'F6584' , 'F5007' , zoom=zoom, outer_grid=outer_grid, iGs=0)
    plot_jointMargPDF(fit, iSource, 'F3727' , 'F5007' , zoom=zoom, outer_grid=outer_grid, iGs=1)
    plot_jointMargPDF(fit, iSource, 'F4363' , 'F5007' , zoom=zoom, outer_grid=outer_grid, iGs=2)
    
    plot_jointMargPDF(fit, iSource, 'O23'   , 'N2O2'  , zoom=zoom, outer_grid=outer_grid, iGs=4)
    plot_jointMargPDF(fit, iSource, 'O23'   , 'O3O2'  , zoom=zoom, outer_grid=outer_grid, iGs=5)
    plot_jointMargPDF(fit, iSource, 'O23'   , 'RO3'   , zoom=zoom, outer_grid=outer_grid, iGs=6)
    
    plot_jointMargPDF(fit, iSource, 'OXYGEN', 'RO3'   , zoom=zoom, outer_grid=outer_grid, iGs=8)
    plot_jointMargPDF(fit, iSource, 'logN2O', 'N2O2'  , zoom=zoom, outer_grid=outer_grid, iGs=9)
    plot_jointMargPDF(fit, iSource, 'OXYGEN', 'logUin', zoom=zoom, outer_grid=outer_grid, iGs=10)
    plot_jointMargPDF(fit, iSource, 'OXYGEN', 'logN2O', zoom=zoom, outer_grid=outer_grid, iGs=11)
#=========================================================================
    
#=========================================================================
def plot_jointPDF(fit, iSource, X, Y, dx = 0, 
                  branches = ['all'], bcolours = [colour('BOND')],
                  ax = None, xlim = None, ylim = None,
                  zoom = False, zoomSigma = 10, pltFancyLabels = True,
                  pltSource = False, addDelta = None,
                  xlab_short = False, ylab_short = False, **kwargs
                  ):
    
    if ax is None:
        ax = plt.subplot(111)

    # Plot joint PDFs for each branch
    for branch, bcolour in zip(branches, bcolours):
        Xgrid, PDF__xy, bins, Rs__ro = fit.calcJointPDF([X, Y], branch=branch, iSource=iSource)

        light = sns.set_hls_values(bcolour, l=1.)
        colors = [sns.set_hls_values(bcolour, l=l) for l in np.linspace(0.95, 0., 12)]
        cmap = sns.blend_palette(colors, as_cmap=True)

        im = ax.pcolormesh(dx+Xgrid[0], Xgrid[1], PDF__xy, cmap = cmap, **kwargs)        
        #PDF__xy /= PDF__xy.max()
        #im = ax.pcolormesh(dx+Xgrid[0], Xgrid[1], utils.safe_ln(PDF__xy), cmap = sns.light_palette(bcolour, as_cmap=True), vmin=-10, vmax=0, **kwargs)

    # Labels
    xlab, ylab = X, Y
    if pltFancyLabels:
        xlab = label(X, short = xlab_short)
        ylab = label(Y, short = ylab_short)
        if dx != 0:
            xlab = '%s + %s' % (dx, xlab)

    # Set limits of the plot
    xmin, xmax = fit.bParLow__pb[X][0], fit.bParUpp__pb[X][-1]
    ymin, ymax = fit.bParLow__pb[Y][0], fit.bParUpp__pb[Y][-1]
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)
    ax.set_xlim(xmin+dx, xmax+dx)
    ax.set_ylim(ymin, ymax)
    nplot.fix_ticks(ax, 3, 4)

    # Plot source
    if pltSource:        
        if (X in fit.sources.tabData.keys()):
            obs = fit.sources.tabData[X][iSource]
            c = colour('obs')
            if (fit.grid.varType__v[X] == 'input'):
                c = colour('Te')
        
            if ('e'+X in fit.sources.tabData.keys()):
                err = fit.sources.tabData['e'+X][iSource] * fit.errCookingFactor
            else:
                err = 0.1 / np.log(10)
            if addDelta is not None:
                addDeltaX = addDelta[0]
                err = np.sqrt(err**2 + addDeltaX**2)
            for nSigma in np.arange(1, 2):
                xL, xU = obs - nSigma * err, obs + nSigma * err
                ax.fill_between( (dx+xL, dx+xU), (-100, -100), (100, 100), lw=0, alpha=0.2, facecolor=c)
                
        if (Y in fit.sources.tabData.keys()):
            obs = fit.sources.tabData[Y][iSource]
            c = colour('obs')
            if (fit.grid.varType__v[Y] == 'input'):
                c = colour('Te')
                
            if ('e'+Y in fit.sources.tabData.keys()):
                err = fit.sources.tabData['e'+Y][iSource] * fit.errCookingFactor
            else:
                err = 0.1 / np.log(10)
            if addDelta is not None:
                addDeltaX = addDelta[-1]
                err = np.sqrt(err**2 + addDeltaX**2)
            for nSigma in np.arange(1, 2):
                xL, xU = obs - nSigma * err, obs + nSigma * err
                ax.fill_betweenx( (xL, xU), (-100, -100), (100, 100), lw=0, alpha=0.2, facecolor=c)

    # Zoom in
    if zoom:

        xlow, xupp = xmin, xmax
        ylow, yupp = ymin, ymax

        # Limits from source
        if (X in fit.sources.tabData.keys()):
            obs = fit.sources.tabData[X][iSource]
            if ('e'+X in fit.sources.tabData.keys()):
                err = fit.sources.tabData['e'+X][iSource] * fit.errCookingFactor
            else:
                err = 0.1 / np.log(10)
            nSigma = zoomSigma
            xlow, xupp = obs-nSigma*err, obs+nSigma*err
        if (Y in fit.sources.tabData.keys()):
            obs = fit.sources.tabData[Y][iSource]
            if ('e'+Y in fit.sources.tabData.keys()):
                err = fit.sources.tabData['e'+Y][iSource] * fit.errCookingFactor
            else:
                err = 0.1 / np.log(10)
            nSigma = zoomSigma
            ylow, yupp = obs-nSigma*err, obs+nSigma*err

        # Limits from fit
        #xlow, xupp = xmax, xmin
        #ylow, yupp = ymax, ymin
        #f = (zi > 1e-10)
        #dx = xi[2] - xi[1]
        #_xlow, _xupp = xx[f].min(), xx[f].max()
        #xlow, xupp = min(_xlow, xlow) - dx, max(_xupp, xupp) + dx
        #dy = yi[2] - yi[1]
        #_ylow, _yupp = yy[f].min(), yy[f].max()
        #ylow, yupp = min(_ylow, ylow) - dy, max(_yupp, yupp) + dy

        ax.set_xlim(xlow+dx, xupp+dx)
        ax.set_ylim(ylow, yupp)

    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])
#=========================================================================
                
#=========================================================================
def plot_jointMargPDF(fit, iSource, X, Y, dx = 0, pltJointPDF = True,
                  branches = ['all'], bcolours = [colour('BOND')],
                  outer_grid = None, iGs = 0,
                  zoom = False, xlim = None, ylim = None, pltFancyLabels = True,
                  pltEllipse = False, pltScens = False,
                  **kwargs):

    # Define outer plot grid
    if outer_grid is None:
        outer_grid = gridspec.GridSpec(1, 1, wspace=0.0, hspace=0.0)
    
    # Define inner plot grid
    if (not pltScens):
        gs = gridspec.GridSpecFromSubplotSpec(8, 8, wspace=0.0, hspace=0.0, subplot_spec=outer_grid[iGs])
        ax = plt.subplot(gs[2:, :-2])
        ax_x = plt.subplot(gs[:2, :-2], sharex=ax)
        ax_y = plt.subplot(gs[2:, -2:], sharey=ax)
    else:
        gs = gridspec.GridSpecFromSubplotSpec(12, 12, wspace=0.0, hspace=0.0, subplot_spec=outer_grid[iGs])
        ax    = plt.subplot(gs[4: ,   :-4])
        ax_x  = plt.subplot(gs[2:4,   :-4], sharex=ax)
        ax_y  = plt.subplot(gs[4: , -4:-2], sharey=ax)
        ax_x2 = plt.subplot(gs[0:2,   :-4], sharex=ax)
        ax_y2 = plt.subplot(gs[4: , -2:  ], sharey=ax)
    
        ax_x2.set_axis_off()
        ax_y2.set_axis_off()

        
    # Plot joint PDFs for each branch
    if pltJointPDF:
        plot_jointPDF(fit, iSource, X, Y, dx = dx, branches = branches, bcolours = bcolours,
                      ax = ax, zoom = zoom, pltFancyLabels = pltFancyLabels, **kwargs)

    xmin, xmax = fit.bParLow__pb[X][0], fit.bParUpp__pb[X][-1]
    ymin, ymax = fit.bParLow__pb[Y][0], fit.bParUpp__pb[Y][-1]

    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])
        
    # Plot source
    if (X in fit.sources.tabData.keys()) and (Y in fit.sources.tabData.keys()):
        pass
        #ax.plot(dx+fit.sources.tabData[X][iSource], fit.sources.tabData[Y][iSource], 'w*', ms = 10)
    if (X in fit.sources.tabData.keys()):
        #ax.axvline(dx+fit.sources.tabData[X][iSource], c=colour('Te'), ls='-', lw=1, alpha = 0.5)
        ax_x.axvline(dx+fit.sources.tabData[X][iSource], c=colour('Te'), ls='-', lw=2, alpha = 0.9)
    if (Y in fit.sources.tabData.keys()):    
        #ax.axhline(   fit.sources.tabData[Y][iSource], c=colour('Te'), ls='-', lw=1, alpha = 0.5)
        ax_y.axhline(   fit.sources.tabData[Y][iSource], c=colour('Te'), ls='-', lw=2, alpha = 0.9)

    if pltEllipse:
        plot_table_ellipse_covMatrix(fit.sources.tabData, ax = ax, dx = dx, flag = utils.ind2flag(fit.sources.tabData, [iSource]), edgecolor = colour('Te'), alpha = 0.9, lw = 2)

    if (X in fit.sources.tabData.keys()):
        obs = fit.sources.tabData[X][iSource]
        c = colour('obs')
        if (fit.grid.varType__v[X] == 'input'):
            c = colour('Te')
        if ('e'+X in fit.sources.tabData.keys()):
            err = fit.sources.tabData['e'+X][iSource] * fit.errCookingFactor
        else:
            err = 0.1 / np.log(10)
        for nSigma in np.arange(5):
            xL, xU = obs - nSigma * err, obs + nSigma * err
            ax_x.fill_between( (dx+xL, dx+xU), (0, 0), (1.1, 1.1), lw=0 , alpha=0.07 , color=c)
            
    if (Y in fit.sources.tabData.keys()):
        obs = fit.sources.tabData[Y][iSource]
        c = colour('obs')
        if (fit.grid.varType__v[Y] == 'input'):
            c = colour('Te')
        if ('e'+Y in fit.sources.tabData.keys()):
            err = fit.sources.tabData['e'+Y][iSource] * fit.errCookingFactor
        else:
            err = 0.1 / np.log(10)
        for nSigma in np.arange(5):
            xL, xU = obs - nSigma * err, obs + nSigma * err
            ax_y.fill_betweenx( (xL, xU), (0, 0), (1.1, 1.1), lw=0 , alpha=0.1 , color=c)

            
    # Plot grid points - debug
    #plot_gridSourceFit(fit, iSource, X, Y)

    for branch, bcolour in zip(branches, bcolours):

        # Plot marginal PDFs
        x = fit.bParCen__pb[X]
        y = fit.PDF__cpsb[branch][X][iSource]
        ax_x.plot(dx+x, y/y.max(), bcolour, lw = 2)
        ax_x.set_ylim(0, 1.)
        nplot.fix_ticks(ax_x, 4, 4)
        ax_x.get_xaxis().set_visible([])
        ax_x.get_yaxis().set_visible([])

        # Plot prior
        #y = fit.gridPDF__cpb[branch][X]
        #ax_x.plot(dx+x, y/y.max(), c=bcolour, ls=':')

        # Plot marginal PDFs
        x = fit.bParCen__pb[Y]
        y = fit.PDF__cpsb[branch][Y][iSource]
        ax_y.plot(y/y.max(), x, bcolour, lw = 2)
        ax_y.set_xlim(0, 1.)
        nplot.fix_ticks(ax_y, 4, 4)
        ax_y.get_xaxis().set_visible([])
        ax_y.get_yaxis().set_visible([])

        # Plot prior
        #y = fit.gridPDF__cpb[branch][Y]
        #ax_y.plot(y/y.max(), x, c=bcolour, ls=':')

    if pltScens:
        ax_x2.set_ylim(-0.1, 1.3)
        ax_y2.set_xlim(-0.1, 1.3)
        
        #x0, y0 = 9.58, -0.42
        x0, y0 = 1.20, 1.20
        dx, dy = 0.10, 0.10
        
        colours = sns.hls_palette(6, l=.4, s=.8)[::-1] #[2:]
        X, Y = 'OXYGEN', 'logN2O'
        
        for i, age in enumerate(fit.grid.inputVars__u['age']):
            for i_f, fr in enumerate(fit.grid.inputVars__u['fr']):
                
                scen = 'Sage%.2ffr%.2f' % (age, fr)
                c = colours[i]
                
                if (fr < 1):
                    lw = 0
                    facecolor = c
                    edgecolor = 'none'
                    geom = 'Filled sphere'
                    s = 40
                else:
                    lw = 2
                    facecolor = 'white'
                    edgecolor = c
                    geom = 'Spherical shell'
                    s = 20
        
            
                #x_mod = fit.PDFSummaries__tcps['mmod'][scen][X][0]
                #x_h68 = fit.PDFSummaries__tcps['mh68'][scen][X][0]
                #    
                #y_mod = fit.PDFSummaries__tcps['mmod'][scen][Y][0]
                #y_h68 = fit.PDFSummaries__tcps['mh68'][scen][Y][0]
                
                x_mod = fit.PDFSummaries__tcps['mmed'][scen][X][0]
                x_h68 = fit.PDFSummaries__tcps['mp68'][scen][X][0]
                    
                y_mod = fit.PDFSummaries__tcps['mmed'][scen][Y][0]
                y_h68 = fit.PDFSummaries__tcps['mp68'][scen][Y][0]
                
                ax_x2.plot(12+x_h68, [y0, y0], color = c, lw=2, alpha=0.5, zorder = 8)
                ax_x2.scatter(12+x_mod, [y0], marker = 'o', lw = lw, s = s, facecolors = facecolor, edgecolors = edgecolor, zorder = 9)
                    
                ax_y2.plot([x0, x0], y_h68, color = c, lw=2, alpha=0.5, zorder = 8)
                ax_y2.scatter([x0], y_mod, marker = 'o', lw = lw, s = s, facecolors = facecolor, edgecolors = edgecolor, zorder = 9)

                if (fr < 1):
                    ax_x2.text(9.3, y0-0.5*dy, '%i Myr' % round(age), ha='center', va='center', fontdict = {'size': 12, 'color' : c})
                            
                x0 -= dx
                y0 -= dy

    return ax
#=========================================================================
        
#=========================================================================
def plot_margPDF(fit, iSource, X, dx = 0, ax = None,
                  branches = ['low', 'upp'], bcolours = ['b', 'r'],
                  outer_grid = None, iGs = 0,
                  zoom = False, pltFancyLabels = True,
                  ):

    # Define outer plot grid
    if outer_grid is None:
        outer_grid = gridspec.GridSpec(1, 1, wspace=0.0, hspace=0.0)
    
    # Define inner plot grid
    if ax is None:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, wspace=0.0, hspace=0.0, subplot_spec=outer_grid[iGs])
        ax = plt.subplot(gs[0, 0])
    
    # Labels
    xlab = X
    if pltFancyLabels:
        xlab = label(X)
        if dx != 0:
            xlab = '%s + %s' % (dx, label(X))
            
    xmin, xmax = fit.bParLow__pb[X][0], fit.bParUpp__pb[X][-1]
    ax.set_xlabel(xlab)
    ax.set_xlim(xmin+dx, xmax+dx)
    nplot.fix_ticks(ax, 4, 4)
    
    if (X in fit.sources.tabData.keys()):
        obs = fit.sources.tabData[X][iSource]
        c = colour('obs')
        if (fit.grid.varType__v[X] == 'input'):
            c = colour('Te')
        if ('e'+X in fit.sources.tabData.keys()):
            err = fit.sources.tabData['e'+X][iSource] * fit.errCookingFactor
        else:
            err = 0.1 / np.log(10)
        for nSigma in np.arange(5):
            xL, xU = obs - nSigma * err, obs + nSigma * err
            ax.fill_between( (dx+xL, dx+xU), (0, 0), (1.1, 1.1), lw=4-nSigma , alpha=0.1 , color=c)


    for branch, bcolour in zip(branches, bcolours):
        
        # Plot marginal PDFs
        x = fit.bParCen__pb[X]
        y = fit.PDF__cpsb[branch][X][iSource]
        ax.plot(dx+x, y/y.max(), bcolour)
        ax.set_ylim(0, 1.)
        nplot.fix_ticks(ax, 4, 4)
        ax.get_xaxis().set_visible([])
        ax.get_yaxis().set_visible([])

        # Plot prior
        #y = fit.gridPDF__cpb[branch][X]
        #ax.plot(dx+x, y/y.max(), c=bcolour, ls=':')


    # Zoom in
    if zoom:

        xlow, xupp = xmin, xmax

        # Limits from source
        if (X in fit.sources.tabData.keys()):
            obs = fit.sources.tabData[X][iSource]
            if ('e'+X in fit.sources.tabData.keys()):
                err = fit.sources.tabData['e'+X][iSource] * fit.errCookingFactor
            else:
                err = 0.1 / np.log(10)
            nSigma = 20
            xlow, xupp = obs-nSigma*err, obs+nSigma*err

        ax.set_xlim(xlow+dx, xupp+dx)
        ax.set_ylim(ylow, yupp)    
#=========================================================================


#=========================================================================
def plot_jPDFs(fit, X = None, Y = None, dx=0, bcolour='r',
               branch = 'all', flag = None, 
               addToTitle = None, title = None, pltTitle = True,
               colours = None, pltLegend = True,  pltFancyLabels = True,
               **kwargs):

    f = fit

    # Plot joint PDFs for the solution branch
    for iSrc in np.where(flag)[0]:
        PDF__xy, Xgrid = f.calcJointPDF([X, Y], iSource=iSrc)
        PDF__xy /= np.max(PDF__xy)
        try:
            PDF_total__xy += np.where(PDF__xy > -999, PDF__xy, 0)
        except:
            PDF_total__xy = PDF__xy

    im = plt.pcolormesh(dx+Xgrid[0], Xgrid[1], utils.safe_ln(PDF_total__xy), cmap = sns.light_palette(bcolour, as_cmap=True), vmin=-3)
    plt.colorbar()
    
    # Labels
    xlab, ylab = X, Y
    if pltFancyLabels:
        xlab = label(X)
        ylab = label(Y)
        if dx != 0:
            xlab = '%s + %s' % (dx, label(X))

    plt.xlabel(xlab)
    plt.ylabel(ylab)            
#=========================================================================

#=========================================================================
def plot_PDFs(fit, iSource):
    plot_fitPDFCDFAll(fit, iSource, gridPart='upp')
    plot_fitPDFCDFAll(fit, iSource, gridPart='low')
#=========================================================================
    
#=========================================================================
def plot_fitPDFCDFAll(fit, iSrc, gridPart = 'upp'):

    f = fit
    
    plt.suptitle(r'iSrc %i errCookingFactor = %s fakeErr = %s' 
                 % (iSrc, f.errCookingFactor, f.fakeErr)
                 , fontsize=15)        

    plt.subplots_adjust(hspace=0.25, wspace=0.0)
    nY = 3

    parsInput = [P for P, T in f.parType__p.items() if (T == 'input')]
    nX1 = len(parsInput)
    for iX, P in enumerate(parsInput):
        plt.subplot(nY , nX1, iX+1, yticklabels=[])
        plot_fitPDFCDF(f, P, iSrc, zoom = False, gridPart = gridPart)
        
    parOutput = [P for P, T in f.parType__p.items() if (T == 'output')]
    nX2 = len(parOutput)
    for iX, P in enumerate(parOutput):
        plt.subplot(nY , nX2 , nX2+iX+1, yticklabels=[])
        plot_fitPDFCDF(f, P, iSrc, zoom = False, gridPart = gridPart)
        plt.subplot(nY , nX2 , 2*nX2+iX+1, yticklabels=[])
        plot_fitPDFCDF(f, P, iSrc, zoom = True, gridPart = gridPart)
        
        
def plot_fitPDFCDF(fit, P, iSrc, gridPart = 'upp', zoom = False):
    '''
    Some more test plots
    '''

    f = fit

    X = P
    yMax = 1.30
    _colorLab , _colorBck = 'r' , 'cyan'

    if X in f.obsFit__f: 
        _colorLab , _colorBck = 'k' , 'g'

    # If X is an observed thing, plot observations - +/- 3 sigma shaded regions
    alpha=0.1
    if X in f.sources.tabData.keys():
        if X in f.grid.outputLineList:
            obs = f.sources.tabData[X][iSrc]
            err = f.sources.tabData['e'+X][iSrc] * f.errCookingFactor
            plt.errorbar( obs , 1.05 , xerr=err, color='g', lw=3, marker='o')
            for nSigma in np.arange(5):
                xL, xU = obs - nSigma * err, obs + nSigma * err
                plt.plot( (xL , xL) , (0, yMax) , 'g-', lw=4-nSigma , alpha=alpha+0.1)
                plt.plot( (xU , xU) , (0, yMax) , 'g-', lw=4-nSigma , alpha=alpha+0.1)
                plt.fill_between( (xL,xU) , (0,0) , (1,1), lw=4-nSigma , alpha=alpha , color=_colorBck)
                
    # Plot priors & PDFs
    x , y , z = f.bParCen__pb[P] , f.PDF__cpsb[gridPart][P][iSrc] , f.CDF__cpsb[gridPart][P][iSrc]
    plt.plot(x, f.gridPDF__cpb[gridPart][P], 'y*-')
    plt.plot(x, f.gridCDF__cpb[gridPart][P], 'y--')
    plt.plot(x, y, 'b*-', x, z, 'r--', lw=2)
    plt.ylim(0, yMax)
    plt.grid(True)
    plt.xlabel(P, color=_colorLab)
    
    # PDF summaries
    ave = f.PDFSummaries__tcps['ave'][gridPart][P][iSrc]
    sig = f.PDFSummaries__tcps['sig'][gridPart][P][iSrc]
    med = f.PDFSummaries__tcps['med'][gridPart][P][iSrc]
    p16 = f.PDFSummaries__tcps['p16'][gridPart][P][iSrc]
    p84 = f.PDFSummaries__tcps['p84'][gridPart][P][iSrc]

    # Plot chosen branch in a more interesting colour
    alpha = 0.2
    try:
        f.branch__s
        if f.branch__s[iSrc] == gridPart:
            alpha = 1.
    except:
        pass

    if X in f.sources.tabData.keys():
        plt.plot( f.sources.tabData[X][iSrc] , 1.05 , 'g*', mew=0, ms=10, label = 'input')            
    plt.plot( f.PDFSummaries__tcps['peak'][gridPart][P][iSrc] , 1.10 , 'c',  marker='o', alpha = alpha, label = 'peak')
    plt.plot( f.PDFSummaries__tcps['best'][gridPart][P][iSrc] , 1.15 , 'r',  marker='o', alpha = alpha, label = 'best')
    plt.errorbar( med , 1.20 , xerr=[ ([med-p16]) , ([p84-med]) ], color='m', lw=3, marker='o', alpha = alpha, label = 'med')
    plt.errorbar( ave , 1.25 , xerr=sig, color='b', lw=3, marker='o', alpha = alpha, label = 'ave')

    if (X == 'OXYGEN') & (gridPart == 'upp'):
        plt.legend(loc='upper right', numpoints=1)
        
    if (zoom):
        if X in f.sources.tabData.keys():
            plt.xlim( obs - 20 * err, obs + 20 * err )

    # Adjust tickmarks in x
    max_xticks = 4
    xloc = plt.MaxNLocator(max_xticks)
    plt.gca().xaxis.set_major_locator( xloc )
#=========================================================================

#=========================================================================
def plot_table_ellipse_covMatrix(table, flag = None, 
                                 pars = ['OXYGEN', 'logN2O'], pref_ave = 'ave_', pref_sig = 'e', pref_rho = 'rho_',
                                 pltAve = False, scale = 1., **kwargs):

    if pars is None:
        pars = fit.parsJointPDF

    if flag is None:
        iSources = range(len(table))
    else:
        iSources = np.where(flag)[0]

    for iSrc in iSources:
        
        ave = [0] * len(pars)
        sig = [0] * len(pars)

        for iP, P in enumerate(pars):
            ave[iP] = table[pref_ave + P][iSrc]
            sig[iP] = table[pref_sig + P][iSrc]

        rho_xy = table[pref_rho + '_'.join(pars)][iSrc]
        cov__xy = utils.calcCovMatrix(sig[0], sig[1], rho_xy)
            
        if not np.any(np.isnan(cov__xy)):
            plot_ellipse_covMatrix(ave, cov__xy, pltAve = pltAve, scale = scale, **kwargs)
#=========================================================================

#=========================================================================
def plot_ellipse_parameters(cen_x, cen_y, sig_x, sig_y, rho_xy, scale = None, flag = None, 
                            pltAve = False, **kwargs):

    if flag is None:
        iSources = range(len(cen_x))
    else:
        iSources = np.where(flag)[0]

    if scale is None:
        scale = np.ones_like(rho_xy)
            
    for iSrc in iSources:
        
        ave = [cen_x[iSrc], cen_y[iSrc]]
        sig = [sig_x[iSrc], sig_y[iSrc]]
        rho = rho_xy[iSrc]
        cov__xy = utils.calcCovMatrix(sig[0], sig[1], rho)            
        sca = scale[iSrc]

        if not np.any(np.isnan(cov__xy)):
            plot_ellipse_covMatrix(ave, cov__xy, pltAve = pltAve, scale = sca, **kwargs)
#=========================================================================

#=========================================================================
def plot_ellipse_covMatrix(ave, cov__xy, scale = 1., dx = 0, dy = 0, ax = None, pltAve = False,
                 edgecolor = 'k', facecolor = 'none', **kwargs):
    
    import scipy
    from matplotlib.patches import Ellipse

    if ax is None:
        ax = plt.gca()


    eigs  = scipy.linalg.eig(cov__xy)
    angle = np.arctan( -eigs[1][0,1] / eigs[1][1,1] ) * (180. / np.pi)

    ellipse = Ellipse( scipy.array([dx + ave[0], dy + ave[1]]), scale * 2*np.sqrt(eigs[0][0]), scale * 2*np.sqrt(eigs[0][1]), angle, 
                       edgecolor = edgecolor, facecolor = facecolor, **kwargs )
        
    ax.add_artist(ellipse)

    if pltAve:
        ax.plot(dx + ave[0], dy + ave[1], '*', ms=10)
#=========================================================================
