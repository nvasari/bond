'''
Helper classes for Bayesian emission line fitting method to derive abundances.

Indices notations:
- __g = grid models
- __s = sources
- __f = observables to be fitted
- __p = all parameters for which PDFs have been calculated (including fitted)
- __v = variables in grid (input + output parameters)
- __u = unique values of v
- __b = bin centres (for PDFs)
- __j = joint PDFs bin centres (2D or ND PDFs)
- __c = chopped grid part (low, upp) to deal with bimodality
- __t = summary type for PDF (ave, med, peak, best)

Originally based on Cid's tmp03.py.

Natalia@Meudon - 01/Jul/2013
'''            

# Import external packages
import os
import sys
import time
import itertools
from copy import deepcopy
from astropy import log

import astropy.table
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.ndimage
import scipy.interpolate

#import bond.other.hickle as hkl
import h5py

# Import our own packages
from bond.natastro import utils


    
# ************************************************************************
class sources(object):
    '''
    Defines astronomical sources & their observed emission line measurements.
    Natalia + LeCid@Falguiere - 04/Juillet/2013
    '''

    def __init__(self, sourcesFile = None, tabData = None, lines2Clean__f = None, 
                 useLog = True, tableIsInLog = False, selectTableRows = None, 
                 fixHb = None, efixHb = None, tableHasLineRatios = False, 
                 kwargs_astropy_table = None,
                 debug = False):

        self.sourcesFile = sourcesFile
        self.tabData = tabData
        self.lines2Clean__f = lines2Clean__f
        self.selectTableRows = selectTableRows
        self.useLog = useLog
        self.tableIsInLog = tableIsInLog
        self.fixHb = fixHb
        self.efixHb = efixHb
        self.debug = debug

        # ATT: This class can be used for actual sources (if sourcesFile is given) or with fake ones generated
        # with grid.toSources (if tabData is given), but only one thing at a time can be done!
        if (self.sourcesFile is not None) & (self.tabData is not None):
            log.warn("!!!WARNING!!! Either define a sourcesFile or a tabData, not both at once")
            sys.exit()

        # Populate table from file or, if using tabData, def sourcesFile & sourceName
        if (self.sourcesFile is not None):
            self.readSourcesFile(kwargs_astropy_table = kwargs_astropy_table)
        elif (self.tabData is not None):
            self.sourceName = self.tabData['name']

        # Clean table if lines2Clean__f is given
        if (self.lines2Clean__f is not None):
            self.cleanZeroErrors()

        # Select only the rows specified in selectTableRows
        if (self.selectTableRows is not None):
            self.tabData = self.tabData[self.selectTableRows]
        
        # Name the table
        self.tabDataName = 'tabData'

        # Count number of sources
        if (self.tabData is not None):
            self.nSources = len(self.tabData)

            # Get default values for Hb
            key = 'F4861'
            if self.fixHb is None:
                if key in self.tabData.keys():
                    self.fixHb = deepcopy(self.tabData[key])
                else:
                    self.fixHb = np.ones(len(self.tabData))
                    
            if self.efixHb is None:
                if 'e'+key in self.tabData.keys():
                    self.efixHb = deepcopy(self.tabData['e'+key])
                else:
                    self.efixHb = np.zeros_like(self.fixHb)
            self.efixHb /= self.fixHb
                    
            # Transform Hb to log if the rest of the table also is or will be
            if (~self.tableIsInLog) & (self.useLog):
                Hb = deepcopy(self.fixHb)
                eHb = deepcopy(self.efixHb)
                self.fixHb  = utils.safe_log10(Hb)
                self.efixHb = eHb / np.log(10.)

            # Transform Fluxes to log Fluxes & error-in-flux to error-in-log-flux
            lineList = ['F3727', 'F4363', 'F5007', 'F4861', 'F6563', 'F6584', 'F6731', 'F6716', 'F9069', 'F9532', 'F4959', 'F5755', 'F6548', 'F7323', 'F7332', 'F3869', 'F4070', 'F4078', 'F6312', 'F7135', 'F4471', 'F5876', 'F4740', 'F4686', 'F6300', 'F7325', 'limF4363', 'limF5755', 'limF6312', 'limF7135', 'limF7325' ]
            if (self.useLog) & (not self.tableIsInLog):
                for key in self.tabData.keys():
                    if key in lineList:
                        _F = self.tabData[key]
                        if 'e'+key in self.tabData.keys():
                            _dF = self.tabData['e'+key]
                        else:
                            _dF = 0.
                        if self.debug: 
                            log.debug('--sources> transforming source ' , key , ' to its log!! | file = ', self.sourcesFile)
                        F, eF, logF, elogF = utils.calcLineRatio(_F, 1., _dF, 0., fluxInLog = False)
                        self.tabData[key] = logF.data
                        self.tabData['e'+key] = elogF.data
                        self.tableIsInLog = True
                        
            # Divide by Hb, which can be == 100 in Grazyna's GHR data tables! 
            if not tableHasLineRatios:
                for key in self.tabData.keys():
                    if (key in lineList) & (key != 'F4861'):
                        if self.debug: log.debug('--sources> dividing (or subtracting if table is in log | %s)' % self.tableIsInLog, key , ' and ' , 'e'+key , ' by ', self.fixHb, ' !! | file = ', self.sourcesFile)
                        _F = self.tabData[key]
                        if 'e'+key in self.tabData.keys():
                            _dF = self.tabData['e'+key]
                        else:
                            _dF = 0.
                        F, eF, logF, elogF = utils.calcLineRatio(_F, self.fixHb, _dF, self.efixHb, fluxInLog = self.tableIsInLog)
                        if self.tableIsInLog:
                            self.tabData[key] = logF.data
                            self.tabData['e'+key] = elogF.data
                        else:
                            self.tabData[key] = F.data
                            self.tabData['e'+key] = eF.data
                            self.tabData['F4861'] = 1.
                            
                if self.tableIsInLog:
                    self.tabData['F4861'] = 0.
                else:
                    self.tabData['F4861'] = 1.
                self.tabData['eF4861'] = self.efixHb.copy()
                    
            # Calc [OIII]4959 and/or 5007 and come useful line indices
            calcLineIndices(self.tabData, tableIsInLog = self.tableIsInLog, calcErrors = True)
        
        if self.debug:
            log.debug(' **sources> done!')
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def readSourcesFile(self, kwargs_astropy_table = None):
        '''
        Reads a table with emission lines. 
        OBS: I've already renamed F6583 & eF6583 to F6584 & eF6584 in Grazyna's file!
        '''

        if kwargs_astropy_table is None:
            self.tabData = astropy.table.Table.read(self.sourcesFile, format = 'ascii')
        else:
            # Pass arguments for the astropy table reader
            self.tabData = astropy.table.Table.read(self.sourcesFile, **kwargs_astropy_table)

        # Add sourceName to table. ATT Can be better coded!!
        self.sourceName = [0] * len(self.tabData)
        for iS in np.arange( len(self.tabData) ):
            if 'name' in self.tabData.keys():
                if 'nu' in self.tabData.keys():
                    self.sourceName[iS] = str( self.tabData['name'][iS] )  + '/' + str( self.tabData['nu'][iS] )
                else:
                    self.sourceName[iS] = str( self.tabData['name'][iS] ) 
                    
        self.tabData['sourceName'] = self.sourceName
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def saveTableToFile(self, saveFile = None, fileType = 'hdf5', overwrite = False, **kwargs):
        '''
        Save a grid or source table to a file.
        '''

        utils.saveTableToFile(self.tabData, saveFile = saveFile, 
                         fileType = fileType, overwrite = overwrite, 
                         table_name = self.tabDataName,
                         **kwargs)
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def cleanZeroErrors(self):
        '''
        Replace tabData with sub-table with clean entries:  lines Flux >= 0 and Error > 0
        ATT: Replaces self.tabData by the cleaned table
        LeCid@Falguiere - 04/Juillet/2013
        '''

        # ATT: Can be better coded!!
        maskTot = [True] * len(self.tabData)
        for line__f in self.lines2Clean__f:
            if line__f in self.tabData.keys():
                maskFlux = self.tabData[line__f] >= 0
                maskErr  = self.tabData['e'+line__f] > 0
                maskTot *= maskFlux * maskErr
                
        self.indsOk  = np.where( maskTot )[0] 
        self.tabData = self.tabData[self.indsOk]

        if (self.debug): 
            log.debug('--sources.cleanZeroErrors> Cleaned tabData to ' , len( self.indsOk ) , ' sources where ' , self.lines2Clean__f , ' are all Ok!')

    # --------------------------------------------------------------------
# ************************************************************************


# ************************************************************************
class grid(object):
    '''
    Defines a grid of photoionization models.
    '''
    
    def __init__(self, gridFile = None, tabGrid = None, whoDidTable = 'Natalia', calcLineInd = False,
                 useLog = True, tableIsInLog = True, flagScenarios = True, selectTableRows = None, 
                 order_grid = True, debug = False):

        self.gridFile = gridFile
        self.tabGrid = tabGrid
        self.whoDidTable = whoDidTable
        self.useLog = useLog
        self.tableIsInLog = tableIsInLog
        self.debug = debug
        
        if (self.gridFile is not None) & (self.tabGrid is not None):
            log.warn("!!!WARNING!!! Either define a gridFile or a tabGrid, not both at once")
            sys.exit()

        # line & parameter lists...
        self.outputLineList = ['O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RO2', 'RN2', 'Ar3Ne3', 'F3727', 'F4363', 'F5007', 'F4861', 'F6563', 'F6584', 'F6731', 'F6716', 'F9069', 'F9532e', 'F4959', 'F5755', 'F6548', 'F7323', 'F7332', 'F3869', 'F4070', 'F4074', 'F4078', 'F6312', 'F6724', 'F7135', 'F4471', 'F5876', 'F4740', 'F4686', 'F6300', 'F7325', 'F128100', 'F155500', 'F883300']
        self.inputParList   = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']

        # Complete list
        #self.outputLineList = ['O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RN2', 'Ar3Ne3', 'F3727', 'F4363', 'F5007', 'F4861', 'F6563', 'F6584', 'F6731', 'F6716', 'F9069', 'F9532e', 'F4959', 'F5755', 'F6548', 'F7323', 'F7332', 'F3869', 'F4070', 'F4074', 'F4078', 'F6312', 'F6724', 'F7135', 'F4471', 'F5876', 'F4740', 'F4686', 'F6300', 'F7325', 'F128100', 'F155500', 'F883300']
        #self.inputParList   = ['fr', 'age', 'logUin', 'OXYGEN', 'logN2O', 'HELIUM', 'CARBON', 'NITROGEN', 'NEON', 'SULPHUR', 'ARGON', 'IRON']
        
        # Read grid file
        if (self.gridFile is not None):
            self.readGridFile()

        if (self.tabGrid is not None):

            # Transform fluxes to log Fluxes
            # ATT: No protection against flux=0, but this does not happen in our grid file ...
            # TO DO: This is also repeated in the Christophe's reader below, fix it
            if (self.useLog) & (not self.tableIsInLog):
                for key in self.tabGrid.keys():
                    if key in self.outputLineList:
                        if self.debug:
                            log.debug('--grid> transforming grid ' , key , ' to log!')
                        self.tabGrid[key] = np.log10( self.tabGrid[key] )
    
            # Select rows
            if (selectTableRows is not None):
                self.tabGrid = self.tabGrid[selectTableRows]
    
            # Name the table
            self.tabGridName = 'tabGrid'
                
            # Define number of models & variables in grid
            self.nGrid   = len(self.tabGrid)

            # Define super-branch
            self.branches = {'all': np.ones(self.nGrid, 'bool')}
        
            # Calc some line indices
            if calcLineInd:
                calcLineIndices(self.tabGrid, tableIsInLog = self.tableIsInLog, calcErrors = False)

            # For convenience, create varName__v list
            self.varName__v = self.tabGrid.keys()

            # Distinguish grid entries into 3 types: varType__v = 'input' / 'output' / 'etc'
            # - output = predicted line fluxes
            # - input  = main input parameters (but excluding those which are fixed, like 'dens' & 'ff')
            # - etc    = everything else (gas temperatures, mean U, etc...)
            self.varType__v = {}
            for V in self.varName__v:
                if V in self.outputLineList: 
                    self.varType__v[V] = 'output'
                elif V in self.inputParList:
                    self.varType__v[V] = 'input'
                else:
                    self.varType__v[V] = 'etc'

            # Get unique value of each input var
            self.inputVars__u = {}
            for V in self.varName__v:
                if V in self.inputParList:
                    x = np.asarray( utils.unique_tol(self.tabGrid[V]) )
                    self.inputVars__u[V] = x
                    if debug: log.debug("%s unique values for %s: %s" % (len(x), V, x))

            # Order grid
            if order_grid:
                self.order_grid(order = self.inputParList)

            # Flag scenarios 
            self.scenarios = {'all': np.ones(self.nGrid, 'bool')}
            
            if flagScenarios:
                self.flagScenarios()

                
        if self.debug:        
            log.debug(' **grid> done!')
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def readGridFile(self, readHDFRAM = False):
        '''
        Read model grid table & perform a few renaming & cleaning operations. Natalia's interpolated grid is
        basically plug'n play, but Christophe's table needs some reformatting...
        '''

        fileName, fileExtension = os.path.splitext(self.gridFile)
        if (fileExtension == '.hdf5'):
            if not readHDFRAM:
                self.tabGrid = utils.read_hdf5_to_table(self.gridFile)
            else:
                self.tabGrid = utils.tableHDFRAM(self.gridFile)
        else:
            self.tabGrid = astropy.table.Table.read(self.gridFile, format = 'ascii')

        # Christophe's version needs some reformatting....
        if self.whoDidTable == 'Christophe':
            self.fixGridChristophe()
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def fixGridChristophe(self):
        '''
        Rename some variables and do some aesthetic changes to Christophe's grid
        '''
            
        # Define new and old names & rename lines to new/std notation
        #    New      :   Old
        namesConversion_NewOld = { 
            'F3727'   :  'TOTL__3727A' , 
            'F3726'   :  'O_II__3726A' , 
            'F3726R'  :  'O_2R__3726A' , 
            'F3729'   :  'O_II__3729A' , 
            'F3729R'  :  'O_2R__3729A' , 
            'F3869'   :  'NE_3__3869A' , 
            'F4070'   :  'S_II__4070A' , 
            'F4078'   :  'S_II__4078A' , 
            'F4340'   :  'H__1__4340A' , 
            'F4363'   :  'TOTL__4363A' , 
            'F4471'   :  'HE_1__4471A' , 
            'F4659'   :  'FE_3__4659A' , 
            'F4686'   :  'HE_2__4686A' , 
            'F4711'   :  'AR_4__4711A' , 
            'F4740'   :  'AR_4__4740A' , 
            'F4861'   :  'H__1__4861A' , 
            'F4959'   :  'O__3__4959A' , 
            'F5007'   :  'O__3__5007A' , 
            'F5518'   :  'CL_3__5518A' , 
            'F5538'   :  'CL_3__5538A' , 
            'F5755'   :  'N__2__5755A' , 
            'F5755R'  :  'N_2R__5755A' , 
            'F5876'   :  'CA_B__5876A' , 
            'F6300'   :  'O__1__6300A' , 
            'F6312'   :  'S__3__6312A' , 
            'F6548'   :  'N__2__6548A' , 
            'F6563'   :  'H__1__6563A' , 
            'F6584'   :  'N__2__6584A' ,  
            'F6716'   :  'S_II__6716A' , 
            'F6731'   :  'S_II__6731A' , 
            'F7135'   :  'AR_3__7135A' , 
            'F7325'   :  'TOTL__7325A' ,
            'F7323'   :  'O_II__7323A' , 
            'F7323R'  :  'O_2R__7323A' , 
            'F7332'   :  'O_II__7332A' , 
            'F7332R'  :  'O_2R__7332A' , 
            'F9069'   :  'S__3__9069A' , 
            'F9532'   :  'S__3__9532A' , 
            'F128100' :  'NE_2_1281M'  ,
            'F155500' :  'NE_3_1555M'  ,
            'F883300' :  'O__3_8833M'  ,
            }
        self.lineFluxes = namesConversion_NewOld.keys()

        for nameNew, nameOld in namesConversion_NewOld.items():
            if nameOld in self.tabGrid.keys():
                self.tabGrid.rename_column(nameOld, nameNew)
                if self.debug: log.debug('Renaming ', nameOld, '-->' , nameNew)

        # Transform Fluxes to log Fluxes & error-in-flux to error-in-log-flux
        if (self.useLog) & (not self.tableIsInLog):
            for key in self.lineFluxes:
                if key in self.tabGrid.keys():
                    F, eF, logF, elogF = utils.calcLineRatio(self.tabGrid[key], 1., 0., 0., fluxInLog = False)
                    self.tabGrid[key] = logF
                    self.tableIsInLog = True

        # Christophe's table gives luminosities in erg/s. Transform all lines to Hb ratios.
        Hb = self.tabGrid['F4861'].copy()
        for lineFlux in self.lineFluxes:
            if lineFlux in self.tabGrid.keys():
                F, eF, logF, elogF = utils.calcLineRatio(self.tabGrid[lineFlux], Hb, 0., 0., fluxInLog = self.tableIsInLog)
                if self.tableIsInLog:
                    self.tabGrid[lineFlux] = logF
                else:
                    self.tabGrid[lineFlux] = F

        # Convert ages to Myr
        self.tabGrid['age'] = self.tabGrid['age'] / 1.0e6

        # Add logN2O = Ab_N - AB_O (rounded to 3 decimals just to provide correct unique values)
        if (self.tableIsInLog):
            self.tabGrid['logN2O'] = np.round( self.tabGrid['NITROGEN'] - self.tabGrid['OXYGEN'] , 3 )
        else:
            self.tabGrid['logN2O'] = np.round( safe_div(self.tabGrid['NITROGEN'], self.tabGrid['OXYGEN']) , 3 )
    # --------------------------------------------------------------------


    # --------------------------------------------------------------------
    def flagScenarios(self, scenVars = None, tabGrid = None, saveScen = True, **kwargs):
        '''
        Usage:

        grid1.flagScenarios(scenVars = {'age': [2, 4], 'fr': [0.03]})
        
        If called with no arguments, select all possible scenarios.         
        '''
        # This python behaviour really tripped me
        # http://stackoverflow.com/questions/14103871/why-python-dictionary-remembers-the-value-of-the-previous-instance-when-i-initia
        if scenVars is None:
            scenVars = {'age': None, 'fr': None}
            
        if tabGrid is None:
            tabGrid = self.tabGrid

        for var, values in scenVars.items():
            if values is None:
                scenVars[var] = self.inputVars__u[var].copy()

        # http://stackoverflow.com/questions/15211568/combine-python-dictionary-permutations-into-list-of-dictionaries
        scens = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]
        #print 'a', scenVars, scens, self.inputVars__u['age'], self.inputVars__u['fr']

        # Start a dictionary to keep scenarios        
        scenarios = {}
        
        # Mask grid according to scenario
        for scenario in scens:
            scenName = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scenario.items()])
            flag = np.ones(len(tabGrid), 'bool')
            for inputVar, value in scenario.items():            
                if value is not None:
                    f = self.flagGrid(inputVar, value, tabGrid = tabGrid, **kwargs)
                    flag &= f
            scenarios[scenName] = flag

        scenarios['all'] = np.ones_like(flag, 'bool')

        #print scenarios.keys()
        #print scenarios
        
        if saveScen:
            self.scenarios = scenarios
            
        return scenarios
    # --------------------------------------------------------------------
        
    # --------------------------------------------------------------------
    def selectScenarios(self, scenVars = None, findBimodality = False):
        '''
        Usage:

        grid1.selectScenarios(scenVars = {'age': [2, 4], 'fr': [0.03]})
        
        If called with no arguments, select all possible scenarios.         
        '''

        if scenVars is None:
            scenVars = {'age': None, 'fr': None}
            
        for var, values in scenVars.items():
            if values is None:
                scenVars[var] = self.inputVars__u[var].copy()

        # http://stackoverflow.com/questions/15211568/combine-python-dictionary-permutations-into-list-of-dictionaries
        scenarios = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]

        # Start a dictionary to keep scenarios
        grids = {}
        
        # Mask grid according to scenario
        self.tabGrid['branch_low'] = True
        self.tabGrid['branch_upp'] = False
        
        for scenario in scenarios:
            scenName = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scenario.items()])
            f, grids[scenName] = self.maskGrid(scenario, returnFlag=True)
            if findBimodality:
                f_low, f_upp = grids[scenName].findBimodality()
                self.tabGrid['branch_low'][f] = f_low
                self.tabGrid['branch_upp'][f] = f_upp            

        return grids
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def findBimodality(self, bimodOverlap = 0., tabGrid = None):
        '''
        Find bimodality for a given scenario
        '''

        
        # Using the grid itself
        if tabGrid is None:
            tabGrid = self.tabGrid
            
            # Start lower and upp branches flags
            f_low = np.zeros(len(tabGrid), 'bool')
            f_upp = np.zeros(len(tabGrid), 'bool')
            
            self.branches['low'] = f_low
            self.branches['upp'] = f_upp

        else:
            
            # Start lower and upp branches flags
            f_low = np.zeros(len(tabGrid), 'bool')
            f_upp = np.zeros(len(tabGrid), 'bool')

        # Go over every scenario
        ages = np.sort( utils.unique_tol( tabGrid['age']    ) ) 
        frs  = np.sort( utils.unique_tol( tabGrid['fr']     ) ) 
        NOs  = np.sort( utils.unique_tol( tabGrid['logN2O'] ) ) 
        Us   = np.sort( utils.unique_tol( tabGrid['logUin'] ) ) 

        for age in ages:
          flag_age = self.flagGrid('age', age, tabGrid = tabGrid)
          for fr in frs:
            flag_fr = self.flagGrid('fr', fr, tabGrid = tabGrid)
            for NO in NOs:
              flag_NO = self.flagGrid('logN2O', NO, tabGrid = tabGrid)
              for U in Us:
                flag_U = self.flagGrid('logUin', U, tabGrid = tabGrid)
                f = (flag_age & flag_fr & flag_NO & flag_U)
                ind_maxO23 = np.argmax(tabGrid['O23'][f])

                bimod_OH = tabGrid['OXYGEN'][f][ind_maxO23]
                f_low[f] = (tabGrid['OXYGEN'][f] <  (bimod_OH + bimodOverlap))
                f_upp[f] = (tabGrid['OXYGEN'][f] >= (bimod_OH - bimodOverlap))
                
        if self.debug:
            plt.clf()
            plt.subplot(221)
            plt.plot(self.tabGrid['OXYGEN'], self.tabGrid['O23'], 'b,')
            plt.xlim(-5.6, -2.4)
            plt.subplot(222)
            plt.plot(self.tabGrid['OXYGEN'][f_low], self.tabGrid['O23'][f_low], 'g,', alpha = 0.5)
            plt.plot(self.tabGrid['OXYGEN'][f_upp], self.tabGrid['O23'][f_upp], 'r,', alpha = 0.5)
            plt.xlim(-5.6, -2.4)
            plt.subplot(223)
            plt.plot(self.tabGrid['OXYGEN'][f_low], self.tabGrid['O23'][f_low], 'g,')
            plt.xlim(-5.6, -2.4)
            plt.subplot(224)
            plt.plot(self.tabGrid['OXYGEN'][f_upp], self.tabGrid['O23'][f_upp], 'r,')
            plt.xlim(-5.6, -2.4)

        # Save to table
        self.tabGrid['branch_low'] = f_low
        self.tabGrid['branch_upp'] = f_upp            
            
        return f_low, f_upp
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def toSources(self, fakeErr = 0.1, tabGrid = None, selectTableRows = None, useLog = True, 
                  tableIsInLog = True, tableHasLineRatios = True, debug = False):
        '''
        Return a fake sources table out of a model grid.
        Natalia 4/Jul/2013
        Heavily scrambled by LeCid@Falguiere - 11/Juillet/2013
        '''

        if tabGrid is None:
            tabGrid = self.tabGrid

        if selectTableRows is not None:
            tabGrid = tabGrid[selectTableRows]
            
        nGrid   = len(tabGrid)

        # Put the model-predicted lines in self.outputLineList inside a source-data-like table t
        # ATT: t contain Fluxes, even if grid contains log Fluxes
        t = astropy.table.Table(tabGrid, copy = True)
        for key in tabGrid.keys():
            if key in self.outputLineList:
                if self.tableIsInLog:
                    e = fakeErr / np.log(10.) + 0 * tabGrid[key]
                else:
                    e = fakeErr * tabGrid[key]
                t['e' + key] = e

        # Now complement table t with fake source name
        gridInd = np.arange(nGrid)
        t['name'] = gridInd

        '''
        # OBS: Rm error in 4861...
        remove_Hb = ['eF4861']
        for key in remove_Hb:
            if key in t.keys():
                t.remove_columns([key])
                if debug: print 'removed ', key
                '''
        # Create fake sources from this grid, with t as tabData
        return sources(tabData = t, useLog = useLog, tableIsInLog = tableIsInLog, tableHasLineRatios = tableHasLineRatios, debug = debug)
    # --------------------------------------------------------------------


    # --------------------------------------------------------------------
    def order_grid(self, order = None, gridType = None, Nround = 8):
        '''
        Order grid by inputVars. Useful to plot the grid and check the
        emission lines as functions of the grid parameters.

        Natalia 26/Oct/2014
        '''

        if order is None:
            order = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']
        
        if (gridType == 'interp'):
            tabGrid = self.tabGridInterp
        else:
            tabGrid = self.tabGrid
        
        # Fix precision in input vars
        for var in order:
            tabGrid[var][:] = np.round(tabGrid[var], Nround)

        # Order
        tabGrid.sort(order)
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def saveTableToFile(self, gridType = None, tabGrid = None, table_name = None, saveFile = None, fileType = 'hdf5', overwrite = False, **kwargs):
        '''
        Save a grid to a file.
        '''

        if tabGrid is None:
            if (gridType == 'interp'):
                tabGrid = self.tabGridInterp
                if table_name is None:
                    table_name = self.tabGridInterpName
            else:
                tabGrid = self.tabGrid
                if table_name is None:
                    table_name = self.tabGridName
                
            
        utils.saveTableToFile(tabGrid, saveFile = saveFile, fileType = fileType,
                         table_name = table_name,
                         overwrite = overwrite, **kwargs)
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def plot(self, xlab = 'OXYGEN', dx = 0, ylab = 'F5007', dy = 0,
             xlog = False, ylog = False,
             inputVars = {'fr': 0.03, 'age': 2., 'logN2O': -1},
             gridType = None, cmap = 'rainbow', branch = 'all',
             marker = '^', axes = None,
             plotPoints = True, plotLines = True,
             **kwargs):
        '''
        Plot grid models.
        '''
        
        # Select branch
        
        # Sort table
        self.order_grid(gridType = gridType)

        # Masked scenario
        newGrid = self.maskGrid(gridType = gridType, inputVars = inputVars).tabGrid

        # Get x, y from masked and sorted table
        x = dx + newGrid[xlab] 
        y = dy + newGrid[ylab] 

        if xlog:
            x = utils.safe_log10(x)
            xlab = 'log %s' % xlab
            
        if ylog:
            y = utils.safe_log10(y)
            ylab = 'log %s' % ylab

            
        # Colours & coloured lines = O/H
        # Marker size & grey lines = U
        cm = plt.get_cmap(cmap)
        gridColour = newGrid['OXYGEN']
        gridSize = 2. * (4.5 + newGrid['logUin'])
        
        plt.xlabel(xlab)
        plt.ylabel(ylab)

        if (dx != 0):
            plt.xlabel('%s + %s' % (dx, xlab))
        if (dy != 0):
            plt.ylabel('%s + %s' % (dy, ylab))

        # Plot points
        if plotPoints:
            plt.scatter(x, y, c = gridColour, s = gridSize, 
                        linewidths = 0, marker = marker, cmap = cm, 
                        **kwargs)

        if plotLines:
            # Plot lines
            colours = np.unique(gridColour)
            sizes = np.unique(gridSize)

            c = iter(cm(np.linspace(0, 1, len(colours))))
            for colour in colours:
                f = (gridColour == colour)
                plt.plot(x[f], y[f], color=next(c))
            
            for size in sizes:
                f = (gridSize == size)
                plt.plot(x[f], y[f], color='0.7')

        # Tickmarks
        from matplotlib import ticker

        if axes is None:
            axes = plt.axes()

        axes.minorticks_on()        
        axes.xaxis.set_major_locator( ticker.MaxNLocator(nbins = 5) )
        axes.yaxis.set_major_locator( ticker.MaxNLocator(nbins = 5) )
    # --------------------------------------------------------------------

    
    # --------------------------------------------------------------------
    def plot_origVSinterp_grid(self, fignumber = 1, ylog = False, addParams = None):
        '''
        Plot grid models to compare original and interpolated grids.

        Natalia@Corrego - 26/Oct/2014
        '''

        xsize = 8.
        fig = plt.figure(fignumber, figsize=(np.sqrt(2)*xsize, xsize))
        plt.clf()
        
        subp = (2,2)
        
        ax = plt.subplot2grid(subp, (0, 0)) 
        self.plot('12 + log O/H', 'F5007', ylog = ylog, marker = 'o', gridType = 'interp', cmap = 'Blues', axes = ax)
        self.plot('12 + log O/H', 'F5007', ylog = ylog, axes = ax)
        if addParams is not None:
            ax.plot(12+addParams['OXYGEN'], addParams['F5007'], 'ko')            
        
        ax = plt.subplot2grid(subp, (0, 1))
        self.plot('12 + log O/H', 'F3727', ylog = ylog, marker = 'o', gridType = 'interp', cmap = 'Blues', axes = ax)
        self.plot('12 + log O/H', 'F3727', ylog = ylog, axes = ax)
        if addParams is not None:
            ax.plot(12+addParams['OXYGEN'], addParams['F3727'], 'ko')            
        
        ax = plt.subplot2grid(subp, (1, 0))
        self.plot('logUin', 'F3727', ylog = ylog, marker = 'o', gridType = 'interp', cmap = 'Blues', axes = ax)
        self.plot('logUin', 'F3727', ylog = ylog, axes = ax)
        if addParams is not None:
            ax.plot(addParams['logUin'], addParams['F3727'], 'ko')            
        
        ax = plt.subplot2grid(subp, (1, 1))
        self.plot('logN2O', 'F6584', ylog = ylog, marker = 'o', gridType = 'interp', cmap = 'Blues', axes = ax)
        self.plot('logN2O', 'F6584', ylog = ylog, axes = ax)
        if addParams is not None:
            ax.plot(addParams['logN2O'], addParams['F6584'], 'ko')            
            
        fig.set_tight_layout(True)
    # --------------------------------------------------------------------
        
    # --------------------------------------------------------------------
    def maskGrid(self, inputVars = None, gridType = None, flag = None, debug = False, returnFlag = False, **kwargs):
        '''
        This is the right way to mask a grid by selecting specific age, fr, etc.

        This should also work, but it is too easy to write `==' instead of using the
        tolerances and then making it all go wrong:        
        maskG = np.where((abs(G2.tabGrid['fr'] - 0.03) < 1e-3) & (abs(G2.tabGrid['age'] - 2.) < 1.e-3))[0]
        G2new = sl.grid(tabGrid = G2.tabGrid, selectTableRows = maskG, useLog = True, tableIsInLog = True)
        '''

        if inputVars is None:
            inputVars = {'age': 2., 'fr': 0.03, 'logN2O': -1.}
        
        if (gridType == 'interp'):
            tabGrid = self.tabGridInterp
        else:
            tabGrid = self.tabGrid

        if flag is None:
            flag = np.ones(self.nGrid, 'bool')
        else:
            flag = flag.copy()
            
        for inputVar, value in inputVars.items():            
            if self.debug: log.debug(inputVar, value)
            if value is not None:
                f = self.flagGrid(inputVar, value, debug = debug, **kwargs)
                flag &= f

        if debug:
            nGrid = len(tabGrid)
            log.debug('The original grid had N_grid = %s elements; this flag selected N_flag = %s.' % (nGrid, sum(flag)))
            x = [self.inputVars__u[inputVar] for inputVar in inputVars.keys()]
            N = [len(x1) for x1 in x]
            Ncheck = nGrid / np.prod(N)
            log.debug('There were %s unique values for %s, so the new flag should have %s elements. ' % (N, inputVars.keys(), Ncheck))
            log.debug(inputVars.keys(), ' = ', x)
            log.debug()
            log.debug('(N_predicted = %s) == (N_flag = %s) ??' % (Ncheck, sum(flag)))
            log.debug()

        if (flag.sum() == 0):
            maskedGrid = None
        else:
            maskedGrid = grid(tabGrid = tabGrid, selectTableRows = flag, 
                              useLog = self.useLog, tableIsInLog = self.tableIsInLog)

            maskedGrid.mask = inputVars

        if returnFlag:
            return flag, maskedGrid
        else:
            return maskedGrid
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def flagGrid(self, inputVar = 'age', value = 2., tabGrid = None, rtol=0., atol=1.e-8, debug = False):
        '''
        This is the right way to select ages, frs, etc from a grid. 
        '''
        
        if tabGrid is None:
            tabGrid = self.tabGrid

        flag = np.isclose(tabGrid[inputVar], value, rtol=rtol, atol=atol)

        if debug:
            nGrid = len(tabGrid)
            log.debug()
            log.debug('The original grid had N_grid = %s elements; this flag selected N_flag = %s.' % (nGrid, sum(flag)))
            x = self.inputVars__u[inputVar]
            N = len(x)
            Ncheck = nGrid / N
            log.debug('There were %s unique values for %s, so the new flag should have %s elements. ' % (N, inputVar, Ncheck))
            log.debug('N_predicted = %s == N_flag = %s ??' % (Ncheck, sum(flag)))
            log.debug(x)
            log.debug()
        
        return flag
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def singleValue_interp_irregularGrid(self, inputParams = {'OXYGEN': -4, 'logN2O': -1, 'logUin': -2, 'fr': None, 'age': None}, 
                                       outputVars = ['OXYGEN', 'logN2O', 'logUin', 'F5007', 'F3727', 'F6584'], 
                                       debug = False, order_grid = False):

        # Re-order grid in input parameters
        inputVars = [k for k in inputParams.keys()]
        if order_grid:
            self.order_grid(order = inputVars)
        
        nX = len(inputVars)
        nV__x = np.zeros( nX, 'int' )
        for i, var in enumerate(inputVars):
            nV__x[i] = len(self.inputVars__u[var])

        # Where do we want to recalculate our grid? 
        points = [self.inputVars__u[V] for V in inputVars]
        xi = [v for v in inputParams.values()]

        # What do we want to output?
        outputParams = {}
        for V in outputVars:
            values = self.tabGrid[V].reshape(nV__x)
            outputParams[V] = scipy.interpolate.interpn(points, values, xi, method='linear')
        return outputParams
    # --------------------------------------------------------------------
    
        
    # --------------------------------------------------------------------
    def multipleValues_interp_regularGrid(self, inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'],
                                          inputParams = {'OXYGEN': [-4.], 'logN2O': [-1.], 'logUin': [-2.], 'fr': [0.03], 'age': [2.]}, 
                                          outputVars = None, order_grid = False, debug = False, Nround = 8):
        
        # Re-order grid in input parameters
        # Grid must already be ordered!
        if order_grid:
            self.order_grid(order = inputVars, Nround = Nround)
                            
        if outputVars is None:
            outputVars = self.tabGrid.keys()

        # Note: the index __x = inputVars to be interpolated
        nX = len(inputVars)

        # Index __k = number of points to interpolate
        try:
            k0 = inputParams.keys()[0]
        except:
            k0 = inputParams.dtype.names[0]
        nK = len(inputParams[k0])

        # Find the indices in __v of each input variable __x.
        # Also find out how many unique values each inputVar has.
        nV__x = np.zeros( nX, 'int' )
        #px__xk = [0] * nX
        px__xk = np.empty((nX, nK))
        flagInGrid__k = np.ones(nK, 'bool')

        # Start with an empty interpolated grid
        newGrid = astropy.table.Table()

        def calcInd(var, n):
            vout = np.round(inputParams[var], Nround)
            vmin = np.round(self.inputVars__u[var][0],  Nround)
            vmax = np.round(self.inputVars__u[var][-1], Nround)
            dv = (vmax - vmin)
            if dv == 0:
                dv = 1
            return (n - 1) * (vout - vmin) / dv

        for i, var in enumerate(inputVars):
            nV__x[i] = len(self.inputVars__u[var])
            #print self.inputVars__u[var]
            k = calcInd(var, nV__x[i])
            #print var, inputParams[var], k
            px__xk[i] = k
            flagInGrid__k &= (px__xk[i] <= (nV__x[i] - 1)) & (px__xk[i] >= 0)
            #print var, inputParams[var][~flagInGrid__k]

            if debug: 
                log.debug()
                log.debug('@debug@', i, var, var)
                log.debug('@debug@', 'number of unique values = ', nV__x[i], self.inputVars__u[var])
                log.debug('@debug@', 'px__xk = ', px__xk[i], '(len = %s)' % len(px__xk[i]))

                
        #print flagInGrid__k.sum(), flagInGrid__k.shape
        #px__xk = px__xk[..., flagInGrid__k]

        for iV, V in enumerate(outputVars):
            #if V in ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age', 'F3727']:
            
                # Create a hypercube as a function of __x1, __x2, __x3, ..., __xN (= __xk)
                # for each parameter in the original grid
                var__xk = self.tabGrid[V].reshape(nV__x)
                
                # Interpolate! See http://stackoverflow.com/questions/16983843/fast-interpolation-of-grid-data
                a = scipy.ndimage.map_coordinates(var__xk, px__xk, order = 1, cval = -999)
                if V in inputVars:
                    aux = utils.unique_tol(a)
                newGrid[V] = a.flatten()
                

        if debug:
            log.debug()
            log.debug('@@> Finished calculating interpolated grid (%s, %s rows):).' % (self.tabGridInterpName, len(self.tabGridInterp)))
            log.debug(newGrid[:10])
            log.debug()
            log.debug('@@> Original grid is still here (%s, %s rows):).' % (self.tabGridName, len(self.tabGrid)))
            log.debug(self.tabGrid[:10])
            
        return newGrid, flagInGrid__k
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def interpolate_regularGrid(self, inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                                nInterp = [2, 5, 5, 1, 1], 
                                saveFile = None, fileType = 'hdf5', overwrite = False,
                                debug = False):
        '''
        Creates a finer grid from a regular grid. This is 
        much faster than the irregular grid interpolation.

        Natalia@UFSC - 25/Oct/2014

        Parameters
        ----------
        inputVars : list
            A list of *all* input parameters that define the grid.

        nInterp : integer or list
            Multiplicative factors that define how smaller the grid bins should be.
        '''

        # Note: the index __x = inputVars to be interpolated
        nX = len(inputVars)

        if debug:
            inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']
            # Uncomment below for a quicker debug
            nInterp = [2, 2, 2, 1, 1]

        if len(inputVars) != nX:
            log.warn("!!! WARNING !!!> Number of inputVars (%s) is different from nInterp (%s)." % (len(inputVars), len(nInterp)))
            sys.exit()

        log.info('@@> Interpolating grid with inputVars = ', inputVars, ' and nInterp = ', nInterp)
        
        # Find the indices in __v of each input variable __x.
        # Also find out how many unique values each inputVar has.
        ##iV__x = np.zeros( nX, 'int' )
        nV__x = np.zeros( nX, 'int' )
        px__xk = [0] * nX

        for i, var in enumerate(inputVars):
            nV__x[i] = len(self.inputVars__u[var])
            nNew = 1 + (nV__x[i] - 1) * nInterp[i]
            px__xk[i] = np.linspace(0, nV__x[i] - 1, nNew)

            if debug: 
                log.debug()
                log.debug('@debug@', i, var, var)
                log.debug('@debug@', 'number of unique values = ', nV__x[i], self.inputVars__u[var])
                log.debug('@debug@', 'divide bins by = ', nInterp[i])
                log.debug('@debug@', 'px__xk = ', px__xk[i], '(len = %s)' % len(px__xk[i]))

        
        # Transform px__xk in a mesh (all possible combinations of xk)
        # Note that for some unfanthomed reason meshgrid swaps x1 by x2 by default!
        # That is why one must *not* forget the indexing='ij'
        # (see http://docs.scipy.org/doc/numpy/reference/generated/numpy.meshgrid.html)
        pxMesh__xk = np.meshgrid(*px__xk, indexing='ij')

        
        # If one of the input parameters = 1 and nInterp > 1, 
        # it cannot be interpolated (only extrapolated).
        if np.sum((nV__x == 1) & (np.asarray(nInterp) != 1)):
            log.warn("!!! WARNING !!!> You are trying to extrapolate one of the the input parameters. Giving up.")
            sys.exit()

            
        # Order original grid by input parameters and reshape the grid
        self.order_grid(order = inputVars)
        #print self.tabGrid[inputVars][:10]
        
        # Start with an empty interpolated grid
        self.tabGridInterp = astropy.table.Table()
        self.tabGridInterpName = 'tabGridInterp'
        self.tabGridInterp.inputVars = inputVars
        self.tabGridInterp.nInterp = nInterp

        for iV, V in enumerate(self.varName__v):
            # Uncomment below for a quicker debug
            #if iV in (1, 4, 7, 16, 63, 62, 40, 28):
            #if iV in (1, 4, 7, 16, 63):
            #if iV in (63, ):
            #if iV in (16, ):
            
                # Create a hypercube as a function of __x1, __x2, __x3, ..., __xN (= __xk)
                # for each parameter in the original grid
                var__xk = self.tabGrid[V].reshape(nV__x)
                
                if debug: 
                    log.debug('@debug@', iV, V, 'orig = ', var__xk.shape, len(utils.unique_tol(var__xk)), utils.unique_tol(np.asarray(var__xk)))

                # Interpolate! See http://stackoverflow.com/questions/16983843/fast-interpolation-of-grid-data
                a = scipy.ndimage.map_coordinates(var__xk, pxMesh__xk, order = 1, cval = -999)
                if V in inputVars:
                    aux = utils.unique_tol(a)
                    log.debug('@@> %s: %s unique values in the original grid; %s unique values in the interpolated grid.' % (V, len(utils.unique_tol(np.asarray(var__xk))), len(aux)))
                    log.debug('    Origin = %s' % (utils.unique_tol(np.asarray(var__xk))))
                    log.debug('    Interp = %s' % (aux))
                if debug:
                    aux = utils.unique_tol(a)
                    log.debug('@debug@', iV, V, 'intp = ', a.shape, len(aux), aux)
                    
                self.tabGridInterp[V] = a.flatten()

        # Recalc line indices (this is safer than relying on the interpolated values)
        calcLineIndices(self.tabGridInterp, tableIsInLog = self.tableIsInLog)
                
        log.info('@@> Finished calculating interpolated grid (%s, %s rows): %s).' % (self.tabGridInterpName, len(self.tabGridInterp), self.tabGridInterp.dtype))
        log.info(self.tabGridInterp[:10])
        log.info('@@> Original grid is still here (%s, %s rows): %s).' % (self.tabGridName, len(self.tabGrid), self.tabGrid.dtype))
        log.info(self.tabGrid[:10])
        
        if saveFile is not None:
            self.saveTableToFile('interp', saveFile=saveFile, fileType=fileType, overwrite=overwrite)
    # --------------------------------------------------------------------


    # --------------------------------------------------------------------
    def interpolate_irregularGrid(self, inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                                nInterp = [2, 5, 5, 1, 1], 
                                outputVars = None,
                                saveFile = None, fileType = 'hdf5', overwrite = False,
                                debug = False):        
        '''
        Creates a finer grid.

        Natalia 4/Jul/2013

        Parameters
        ----------
        inputVars : list
            A list of *all* input parameters that have defined the grid.

        nInterp : integer or list
            Multiplicative factores to define how finer the grid will be on the input parameters.
        '''

        # Note: the index __x = inputVars to be interpolated
        nX = len(inputVars)

        if debug:
            inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']
            nInterp = [1, 2, 1, 1, 1]

        if len(inputVars) != nX:
            log.warn("!!! WARNING !!!> Number of inputVars (%s) is different from nInterp (%s)." % (len(inputVars), len(nInterp)))
            sys.exit()

        log.info('@@> Interpolating grid with inputVars = ', inputVars, ' and nInterp = ', nInterp)
        
        # Find the indices in __v of each input variable __x
        iV__x = np.zeros( nX, 'int' )
        for i, var in enumerate(inputVars):
            iV__x[i] = np.where(np.array(self.varName__v) == var)[0][0]
            if debug: log.debug('@debug@', i, var, self.varName__v[iV__x[i]], ' index in grid = ', iV__x[i])

                
        # Get unique values for all parameters in grid
        # Start with empty arrays
        nVar = len(self.tabGrid.keys())
        self.var__vg = np.zeros( (nVar, self.nGrid) )
        self.nuVar__v    = np.zeros(nVar, dtype=int)
        self.uVar__vu    = [0] * nVar
        self.uVarLow__vu = [0] * nVar
        self.uVarUpp__vu = [0] * nVar
        self.duVar__vu   = [0] * nVar

            
        # Fill in unique arrays
        for iV, V in enumerate(self.tabGrid.keys()):
            
            self.var__vg[iV,:] = self.tabGrid[V]
            self.uVar__vu[iV]  = np.sort( utils.unique_tol( self.var__vg[iV,:] ) )
            self.nuVar__v[iV]  = len( self.uVar__vu[iV] )

            if (self.nuVar__v[iV] > 1):
                # Calculate du bins correctly wherever possible
                aux = ( self.uVar__vu[iV][1:] - self.uVar__vu[iV][:-1] ) / 2
                self.uVarLow__vu[iV] = self.uVar__vu[iV] - np.hstack([aux[0], aux])
                self.uVarUpp__vu[iV] = self.uVar__vu[iV] + np.hstack([aux, [aux[-1]]])
                # TODO: must be more careful with extremes. Ages were going to negative Myrs!
                self.duVar__vu[iV]   = self.uVarUpp__vu[iV] - self.uVarLow__vu[iV]
            else:
                # Invents du for variables with only one bin
                self.uVarLow__vu[iV] = 0.95 * self.uVar__vu[iV]
                self.uVarUpp__vu[iV] = 1.05 * self.uVar__vu[iV]
                self.duVar__vu[iV]   = self.uVarUpp__vu[iV] - self.uVarLow__vu[iV] 

            #if debug: print '@debug@', iV, V, self.uVar__vu[iV], self.nuVar__v[iV]

                
        # Find the size of grid for each input parameter
        nuVar__x  = self.nuVar__v[iV__x]
        uVar__xu  = np.asarray(self.uVar__vu)[iV__x]
        duVar__xu = np.asarray(self.duVar__vu)[iV__x]
        if debug:
            log.debug('@debug@ Summary:')
            log.debug('@debug@ inputVars = ', np.asarray(self.varName__v)[iV__x])
            log.debug('@debug@ index in grid = ', iV__x)
            log.debug('@debug@ number of unique values = ', nuVar__x)
            log.debug('@debug@ unique values = ', uVar__xu)
            log.debug('@debug@ --end summary--')

            
        # If one of the input parameters = 1 and nInterp > 1, 
        # it cannot be interpolated (only extrapolated).
        if np.sum((nuVar__x == 1) & (np.asarray(nInterp) != 1)):
            log.warn("!!! WARNING !!!> You are trying to extrapolate one of the the input parameters. Giving up.")
            sys.exit()

        # Start a new expanded (interpolated) grid
        uVarInterp__xu = [0] * nX
        for i, iV in enumerate(iV__x):
            # nI = by how much the grid should be expanded for this inputVar
            nI = nInterp[i]
            # Save the index for the original grid of each inputVar
            indUniqVar__g = np.arange(nuVar__x[i])
            # Save the index for the interpolated grid of each inputVar
            indUniqVarInterp__g = np.arange(nuVar__x[i] * nI)
            # Save the unique values of inputVars for the new interpolated grid
            if (nI == 1):
                uVarInterp__xu[i] = uVar__xu[i]
            elif (nI > 1):
                aux = np.interp(indUniqVarInterp__g, indUniqVar__g*nI, uVar__xu[i])
                uVarInterp__xu[i] = aux[:-1]

                
        # Create a matrix to hold the interpolated values.
        # np.meshgrid: http://docs.scipy.org/doc/numpy/reference/generated/numpy.meshgrid.html
        #   Input:  *uVarInterp__xu = x1, x2,..., xn: 1-D arrays representing the coordinates of a grid.
        #   Output: X1, X2,..., XN: For vectors x1, x2,..., xn with lengths Ni=len(xi), return (N1, N2, N3,...Nn) shaped arrays if indexing=ij or (N2, N1, N3,...Nn) shaped arrays if indexing=xy with the elements of xi repeated to fill the matrix along the first dimension for x1, the second for x2 and so on.
        uVarInterpMesh__xk = np.meshgrid(*uVarInterp__xu)
        # Flatten the new inputVars arrays. This generates all combinations of inputVars for the new interpolated grid. 
        # For instance, if we have inputVar1 = [1, 2] and inputVar2 = [3, 4, 5] (already interpolated),
        # we end up with:
        # uVarInterpMesh__xk[0] = [1 1 1 2 2 2]        
        # uVarInterpMesh__xk[1] = [3 4 5 3 4 5]
        # The index __k stands for = `k'ombinatory (__c was taken!)
        for i, u in enumerate(uVarInterpMesh__xk):
            uVarInterpMesh__xk[i] = u.flatten()
            if debug: log.debug('@debug@', i, uVarInterpMesh__xk[i].shape, uVarInterpMesh__xk[i])

            
        # Also create the mesh for the original grid
        # Order original grid by input parameters and reshape the grid
        self.order_grid(order=inputVars)
        uVarMesh__xk = [0] * nX
        for i, iV in enumerate(iV__x):
            V = self.varName__v[iV]
            uVarMesh__xk[i] = self.tabGrid[V]
            if debug: log.debug('@debug@', i, uVarMesh__xk[i].shape, uVarMesh__xk[i])

        # Start with an empty interpolated grid
        self.tabGridInterp = astropy.table.Table()
        self.tabGridInterpName = 'tabGridInterp'

        if outputVars is not None:
            varName__v = outputVars
        else:
            varName__v = self.varName__v
            
        for iV, V in enumerate(varName__v):
            # Uncomment below to debug
            #if iV in (1, 4, 7, 16, 63, 62, 40, 28):
            #if iV in (63, ):

                # Create a hypercube as a function of __x1, __x2, __x3, ..., __xN (= __xk)
                # for each parameter in the original grid
                var__xk = self.tabGrid[V].reshape(nuVar__x)
                if debug: log.debug('@debug@', iV, V, var__xk.shape)

                # Interpolate! See http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.interpolate.griddata.html
                # This is very slow because it is meant for irregular grids...
                # A way to speed it up would be: http://stackoverflow.com/questions/20915502/speedup-scipy-griddata-for-multiple-interpolations-between-two-irregular-grids
                # I haven't done because our grids are regular and much faster using scipy.ndimage.zoom
                a = scipy.interpolate.griddata(tuple(uVarMesh__xk), var__xk.flatten(), tuple(uVarInterpMesh__xk), method='linear')

                self.tabGridInterp[V] = a

                    
        log.info('@@> Finished calculating interpolated grid (.%s: %s).' % (self.tabGridInterpName, self.tabGridInterp.dtype))
        log.info(self.tabGridInterp[:10])
        log.info('@@> Original grid is still here (.%s: %s).' % (self.tabGridName, self.tabGrid.dtype))
        log.info(self.tabGrid[:10])

        return self.tabGridInterp
    
        if saveFile is not None:
            self.saveTableToFile('interp', saveFile=saveFile, fileType=fileType, overwrite=overwrite)
        # --------------------------------------------------------------------

        
# ************************************************************************


# ************************************************************************
class fit(object):
    '''
    Calculates and keeps probability distribution functions for given
    sources and grid. PDFs are only computed for variables in parName__p.
    '''
    
    def __init__(self, fitFile = None, grid = None, sources = None, debug = False, **kwargs):
        '''
        For a new fit, define both the grid and sources. 
        To read a fit saved on a file, define the fitFile.
        '''

        self.debug = debug  
            
        if (fitFile is not None) & ((grid is not None) | (sources is not None)):
            log.warn('@@> You have tried to create a fit object with sources and/or a grid, and a fitFile.\n    Either define the fitFile to read from, or the sources and grid for a new fit.')
            sys.exit(1)
        
        elif (fitFile is not None):
            self.loadData(fileName = fitFile, debug = debug, **kwargs)

        elif (grid is not None) & (sources is not None):
            self.new_fit(grid = grid, sources = sources, debug = debug, **kwargs)

        else:
            if self.debug:
                log.debug('@@> Not enough parameters passed, starting an empty fit.')
    # --------------------------------------------------------------------
            
    # --------------------------------------------------------------------
    def new_fit(self, grid = None, 
                gridParts = None,
                flagGrid = None,
                sources = None,  
                sourcesRows = None,
                obsFit__f = ['F3727', 'F5007', 'F6584'],
                errCookingFactor = 1.0,
                fakeErr = None,
                mode = 'chi2',
                parName__p = None,
                removePars = False,
                nBins = 400,
                calcAll = True,
                PDFquick = False,
                baseName = None, 
                dirName = None,
                debug = False):
        '''
        Fit sources with grid.
        '''
        self.grid = grid
        self.sources = sources
        self.sourcesRows = sourcesRows
        self.obsFit__f = obsFit__f
        self.errCookingFactor = errCookingFactor
        self.fakeErr = fakeErr
        self.likelihoodMode = mode
        self.nBins = nBins
        self.baseName = baseName
        self.dirName = dirName
        self.debug = debug

        # Selecting sources
        self.flagSourcesFitted()

        # Checking uncertainties
        if (errCookingFactor != 1.) & (fakeErr is not None):
            log.warn('!!!Either do errCookingFactor != 1 or fakeErr = [...]!!!')
            sys.exit()

        # Check input-given parNames__p, define parType__p, test whether parName__p is of type etc and whether 
        # it has more than one value in the grid (otherwise thing may go bananas)
        if (parName__p is None):
            log.warn('!!!You MUST specify parName__p!!!')
            sys.exit()

        self.parName__p = parName__p
        self.nPar = len(parName__p)

        self.parType__p = {key: 'etc' for key in self.parName__p}
        for P in self.parName__p:
            self.parType__p[P] = self.grid.varType__v[P]
            #print '--fit> ', P , '|' , indParinVar , self.grid.varName__v[indParinVar] , self.grid.varType__v[indParinVar]

        if 'etc' in self.parType__p.values():
            log.warn('!!!ATT: One of your pars is of type **etc**!! This is not allowed (yet)...!!!')
            #    sys.exit(1)

        # Flag for the grid
        self.flagGrid = flagGrid
            
        # Remove single input parameters from grid
        if removePars:
            for P in self.grid.inputVars__u:
                if ( len(self.grid.inputVars__u[P]) <= 1):
                    if debug:
                        log.debug('!!!ATT: Par ' , P  , ' has only ' , len(self.grid.inputVars__u[P]) , ' entries, and so was **REMOVED** from you list')
                    f = (self.parName__p != P)
                    self.parName__p = self.parName__p[f]

        # Grid parts to consider separately (branches, scenarios...)
        if gridParts is None:
            self.gridParts = grid.branches.keys()
        else:
            self.gridParts = gridParts
        
        # Calculations
        if (self.grid is not None) & (self.sources is not None):

            if (self.sources.tableIsInLog != self.grid.tableIsInLog):
                log.warn('!!!ATT: Both the sources and the grid table must be linear or in log')
                sys.exit()

            # Reset probabilities
            self.lnProb__sg = self._flatP()
            self.lnProb_norm__s = np.zeros(self.sources.nSources)
                
            # Keep track of where the probabity has been calculated and is greater than the prec/nGrid (after normalization)
            self.flagProb__sg = np.zeros( (self.sources.nSources, self.grid.nGrid), 'bool' )
                                
            # Initialize prior
            self.flatPrior()

            # Set fake errors
            self.setFakeErr()
                
            # Finally go for the actual PDF calculations...
            if calcAll:
                log.info('@@> Starting fit...')
                
                # Calc likelihood
                self.calcLikelihood(mode=mode)

                # Calc PDFs
                self.calcBinnedPDFs()
                self.calcPDFSummaries()

                # Calc number of solutions
                self.calcNSolutions()
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def fitOneSource(self, iSource):
        # Copy all source info
        S = sources()
        S.__dict__ = self.sources.__dict__.copy()

        # Select one source
        S.tabData = S.tabData[[iSource]]
        S.efixHb = S.efixHb[[iSource]]
        S.fixHb = S.fixHb[[iSource]]
        S.sourceName = [S.sourceName[iSource]]
        S.nSources = 1

        # Copy all fit info
        f = fit()
        f.__dict__ = self.__dict__.copy()

        # Select one source for fit info
        f.sources = S
        f.sourcesRows = np.array([0])
        f.sourcesRowsFitted = np.array([0])
        f.flagSourcesFitted()
        f.lnProb__psg = {k: v[iSource][np.newaxis, ...] for k, v in f.lnProb__psg.items()}
        
        return f
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def setFakeErr(self):
        # Adjusting uncertainties
        if self.fakeErr is None:
            self.fakeErr = False
        else:
            e = {}
            for varFit in self.obsFit__f:
                if self.sources.tableIsInLog:
                    # d (log F) = d (ln F) / ln 10 = (dF/F) / ln 10 = fakeErr / ln 10
                    e[varFit] = fakeErr / np.log(10.)
                else:
                    # dF = fakeErr * F
                    e[varFit] = fakeErr * self.sources.tabData[varFit]
            self.fakeErrVars = e        
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def flagSourcesFitted(self):
        try:
            self.sourcesRows
        except:
            self.sourcesRows = None
        
        try:
            self.sourcesRowsFitted
        except:
            self.sourcesRowsFitted = None
            
        if self.sourcesRows is None:
            if self.sourcesRowsFitted is not None:
                self.sourcesRows = deepcopy(self.sourcesRowsFitted)
            else:
                self.sourcesRows = np.arange(self.sources.nSources)
            
        # Only fit the sources selected by the user
        self.flagSources = np.zeros( self.sources.nSources, 'bool' )
        self.flagSources[self.sourcesRows] = 1

        # Flag out sources which do not have one of observables
        try:
            self.sources.tabData
            
            obsFitMin__s = np.zeros( self.sources.nSources )
            
            for varFit in self.obsFit__f:
                obsFitMin__s = np.min((self.sources.tabData[varFit], obsFitMin__s), axis = 0)
            
            flagOut = (obsFitMin__s <= -999)
            self.flagSources[flagOut] = 0

            # Save which sources were actually fitted
            ind = np.arange(self.sources.nSources)
            self.sourcesRowsFitted = ind[self.flagSources]

        except:
            pass
    # --------------------------------------------------------------------
                    
    # --------------------------------------------------------------------
    #@utils.mtimeit
    def calcLikelihood(self, mode = 'chi2', prec = 1e-20, removeLowProb = False, resetProb = True, **kwargs):

        self.likelihoodMode = mode
        
        if resetProb:
            self.flagProb__sg = np.zeros( (self.sources.nSources, self.grid.nGrid), 'bool' )

        # Calc likelihood for strong lines and/or emission lines defined by the user
        if mode == 'chi2':
            self.lnProb__sg, self.lnProb_norm__s = self._gaussianP(norm = False, removeLowProb = removeLowProb, **kwargs)
        elif mode == 'chebyshev':
            self.lnProb__sg, self.lnProb_norm__s = self._infNormDistP(norm = False, removeLowProb = removeLowProb, **kwargs)
        else:
            log.warn('!!> No valid mode for the likelihood (mode = %s).')
            sys.exit(1)
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def removeLowProb(self, iSrc, lnProb = None, prec = 1e-20):
        if lnProb is None:
            lnProb = self.lnProb__sg[iSrc]

        fp = self.flagProb__sg[iSrc]
        
        # Replace likelihood by a sparse array, getting rid of all values below prec/nGrid.
        tol = prec / self.grid.nGrid
        norm = np.max(lnProb)
        fmask = ((lnProb[fp] - norm) < np.log(tol))
        flag = np.where(fp)[0][fmask]
        self.flagProb__sg[iSrc, flag] = False
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def addDirBaseNames(self, baseName = None, dirName = None):
        if baseName is not None:
            self.baseName = baseName
        if dirName is not None:
            self.dirName = dirName

        if self.dirName is None:
            self.dirName = './'
            
        if self.baseName is None:
            log.warn('!!> Sorry, I could not save to a file: no baseName given.')
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    #@utils.mtimeit
    def setupParBins(self, doNotBin = ['age', 'fr'], debug = False, nBins = None):
        '''
        Define bins in all variables... used to compute PDFs
        LeCid@Falguiere - 07/Juillet/2013
        '''

        if nBins is not None:
            self.nBins = nBins

        # Reset bin-stuff: Note that bPar*__pb are **dictionaries**
        self.nbPar__p    = {}
        self.bParCen__pb = {}
        self.bParLow__pb = {}
        self.bParUpp__pb = {}
        self.dbPar__pb   = {}

        for iP, P in enumerate(self.parName__p):

            # Some input pars are binned as they as provided
            if P in doNotBin:
                aux1 = np.sort( utils.unique_tol( self.grid.tabGrid[P] ) ) 
                
                if len(aux1) == 1:
                    self.nbPar__p[P]     = len(aux1)
                    self.bParCen__pb[P]  = np.array(aux1)
                    self.bParLow__pb[P]  = np.array(aux1)
                    self.bParUpp__pb[P]  = np.array(aux1)
                    self.dbPar__pb[P]    = np.array(0.)             
                else:
                    aux2 = (aux1[1:] - aux1[:-1] ) / 2
                    
                    self.nbPar__p[P]     = len(aux1)
                    self.bParCen__pb[P]  = np.array(aux1)
                    self.bParLow__pb[P]  = np.array(aux1 - np.hstack([aux2[0], aux2]))
                    self.bParUpp__pb[P]  = np.array(aux1 + np.hstack([aux2, [aux2[-1]]]))
                    self.dbPar__pb[P]    = np.array(self.bParUpp__pb[P] - self.bParLow__pb[P] )
                    #print iP, P, 'Cen: ', self.bParCen__pb[P], '\n', iP, P, 'Low: ', self.bParLow__pb[P], '\n', iP, P, 'Upp: ', self.bParUpp__pb[P], '\n', iP, P, 'dp : ', self.dbPar__pb[P]

            # Output pars are binned in a linear grid (they are already log's anyway!)
            # number-of-bins given by self.nBins. limits are in & max, except when < -3 & > 1.
            else:

                prec = 8
                base = 5. * 10**-prec
                bPmin = utils.arbitrary_round( np.min(self.grid.tabGrid[P]), prec = prec, base = base)
                bPmax = utils.arbitrary_round( np.max(self.grid.tabGrid[P]), prec = prec, base = base)

                # Simple fixed-bin-grid scheme!
                pcen, dp = np.linspace( bPmin, bPmax, self.nBins, endpoint=True, retstep=True)

                self.nbPar__p[P]    = len(pcen)
                self.bParCen__pb[P] = np.array(pcen)
                self.bParLow__pb[P] = np.array(pcen - dp / 2.)
                self.bParUpp__pb[P] = np.array(pcen + dp / 2.)
                self.dbPar__pb[P]   = np.array(0 * pcen + dp)

        # Calc grid priors
        self.gridPDF__cpb = {}
        self.gridCDF__cpb = {}

        for gPart in self.gridParts:
            gridPDF__pb = {}
            gridCDF__pb = {}
                
            for P in self.parName__p:                
                # gridPDF is just a histogram
                gridPDF__pb[P] = self.calcMargPDF(P, branch=gPart)
                
                # Normalize PDF and calculate CDF. Same for gridPDF & gridCDF
                gridPDF_norm__s = gridPDF__pb[P].sum(axis = 0)
                gridPDF__pb[P] = np.where(gridPDF_norm__s > 0, utils.safe_div(gridPDF__pb[P], gridPDF_norm__s), gridPDF__pb[P])
                gridCDF__pb[P] = np.cumsum(gridPDF__pb[P], axis = 0)
                
            # Save results for each branch
            self.gridPDF__cpb[gPart] = gridPDF__pb
            self.gridCDF__cpb[gPart] = gridCDF__pb
            
            # Debugging
            if debug:
                log.debug(P, bPmin, bPmax, self.dbPar__pb[P][0])
                log.debug(iP, P, 'Cen: ', self.bParCen__pb[P], '\n', iP, P, 'Low: ', self.bParLow__pb[P], '\n', iP, P, 'Upp: ', self.bParUpp__pb[P], '\n', iP, P, 'dp : ', self.dbPar__pb[P])
    # --------------------------------------------------------------------
    
    
    # --------------------------------------------------------------------
    def calcMargPDF(self, X, tabGrid = None, bins = None, nBins = None,
                   branch = None, iSource = None, calcPDF = True, 
                   debug = False):

        # Get grid and flags
        tabGrid, flagScenarios = self.get_tabGrid_and_flags(iSource)
        
        if branch is None:
            flag_branch = np.ones(len(tabGrid))
        else:
            flag_branch = flagScenarios[branch]

        # If no source is given, simply calculate the grid priors
        if iSource is None:
            PDF__i = np.ones(len(tabGrid))
        else:
            lnPDF, fp = self.get_lnPDF_and_flag(iSource)
            PDF__i = np.exp(lnPDF) 
            flag_branch = flag_branch & fp

        PDF__i *= flag_branch

        # Get bins and resampling matrix
        bins_orig  = self.getBinsOrigGrid(X, iSource, tabGrid)
        bins_resam = self.getBinsResamGrid(X, nBins)
        R__ro = utils.ReSamplingMatrixNonUniform_sparseArray(bins_resam, bins_orig)

        if calcPDF:
            # Calculate the marginilized PDF
            PDF__r = R__ro.dot(PDF__i)
            
            # Normalize the PDF
            br_low, br_upp, br_cen, br_size, Nr = utils.getBinsLims(bins_resam)
            PDF__r /= np.ma.masked_invalid(PDF__r * br_size).sum()

            if debug:
                # Check integral
                log.debug('int', (PDF__r * br_size).sum())
    
                # Check PDF
                plt.figure()
                plt.clf()
                plt.plot(12+br_cen, PDF__r / PDF__r.sum(), drawstyle = 'steps-mid')
                plt.plot(12+br_cen, np.cumsum(PDF__r) / PDF__r.sum(), drawstyle = 'steps-mid')

            return PDF__r
        
        else:
            return PDF__i, bins_resam, R__ro
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------        
    def calcParameterJointPDF(self, X, iSource, PDF, Rs__ro, debug = False):

        tabGrid, flagScenarios = self.get_tabGrid_and_flags(iSource)

        # Get bins
        bins_orig  = self.getBinsOrigGrid(X, iSource, tabGrid)
        bo_low, bo_upp, bo_cen, bo_size, No = utils.getBinsLims(bins_orig)

        # Calculate the normalization: sum over dP/dNdO * dx
        aux = utils.multiply_sparse(Rs__ro[0], PDF * bo_size)
        norm = Rs__ro[1].dot(aux.T).T
                
        # Calculate the sum over dP/dNdO * x * dx
        # The second line is faster than the next one, and the result is the same
        # aux = utils.multiply_sparse(Rs__ro[0], tabGrid[P] * PDF * bo_size)
        aux = utils.multiply_sparse(aux, tabGrid[X])
        X__xy = Rs__ro[1].dot(aux.T).T

        # Normalize
        X__xy /= norm
        X__xy = utils.mask_nan(np.array(X__xy))

        return X__xy
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------        
    def calcJointPDF(self, Xs, nBins_Xs = None, debug = False, **kwargs):

        if nBins_Xs is None:
            nBins_Xs = [None] * len(Xs)
        
        bins   = [0] * len(Xs)
        Rs__ro = [0] * len(Xs)
        
        for i, X in enumerate(Xs):
            PDF__i, bins[i], R__ro = self.calcMargPDF(X, calcPDF = False, nBins = nBins_Xs[i], **kwargs)
            # This sparse matrix seems faster for multiplication and dot operations
            Rs__ro[i] = R__ro.tocsr()
            
        aux = utils.multiply_sparse(Rs__ro[0], PDF__i)
        PDF__xy = Rs__ro[1].dot(aux.T).T.toarray()

        Xgrid = np.meshgrid(bins[0], bins[1], indexing='ij')

        # Normalize PDF
        bx_low, bx_upp, bx_cen, bx_size, Nx = utils.getBinsLims(bins[0])
        by_low, by_upp, by_cen, by_size, Ny = utils.getBinsLims(bins[1])
        db__xy = bx_size[..., np.newaxis] * by_size[np.newaxis, ...]
        PDF__xy /= np.ma.masked_invalid(PDF__xy * db__xy).sum()
        
        if debug:
            # Check integral
            log.debug('int', (PDF__xy * db__xy).sum())

            # Check PDF (only works in 2D)
            plt.figure(1)
            plt.clf()
            plt.pcolormesh(12 + Xgrid[0], Xgrid[1], PDF__xy, cmap = plt.get_cmap('OrRd'))
            plt.colorbar()

        
        return Xgrid, PDF__xy, bins, Rs__ro
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def getBinsResamGrid(self, X, nBins = None):
        '''
        Get resampled bins.
        '''
        
        if nBins is not None:
            bins_res = np.linspace(self.bParLow__pb[X][0], self.bParUpp__pb[X][-1], nBins+1)
        else:
            bins_res = np.hstack((self.bParLow__pb[X], self.bParUpp__pb[X][-1]))
            
        return bins_res
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def getBinsOrigGrid(self, X, iSource = None, tabGrid = None):
        '''
        Get original bins from a grid
        '''

        tabGrid, flagScenarios = self.get_tabGrid_and_flags(iSource)

        if 'd%s' % X in tabGrid.keys():
            
            bo_low  = tabGrid[X] - 0.5 * tabGrid['d%s' % X]
            bo_upp  = tabGrid[X] + 0.5 * tabGrid['d%s' % X]
            bo_size = tabGrid['d%s' % X]
            
        else:

            bo_low, bo_upp, bo_size = self.calcBinsGrid(X, tabGrid)
            
        # Save only bins info on a 3D array
        bins = np.vstack([bo_low, bo_upp, bo_size])

        return bins
    # --------------------------------------------------------------------
            
    # --------------------------------------------------------------------
    def calcBinsGrid(self, X, tabGrid):
        
        # Create bin size in tabGrid
        bo_low  = np.zeros_like(tabGrid[X])
        bo_upp  = np.zeros_like(tabGrid[X])
        bo_size = np.zeros_like(tabGrid[X])

        # Sort this variable
        ind_x = np.argsort(tabGrid[X])
        x_sorted = tabGrid[X][ind_x]
        x_low = tabGrid[X][ind_x]
        x_upp = tabGrid[X][ind_x]

        # Get bin sizes
        dx = np.zeros_like(x_sorted)
        _dx = x_sorted[1:] - x_sorted[:-1]
        dx[:-1] += _dx / 2.
        dx[1:]  += _dx / 2.
        x_low[1:]  -= _dx / 2.
        x_upp[:-1] += _dx / 2.
        
        # Double bin sizes on the edges
        dx[0]  *= 2.
        dx[-1] *= 2.
        x_low[0]  -= dx[0]  / 2.
        x_upp[-1] += dx[-1] / 2.
        
        # Save bins in the right order
        bo_low[ind_x]  = x_low
        bo_upp[ind_x]  = x_upp
        bo_size[ind_x] = dx

        return bo_low, bo_upp, bo_size
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def flatPrior(self):
        # Keep track of where the posterior (= prior * likelihood) has been calculated
        self.flagProb__sg = np.zeros( (self.sources.nSources, self.grid.nGrid), 'bool' )

        # Start flat prior
        self.lnProb__psg = {'None prior flat': self._flatP()}

        # Add to probability - too slow and memory consuming
        ##self.lnProb__sg += self._flatP()
        
    def _flatP(self):
        return np.zeros( (self.sources.nSources, self.grid.nGrid) )
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def _quadraticDistance(self, obs, obs__s, err__s, flag__g = None, linear = False, tabGrid = None):
        d__g = 0.

        if (obs__s > -999.):

            # Get grid data
            if tabGrid is None:
                tabGrid = self.grid.tabGrid
            mod__g = tabGrid[obs][flag__g]
                
            # Get flag for grid
            if flag__g is None:
                flag__g = np.ones_like(tabGrid[obs], 'bool')

            # Transform to linear if needed
            if linear & self.sources.tableIsInLog:
                obs__s = utils.safe_pow(10., obs__s)
                err__s = err__s * obs__s * np.log(10.)
                
            if linear & self.grid.tableIsInLog:
                mod__g = utils.safe_pow(10., mod__g)

            # Calc the quadratic distance in units of err__s
            wei__s = utils.safe_div(1., err__s, 0.)
            d__g = ((obs__s - mod__g) * wei__s)**2
            
        return d__g
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def _infNormDistP(self, sourcesRows = None, norm = False, removeLowProb = False, linear = False, **kwargs):
        '''
        A simple infinite norm/Chebyshev distance probability. 
        '''
        
        if sourcesRows is None:
            sourcesRows = self.sourcesRowsFitted
            
        obs__p, delta__ps, suf = self._obs_delta(**kwargs)

        # Reset probability (might either be a likelihood or a prior)
        lnProb__sg = self._flatP()
        lnProb_norm__s = np.zeros(self.sources.nSources)

        # Calculate
        for iS in self.sourcesRowsFitted:

            # Start flag if needed
            fg = self.flagProb__sg[iS]
            if (fg.sum() == 0):
                self.flagProb__sg[iS] = True

            # Start with a very low probabilty 
            lnProb__sg[iS] = -1e30
            
            for obs in obs__p:
                key = '%s infnorm %s' % (obs, suf)
                self.lnProb__psg[key] = self._flatP()
                obs__s = self.sources.tabData[obs][iS]
                if (obs__s > -999.):
                    err__s = delta__ps[obs][iS]
                    lP__g = -0.5 * self._quadraticDistance(obs, obs__s, err__s, fg, linear = linear)
                    ff = (lP__g > lnProb__sg[iS, fg])
                    flag = np.where(fg)[0][ff]
                    lnProb__sg[iS, flag] = lP__g[ff]
                    self.lnProb__psg[key][iS, fg] = lP__g
                    if removeLowProb:
                        self.removeLowProb(iS, lnProb__sg[iS])
                        
            if norm:
                lnProb_norm__s[iS] = np.max(lnProb__sg[iS])
                lnProb__sg[iS] -= lnProb_norm__s[iS]
        
        if norm:
            return lnProb__sg, lnProb_norm__s
        else:        
            return lnProb__sg
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def gaussianProb(self, addDeltas = None, **kwargs):
        
        if addDeltas is None:
            gaussianP, n = self._gaussianP(**kwargs)
        else:
            gaussianP = self._flatP()

            # Tested in tests_marg_uncertainties.py

            # Create an array with deltas. In log space this converges much faster!
            dmin, dmax, nd = addDeltas 
            deltas = np.logspace(np.log10(dmin), np.log10(dmax), nd)

            # Now get the bin sizes
            dds = np.empty_like(deltas)
            _d = deltas[1:] - deltas[:-1]
            dds[0]  = _d[0]
            dds[-1] = _d[-1]
            dds[1:-1] = _d[1:]/2. + _d[:-1]/2

            for delta, dd in zip(deltas, dds):
                _gaussianP, n = self._gaussianP(calcSigContr = True, norm = False, addDelta = delta, saveToProb = False, **kwargs)
                gaussianP += np.exp(_gaussianP) * dd

            gaussianP = np.log(gaussianP)
            
            obs__p, delta__ps, suf = self._obs_delta(**kwargs)
            key = '%s gaussian ad=%s' % (' '.join(obs__p), addDeltas)
            self.lnProb__psg[key] = self._flatP()
            
            
        for iS in self.sourcesRowsFitted:
            fg = self.flagProb__sg[iS]
            self.lnProb__sg[iS, fg] += gaussianP[iS, fg]
            if addDeltas is not None:
                self.lnProb__psg[key][iS, fg] = gaussianP[iS, fg]

                

    def _gaussianP(self, sourcesRows = None, calcSigContr = False, norm = False, 
                   removeLowProb = False, linear = False, saveToProb = True, tabGrid = None, 
                   **kwargs):
        '''
        A simple gaussian probability. 
        '''

        if sourcesRows is None:
            sourcesRows = self.sourcesRowsFitted
            
        obs__p, delta__ps, suf = self._obs_delta(**kwargs)

        # Reset probability
        lnProb__sg = self._flatP()
        lnProb_norm__s = np.zeros(self.sources.nSources)

        # Calculate
        for iS in self.sourcesRowsFitted:

            # Start flag if needed
            fg = self.flagProb__sg[iS]
            if (fg.sum() == 0):
                self.flagProb__sg[iS] = True
                
            for obs in obs__p:
                
                if saveToProb:
                    key = '%s gaussian %s' % (obs, suf)
                    self.lnProb__psg[key] = self._flatP()
                    
                obs__s = self.sources.tabData[obs][iS]
                
                if (obs__s > -999.):
                    
                    err__s = delta__ps[obs][iS]
                    lP__g = -0.5 * self._quadraticDistance(obs, obs__s, err__s, fg, tabGrid = tabGrid, linear = linear)
                    lnProb__sg[iS, fg] += lP__g
                    
                    if calcSigContr:
                        lnProb__sg[iS, fg] += -0.5 * np.log(err__s)                        

                    if saveToProb:
                        self.lnProb__psg[key][iS, fg] = lP__g
                        
                    if removeLowProb:
                        self.removeLowProb(iS, lnProb__sg[iS])

            if norm:
                lnProb__sg[iS] += self.grid.tabGrid['ln_dV']
                lnProb_norm__s[iS] = np.log(np.sum(np.exp(lnProb__sg[iS][fg])))
                lnProb__sg[iS] -= lnProb_norm__s[iS]

        return lnProb__sg, lnProb_norm__s
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def boxProb(self, resetProb = False, **kwargs):
        boxP = self._boxP(**kwargs)
        
        if resetProb:
            self.flagProb__sg = np.zeros( (self.sources.nSources, self.grid.nGrid), 'bool' )
            
        for iS in self.sourcesRowsFitted:
            fg = self.flagProb__sg[iS]
            self.lnProb__sg[iS, fg] += boxP[iS, fg]
            
    def _boxP(self, sourcesRows = None, **kwargs):
        '''
        Apply a box probability of N * sigma (or of delta) to a set of observed lines/line ratios. 
        The default is to apply it to all fitted lines (obsFit__f).
        '''
        
        if sourcesRows is None:
            sourcesRows = self.sourcesRowsFitted
        
        obs__p, delta__ps, suf = self._obs_delta(**kwargs)

        lnProb__sg = self._flatP()
        
        for iS in self.sourcesRowsFitted:
            
            fg = self.flagProb__sg[iS]
            Nfg = fg.sum()
            if (Nfg == 0):
                self.flagProb__sg[iS] = True

            for obs in obs__p:

                fmask = ( np.abs(self.grid.tabGrid[obs][fg] - self.sources.tabData[obs][iS]) > delta__ps[obs][iS] )
                flag = np.where(fg)[0][fmask]
                self.flagProb__sg[iS][flag] = False
                self.lnProb__psg['%s box %s' % (obs, suf)] = self._flatP()

        return lnProb__sg
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def stepProb(self, **kwargs):
        stepP = self._stepP(**kwargs)

        '''
        for iS in self.sourcesRowsFitted:
            fg = self.flagProb__sg[iS]
            self.lnProb__sg[iS, fg] += stepP[iS, fg]
            '''
            
    def _stepP(self, obs__p = ['F4363', 'F5755'], flim = 1., sourcesRows = None):
        '''
        Apply a step function probability to a set of line/line ratio upper limits. 

        Usage: 
        fit.stepPro(obs__p = ['F4363', 'F5755', 'F6312', 'F7135', 'F7325'])
        '''

        if sourcesRows is None:
            sourcesRows = self.sourcesRowsFitted

        lnProb__sg = self._flatP()
        for iS in sourcesRows:
            for obs in obs__p:

                # Find upper limits or observations
                llim = 'lim'+obs
                lsig = 'e'+obs
                
                lim  = -999.
                flux = -999.
                sig  = -999.
                
                if obs in self.sources.tabData.keys():
                    flux = self.sources.tabData[obs][iS]
                if lsig in self.sources.tabData.keys():
                    sig  = self.sources.tabData[lsig][iS]
                if llim in self.sources.tabData.keys():
                    lim = self.sources.tabData[llim][iS]

                # Use observation & sigma as an upper limit
                if (lim <= -999) & (flux > -999) & (sig > -999):
                    if self.sources.tableIsInLog:
                        F = 10.**flux
                        dF = sig * F * np.log(10.)
                        lim = np.log10(F + 2. * dF)
                    else:
                        lim = flux + 2. * sig
                    
                if (lim > -999):
                    if self.sources.tableIsInLog:
                        lim += np.log10(flim)
                    else:
                        lim *= flim
                    
                    fg = self.flagProb__sg[iS]
                    faux = (self.grid.tabGrid[obs][fg] > lim)
                    flag = np.where(fg)[0][faux]
                    self.flagProb__sg[iS, flag] = 0
                    self.lnProb__psg['%s step upperlim' % obs] = self._flatP()
                    self.lnProb__psg['%s step upperlim' % obs][iS, flag] = -999.

        return lnProb__sg
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def _obs_delta(self, obs = None, nSigma = 1, delta = None, addDelta = 0, deltaMin = False):
        '''
        Select observed lines and their deltas (according to N * sigma if needed)
        for prior/likelihood calculations.

        Note delta works best in log line fluxes/ratios.
                
        Output:
        sqrt((nSigma * delta)**2 + addDelta**2)
        OR
        sqrt((nSigma * uncertaity)**2 + addDelta**2)
        '''

        # Get observed constraints
        obs__p = obs
        if obs__p is None:
            obs__p = self.obsFit__f

        # Start variables
        delta__ps = {}
        
        # Get line uncertainty from table
        for obs in obs__p:

            if (delta is None) & ('e'+obs not in self.sources.tabData.keys()):
                log.warn('!!! Could not find uncertainty for line ratio %s and no delta given.' % (obs))
                sys.exit(1)                
                
            deltaMin__s = np.zeros(self.sources.nSources)
            if delta is not None:
                deltaMin__s += np.sqrt( (delta* nSigma)**2 + addDelta**2)
                
            deltaNom__s = np.zeros(self.sources.nSources)
            if 'e'+obs in self.sources.tabData.keys():
                deltaNom__s = np.sqrt( (self.sources.tabData['e'+obs] * nSigma)**2 + (addDelta)**2 )

            if delta is None:
                delta__ps[obs] = deltaNom__s
            elif not deltaMin:
                delta__ps[obs] = deltaMin__s
            else:
                delta__ps[obs] = np.maximum(deltaMin__s, deltaNom__s)
                

        # Add parameters to suffix
        suf = ''
        
        if not np.isclose(nSigma, 1.):
            suf += 'nSig%s' % nSigma

        if delta is not None:
            suf += 'd%s' % delta
            
        if not np.isclose(addDelta, 0.):
            suf += 'ad%s' % addDelta

        return obs__p, delta__ps, suf
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def get_tabGrid_and_flags(self, iSource = None):

        try:
            self.flagScenarios__s
        except:
            self.flagScenarios__s = {}
            
        try:
            tabGrid = self.tabGrids__si[iSource]
            try:
                flagScenarios = self.flagScenarios__s[iSource]
            except:
                self.flagScenarios__s[iSource] = self.grid.flagScenarios(tabGrid = tabGrid, scenVars = {'age': self.bParCen__pb['age'], 'fr': self.bParCen__pb['fr']}, saveScen = False)
                flagScenarios = self.flagScenarios__s[iSource]              
        except:
            tabGrid = self.grid.tabGrid          
            try:
                flagScenarios = self.grid.scenarios
            except:
                flagScenarios = self.grid.flagScenarios()

        return tabGrid, flagScenarios
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def get_lnPDF_and_flag(self, iSource = None):

        try:
            lnPDF = self.lnPDF__si[iSource]
            flagProb = np.ones(len(lnPDF), 'bool')
        except:
            if iSource is None:
                lnPDF = self.lnPDF__sg
                flagProb = self.flagProb__sg
            else:
                lnPDF = self.lnProb__sg[iSource]   
                flagProb = self.flagProb__sg[iSource]

        if (len(self.lnProb__psg.keys()) == 1) & ('None prior flat' in self.lnProb__psg.keys()):
            flagProb = np.ones_like(flagProb)
            
        return lnPDF, flagProb
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------    
    def octree_startup(self, iSource, inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'],
                    interpVars = ['OXYGEN', 'logN2O', 'logUin'], reset = False):        
        '''
        Reset grid for octree
        '''

        try:
            self.tabGrids__si
            self.lnPDF__si       
            self.lnPDF__spi      
        except:
            reset = True
            self.tabGrids__si = {}
            self.lnPDF__si    = {}
            self.lnPDF__spi   = {}

            
        # Get the PDF & its flag
        flag__g = self.flagProb__sg[iSource].copy()
        lnPDF__g = self.lnProb__sg[iSource].copy()
        tabGrid = self.grid.tabGrid 
        
        # Fix flag when only the prior has been calculated
        if (len(self.lnProb__psg.keys()) == 1) & ('None prior flat' in self.lnProb__psg.keys()):
            inds = np.random.randint(0, len(flag__g)+1, size=10)
            flag__g[inds] = True

            
        # Calculate the volume for each part of the original grid
        self.grid.tabGrid['ln_dV'] = np.zeros(self.grid.nGrid)
        for var in inputVars:
            try:
                self.grid.tabGrid['d%s' % var] = self.grid.inputVars__u[var][1] - self.grid.inputVars__u[var][0]
            except:
                self.grid.tabGrid['d%s' % var] = 0.
            if var in interpVars:
                self.grid.tabGrid['ln_dV'] += np.log(self.grid.tabGrid['d%s' % var])

                
        # Save individual PDF calculations (note: PDFs, not probabilities)
        if reset:
            self.lnPDF__spi[iSource] = {p: lnP[iSource] for p, lnP in self.lnProb__psg.items()}

            
        return tabGrid, flag__g, lnPDF__g
    # --------------------------------------------------------------------    
        
    # --------------------------------------------------------------------    
    def octree_grid(self, fit_function, fit_function_kwargs = None, iSource = 0,
                    inputVars = None, interpVars = None, prec = 1e-20, fracMax = 1e-4, 
                    mapScenarios = False, scenario = None, reset = False, debug = False):
        '''
        Interpolate grid on grid points of high probability (PDF * dV).
        This is usually done in 3D, so the name octree: branches out 2*2*2 = 8 new cells.
        '''

        if inputVars is None:
            inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']

        if interpVars is None:
            interpVars = ['OXYGEN', 'logN2O', 'logUin']

        if (reset) & (scenario is not None):
            '@@> Sorry, if you want to reset the octree do not pass a scenario.'
            sys.exit(1)

        if fit_function_kwargs is None:
            fit_function_kwargs = {}
        
        # Note: Index __g for the original grid, index __i for the reduced original grid + octree grid
        # Creating new grids & probability arrays
        orig_tabGrid, orig_flag__g, orig_lnPDF__g = self.octree_startup(iSource = iSource, inputVars = inputVars, interpVars = interpVars, reset = reset)
        tabGrid, flag__i, lnPDF__i = orig_tabGrid, orig_flag__g, orig_lnPDF__g

        if (not reset):
            try:
                tabGrid = self.tabGrids__si[iSource].copy()
                flag__i = np.ones(len(tabGrid), 'bool')
                lnPDF__i = self.lnPDF__si[iSource].copy()
            except:
                pass


        # Calculate the probability for each scenario ==>
        # Let the tree explore each scenario (rather slow & memory consuming!)
        def calcFracP__scenarios(lnProb, flag = None, tabGrid = None, scenario = None, mapScenarios = False):
            if flag is None:
                flag = np.ones_like(lnProb, 'bool')
                
            fracP = np.exp(lnProb)
                
            if (not mapScenarios) & (scenario is None):
                pTot = np.sum( np.exp(lnProb[flag]) )
                fracP /= pTot

            else:

                # Get flags for this grid
                if (tabGrid is None):
                    try:
                        flagScenarios = self.grid.scenarios
                    except:
                        flagScenarios = self.grid.flagScenarios()
                else:
                    flagScenarios = self.grid.flagScenarios(tabGrid = tabGrid, saveScen = False)

                # Calc fraction for each scenario 
                if mapScenarios:
                    for scen, fs in flagScenarios.items():
                        if (scen != 'all'):
                            pTot = np.sum( np.exp(lnProb[flag&fs]) )
                            if pTot > 1e-323:
                                fracP[fs] /= pTot

                # Calc fraction for one scenario
                if scenario is not None:
                    fs = flagScenarios[scenario]
                    pTot = np.sum( np.exp(lnProb[flag&fs]) )
                    aux = np.exp(lnProb[fs])
                    if pTot > 0:
                        fracP[fs] /= pTot
                    else:
                        fracP[fs] = 0.
                    fracP[~fs] = 0.
                    
            return fracP


        # Remove bins which contribute too little to the total probability; do this scenario by scenario
        fracP__i = calcFracP__scenarios(lnPDF__i, flag = flag__i, tabGrid = tabGrid, scenario = None, mapScenarios = True)
        tol = prec / self.grid.nGrid
        flag__i &= (fracP__i > tol)

        if debug:
            log.debug('@@> Original grid reduced to %s points.' % flag__i.sum())

        # Select the PDF & grid where the grid is OK
        lnPDF__i = lnPDF__i[flag__i]
        tabGrid  = tabGrid[flag__i]        

        # Save individual PDF calculations (note: PDFs, not probabilities)
        self.lnPDF__spi[iSource] = {p: lnP[flag__i] for p, lnP in self.lnPDF__spi[iSource].items()}

        # Calculate the probability of each dV and find where to subdivide
        lnProb__i = lnPDF__i + tabGrid['ln_dV']
        fracP__i = calcFracP__scenarios(lnProb__i, tabGrid = tabGrid, scenario = scenario, mapScenarios = mapScenarios)
        indTree__i = np.where(fracP__i > fracMax)[0]

                
        # Subdivide if this point contributes too much to the total probability
        while any(fracP__i > fracMax):

            if debug:
                log.debug('    Branching octree for %s grid points' % len(indTree__i))

            # Interpolate on the the highest probability hypercube
            dInterpVars = ['ln_dV'] + ['d%s' % k for k in interpVars] + ['branch_low', 'branch_upp']
            newTable = np.array(tabGrid[inputVars + dInterpVars][indTree__i].copy())
            
            for var in interpVars:
                c = newTable[var]
                d = newTable['d%s' % var]
                t1 = newTable.copy()
                t2 = newTable.copy()
                t1[var] -= 0.25 * d
                t2[var] += 0.25 * d
                newTable = np.hstack([t1, t2])

            newGrid, flagInGrid = self.grid.multipleValues_interp_regularGrid(inputParams = newTable[inputVars], outputVars = None)

            # Fix upper/lower metallicity branch labeling
            newGrid['branch_low'] = newTable['branch_low']
            newGrid['branch_upp'] = newTable['branch_upp']

            # Calculate new volumes
            for var in interpVars:
                newGrid['d%s' % var] = newTable['d%s' % var] / 2.
            nI = len(interpVars)
            newGrid['ln_dV'] = newTable['ln_dV'] - nI * np.log(2.)

            # Fit with new grid
            newGrid = newGrid[flagInGrid]
            G = grid(tabGrid = newGrid)
            f = fit_function(grid = G, sources = self.sources, obsFit__f = self.obsFit__f, parName__p = self.parName__p, **fit_function_kwargs)
            
            # Get new results
            lnPDF__n = f.lnProb__sg[iSource]
            flag__n = f.flagProb__sg[iSource]

            # Remove branch point from the grid
            mask__i = np.zeros(len(tabGrid), 'bool')
            mask__i[indTree__i] = True
            tabGrid = np.hstack([tabGrid[~mask__i], newGrid[flag__n]])
            
            # Recalc the PDF
            lnPDF__i = np.append(lnPDF__i[~mask__i], lnPDF__n[flag__n])

            # Calculate the probability of each dV
            lnProb__i = lnPDF__i + tabGrid['ln_dV']

            # Save individual PDFs
            for p, lnP in self.lnPDF__spi[iSource].items():
                self.lnPDF__spi[iSource][p] = np.append(lnP[~mask__i], f.lnProb__psg[p][iSource][flag__n])
            
            # Check the contribution to the probability of each grid point
            fracP__i = calcFracP__scenarios(lnProb__i, tabGrid = tabGrid, scenario = scenario, mapScenarios = mapScenarios)
            
            # Select grid points to be subdivided
            indTree__i = np.where(fracP__i > fracMax)[0]

            # Uncomment to stop the while loop on the first run
            #fracP__i = np.array([1e-30])

        if debug:
            log.debug('@@> Done for a total of %s grid points.' % (len(tabGrid)))


        # Save results
        self.tabGrids__si[iSource] = astropy.table.Table(tabGrid)
        self.lnPDF__si[iSource] = lnPDF__i
        
        # Save flagScenarios
        self.flagScenarios__s[iSource] = self.grid.flagScenarios(tabGrid = tabGrid, scenVars = {'age': self.bParCen__pb['age'], 'fr': self.bParCen__pb['fr']}, saveScen = False)
        self.flagScenarios__s[iSource]['low'] = tabGrid['branch_low']
        self.flagScenarios__s[iSource]['upp'] = tabGrid['branch_upp']
        
        if debug:
            # Check individual probabilities
            lnP__i = np.zeros_like(lnPDF__i)
            for p, lnP in self.lnPDF__spi[iSource].items():
                lnP__i += lnP
            lnP__i += tabGrid['ln_dV']
            diff = lnP__i - lnProb__i
            log.debug('Diff between summed prob and sum:', np.min(np.abs(diff)), np.mean(np.abs(diff)), np.max(np.abs(diff)))

            # Check grid
            plt.figure()
            plt.clf()
            plt.scatter(12+self.grid.tabGrid['OXYGEN'], self.grid.tabGrid['logN2O'], c ='r', marker=',', lw=0, alpha = 0.2, s=2)
            plt.scatter(12+tabGrid['OXYGEN'], tabGrid['logN2O'], c ='b', marker=',', lw=0, alpha = 0.2, s=2)
            if 'OXYGEN' in self.sources.tabData.keys():
                x, y = self.sources.tabData['OXYGEN'], self.sources.tabData['logN2O']
                if (x > -999) & (y > -999):
                    plt.scatter(12+x, y, c ='k', marker = '*', s=30, zorder=10)
    # --------------------------------------------------------------------
        
    # --------------------------------------------------------------------    
    def calcCrossValidation(self, fit_function = None, fit_function_kwargs = None, fracMax = 1e-4, 
                            scenNames = None, iSources = None, 
                            debug = False):

        # Get scenario names
        if scenNames is None:
            scenNames = np.sort(np.fromiter(self.grid.scenarios.keys(), dtype='U20'))
        
        # Calc Cross-validation for each scenario
        self.crossVal__sl = {}
        
        # Select sources
        if iSources is None:
            iSources = self.sourcesRowsFitted
            
        # Loop over sources
        for iS in iSources:
            
            tabGrid, flagScenarios = self.get_tabGrid_and_flags(iS)

            lnLikel__c = np.zeros(len(scenNames))
            
            for p, lnPDF in self.lnPDF__spi[iS].items():
                
                par   = p.split(' ')[0]
                ptype = p.split(' ')[1]

                if (par != 'None') & (ptype != 'step'):
                    
                    # Sum up the PDFs of all leave-one-out data points
                    lnPC = np.zeros_like(lnPDF)
                    for _p, _lnPDF in self.lnPDF__spi[iS].items():
                        _par   = _p.split(' ')[0]
                        _ptype = _p.split(' ')[1]
                        if (_p != p) & (_par != 'None') & (_ptype != 'step'):
                            lnPC += _lnPDF
                            if debug:
                                log.debug(p, _p)
                            
                    # Predict left out point (via maximum likelihood, since we cannot marginilize the likelihood)
                    # and calculate the predictive power of a given scenario
                    for iC, scen in enumerate(scenNames):
                        fs = flagScenarios[scen]                       
                        if (fs.sum() > 0):
                            iPC = np.argmax(lnPC[fs])
                            predPar = tabGrid[par][fs][iPC]
                            obs__s = self.sources.tabData[par][iS]
                            err__s = self.sources.tabData['e'+par][iS]
                            wei__s = utils.safe_div(1., err__s, 0.)
                            lnLikel__c[iC] += -0.5 * ((obs__s - predPar) * wei__s)**2
                        else:
                            lnLikel__c[iC] = -999
                        
            # Sort scenarios by their predictive power
            indMod = np.argsort(lnLikel__c)[::-1]
            crossResults = astropy.table.Table([scenNames[indMod], lnLikel__c[indMod]], names=('scenario', 'lnLikelihood'))
            self.crossVal__sl[iS] = np.array(crossResults)


        # Calculate info for best scenario
        bestScen = [scen for scen in self.crossVal__sl[iS]['scenario'] if scen != 'all'][0]

        # Get a better sampling around the best scenario
        if fit_function is not None:
            self.octree_grid(fit_function, fit_function_kwargs = fit_function_kwargs, scenario = bestScen, fracMax = fracMax, debug = debug)
                
        self.flagScenarios__s[iS]['bestScen'] = self.flagScenarios__s[iS][bestScen]
        

        if debug:
            log.debug(self.crossVal__sl)
    # --------------------------------------------------------------------    

    # --------------------------------------------------------------------    
    def calcBinnedPDFs(self, iSources = None, gridParts = None, parsJointPDF = ['OXYGEN', 'logN2O'], debug = False):
        '''
        Given (previously calculated) lnPDF, calculate marginalized PDFs & CDFs 
        for each quantity in parName__p, bin-value & source ==> PDF__psb & CDF__psb.
        LeCid@Falguiere - 07/Juillet/2013

        Calculate PDFs for each source separately.
        Natalia@Meudon - 29/Aug/2013
        
        Calculate joint PDFs for N/O and O/H.
        Natalia@Meudon - 29/Aug/2013
        '''
        
        # Select sources
        if iSources is None:
            iSources = self.sourcesRowsFitted
            
        # Reset stats dictionaries
        self.parsJointPDF = parsJointPDF
        try:
            self.bins__j
            self.PDF__cpsb
            self.CDF__cpsb
            self.jPDF__csj
            self.jPDF_Rs__csj
            self.jPDF_Xgrid
            self.jPDF_bins
        except:
            self.bins__j      = {}
            self.PDF__cpsb    = {}
            self.CDF__cpsb    = {}
            self.jPDF__csj    = {}
            self.jPDF_Rs__csj = {}
            self.jPDF_Xgrid   = {}
            self.jPDF_bins    = {}

        if gridParts is None:
            try:
                for iSource in iSources:
                    self.flagScenarios__s[iSource]['bestScen']
                gridParts = ['all', 'bestScen']
            except:
                gridParts = self.gridParts
                # too slow: gridParts = self.grid.scenarios.keys()

        # Loop over grid parts / scenarios
        for gPart in gridParts:
            
            # Reset PDF/CDF arrays            
            PDF__psb = {}
            CDF__psb = {}
            
            # For each par (P), calc PDF for each par-bin, adding up probabilities from each model in the grid within each par-bin
            for P in self.parName__p:
                
                # Reset PDFs
                PDF__psb[P] = np.zeros( (self.sources.nSources, self.nbPar__p[P]) )

                # Calc PDFs
                for iS in iSources:
                    PDF__psb[P][iS] = self.calcMargPDF(P, branch = gPart, iSource = iS)

                # Calculate CDF
                CDF__psb[P] = np.cumsum(PDF__psb[P], axis = 1)
                flagNorm = (CDF__psb[P][..., -1] > 0)                
                CDF__psb[P][flagNorm] /= CDF__psb[P][..., -1:][flagNorm]

            # Save results for each branch
            self.PDF__cpsb[gPart] = PDF__psb
            self.CDF__cpsb[gPart] = CDF__psb

            
            # Calc & save joint PDF
            self.jPDF__csj[gPart] = None

            # Loop over sources
            for iS in iSources:
                # Calc
                Xgrid, PDF__xy, bins, Rs__ro = self.calcJointPDF(self.parsJointPDF, branch = gPart, iSource = iS)
            
                # Reset
                if self.jPDF__csj[gPart] is None:
                    self.jPDF__csj[gPart] = np.empty((self.sources.nSources, Xgrid[0].shape[0]-1, Xgrid[0].shape[1]-1))
                    self.jPDF_Rs__csj[gPart] = {}

                # Save
                self.jPDF_Xgrid  = Xgrid
                self.jPDF_bins   = bins
                self.jPDF__csj[gPart][iS] = PDF__xy
                self.jPDF_Rs__csj[gPart][iS] = Rs__ro
    # --------------------------------------------------------------------
        
    # --------------------------------------------------------------------    
    def calcPDFSummaries(self, gridParts = None, *args, **kwargs):
        '''
        Compute ave, sig & med for each variable & source ==> ave__vs, sig__vs & med__vs
        OBS: Only variables in PDFpar__p!
        LeCid@Falguiere - 04/Juillet/2013

        Added median & percentiles eons ago.
        Natalia@Meudon - 24/Feb/2015
        '''

        # Reset stats dictionaries
        # parBest = Peak for the full posterior
        # marg... = Summaries the marginalized posterior
        # join... = Summaries the joint PDF (on O/H vs N/O)
        # P = percentiles/quantiles
        # H = highest probabilty region (extremes)
        # C = credible region (ellipse)

        # Maximum global solution
        self.parBest__cps = {}
        
        # Marginalized PDFs summaries
        self.margAve__cps = {}
        self.margSig__cps = {}
        
        self.margMed__cps = {}
        self.margP50__cps = {}
        self.margP05__cps = {}
        self.margP68__cps = {}
        self.margP95__cps = {}
        
        self.margMod__cps = {}
        self.margH05__cps = {}
        self.margH50__cps = {}
        self.margH68__cps = {}
        self.margH95__cps = {}

        # Joint PDFs summaries
        self.joinAve__cps = {}
        self.joinSig__cps = {}
        
        self.joinMod__cps = {}
        self.joinH05__cps = {}
        self.joinH50__cps = {}
        self.joinH68__cps = {}
        self.joinH95__cps = {}
        
        self.joinC05__cps = {}
        self.joinC50__cps = {}
        self.joinC68__cps = {}
        self.joinC95__cps = {}
        
                    
        # Calc stats for each subgrid
        if gridParts is None:
            if 'bestScen' in self.PDF__cpsb.keys():
                gridParts = ['all', 'bestScen']
            else:
                gridParts = self.gridParts


        # Calc Maximum global solution
        for gPart in gridParts:
            self.calcPDFSummaries_Max(gPart, self.parName__p, *args, **kwargs)

        # Calc Marginalized PDFs summaries
        for gPart in gridParts:
            self.calcPDFSummaries_Marg(gPart, self.parName__p, *args, **kwargs)            
            
        # Calc Joint PDFs summaries
        for gPart in gridParts:
            self.calcPDFSummaries_Joint(gPart, self.parName__p, *args, **kwargs)
            
        # Save solution summaries
        self.PDFSummaries__tcps = { 
            'best': self.parBest__cps ,

            'mave': self.margAve__cps ,
            'msig': self.margSig__cps ,

            'mmed': self.margMed__cps ,
            'mp50': self.margP50__cps ,
            'mp05': self.margP05__cps ,
            'mp68': self.margP68__cps ,
            'mp95': self.margP95__cps ,

            'mmod': self.margMod__cps ,
            'mh05': self.margH05__cps ,
            'mh50': self.margH50__cps ,
            'mh68': self.margH68__cps ,
            'mh95': self.margH95__cps ,

            'jave': self.joinAve__cps ,
            'jsig': self.joinSig__cps ,
            
            'jmod': self.joinMod__cps ,
            'jh05': self.joinH05__cps ,
            'jh50': self.joinH50__cps ,
            'jh68': self.joinH68__cps ,
            'jh95': self.joinH95__cps ,

            'jc05': self.joinC05__cps ,
            'jc50': self.joinC50__cps ,
            'jc68': self.joinC68__cps ,
            'jc95': self.joinC95__cps ,            
            }
    # --------------------------------------------------------------------    
            
    # --------------------------------------------------------------------    
    def calcPDFSummaries_Max(self, gPart, pars, iSources = None, debug = False):
        
        # Calc Maximum global solution
        parBest__ps = {P: np.zeros((self.sources.nSources, 1)) for P in pars}

        # Select sources
        if iSources is None:
            iSources = self.sourcesRowsFitted
            
        # Loop over sources
        for iS in iSources:
                
            lnPDF, flagProb = self.get_lnPDF_and_flag(iS)
            tabGrid, flagScenarios = self.get_tabGrid_and_flags(iS)

            flagScen = flagScenarios[gPart]
            
            # Trick to discard unwanted grid branches: np.log(0) = -inf
            indBest = np.argmax(lnPDF + np.log(flagProb) + np.log(flagScen))
            for P in pars:
                parBest__ps[P][iS] = tabGrid[P][indBest]
                

            if debug:
                bestProb = lnPDF[indBest]
                log.debug(gPart, iS, bestProb)
                log.debug(astropy.table.Table(parBest__ps))
                log.debug(self.sources.tabData[iS:iS+1])

        # Save
        self.parBest__cps[gPart] = parBest__ps
    # --------------------------------------------------------------------    
            
    # --------------------------------------------------------------------    
    def calcPDFSummaries_Marg(self, gPart, pars, iSources = None, debug = False):

        # Calc Marginalized PDFs summaries
        margAve__ps = {}
        margSig__ps = {}
        
        margMed__ps = {P: np.zeros((self.sources.nSources, 1)) for P in pars}
        margP05__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margP50__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margP68__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margP95__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        
        margMod__ps = {P: np.zeros((self.sources.nSources, 1)) for P in pars}
        margH05__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margH50__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margH68__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        margH95__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}

        # Select sources
        if iSources is None:
            iSources = self.sourcesRowsFitted

        # Loop over parameters
        for P in pars:

            PDF__sb     = self.PDF__cpsb[gPart][P]
            CDF__sb     = self.CDF__cpsb[gPart][P]
            bParCen__b  = self.bParCen__pb[P]
            dbPar__b    = self.dbPar__pb[P]
            
            # ATT: ave & sig are computed in the bins-grid!! Ok as long as bins are small!
            margAve__ps[P] = np.sum( PDF__sb * bParCen__b * dbPar__b, axis = 1)
            margAve__sb    = margAve__ps[P][..., np.newaxis]
            variance__s    = np.sum( PDF__sb * (bParCen__b - margAve__sb)**2 * dbPar__b, axis = 1)
            margSig__ps[P] = np.sqrt(variance__s)

            
            # Calc Mode 
            indPeakMargPDF__s = np.argmax(PDF__sb, axis = 1)
            margMod__ps[P] = bParCen__b[indPeakMargPDF__s]
            
            # Calc quantiles / HPD
            bins_resam = self.getBinsResamGrid(P)
            
            for iS in iSources:
                
                # Calc Median & percentiles (quantiles) 
                CDF__b = np.append(0.0, CDF__sb[iS])
                margMed__ps[P][iS] = np.interp( 0.50, CDF__b, bins_resam )
                margP05__ps[P][iS] = np.interp( [0.475, 0.525], CDF__b, bins_resam )
                margP50__ps[P][iS] = np.interp( [0.25 , 0.75 ], CDF__b, bins_resam )
                margP68__ps[P][iS] = np.interp( [0.16 , 0.84 ], CDF__b, bins_resam )
                margP95__ps[P][iS] = np.interp( [0.025, 0.975], CDF__b, bins_resam )

                # Calc Highest posterior density regions
                PDF__b = PDF__sb[iS]
                h05, h50, h68, h95 = self.get_CredibilityRegion([0.05, 0.50, 0.68, 0.95], PDF__b, bins_resam)
                margH05__ps[P][iS] = h05
                margH50__ps[P][iS] = h50
                margH68__ps[P][iS] = h68
                margH95__ps[P][iS] = h95

                
            if debug:
                log.debug(P, margAve__ps[P], margMed__ps[P], margMod__ps[P])
                if P in self.sources.tabData.keys(): log.debug(np.array(self.sources.tabData[P]))

                if P in self.parsJointPDF:
                    plt.figure()
                    plt.clf()
                    plt.title(gPart)
                    plt.xlabel(P)
                    
                    plt.plot(bParCen__b, PDF__sb[iS]/PDF__sb[iS].max(), 'k:')
                    plt.plot(bins_resam, CDF__b, 'k')
                    
                    if (P in self.sources.tabData.keys()):
                        if (self.sources.tabData[P] > -999):
                            plt.plot(self.sources.tabData[P][iS], [0.02], 'b*', ms=5)
                            
                    plt.plot(self.parBest__cps[gPart][P][iS], [0.05], 'bo', ms=5)
                    
                    plt.plot(margP95__ps[P][iS], [0.10, 0.10], 'g', lw=1, alpha=0.9)
                    plt.plot(margP68__ps[P][iS], [0.10, 0.10], 'g', lw=2, alpha=0.9)
                    plt.plot(margP50__ps[P][iS], [0.10, 0.10], 'g', lw=4, alpha=0.9)
                    plt.plot(margP05__ps[P][iS], [0.10, 0.10], 'g', lw=8, alpha=0.9)
                    plt.plot(margMed__ps[P][iS], [0.10], 'ko', ms=5)

                    sigpm = np.array([margSig__ps[P][iS], - margSig__ps[P][iS]])
                    plt.plot(margAve__ps[P][iS] + 2*sigpm, [0.15, 0.15], 'b', lw=1, alpha=0.9)
                    plt.plot(margAve__ps[P][iS] + 1*sigpm, [0.15, 0.15], 'b', lw=2, alpha=0.9)
                    plt.plot(margAve__ps[P][iS] + 0.68*sigpm, [0.15, 0.15], 'b', lw=4, alpha=0.9)
                    plt.plot(margAve__ps[P][iS], [0.15], 'ko', ms=5)
                    
                    plt.plot(margH95__ps[P][iS], [0.20, 0.20], 'r', lw=1, alpha=0.9)
                    plt.plot(margH68__ps[P][iS], [0.20, 0.20], 'r', lw=2, alpha=0.9)
                    plt.plot(margH50__ps[P][iS], [0.20, 0.20], 'r', lw=4, alpha=0.9)
                    plt.plot(margH05__ps[P][iS], [0.20, 0.20], 'r', lw=8, alpha=0.9)
                    plt.plot(margMod__ps[P][iS], [0.20], 'ko', ms=5)
                    
        # Save
        self.margAve__cps[gPart] = margAve__ps
        self.margSig__cps[gPart] = margSig__ps
                                 
        self.margMed__cps[gPart] = margMed__ps
        self.margP05__cps[gPart] = margP05__ps
        self.margP50__cps[gPart] = margP50__ps
        self.margP68__cps[gPart] = margP68__ps
        self.margP95__cps[gPart] = margP95__ps
                                 
        self.margMod__cps[gPart] = margMod__ps
        self.margH05__cps[gPart] = margH05__ps
        self.margH50__cps[gPart] = margH50__ps
        self.margH68__cps[gPart] = margH68__ps
        self.margH95__cps[gPart] = margH95__ps
    # --------------------------------------------------------------------    
    
    # --------------------------------------------------------------------    
    def calcPDFSummaries_Joint(self, gPart, pars, iSources = None, debug = False):
        
        # Calc Joint PDFs summaries
        joinAve__ps = {P: np.zeros((self.sources.nSources, 1)) for P in pars}
        joinSig__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        
        joinMod__ps = {P: np.zeros((self.sources.nSources, 1)) for P in pars}
        joinH05__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        joinH50__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        joinH68__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}
        joinH95__ps = {P: np.zeros((self.sources.nSources, 2)) for P in pars}

        joinC05__ps = {P: np.zeros((self.sources.nSources, 4)) for P in pars}
        joinC50__ps = {P: np.zeros((self.sources.nSources, 4)) for P in pars}
        joinC68__ps = {P: np.zeros((self.sources.nSources, 4)) for P in pars}
        joinC95__ps = {P: np.zeros((self.sources.nSources, 4)) for P in pars}
        

        # Select sources
        if iSources is None:
            iSources = self.sourcesRowsFitted

        # Get info for joint PDF
        Xgrid  = self.jPDF_Xgrid
        bins   = self.jPDF_bins
            
        bx_low, bx_upp, bx_cen, bx_size, Nx = utils.getBinsLims(bins[0])
        by_low, by_upp, by_cen, by_size, Ny = utils.getBinsLims(bins[1])
        db__xy = bx_size[..., np.newaxis] * by_size[np.newaxis, ...]

        
        # Loop over sources
        for iS in iSources:
            
            lnPDF, flagProb = self.get_lnPDF_and_flag(iS)
            tabGrid, flagScenarios = self.get_tabGrid_and_flags(iS)
            
            PDF = np.exp(lnPDF)
            prob = np.exp(lnPDF + tabGrid['ln_dV'])
            PDF /= prob.sum()
            
            PDF__xy = self.jPDF__csj[gPart][iS]
            Rs__ro = self.jPDF_Rs__csj[gPart][iS]

            # Find peak of the PDF
            indJMod = np.unravel_index(PDF__xy.argmax(), PDF__xy.shape)

            # Save results to calc the covariance matrix
            Xs__xy = [0] * len(self.parsJointPDF)
            ave = [0] * len(self.parsJointPDF)
            sig = [0] * len(self.parsJointPDF)

            # Save map of P__xy
            X__pxy = {}
            
            # Loop over all parameters
            for P in pars:
                
                # ** Find the `marginalized' parameters the joint PDF
                X__xy = self.calcParameterJointPDF(P, iS, PDF, Rs__ro)
                X__pxy[P] = X__xy
                
                # ** Get averages of parameters
                aveX, sigX = self.get_AverageDispersion(PDF__xy, X__xy, db__xy = db__xy)
                joinAve__ps[P][iS] = aveX[0]
                joinSig__ps[P][iS][0] = sigX[0]
                
                # ** Get parameters on the point of higher probabilty
                joinMod__ps[P][iS] = X__xy[indJMod]       
                h05, h50, h68, h95 = self.get_CredibilityRegion([0.05, 0.50, 0.68, 0.95], PDF__xy, X__xy)
                joinH05__ps[P][iS] = h05
                joinH50__ps[P][iS] = h50
                joinH68__ps[P][iS] = h68
                joinH95__ps[P][iS] = h95


                # Save results to calc the covariance matrix
                if P in self.parsJointPDF:
                    i = self.parsJointPDF.index(P)
                    Xs__xy[i] = X__xy
                    ave[i] = aveX[0]
                    sig[i] = sigX[0]

                    
            # Calc stuff for the joint covariance matrix
            aux = self.get_AverageCovariance(PDF__xy, Xs__xy, ave, sig)
            rho_xy = aux[-1]
            
            Ps = self.parsJointPDF
            joinSig__ps[Ps[0]][iS][1] = rho_xy
            joinSig__ps[Ps[1]][iS][1] = rho_xy

            # Calc stuff for the joint covariance matrix scaled for the credibility regions
            masks   = self.get_CredibilityRegion([0.05, 0.50, 0.68, 0.95], PDF__xy, returnMasks = True)
            aveSigs = [self.get_AverageCovariance(PDF__xy, Xs__xy, mask__xy = mask) for mask in masks]
            
            # Scale ellipse so it covers the same area as the credibility region
            scales  = [np.sqrt((len(mask[0]) * db__xy[0,0]) / (np.pi * aux[2] * aux[3])) for mask, aux in zip(masks, aveSigs)]
            
            # Save results both to O/H and N/O
            for i, P in enumerate(Ps):
                
                aux = [aveSig[i:-1:2] for aveSig in aveSigs]
                joinC05__ps[P][iS][:2], joinC50__ps[P][iS][:2], joinC68__ps[P][iS][:2], joinC95__ps[P][iS][:2] = aux
                
                aux = [aveSig[-1] for aveSig in aveSigs]                
                joinC05__ps[P][iS][2], joinC50__ps[P][iS][2], joinC68__ps[P][iS][2], joinC95__ps[P][iS][2] = aux
                joinC05__ps[P][iS][-1], joinC50__ps[P][iS][-1], joinC68__ps[P][iS][-1], joinC95__ps[P][iS][-1] = scales


            # Get averages and variances of other parameters in the credibility region
            for P in pars:
                if P not in self.parsJointPDF:

                    X__xy = X__pxy[P]
                    aveX, sigX = self.get_AverageDispersion(PDF__xy, X__xy, masks__xy = masks)

                    joinC05__ps[P][iS][0], joinC50__ps[P][iS][0], joinC68__ps[P][iS][0], joinC95__ps[P][iS][0] = aveX
                    joinC05__ps[P][iS][1], joinC50__ps[P][iS][1], joinC68__ps[P][iS][1], joinC95__ps[P][iS][1] = sigX
                    
                    if debug:
                        if P in ['F5007', 'F3727', 'F6584']:
                            log.debug(gPart, P, aveX, sigX)
                            if P in self.sources.tabData.keys(): log.debug(np.array(self.sources.tabData[P]))


                        
            # Some useful plots when debugging                        
            if debug:
                for P in self.parsJointPDF:
                    log.debug(gPart, P, joinAve__ps[P], joinMod__ps[P], joinH05__ps[P], joinH95__ps[P])
                    if P in self.sources.tabData.keys(): log.debug(np.array(self.sources.tabData[P]))

                import plotHelpers as pl

                plt.figure()
                plt.clf()
                plt.title(gPart)
                plt.pcolormesh(12 + Xgrid[0], Xgrid[1], PDF__xy, cmap = plt.get_cmap('OrRd'))
                plt.colorbar()

                plt.plot(12+joinMod__ps[Ps[0]], joinMod__ps[Ps[1]], 'g.')
                plt.plot(12+joinAve__ps[Ps[0]], joinAve__ps[Ps[1]], 'b.')
                
                ave, cov__xy = self.get_AverageCovariance(PDF__xy, Xs__xy, ave, sig, returnCovMat = True)
                pl.plot_ellipse_covMatrix(ave, cov__xy, dx = 12, edgecolor='b')

                for j in [joinC05__ps, joinC50__ps, joinC68__ps, joinC95__ps]:
                    ave = [0] * len(self.parsJointPDF)
                    sig = [0] * len(self.parsJointPDF)
                    for i, P in enumerate(self.parsJointPDF):
                        ave[i] = j[P][iS][0]
                        sig[i], rho_xy, scale = j[P][iS][1:]
                    cov__xy = utils.calcCovMatrix(sig[0], sig[1], rho_xy)
                    if not np.any(np.isnan(cov__xy)):
                        plt.plot(12+ave[0], ave[1], 'k.')
                        pl.plot_ellipse_covMatrix(ave, cov__xy, dx = 12, scale = scale, alpha = 0.2, facecolor='g')

                pltSrc = True
                for P in self.parsJointPDF:
                    pltSrc &= (P in self.sources.tabData.keys())
                    try:
                        pltSrc &= (self.sources.tabData[P] > -999)
                    except:
                        pltSrc = False
                    
                if pltSrc:
                    plt.plot(12+self.sources.tabData[Ps[0]], self.sources.tabData[Ps[1]], 'r*')

                    
        # Save
        self.joinAve__cps[gPart] = joinAve__ps
        self.joinSig__cps[gPart] = joinSig__ps
                                 
        self.joinMod__cps[gPart] = joinMod__ps
        self.joinH05__cps[gPart] = joinH05__ps
        self.joinH50__cps[gPart] = joinH50__ps
        self.joinH68__cps[gPart] = joinH68__ps
        self.joinH95__cps[gPart] = joinH95__ps
        
        self.joinC05__cps[gPart] = joinC05__ps
        self.joinC50__cps[gPart] = joinC50__ps
        self.joinC68__cps[gPart] = joinC68__ps
        self.joinC95__cps[gPart] = joinC95__ps
    # --------------------------------------------------------------------    
    
    # --------------------------------------------------------------------    
    def getCovMatrix(self, iSource, gPart = 'all'):

        Ps = self.parsJointPDF
        
        sig_x = self.joinSig__cps[gPart][Ps[0]][iSource][0]
        sig_y = self.joinSig__cps[gPart][Ps[1]][iSource][0]
        rho_xy = self.joinSig__cps[gPart][Ps[0]][iSource][1]
        
        cov__xy = utils.calcCovMatrix(sig_x, sig_y, rho_xy)
        return cov__xy
    # --------------------------------------------------------------------    
    
    # --------------------------------------------------------------------
    def get_AverageDispersion(self, PDF__xy, X__xy, db__xy = None, masks__xy = None):

        # Start masks
        if masks__xy is None:
            masks__xy = [np.ones_like(PDF__xy, 'bool')]

        if db__xy is None:
            calcNorm = True
            db__xy = 1.
        else:
            calcNorm = False

        # This only need to be calculated once
        Xm = (PDF__xy * X__xy * db__xy)

        # Calc average and variance for each mask
        ave = [0] * len(masks__xy)
        var = [0] * len(masks__xy)

        for i, mask__xy in enumerate(masks__xy):

            # Normalization
            if calcNorm:
                norm = (PDF__xy)[mask__xy].sum()
            
            ave[i] = Xm[mask__xy].sum()
            if calcNorm:
                ave[i] /= norm
                    
            Vm = (PDF__xy * (X__xy - ave[i])**2 * db__xy)
            var[i] = Vm[mask__xy].sum()
            if calcNorm:
                var[i] /= norm
 
        return np.array(ave), np.sqrt(np.array(var))
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def get_AverageCovariance(self, PDF__xy, Xs__xy, ave = None, sig = None, 
                              mask__xy = None, returnCovMat = False):

        if mask__xy is None:
            mask__xy = np.ones_like(PDF__xy, 'bool')

        norm = (PDF__xy)[mask__xy].sum()
            
        if ave is None:
            ave = [0] * len(Xs__xy)
            for i, X__xy in enumerate(Xs__xy):
                ave[i] = (PDF__xy * X__xy)[mask__xy].sum()
                ave[i] /= norm
            
        if sig is None:
            sig = [0] * len(Xs__xy) 
            for i, X__xy in enumerate(Xs__xy):
                sig[i] = (PDF__xy * (X__xy - ave[i])**2)[mask__xy].sum()
                sig[i] /= norm
                sig[i] = np.sqrt(sig[i])

        ave_x, ave_y = ave[0], ave[1]
        sig_x, sig_y = sig[0], sig[1]
        
        var_xy = (PDF__xy * (Xs__xy[0] - ave[0]) * (Xs__xy[1] - ave[1]))[mask__xy].sum()
        var_xy /= norm
        rho_xy = var_xy / (sig[0] * sig[1])
        
        if not returnCovMat:
            return ave_x, ave_y, sig_x, sig_y, rho_xy
        else:
            cov__xy = utils.calcCovMatrix(sig_x, sig_y, rho_xy)
            return ave, cov__xy 
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def get_CredibilityRegion(self, alphas, PDF__b, bParCen__b = None, returnMasks = False, debug = False):
        '''
        Get credibility region of highest posterior density for 
        a probability >= alpha. 
        '''

        indSort = PDF__b.argsort(axis = None)
        pSum = np.cumsum(PDF__b.flat[indSort]) / PDF__b.sum()
        pSum = np.append(0.0, pSum)
        bs = [0] * len(alphas)
        masks = [0] * len(alphas)

        for i, alpha in enumerate(alphas):            
            indCR = np.where(pSum <= (1.-alpha))[0][-1]

            if bParCen__b is not None:
                bCR = bParCen__b.flat[indSort[indCR:]]
                bs[i] = [bCR.min(), bCR.max()]
                
            if returnMasks:
                indMask = np.unravel_index(indSort[indCR:], PDF__b.shape)
                masks[i] = indMask

                if debug:
                    flagPDF = utils.ind2flag(PDF__b, indMask)
                    plt.figure(2)
                    plt.clf()
                    plt.pcolormesh(12 + self.jPDF_Xgrid[0], self.jPDF_Xgrid[1], flagPDF, alpha = 0.3)
                
        if not returnMasks:
            return bs
        else:
            return masks  
    # --------------------------------------------------------------------
    
    # --------------------------------------------------------------------
    def gatherData(self, saveCompletePDF = False, saveSources = False, saveGrid = False, saveOctree = True, tabGridSummary = True):
        '''
        Returns all the important and detailed data from the fitting into a 
        single python dictionary.
        '''

        try:
            if tabGridSummary:
                inputVars = self.grid.inputVars__u.keys()
                inputVars.extend(['d%s' % k for k in self.grid.inputVars__u.keys()])
                inputVars.append('ln_dV')
                tabGrids__si = {s: np.array(t[inputVars]) for s, t in self.tabGrids__si.items()}    #{v: np.array(self.tabGrids__si[]) for v in inputVars}
            else:
                tabGrids__si = {s: np.array(t) for s, t in self.tabGrids__si.items()}
        except:
            saveOctree = False
        
        data = { 
                 'obsFit__f'               : self.obsFit__f               , # Fitting options & parameters
                 'errCookingFactor'        : self.errCookingFactor        ,
                 'fakeErr'                 : self.fakeErr                 ,
                 'parName__p'              : self.parName__p              ,
                 'parType__p'              : self.parType__p              ,
                 'nPar'                    : self.nPar                    ,
                 'sources.nSources'        : self.sources.nSources        , # Sources
                 'sourcesRowsFitted'       : self.sourcesRowsFitted       ,
                 'grid.nGrid'              : self.grid.nGrid              , # Grid
                 'gridParts'               : self.gridParts               ,
                 'nBins'                   : self.nBins                   , # PDF bins 
                 'nbPar__p'                : self.nbPar__p                ,
                 'bParCen__pb'             : self.bParCen__pb             ,
                 'bParLow__pb'             : self.bParLow__pb             ,
                 'bParUpp__pb'             : self.bParUpp__pb             ,
                 'dbPar__pb'               : self.dbPar__pb               ,
                 'PDFSummaries__tcps'      : self.PDFSummaries__tcps      , # PDF summaries
                 'parsJointPDF'            : self.parsJointPDF            , # Joint pdf
                 'jPDF_Xgrid'              : self.jPDF_Xgrid              ,
                 'jPDF_bins'               : self.jPDF_bins               ,
                 'likelihoodMode'          : self.likelihoodMode          ,
                 'gridCDF__cpb'            : self.gridCDF__cpb            , # Grid priors
                 'gridPDF__cpb'            : self.gridPDF__cpb            ,
               }

            
        if saveCompletePDF:
            completePDF = { 
                 'PDF__cpsb'               : self.PDF__cpsb               , # Marginilized PDFs
                 'CDF__cpsb'               : self.CDF__cpsb               ,
                 'jPDF__csj'               : self.jPDF__csj               , # Joint pdf
               }

        if saveOctree:
            octree = {
                 'tabGrids__si'            : tabGrids__si                 , # Octree
                 'lnPDF__si'               : self.lnPDF__si               , # PDFs
                 'lnPDF__spi'              : self.lnPDF__spi              ,
                 'crossVal__sl'            : self.crossVal__sl            , # Cross validation
               }

        if saveSources:
            sources = { 
                 'sources.tabDataName'     : self.sources.tabDataName     , # Sources
                 'sources.tableIsInLog'    : self.sources.tableIsInLog    ,
                 'sources.useLog'          : self.sources.useLog          ,
                 'sources.tabData'         : np.array(self.sources.tabData) ,
               }

        if saveGrid:
            grid = {
                 'grid.tabGridName'        : self.grid.tabGridName        , # Grid
                 'grid.tableIsInLog'       : self.grid.tableIsInLog       ,
                 'grid.useLog'             : self.grid.useLog             ,
                 'grid.tabGrid'            : np.array(self.grid.tabGrid)  ,
                 'grid.branches'           : self.grid.branches           ,
               }
                    
        if saveCompletePDF:
            data.update(completePDF)
            
        if saveOctree:
            data.update(octree)
            
        if saveSources:
            data.update(sources)

        if saveGrid:
            data.update(grid)
            
        options = {
            'saveCompletePDF' : saveCompletePDF ,
            'saveOctree'      : saveOctree      ,
            'saveSources'     : saveSources     ,
            'saveGrid'        : saveGrid        ,
        } 

        data.update(options)
        
        self.data = data
        return data
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def loadData(self, fileName, debug = False):
        self.sourcesRowsFitted = np.asarray(astropy.table.Table.read(fileName, path='bond/sourcesRowsFitted')['sources'])

        # Get results
        data = astropy.table.Table.read(fileName, path='bond/fit')
        branches          = astropy.table.Table.read(fileName, path='bond/branches'    )['branch'      ]
        sumTypes          = astropy.table.Table.read(fileName, path='bond/sumTypes'    )['sumType'     ]
        outputVars        = astropy.table.Table.read(fileName, path='bond/outputVars'  )['outputVar'   ]
        self.parName__p   = astropy.table.Table.read(fileName, path='bond/parName'     )['parName'     ]
        self.parsJointPDF = astropy.table.Table.read(fileName, path='bond/parsJointPDF')['parsJointPDF']

        log.info('@@> Creating empty PDF arrays.')
        self.PDFSummaries__tcps = {}
        for sumType in sumTypes:
            self.PDFSummaries__tcps[sumType] = {}
            for branch in branches:
                self.PDFSummaries__tcps[sumType][branch] = {}
                
        for branch in branches:
            for var in outputVars:
                for sumType in sumTypes:
                    # Get result for this summary type (average, sigma, median, ...)
                    colName = '%s_%s_%s' % (branch, var, sumType)
                    if colName in data.keys():
                        self.PDFSummaries__tcps[sumType][branch][var] = np.array(data[colName])
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def combineSources_singleFile(self, iSources = None, iSources_done = None,
                                  reAddSources = False,
                                  gridParts = None,
                                  baseName = None, dirName = None, nChars = 4,
                                  fileName = None, overwrite = False,
                                  saveCompletePDF = False,
                                  saveOctree = False, saveCrossVal = True,
                                  ignoreMissing = True, debug = True,
                                  branches = ['all'],
                                  sumTypes = ['jmod', 'jc68', 'mmed', 'mp68'], 
                                  utputVars = ['OXYGEN', 'logN2O', 'logUin', 'age', 'fr',
                                               'F3727', 'F5007', 'F6584', 'O23', 'O3O2', 'N2O2',
                                               'F5876', 'Ar3Ne3',
                                               'F4363',  'F5755', 'F6312', 'F7325', 'F7135',
                                               'limF4363',  'limF5755', 'limF6312', 'limF7325', 'limF7135'
                                               ],
                                  ):
        '''
        Saves fits for all sources into a single file.
        '''

        # Add file and dir names
        self.addDirBaseNames(baseName, dirName)               

        # Sources to look for
        if iSources is None:
            iSources = list(self.sourcesRowsFitted)
        if len(iSources) == 0:
            iSources = range(self.sources.nSources)

        # Start output table
        tab = astropy.table.Table()

        # Save source info
        tab['name'] = self.sources.tabData['name']

        # Create empty PDF arrays
        try:
            self.PDF__cpsb
            self.PDFSummaries__tcps
        except:
            log.info('@@> Creating empty PDF arrays.')
            aux = self.sourcesRows
            self.flatPrior()
            self.octree_grid(None, iSource = aux[0], fracMax = 1., debug = False)
            self.calcBinnedPDFs(iSources = aux[:1], gridParts = gridParts)
            self.calcPDFSummaries(iSources = aux[:1], gridParts = gridParts)

                    
        # Save empty output file
        if fileName is None:
            fileName = os.path.join(self.dirName, '%s.hdf5' % self.baseName)
        if (not os.path.exists(fileName)) | (overwrite):
            log.info('@@> Saving empty output file.')
            self.sourcesRowsFitted = []
            self.saveToHDF5(fileName = fileName, saveOctree = saveCrossVal&saveOctree, overwrite = overwrite)
            
        # Open output file
        outFile = h5py.File(fileName, mode='a')

        # Check sources already added to file
        if iSources_done is not None:
            iSources_done = list(iSources_done)
        else:
            if (os.path.exists(fileName)) & (not reAddSources):
                if 'sourcesRowsFitted' in outFile['bond'].keys():
                    iSources_done = list(outFile['bond/sourcesRowsFitted'][:])
                else:
                    iSources_done = []
            else:
                iSources_done = []

        # Choose sources to add
        if (not reAddSources):
            iSources = [iSrc for iSrc in iSources if iSrc not in iSources_done]

        # Check each source file
        _iSources = []
        for iSrc in iSources:
            sourceFile = self.sourceFileName(iSrc, self.baseName, self.dirName, nChars)
            if (ignoreMissing):                
                if (os.path.exists(sourceFile)):
                    if (os.stat(sourceFile).st_size > 0):
                        _iSources.append(iSrc)
            else:
                if (not os.path.exists(sourceFile)):
                    log.warn('!!> File for source %s not found, giving up (sourceFile = %s).' % (iSrc, sourceFile))
                    sys.exit(1)
                elif (os.stat(sourceFile).st_size == 0):
                    log.warn('!!> File for source %s is empty (0 byte), giving up (sourceFile = %s).' % (iSrc, sourceFile))
                    sys.exit(1)

        if ignoreMissing:
            iSources = _iSources

        # ** Read each source file and add to empty output files
        log.info('@@> Populating output files.')

        for iSrc in iSources:
            sourceFile = self.sourceFileName(iSrc, self.baseName, self.dirName, nChars)
            if (os.path.exists(sourceFile)):
                if debug:
                    log.info('--> Loading iSource = %s' % (iSrc))
                sFile = h5py.File(sourceFile, mode='r')
                self.saveSource_singleFile(iSrc, iSources, sFile, outFile, saveCompletePDF = saveCompletePDF, saveOctree = saveOctree, saveCrossVal = saveCrossVal, overwrite = overwrite)
                sFile.close()
                
        # ** Correct the sources fitted to be the same as the files read
        if 'sourcesRowsFitted' in outFile['bond'].keys():
            del outFile['bond/sourcesRowsFitted']
        outFile.close()

        aux = iSources + iSources_done
        t = astropy.table.Table({'sources': np.sort(aux)})
        t.write(fileName, path='bond/sourcesRowsFitted', append=True)
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def saveSource_singleFile(self, iSrc, iSources, sourceFile, outFile, saveBinnedPDFs = False, 
                              saveCompletePDF = False, saveOctree = False, saveCrossVal = True,
                              overwrite = False):
        '''
        Low level, fast copying of HDF5 contents from single source file to combined file.
        '''
        
        # Generic function to copy arrays to a dataset in a HDF5 file
        def copySrcArray(nSources, outFile, sourceFile, dArray, key):
            
            group = os.path.join('bond', dArray)
            array = os.path.join(group, key)
            _array = os.path.join(group, "_%s" % key)

            if (key not in outFile[group].keys()) & (not key.startswith('_')):
                dset = outFile.create_dataset(array, data = np.zeros(nSources))
                h5py.h5o.copy(sourceFile.id, _array, outFile.id, _array)                
            else:                
                dset = outFile[array]
                
            if (not key.startswith('_')):
                if dset[iSrc].shape == ():
                    dset[iSrc] = sourceFile[array][0]
                else:
                    for i in range(len(dset[iSrc])):
                        dset[iSrc, i] = sourceFile[array][0, i]

        # ** Copy simple arrays
        dataArrays = ['fit']
        
        for dataArray in dataArrays:
            copySrcArray(self.sources.nSources, outFile, sourceFile, '', dataArray)
    # --------------------------------------------------------------------
            
    # --------------------------------------------------------------------
    def sourceFileName(self, iSrc, baseName = None, dirName = None, nChars = None, fullPath = True):
        '''
        Save files source by source.
        '''
        
        # Add file and dir names
        self.addDirBaseNames(baseName, dirName)

        if nChars is None:
            nChars = 1 + len(str(self.sources.nSources))

        newBase = '%s_s%s' % (self.baseName, str(iSrc).zfill(nChars))
        newFile = '%s.hdf5' % newBase
        newDir = os.path.join(self.dirName, self.baseName)
        
        if (not os.path.exists(newDir)):
            os.makedirs(newDir)
            
        fileName = os.path.join(newDir, newFile)
        
        if fullPath:
            return fileName
        else:
            return newDir, newBase, fileName
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    def saveToHDF5(self, fileName, **kwargs):
        return self.saveTable(fileName=fileName, format='hdf5', path='fit', **kwargs)
        
    def saveToTXT(self, fileName, **kwargs):
        return self.saveTable(fileName=fileName, format='ascii.fixed_width_two_line', path=None, **kwargs)
        
    def saveTable(self, fileName = None, baseName = None, dirName = None, 
                   saveOctree = True, saveSources = False, saveGrid = False, 
                   tabGridSummary = True, 
                   branches = ['all'],
                   sumTypes = ['jmod', 'jc68', 'mmed', 'mp68'], 
                   outputVars = ['OXYGEN', 'logN2O', 'logUin', 'age', 'fr',
                                 'F3727', 'F5007', 'F6584', 'O23', 'O3O2', 'N2O2',
                                 'F5876', 'Ar3Ne3',
                                 'F4363',  'F5755', 'F6312', 'F7325', 'F7135',
                                 'limF4363',  'limF5755', 'limF6312', 'limF7325', 'limF7135'
                                 ],
                   format='hdf5', path='fit', overwrite = False,
                   debug = False):
        '''
        Save a summary of the fitting results
        '''
        
        # Start output table
        tab = astropy.table.Table()

        # Save source info
        tab['name'] = self.sources.tabData['name']

        if saveSources:
            for var in outputVars:
                if var in self.sources.tabData.keys():
                    tab['obs_%s' % var] = self.sources.tabData[var]
        
        # Pick result to save
        # Save results
        for branch in branches:
            for var in outputVars:

                if var not in self.parName__p:
                    if debug:
                        print("@@ %s was not included in fit; not saving it for the grid." % var)
                else:
                    
                    for sumType in sumTypes:
                    
                        # Get result for this summary type (average, sigma, median, ...)
                        summary = self.PDFSummaries__tcps[sumType]
                    
                        # Select result for this parameter
                        #iP = np.where( np.asarray(self.parName__p) == var )[0][0]

                        # Save to output table
                        colName = '%s_%s_%s' % (branch, var, sumType)
                        tab[colName] = summary[branch][var]
                        
        # Save output table
        if fileName is not None:
            if (not overwrite) and (os.path.exists(fileName)):
                print('!!> Output file %s already exists.\nUse the overwrite option.' % fileName)
            else:
                if path is None:
                    tab.write(fileName, format=format, overwrite=True)
                else:
                    tab.write(fileName, format=format, path='bond/%s' % path, overwrite=True)
                    astropy.table.Table({'branch'       : branches         }).write(fileName, format=format, path='bond/branches'    , append=True)
                    astropy.table.Table({'sumType'      : sumTypes         }).write(fileName, format=format, path='bond/sumTypes'    , append=True)
                    astropy.table.Table({'outputVar'    : outputVars       }).write(fileName, format=format, path='bond/outputVars'  , append=True)
                    astropy.table.Table({'parName'      : self.parName__p  }).write(fileName, format=format, path='bond/parName'     , append=True)
                    astropy.table.Table({'parsJointPDF' : self.parsJointPDF}).write(fileName, format=format, path='bond/parsJointPDF', append=True)

        return tab
    # --------------------------------------------------------------------
    

    
# ************************************************************************


    
# --------------------------------------------------------------------
def calcLineIndices(table, tableIsInLog = True, calcErrors = True, force = False):
    '''
    Calc:

    F5007 and/or F4959

    F4074  = ([S II]4070  + [S II]4078) / Hbeta
    F6724  = ([S II]6716  + [S II]6731) / Hbeta
    O23    = ([O III]5007 + [O III]4959 + [O II]3727) / Hbeta
    
    N2Ha   = [N II]6584   / Ha
    O3O2   = ([O III]5007 + [O III]4959)  / [O II]3727
    N2O2   = [N II]6584   / [O II]3727
    O3N2   = [O III]5007  / [N II]6584
    RO3    = [O III]4363  / [O III]5007
    RO2    = [O II]7325   / [O II]3727
    RN2    = [N II]5755   / [N II]6584
    RS3    = [S III]6312  / [S III]9069
    RS2    = [S II]4074   / [S II]6724
    Ar3O3  = [Ar III]7135 / [O III]5007
    S3Ne3  = [S III]9069  / [O III]5007
    Ar3Ne3 = [Ar III]7135 / [Ne III]3869
    S3Ne3  = [S III]9069  / [Ne III]3869

    Natalia - 22/Jan/2015
    '''

    # ** Lines
    # Calc 4959 from 5007
    table = calcLineTable(table, key_in = 'F5007', key_out = 'F4959', ratio = 1./2.97, tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # Calc 5007 from 4959
    table = calcLineTable(table, key_in = 'F4959', key_out = 'F5007', ratio = 2.97, tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    
    # ** Line sums
    # O23   = ([O III]5007 + [O III]4959 + [O II]3727) / Hbeta, 
    #         where ([O III]5007 + [O III]4959) = [O III]5007 * 1.34
    table = calcLineSumTable(table, sum_key = 'O23', lines = ['F5007', 'F3727'], f = [1.34, 1.], tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # F4074 = ([S II]4070  + [S II]4078) / Hbeta
    table = calcLineSumTable(table, sum_key = 'F4074', lines = ['F4070', 'F4078'], tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # F6724 = ([S II]6716  + [S II]6731) / Hbeta
    table = calcLineSumTable(table, sum_key = 'F6724', lines = ['F6716', 'F6731'], tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    
    # ** Line ratios
    # N2Ha = [N II]6584  / Ha
    table = calcLineRatioTable(table, ratio = 'N2Ha', line1 = 'F6584', line2 = 'F6563', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # O3O2 = ([O III]5007 + [O III]4959) / [O II]3727
    #         where ([O III]5007 + [O III]4959) = [O III]5007 * 1.34
    table = calcLineRatioTable(table, ratio = 'O3O2', line1 = 'F5007', line2 = 'F3727', f = [1.34, 1.], tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # N2O2 = [N II]6584  / [O II]3727
    table = calcLineRatioTable(table, ratio = 'N2O2', line1 = 'F6584', line2 = 'F3727', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # O3N2 = [O III]5007 / [N II]6584
    table = calcLineRatioTable(table, ratio = 'O3N2', line1 = 'F5007', line2 = 'F6584', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # RO3 = [O III]4363 / [O III]5007
    table = calcLineRatioTable(table, ratio = 'RO3',  line1 = 'F4363', line2 = 'F5007', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)

    # RO2 = [O II]7325 / [O II]3727
    table = calcLineRatioTable(table, ratio = 'RO2',  line1 = 'F7325', line2 = 'F3727', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # RN2 = [N II]5755 / [N II]6584
    table = calcLineRatioTable(table, ratio = 'RN2',  line1 = 'F5755', line2 = 'F6584', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # RS3   = [S III]6312  / [S III]9069
    table = calcLineRatioTable(table, ratio = 'RS3',  line1 = 'F6312', line2 = 'F9069', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # RS2   = [S II]4074   / [S II]6724
    table = calcLineRatioTable(table, ratio = 'RS2',  line1 = 'F4074', line2 = 'F6724', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # Ar3O3 = [Ar III]7135 / [O III]5007
    table = calcLineRatioTable(table, ratio = 'Ar3O3',  line1 = 'F7135', line2 = 'F5007', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # S3Ne3 = [S III]9069 / [O III]5007
    table = calcLineRatioTable(table, ratio = 'S3Ne3',  line1 = 'F9069', line2 = 'F5007', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # Ar3Ne3 = [Ar III]7135 / [Ne III]3869
    table = calcLineRatioTable(table, ratio = 'Ar3Ne3',  line1 = 'F7135', line2 = 'F3869', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # S3Ne3 = [S III]9069 / [Ne III]3869
    table = calcLineRatioTable(table, ratio = 'S3Ne3',  line1 = 'F9069', line2 = 'F3869', tableIsInLog = tableIsInLog, calcErrors = calcErrors, force = force)
    
    # ** The end    
    return table

def calcLineRatioTable(table, ratio, line1, line2, f = None, tableIsInLog = True, calcErrors = True, force = False):
    # Multlipying factors
    if f is None:
        f = [1, 1]
    if tableIsInLog:
        f = utils.safe_log10(np.array(f))

    # Calc line ratios
    if (ratio not in table.keys()) | (force):
        if (line1 in table.keys()) & (line2 in table.keys()):
            eline1 = 0.
            eline2 = 0.
            if ('e'+line1 in table.keys()):
                eline1 = utils.safe_mult(table['e'+line1], f[0], tableIsInLog)
            if ('e'+line2 in table.keys()):
                eline2 = utils.safe_mult(table['e'+line2], f[1], tableIsInLog)
            F1 = utils.safe_mult(table[line1], f[0], tableIsInLog)
            F2 = utils.safe_mult(table[line2], f[1], tableIsInLog)
            F, eF = utils.calcLineRatio(F1, F2, eline1, eline2, fluxInLog = tableIsInLog, returnAll = False)
            table[ratio] = F
            if calcErrors:
                table['e'+ratio] = eF

    return table


def calcLineSumTable(table, sum_key, lines, f = None, tableIsInLog = True, calcErrors = True, force = False):
    # Multlipying factors
    if f is None:
        f = np.ones_like(lines, 'float64')
    if tableIsInLog:
        f = utils.safe_log10(np.array(f))

    # Calc line sum
    if (sum_key not in table.keys()) | (force):
        if all(line in table.keys() for line in lines):            
            ftot = 0
            for i, line in enumerate(lines):
                fline = utils.safe_mult(table[line], f[i], tableIsInLog)
                eline = 0.
                if ('e'+line in table.keys()):
                    eline = utils.safe_mult(table['e'+line], f[i], tableIsInLog)
                # First line
                if (i == 0):
                    ftot = fline
                    etot = eline
                else:
                    F, eF = utils.calcLineSum(ftot, fline, etot, eline, fluxInLog = tableIsInLog, returnAll = False)
                    ftot = F
                    etot = eF             
        else:
            ftot = np.full((len(table)), -999.)
            etot = np.full((len(table)), -999.)

        table[sum_key] = ftot
        if calcErrors:
            table['e'+sum_key] = etot

    return table


def calcLineTable(table, key_in, key_out, ratio, tableIsInLog = True, calcErrors = True, force = False):
    '''
    Calc one emission line from another, e.g. [OIII]4959 from 5007.
    '''
    
    if (key_in in table.keys()) & (not force):
        
        # Only replace masked/-999 values
        if key_out in table.keys():
            flag = (table[key_out] <= -999.) & (table[key_in] > -999.)
            try:
                f2 = (table[key_out].mask)
                flag |= f2
            except:
                pass
            
        # Recalc everything
        else:
            flag = np.ones_like(table[key_in], 'bool')
            table[key_out] = np.full_like(table[key_in], -999.)
            if (calcErrors) & ('e'+key_in in table.keys()):
                table['e'+key_out] = np.full_like(table['e'+key_in], -999.)
            
        if tableIsInLog:
            table[key_out][flag] = table[key_in][flag] + np.log10(ratio)
            if (calcErrors) & ('e'+key_in in table.keys()):
                table['e'+key_out][flag] = table['e'+key_in][flag]
        else:
            table[key_out][flag] = table[key_in][flag] * ratio
            if (calcErrors) & ('e'+key_in in table.keys()):
                table['e'+key_out][flag] = table['e'+key_in][flag] * ratio

    return table
    # --------------------------------------------------------------------


    #PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP

    

## ************************************************************************



