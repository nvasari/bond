'''
Save new grid after calculating line indices.

Natalia@UFSC - 08/Dec/2015
'''

import numpy as np
import os

import strongLinesHelper as sl
import utils

import ipython_memory_usage.ipython_memory_usage as imu
imu.start_watching_memory()

#aux = utils.tableHDFRAM("/Users/natalia/data/bond/grids/tab_HII_15_All.hdf5")
#aux = utils.tableHDFRAM("/Users/natalia/data/bond/grids/tab_HII_15_All_interpZoom.hdf5")

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#G = sl.grid(gridFile = os.path.join(dataDir, 'grids/tab_HII_15_All_interpZoom_orig.hdf5'), whoDidTable = 'Natalia', useLog = True, tableIsInLog = True, calcLineInd = True)
#G.saveTableToFile(saveFile = os.path.join(dataDir, 'grids/tab_HII_15_All_interpZoom.hdf5'))

#G = sl.grid(gridFile = os.path.join(dataDir, 'grids/tab_HII_15_All_orig.hdf5'), whoDidTable = 'Natalia', useLog = True, tableIsInLog = True, calcLineInd = True)
#G.saveTableToFile(saveFile = os.path.join(dataDir, 'grids/tab_HII_15_All.hdf5'))

G = sl.grid(gridFile = os.path.join(dataDir, 'grids/tab_HII_15_All_NOvsOH_orig.hdf5'), whoDidTable = 'Natalia', useLog = True, tableIsInLog = True, calcLineInd = True)
G.saveTableToFile(saveFile = os.path.join(dataDir, 'grids/tab_HII_15_All_NOvsOH.hdf5'))
