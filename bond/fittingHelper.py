'''
Fitting helper.

Here are the main parameters for BOND paper 1.

Natalia@UFSC - 14/Dec/2015
'''

import os
import time
import numpy as np
from astropy import log

import multiprocessing as mp

from . import strongLinesHelper as sl

def BONDfit_paper1(grid, sources, 
                   obsFit__f, parName__p = None, 
                   baseName = None, dirName = None,
                   gridParts = ['all'], errCookingFactor = 1., 
                   calcAll = False, removePars = False, debug = False, 
                   setupParBins = True, globalFit = True, *args, **kwargs):

    G = grid
    S = sources

    if parName__p is None:
        physPars4PDF = ['logUin', 'OXYGEN', 'logN2O', 'fr', 'age']
        extraLines4PDF = ['F3727', 'F3869', 'F4074', 'F4363', 'F5007', 'F5755', 'F5876', 'F6312', 'F6584', 'F6724', 'F7135', 'F7325', 'F9069', 'O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RN2', 'Ar3Ne3']
        parName__p = np.unique(physPars4PDF + obsFit__f + extraLines4PDF)

    # ===> Start fitting
    fit = sl.fit(grid = grid, sources = sources, 
               obsFit__f = obsFit__f, parName__p = parName__p, 
               baseName = baseName, dirName = dirName, 
               gridParts = gridParts, errCookingFactor = errCookingFactor,
               calcAll = calcAll, removePars = removePars,
               debug = debug, *args, **kwargs)

    # Set up parameter bins
    if setupParBins:
        fit.setupParBins()
       
    if globalFit:
        global f
        f = fit
        
    return fit

def fit_source_chi2(f_source):
    f_source.calcLikelihood(mode='chi2')
    return f_source
    
def fit_source_stepProb(f_source):
    f_source.stepProb(flim = 1.)
    return f_source

def fit_source_Ar3Ne3(f_source):
    f_source.gaussianProb(obs = ['F7135'], addDeltas = (0.01, 1. / np.log(10), 20))
    f_source.gaussianProb(obs = ['F3869'], addDeltas = (0.01, 1. / np.log(10), 20))
    return f_source

def fit_source_HeI(f_source):
    f_source.gaussianProb(obs = ['F5876'], addDeltas = (0.01, 1. / np.log(10), 20))
    return f_source

def fit_constraints(f_source, constraints = None, debug = False):

    if constraints is None:
        constraints = ['SL', 'upperLim', 'Ar3Ne3', 'HeI']

    # Fitting
    all_fits = {
        'SL'       : fit_source_chi2     ,
        'upperLim' : fit_source_stepProb ,
        'Ar3Ne3'   : fit_source_Ar3Ne3   ,
        'HeI'      : fit_source_HeI      ,
        }

    for constraint in constraints:
        if debug:
            log.debug('@@> Fitting %s.' % constraint)
        this_fit = all_fits[constraint]
        f_source = this_fit(f_source)
        
    return f_source


def BONDfit_newFit_octree(setupParBins = False, globalFit = False, constraints = None, *args, **kwargs):
    f_octree = BONDfit_paper1(setupParBins = setupParBins, globalFit = globalFit, *args, **kwargs)
    f_octree = fit_constraints(f_octree, constraints = constraints)
    return f_octree


def fit_source(iSource, saveFile = True, smallFiles = False, recalc = False, overwrite = True,
               constraints = None, octree = True, wait = 0., gridParts = None, nChars = 4):

    if gridParts is None:
        gridParts = ['all']
    
    # Temp file name for this source
    fileName = f.sourceFileName(iSource, f.baseName, dirName = f.dirName, nChars = nChars)

    # Wait a bit before testing if the file exists - useful for running the code in parallel
    time.sleep(wait * np.random.random())

    
    if (os.path.exists(fileName)) & (not recalc):

        log.info('@@> Skipping source %s' % iSource)
        
    else:
        
        log.info('@@> Fitting source %s' % iSource)
        t1 = time.time()

        # Create empty temp file (to be able to run more instances at once)
        if (saveFile) & (not os.path.exists(fileName)):
            touch = 'touch "%s"' % fileName
            os.system(touch)
        
        # Select one source
        f_source = f.fitOneSource(iSource)

        # First fit with all constraints given
        f_source = fit_constraints(f_source, constraints = constraints, debug = True)
        
        # Octree binning fit
        if octree:
            f_source.octree_grid(BONDfit_newFit_octree, fit_function_kwargs = {'constraints': constraints})
            f_source.calcCrossValidation(BONDfit_newFit_octree, fit_function_kwargs = {'constraints': constraints})
        else:
            f_source.octree_startup(0)

        
        # Calc PDFs
        f_source.calcBinnedPDFs(gridParts = gridParts)
        f_source.calcPDFSummaries(gridParts = gridParts)

        # Save file for source
        if saveFile:
            if (os.stat(fileName).st_size == 0):
                os.remove(fileName)
            if smallFiles:
                f_source.saveToHDF5(fileName = fileName, saveOctree = False, overwrite = overwrite)
            else:
                f_source.saveToHDF5(fileName = fileName, saveOctree = octree, overwrite = overwrite)
        
        log.info('@@> Fitted source %s in %s s' % (iSource, time.time()-t1))
        
        return f_source
    
    
def run(nProc = 1, **kwargs):
        
    if nProc == 1:
        
        for iSrc in f.sourcesRowsFitted:
            fit_source(iSrc, **kwargs)
            
    else:

        # Parallel computing
        p = mp.Pool(processes = nProc, maxtasksperchild = 5)
        for iSrc in f.sourcesRowsFitted:
            p.apply_async(fit_source, args=(iSrc,), kwds = kwargs)
        p.close()
        p.join()
        
    return f
