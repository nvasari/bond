'''
Interpolate and save grid.

Natalia@UFSC - 25/Oct/2014
'''

import numpy as np
import astropy.table
#import atpy
import os

# The core of the fitting routines
import strongLinesHelper as sl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

# Read original grid file
Gorigin = sl.grid(gridFile = os.path.join(dataDir, "grids/tab_HII_15_All.dat.gz"), whoDidTable = 'Christophe', useLog = True, tableIsInLog = True)
#Gorigin.saveTableToFile(saveFile="grids/tab_HII_15_All.hdf5", overwrite = True)


# ---> Calculate and save finer interpolated grids

'''
# ** Grid 5 - for octree
Gorigin.interpolate_regularGrid(inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                                nInterp = [2, 5, 5, 1, 1], 
                                saveFile = os.path.join(dataDir, "grids/tab_HII_15_All_interpOctree.hdf5"), 
                                overwrite = False,
                                debug = False)
'''

# ** Grid 4 - for GNO

# Create N/O vs O/H using the relation from Pilyugin et al 2012 (Eq 2, 2012MNRAS.421.1624P)
logO2H = np.linspace(Gorigin.inputVars__u['OXYGEN'][0], Gorigin.inputVars__u['OXYGEN'][-1], 281)
Z0 = - 12. + 8.14
f_low  = (logO2H < Z0)
logN2O = np.where(f_low, -1.493, 1.489 * (12 + logO2H) - 13.613)

# Create log U
logUin = np.linspace(Gorigin.inputVars__u['logUin'][0], Gorigin.inputVars__u['logUin'][-1], 301)

# Put the grid together
_logUin = np.repeat(logUin, len(logO2H))
_logO2H = np.tile  (logO2H, len(logUin))
_logN2O = np.tile  (logN2O, len(logUin))
_age    = np.full_like(_logO2H, '2.'  )
_fr     = np.full_like(_logO2H, '0.03')

t = astropy.table.Table([_logO2H, _logN2O, _logUin, _age, _fr], names=['OXYGEN', 'logN2O', 'logUin', 'age', 'fr'])

newGrid, flagInGrid = Gorigin.multipleValues_interp_regularGrid(inputParams = t, outputVars = None)
newGrid = newGrid[flagInGrid]

# Uncomment below to check grid
import matplotlib.pyplot as plt
plt.figure()
plt.clf()
plt.plot(Gorigin.tabGrid['OXYGEN'], Gorigin.tabGrid['logN2O'], 'k.')
plt.plot(newGrid['OXYGEN'], newGrid['logN2O'], 'r.')

# Save to file
G = sl.grid(tabGrid = newGrid, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True, calcLineInd = True)
G.saveTableToFile(saveFile = os.path.join(dataDir, 'grids/tab_HII_15_All_NOvsOH.hdf5'), overwrite = True)



# ---> Calculate and save finer interpolated grids


# ** Grid 1 - for tests
Gorigin.interpolate_regularGrid(inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                                nInterp = [2, 5, 1, 1, 1], 
                                saveFile = os.path.join(dataDir, "grids/tab_HII_15_All_interpTests.hdf5"), 
                                overwrite = True,
                                debug = False)


# Check
#Gorigin.plot_origVSinterp_grid()

# Save to ascii file -- very slow and creates a huge file!
#Gorigin.saveTableToFile(grid = 'interp', saveFile="grids/tab_HII_4_interp.dat", fileType='ascii', overwrite = True, Writer = atpy.asciitables.ascii.Tab)



# ** Grid 2 - for fits
Gorigin.interpolate_regularGrid(inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                                nInterp = [10, 10, 10, 1, 1], 
                                saveFile = os.path.join(dataDir, "grids/tab_HII_15_All_interpZoom.hdf5"), 
                                overwrite = True,
                                debug = False)
'''

# Check
#Gorigin.plot_origVSinterp_grid()


# ** Grid 3 - finely interpolated for N/O vs O/H tests
G2 = sl.grid(gridFile = 'grids/tab_HII_15_All_interpZoom.hdf5', whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
G3 = G2.maskGrid({'age': 2., 'fr': 0.03})

# Check
#G3.plot()

G3.interpolate_regularGrid(inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], 
                           nInterp = [1, 4, 1, 1, 1], 
                           saveFile = "grids/tab_HII_15_All_interpNOvsOH.hdf5", 
                           overwrite = True,
                           debug = False)



# ** Grid 4 - Select N/O vs O/H from the grid using the relation from Pilyugin et al 2012 (Eq 2, 2012MNRAS.421.1624P)
#import strongLinesReader as slr
#G3 = slr.init('GinterpNOvsOH', gridFile = 'grids/tab_HII_15_All_interpNOvsOH.hdf5', whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)

P_OH = 12 + G3.tabGrid['OXYGEN']
P_NO = np.where(P_OH < 8.14, -1.493, 1.489 * P_OH - 13.613)

diff_NO = np.abs(G3.tabGrid['logN2O'] - P_NO)
f1 = (diff_NO < 7.e-3) & (P_OH < 8.14)
f2 = (diff_NO < 1.495e-2) & (P_OH >= 8.14)
flag_P12 = f1 | f2
    
GNO = sl.grid(tabGrid = G3.tabGrid, selectTableRows = flag_P12, useLog = G3.useLog, tableIsInLog = G3.tableIsInLog)
print '@@> Number of points in the N/O vs O/H grid: %s' % GNO.nGrid

GNO.saveTableToFile(saveFile = "grids/tab_HII_15_All_NOvsOH.hdf5")

# Plots
#GNO.tabGridInterp = G3.tabGrid
#GNO.plot_origVSinterp_grid()
#import matplotlib.pyplot as plt
#plt.clf()
#plt.plot(P_OH[flag_P12], G3.tabGrid['logN2O'][flag_P12], '.')


'''
