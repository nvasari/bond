'''
Saving table.

Natalia@Corrego - 27/Apr/2015
'''

import os

import numpy as np
import astropy.table
from astropy import log

from bond import strongLinesHelper as sl #@UnusedImport
from bond.natastro import utils

#=========================================================================
# Selecting subsample of table to save

def saveTableTxt(fit, outfile, sources = None, flag = None, saveKeys = None, overwrite = False):

    # Select sources
    if sources is None:
        sources = fit.sources
    
    # Select flag
    if flag is None:
        flag = np.ones(sources.nSources, 'bool')
    
    # Print stats for flag
    log.info('Sample to save: N = %s' % np.sum(flag))
    
    # Keys to save
    ids = ['id']
        
    if saveKeys is None:
        info = ['name']
    else:
        info = saveKeys

    # Start table
    table = astropy.table.Table({'id' : np.zeros(sources.nSources, 'int')})

    # Create new ids
    table['id'][flag] = np.arange(0, flag.sum())

    # Save info
    for k in info:
        table[k] = sources.tabData[k]
        
    # Summaries to save
    ts = ['jmod', 'jc68', 'mmed', 'mp68']
    branch = 'all'
    ps = ['OXYGEN', 'logN2O']
    
    ps_outLabels = {
      'OXYGEN' : 'logO/H' , 
      'logN2O' : 'logN/O' , 
    }
    
    ts_outLabels = {
        'jmod' : ['jmod'] ,
        'jc68' : ['jc68_cen', 'jc68_sig', 'jc68_cov', 'jc68_scale'] ,
        'mmed' : ['mmed'] ,
        'mp68' : ['mp68_low', 'mp68_upp'],
        }
    
    rename = {
        'logO/H_jc68_cov'   : None         ,
        'logO/H_jc68_scale' : None         ,
        'logN/O_jc68_cov'   : 'jc68_cov'   ,
        'logN/O_jc68_scale' : 'jc68_scale' , 
    }
    
    # Generate table
    out_table = table[flag]
    
    for t in ts:
        for p in ps:
            outVals = fit.PDFSummaries__tcps[t][branch][p][flag]
            for i, key in enumerate(ts_outLabels[t]):
                outVal = outVals[..., i]
                outKey = '%s_%s' % (ps_outLabels[p], key)
                out_table[outKey] = outVal 
    
                if outKey in rename.keys():
                    newKey = rename[outKey]
                    if newKey is not None:
                        out_table[newKey] = out_table[outKey]
                    del out_table[outKey]
    
    
                    
    # ****** ASCII table
    # Formatting
    formats = {}
    for k in out_table.keys():
        if k == 'id':
            formats[k] = '%03i'
        elif (k in info) | (k.startswith('\dots')):
            formats[k] = '%s'
        else:
            formats[k] = '%12.4e'
    
    # Masking
    for k in out_table.keys():
        out_table[k] = utils.mask_minus999(out_table[k], fill=0.)
    
    # Save table
    if (overwrite) | (not os.path.exists(outfile)):
        log.info('@@> Saving summary of results to %s' % outfile)
        out_table.write(outfile, format='ascii.fixed_width_two_line', formats=formats)
    
#=========================================================================
# EOF
