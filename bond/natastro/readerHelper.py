'''
Clever way not to reread the data files all the time.

Natalia@UFSC - 26/Jun/2016
'''

import os
import astropy.table
from astropy import log

# ************************************************************************
# Common reading helper

# Global parameters
skip = {}
f = {}

def __init__():
    global skip
    global f

    
def reader(fn):
    
    def _reader_(runName, forceRead = False, *args, **kwargs):
        '''
        Clever way not to reread huge files all the time.
    
        If skip[runName] is True, then we do not read the file again.
        If not, the file is read to f[runName] and returned by this function.
        '''
        # If this has never been run, initialize skip[runName]
        try:
            skip[runName]
        except:
            skip[runName] = False
    
        # Force (re)reading
        if forceRead:
            skip[runName] = False
            
        # Skip if the file has ben read; read it otherwise 
        if skip[runName]:
            log.info("@@> Already read %s." % runName)
        else:
            log.info("@@> Reading %s..." % runName)
            f[runName] = fn(runName, *args, **kwargs)
            skip[runName] = True
            
        return f[runName]
    
    return _reader_    
# ************************************************************************

# ************************************************************************
# Main

def main():
    pass   
    
if __name__ == "__main__":
    main()
# ************************************************************************

