'''
Clever way not to reread the grids/source files all the time.
Based on dustZonesReader.py.

Natalia@UFSC - 06/May/2014
'''

from configparser import ConfigParser
import json
import os
import numpy as np
import astropy.table
from astropy import log

from bond.natastro import utils
from . import strongLinesHelper as sl


# ************************************************************************
# Common reading helper

# Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')
#dataDir = '/Volumes/Ultra/snoopy/data/bond/'

# Global parameters
skip = {}
f = {}

def __init__():
    global skip
    global f

    
def readSL(fn):
    
    def _readSL_(runName, forceRead = False, *args, **kwargs):
        '''
        Clever way not to reread huge files all the time.
    
        If skip[runName] is True, then we do not read the file again.
        If not, the file is read to f[runName] and returned by this function.
        '''
        # If this has never been run, initialize skip[runName]
        try:
            skip[runName]
        except:
            skip[runName] = False
    
        # Force (re)reading
        if forceRead:
            skip[runName] = False
            
        # Skip if the file has ben read; read it otherwise 
        if skip[runName]:
            log.info("@@> Already read %s." % runName)
        else:
            log.info("@@> Reading %s..." % runName)
            f[runName] = fn(runName, *args, **kwargs)
            skip[runName] = True
            
        return f[runName]
    
    return _readSL_
    

@readSL
def readSources(runName, fileName=None, tableData=None, debug=False, **kwargs):
    if runName == 'SPIRALS-HEBCD':
        S = read_Spirals_HeBCD(**kwargs)
    elif runName == 'SPIRALS-HEBCD-MC':
        S = read_Spirals_HeBCD_MC(**kwargs)
    elif runName.startswith('SPIRALS-HEBCD'):
        S = read_Spirals_HeBCD(**kwargs)
    else:
        S = sl.sources(sourcesFile = fileName, tabData = tableData, **kwargs)

    # Check sources
    if debug:
        import plotHelpers as pl
        pl.allPlotsSources(S, figNumber = 101)

    return S

@readSL
def readFit(runName, fileName, **kwargs):
    return sl.fit(fitFile = fileName, **kwargs)

@readSL
def readGrid(runName, fileName, **kwargs):
    return sl.grid(gridFile = fileName, **kwargs)

@readSL
def gridScenarios(runName, gridName, **kwargs):
    return f[gridName].selectScenarios(**kwargs)
# ************************************************************************


# ************************************************************************
# Case-by-case reading

def options(*argv):
    # ==> Fit lines or line ratios?
    try:
        argv[1]
        fit = argv[1]
    except:
        fit = 'lines'

    if fit == 'lines':
        obsFit__f = ['F3727', 'F5007', 'F6584']
    elif fit == 'all_lines':
        obsFit__f = ['F3727', 'F5007', 'F6584', 'F4363']
    elif fit == 'ratios':
        obsFit__f = ['O23', 'O3O2', 'N2O2']
    elif fit == 'all_ratios':
        obsFit__f = ['O23', 'O3O2', 'N2O2', 'RO3']
    
    # ==> Fit with another grid?
    try:
        argv[2]
        grid = argv[2]
    except:
        grid = 'octr'
    
    # ==> Select which scenarios?
    try:
        argv[3]
        scen = argv[3]
    except:
        scen = None

    # ==> Which sources?
    try:
        argv[4]
        srcName = argv[4]
    except:
        srcName = 'HIIBCD'

    # ==> Which sources?
    try:
        argv[5]
        srcName = argv[5]
    except:
        srcName = 'HIIBCD'

        
    if srcName == 'HIIBCD':
        sources = 'SPIRALS-HEBCD'
    elif srcName == 'SDSS':
        sources = 'DR7_bond_MZ'
        
    log.info("@@ Fitting %s with grid %s." % (obsFit__f, grid))

    return obsFit__f, grid, scen, sources
# ************************************************************************


# ************************************************************************
def read_OurGrids(grid, scen, forceRead = False):
        
    # ===> Read grid
    # Read original grid file
    if grid == 'orig':
        Gorigin = readGrid('Gorigin', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        
    # Read grid file for octree binning
    if grid == 'octr':
        Gorigin = readGrid('Goctree', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_interpOctree.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        
    # Read grid file for octree binning - upper branch
    if grid == 'octrupp':
        Gorigin = readGrid('GoctreeUpp', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_interpOctreeUpp.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)

    # Read grid file for octree binning - lower branch
    if grid == 'octrlow':
        Gorigin = readGrid('GoctreeLow', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_interpOctreeLow.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        
    # Read interpolated grid file
    if grid == 'full':
        Ginterp = readGrid('Ginterp', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_interpZoom.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        
    # Read interpolated tests grid file
    if grid == 'test':
        Ginterp = readGrid('Gtests', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_interpTests.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        
    # Read N/O vs O/H grid
    if grid == 'NO':
        GNO = readGrid('GNO', fileName = os.path.join(dataDir, 'grids/tab_HII_15_All_NOvsOH.hdf5'), forceRead = forceRead, whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
        Gs = {'Sage2.00fr0.03': GNO}
    
    # ===> Scenarios
    # Selecting grid scenarios by restricting to a single fr & age
    if scen == 'all':
        scenVars = {'age': [1, 2, 3, 4, 5, 6], 'fr': [0.03, 3.00]}
    elif scen == 'few':
        scenVars = {'age': [2, 6], 'fr': [0.03, 3.00]}

    if (scen is None) | (scen == 'none'):
        if grid == 'orig':
            Gs = {'Gorigin': Gorigin}  
        if grid == 'octr':
            Gs = {'Goctree': Gorigin}  
        if grid == 'octrupp':
            Gs = {'GoctreeUpp': Gorigin}  
        if grid == 'octrlow':
            Gs = {'GoctreeLow': Gorigin}  
        if grid == 'full':
            Gs = {'Ginterp': Ginterp}  
        if grid == 'test':
            Gs = {'Gtests':  Ginterp}  
    else:
        if grid == 'orig':
            Gs = gridScenarios('Gorigin_scen', gridName = 'Gorigin', forceRead = forceRead, scenVars = scenVars)
        if grid == 'octr':
            Gs = gridScenarios('Goctree_scen', gridName = 'Goctree', forceRead = forceRead, scenVars = scenVars)
        if grid == 'octrupp':
            Gs = gridScenarios('GoctreeUpp_scen', gridName = 'GoctreeUpp', forceRead = forceRead, scenVars = scenVars)
        if grid == 'octrlow':
            Gs = gridScenarios('GoctreeLow_scen', gridName = 'GoctreeLow', forceRead = forceRead, scenVars = scenVars)
        if grid == 'full':
            Gs = gridScenarios('Ginterp_scen', gridName = 'Ginterp', forceRead = forceRead, scenVars = scenVars)
        if grid == 'test':
            Gs = gridScenarios('Gtests_scen',  gridName = 'Gtests',  forceRead = forceRead, scenVars = scenVars)
                     
    return Gs
# ************************************************************************


# ************************************************************************
def read_Spirals_HeBCD(useLog = True, debug = False):
    '''
    Read sources files:
    ABRAI.SPIRALS.fix
    ABRAI.HEBCD.fix
    ABRESU.ALL.fix
    '''

    # ===> Read ABRESU
    tabTemp = astropy.table.Table.read(os.path.join(dataDir, 'sources/ABRESU.ALL.fix'), format = 'ascii', header_start = 2, data_start = 3)
    tempO__s = utils.safe_log10(tabTemp['abo'])
    auxN = utils.safe_log10(tabTemp['abn'])
    tempN__s = utils.safe_sub(auxN, tempO__s)
    flagOk_O__s = ~tempO__s.mask
    flagOk_N__s = ~tempN__s.mask
    flagOk__s = flagOk_O__s & flagOk_N__s

    # Rename
    for key in tabTemp.keys():
        if key.startswith('e'):
            newkey = key.replace('e', 'eF')
            tabTemp.rename_column(key, newkey)
        if key[:1].isdigit():
            newkey = 'F' + key
            tabTemp.rename_column(key, newkey)

    # ===> Read Monte Carlo uncertainties
    fileMC_unc = os.path.join(dataDir, 'sources/ABRESU.ALLm.uncert.txt')            
    if os.path.exists(fileMC_unc):
        tabUnc = astropy.table.Table.read(fileMC_unc, format="ascii.fixed_width_two_line")
        for k in tabUnc.keys():
            tabTemp[k] = tabUnc[k]
            
    # Read ABRAI.SPIRALS => emission line limits
    # To check parameters: astropy.io.ascii.read?
    tabSpirals = astropy.table.Table.read(os.path.join(dataDir, 'sources/ABRAI.SPIRALS.fix'), format = 'ascii')
    tabSpirals.rename_column('name_nu', 'name')
    tabSpirals.rename_column('ref', 'r')
    tabSpirals['n'] = np.arange(1, len(tabSpirals)+1)

    # Read ABRAI.HEBCD
    tabHebcd = astropy.table.Table.read(os.path.join(dataDir, 'sources/ABRAI.HEBCD.fix'), format = 'ascii')
    tabHebcd.rename_column('Name', 'name')
    tabHebcd['n'] = np.arange(1, len(tabHebcd)+1)
    
    # Put the three tables together
    ffbHebcd = (tabTemp['r'] == 'z')
    ffbSpira = (tabTemp['r'] != 'z')

    # Get EWHb
    tabTemp.rename_column('EWHB', 'EWHb')
    tabTemp['EWHb'][ffbHebcd] = tabHebcd['ewHb']

    for linelim in ['4363', '5755', '6312', '7135', '7325']:
        # Add limits for GHR sample
        tabTemp['limF'+linelim] = 0.
        tabTemp['limF'+linelim][ffbSpira] = tabSpirals['l'+linelim]

    # Add Ha from ABRAI tables
    for line in ['F6563']:
        tabTemp[line] = -999.
        tabTemp['e'+line] = -999.
        tabTemp[line][ffbSpira]     = tabSpirals['h6563']
        tabTemp['e'+line][ffbSpira] = tabSpirals['e_h6563']
        tabTemp[line][ffbHebcd]     = tabHebcd['f6563']
        tabTemp['e'+line][ffbHebcd] = tabHebcd['e6563']

    # Add [Fe III]4658 from ABRAI.HEBCD
    for line in ['F4658']:
        tabTemp[line] = -999.
        tabTemp['e'+line] = -999.
        tabTemp[line][ffbHebcd]     = tabHebcd['f4658']
        tabTemp['e'+line][ffbHebcd] = tabHebcd['e4658']
    tabTemp['F4658']  = -999.
    tabTemp['eF4658'] = -999.
    tabTemp['F4658' ][ffbHebcd] = tabHebcd['f4658']
    tabTemp['eF4658'][ffbHebcd] = tabHebcd['e4658']

    # Transform tables to sources
    S = sl.sources(tabData = tabTemp, tableIsInLog = False, useLog = useLog)
    S.tabDataName = 'SPIRALS-HEBCD'

    S.tabData['OXYGEN'] = tempO__s
    S.tabData['logN2O'] = tempN__s
    S.tabData['TOTLHB'] = tabTemp['TOTLHB']
    S.tabData['flagOk_OH'] = flagOk_O__s
    S.tabData['flagOk_NO'] = flagOk_N__s
    S.tabData['flagOk_ab'] = flagOk__s
    S.tabData['des2'] = utils.safe_log10(tabTemp['des2'])  # Density in log
    keys = ['tro3', 'trn2', 'tro2', 'trs2', 'trs3', 'tlo3', 'tln2']
    for key in keys:
        # Temperatures in 10^3 K
        S.tabData[key] = np.where((tabTemp[key] > 0), 1.e-3 * tabTemp[key], -999.)

    
    #=========================================================================
    # ===> Info galaxies

    # de Vaucouleur types & other info - from Grazyna, 26/Feb/2015
    #
    # 1: Sa 2: Sab 3: Sb 4: Sbc 5: S? 6: Sc 7: Scd 8: Sd 9: Sm 10: Irr
    # RC3 mtypes.tex
    #   E      & E    & -5  
    #   E-S0   & L-:  & -3: 
    #   S0     & L    & -2  
    #   IrrII  & I0   &  0: 
    #   S0/a   & S0/a &  0  
    #   Sa     & Sa   &  1  
    #   Sa-b   & Sab  &  2  
    #   Sb     & Sb   &  3  
    #   Sb-c   & Sbc  &  4  
    #   Sc     & Scd: &  6: 
    #   Sc-Irr & Sdm: &  8: 
    #   IrrI   & Im   & 10  
    #
    # \cite{deVaucouleurs.etal.1991a}
    # tar xfO ~/Papers/1991/deVaucouleurs.etal.1991a.tar deVaucouleurs.etal.1991a.ftp/rc3.gz | gzcat | cut -c63-74,75-89,118-124,132-135,190-194 | grep "NGC   598"
    #
    S.gal_info = [
        { 'namegal' : 'NGC_0224', 'bar' : -999., 'R25' : -999., 'VT': 3., 'bm' :  4.36 , 'opName' : 'M031' } , # Nat: added
        { 'namegal' : 'NGC_0300', 'bar' :    0., 'R25' : -999., 'VT': 7., 'bm' :  8.72 , 'opName' : '-999' } , # Nat: corrected bm
        { 'namegal' : 'NGC_0598', 'bar' :    0., 'R25' :    1., 'VT': 6., 'bm' :  6.27 , 'opName' : 'M033' } ,            
        { 'namegal' : 'NGC_0628', 'bar' :    0., 'R25' :  314., 'VT': 5., 'bm' :  9.95 , 'opName' : 'M074' } ,
        { 'namegal' : 'NGC_0925', 'bar' :    0., 'R25' :  314., 'VT': 7., 'bm' : 10.69 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_1232', 'bar' :    0., 'R25' :  222., 'VT': 5., 'bm' : 10.52 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_1365', 'bar' :    2., 'R25' :  337., 'VT': 3., 'bm' : 10.32 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_1512', 'bar' : -999., 'R25' : -999., 'VT': 1., 'bm' : 11.13 , 'opName' : '-999' } , # Nat: added
        { 'namegal' : 'NGC_1637', 'bar' :    0., 'R25' :  120., 'VT': 5., 'bm' : 11.47 , 'opName' : '-999' } , # VT: SAB(rs)c NED; bm: ? NED
        { 'namegal' : 'NGC_2403', 'bar' :    0., 'R25' :  656., 'VT': 6., 'bm' :  8.93 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_2805', 'bar' :    0., 'R25' :  189., 'VT': 7., 'bm' : 11.52 , 'opName' : '-999' } , # VT: SAB(rs)d NED; bm: ? NED
        { 'namegal' : 'NGC_2903', 'bar' :    0., 'R25' :  378., 'VT': 4., 'bm' :  9.68 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_2997', 'bar' :    2., 'R25' :  300., 'VT': 5., 'bm' : 10.06 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_3031', 'bar' :    0., 'R25' :  807., 'VT': 2., 'bm' :  7.89 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_3184', 'bar' :    0., 'R25' :  222., 'VT': 6., 'bm' : 10.36 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_3621', 'bar' : -999., 'R25' : -999., 'VT': 7., 'bm' : 10.28 , 'opName' : '-999' } , # Nat: added
        { 'namegal' : 'NGC_4258', 'bar' : -999., 'R25' : -999., 'VT': 4., 'bm' :  9.10 , 'opName' : 'M106' } , # Nat: added
        { 'namegal' : 'NGC_4395', 'bar' :    0., 'R25' :  395., 'VT': 9., 'bm' : 10.64 , 'opName' : '-999' } ,
        { 'namegal' : 'NGC_4625', 'bar' : -999., 'R25' : -999., 'VT': 9., 'bm' : 12.92 , 'opName' : '-999' } , # Nat: added
        { 'namegal' : 'NGC_5194', 'bar' :    0., 'R25' :  337., 'VT': 4., 'bm' :  8.96 , 'opName' : 'M051' } , # R25(no)=100. !la table donne deja 100*R/R25
        { 'namegal' : 'NGC_5236', 'bar' :    0., 'R25' :  395., 'VT': 5., 'bm' :  8.20 , 'opName' : 'M081' } , # Nat: corrected bm
        { 'namegal' : 'NGC_5457', 'bar' :    0., 'R25' :  865., 'VT': 6., 'bm' :  8.31 , 'opName' : 'M101' } , # if (ref(no).eq.'c'.or.ref(no).eq.'i') R25(no)=100.  ! la table donne deja 100* R/R25 / if (ref(no).eq.'d') R25(no)=865.
    ]
    
    S.VT_colours = [
        { 'VT' :  1. , 'VT_type' : 'Sa'  , 'colour' : '#7A0000' } , # dark red
        { 'VT' :  2. , 'VT_type' : 'Sab' , 'colour' : '#FF0000' } , # rouge
        { 'VT' :  3. , 'VT_type' : 'Sb'  , 'colour' : '#FF7519' } , # orange
        { 'VT' :  4. , 'VT_type' : 'Sbc' , 'colour' : '#FFFF00' } , # jaune
        { 'VT' :  5. , 'VT_type' : 'S?'  , 'colour' : '#66FF66' } , # vert clair
        { 'VT' :  6. , 'VT_type' : 'Sc'  , 'colour' : '#66FFFF' } , # cyan
        { 'VT' :  7. , 'VT_type' : 'Scd' , 'colour' : '#3333FF' } , # bleu
        { 'VT' :  8. , 'VT_type' : 'Sd'  , 'colour' : '#003399' } , # bleu moyen
        { 'VT' :  9. , 'VT_type' : 'Sm'  , 'colour' : '#CC00FF' } , # violet
        { 'VT' : 10. , 'VT_type' : 'Irr' , 'colour' : '#00FFCC' } , # vert bleute            
    ]


    # Join tables
    #f_namegal = S.tabData['namegal'].mask
    #S.tabData['namegal'][f_namegal] = S.tabData['sourceName'][f_namegal]
    #S.tabData['namegal'].mask = False
    
    S.tabData['id'] = np.arange(len(S.tabData))        
    aux1 = astropy.table.join(S.tabData, S.gal_info, keys = 'namegal', join_type='left')
    aux1['VT'] = aux1['VT'].filled(-999.)
    aux2 = astropy.table.join(aux1, S.VT_colours, keys = 'VT', join_type='left')
    aux2['colour'] = aux2['colour'].filled("#999999") # gray
    aux2.sort(['id'])
    S.tabData = aux2

    return S
# ************************************************************************

# ************************************************************************
def read_Spirals_HeBCD_MC(debug = False):
    '''
    Read Monte Carlo file:
    ABRESU.ALLm.fix
    '''
    
    tabTemp = astropy.table.Table.read(os.path.join(dataDir, 'sources/ABRESU.ALLm.fix'), format = 'ascii', header_start = 2, data_start = 3)
    
    tempO__s = utils.safe_log10(tabTemp['abo'])
    auxN = utils.safe_log10(tabTemp['abn'])
    tempN__s = utils.safe_sub(auxN, tempO__s)
    tabTemp['OXYGEN'] = tempO__s
    tabTemp['logN2O'] = tempN__s
    
    for key in tabTemp.keys():
        if key.startswith('e'):
            newkey = key.replace('e', 'eF')
            tabTemp.rename_column(key, newkey)
        if key[:1].isdigit():
            newkey = 'F' + key
            tabTemp.rename_column(key, newkey)
    
    # Transform tables to sources
    S = sl.sources(tabData = tabTemp, useLog = True, tableIsInLog = False)
    S.tabDataName = 'SPIRALS-HEBCD-MC'

    return S
# ************************************************************************

# ************************************************************************
# Main

def main():
    pass   
    
if __name__ == "__main__":
    main()
