'''
Examples of pdfs to check fits

Natalia@IAG - 29/Apr/2016
'''

import os

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

from bond import strongLinesHelper as sl #@UnusedImport
from bond.natastro import utils
from bond import plotHelpers as pl

import bond.natastro.plotutils as nplot

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.

def plot_setup(figNumber = 1, plotType = 'times', **kwargs):
    
    # Define plot type---times and minion use latex
    if plotType == 'times':
        psetup = nplot.plotSetup
    elif plotType == 'minion':
        psetup = nplot.plotSetupMinion
    else:
        psetup = nplot.plotSetup

    psetup(**kwargs)
    fig = plt.figure(figNumber)
    plt.clf()

    return fig
    
#=========================================================================
# ===> Plot N/O vs O/H PDFs for all sources

def plot_NOvsOH_all(fit, flag = None, outFile = None, outDir = None,
                    figNumber = 1, plotType = 'times', 
                    savePDF = True, saveEPS = False, pdftocairo = False, epsFromPdf = False):

    # Start new figure
    fig = plot_setup(figNumber = figNumber, plotType = plotType, 
                     fig_width_pt = textwidth, aspect = 0.5, lw = 1., fontsize = 15)

    subp = 1, 2

    # Figure naming
    if outDir is None:
        outDir = '.'
        
    if outFile is None:
        outFile = 'NO_Vs_OH_BOND'

    outFile = os.path.join(outDir, outFile)
        
    # Rename fit and sources
    f = fit
    S = f.sources

    # Select flag
    if flag is None:
        flag = np.ones(S.nSources, 'bool')

    # Selecting the default PDF summaries  
    ftype = 'jmod'
    branch = 'all'

    # Colours and colour maps
    c = pl.colour('BOND')
    light = sns.set_hls_values(c, l=1.)
    colours = [sns.set_hls_values(c, l=l) for l in np.linspace(1, 0.0, 12)]
    cmap = sns.blend_palette(colours, as_cmap=True)
    
    # BOND N/O vs O/H joint PDF for all sources 
    ax = plt.subplot2grid(subp, (0, 0)) 
    jPDF__j = f.jPDF__csj[branch][flag].sum(axis = 0)
    Xgrid = f.jPDF_Xgrid
    im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = cmap, zorder = 0)
    
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    plt.xlabel('12 + %s' % pl.label('OXYGEN'))
    plt.ylabel(pl.label('logN2O'))
    nplot.fix_ticks(ax, 5, 4)
    pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = flag, ax=ax, pltNpoi = True, labSize = 'small', pltTitle = False, pltLegend=False, marker='.', s=30, colours=c)
    
    # BOND N/O vs O/H M.A.P. and covariance ellipse
    ax = plt.subplot2grid(subp, (0, 1)) 
    pl.plot_fit_confEllipse(f, jtype = 'jc68', branch = branch, flag = flag, facecolor = colours[3], alpha = 0.2, edgecolor = 'none', zorder = 0)
    pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = flag, ax=ax, pltNpoi = True, labSize = 'small', pltTitle = False, pltLegend=False, marker='.', s=30, colours=c, annotatePts = False, zorder=10)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    nplot.fix_ticks(ax, 5, 4)
    
    pl.label_panels(fig, addToLabel=[r'Joint PDF + MAP', r'68\% credibility + MAP'], fontdict = {'size': 'small'})
    pl.adjust_subplots(fig)
    fig.set_tight_layout(True)
    pl.save_figs_paper(fig, outFile, savePDF = savePDF, saveEPS = saveEPS, pdftocairo = pdftocairo, epsFromPdf = epsFromPdf)

#=========================================================================
# ===> Plot N/O vs O/H PDFs for all sources

def plot_NOvsOH_summary(fit, flag = None, outFile = None, outDir = None,
                        figNumber = 2, plotType = 'times', 
                        savePDF = True, saveEPS = False, pdftocairo = False, epsFromPdf = False):

    # Start new figure
    fig = plot_setup(figNumber = figNumber, plotType = plotType, 
                     fig_width_pt = columnwidth, aspect = 1, lw = 1., fontsize = 15)

    subp = 1, 1

    # Figure naming
    if outDir is None:
        outDir = '.'
        
    if outFile is None:
        outFile = 'NO_Vs_OH_BOND'

    outFile = os.path.join(outDir, outFile)
        
    # Rename fit and sources
    f = fit
    S = f.sources

    # Select flag
    if flag is None:
        flag = np.ones(S.nSources, 'bool')

    # Selecting the default PDF summaries  
    ftype = 'jmod'
    branch = 'all'

    # Colours and colour maps
    c = pl.colour('BOND')
    light = sns.set_hls_values(c, l=1.)
    colours = [sns.set_hls_values(c, l=l) for l in np.linspace(1, 0.0, 12)]
    cmap = sns.blend_palette(colours, as_cmap=True)
        
    # BOND N/O vs O/H M.A.P. and covariance ellipse
    ax = plt.subplot2grid(subp, (0, 0)) 
    pl.plot_fit_confEllipse(f, jtype = 'jc68', branch = branch, flag = flag, facecolor = colours[3], alpha = 0.2, edgecolor = 'none', zorder = 0)
    pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = flag, ax=ax, pltNpoi = True, labSize = 'small', pltTitle = False, pltLegend=False, marker='.', s=30, colours=c, annotatePts = False, zorder=10)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    nplot.fix_ticks(ax, 5, 4)
    
    pl.label_panels(fig, addToLabel=[r'Joint PDF + MAP', r'68\% credibility + MAP'], fontdict = {'size': 'small'})
    pl.adjust_subplots(fig)
    fig.set_tight_layout(True)
    pl.save_figs_paper(fig, outFile, savePDF = savePDF, saveEPS = saveEPS, pdftocairo = pdftocairo, epsFromPdf = epsFromPdf)
    
#=========================================================================
# ==> Plot PDFs for emission lines for a single source

def plot_PDFs_source(fit, iSource = None, outfile = None, figNumber = 3,
                     savePDF = True, saveEPS = False, 
                     plotType = 'times', pdftocairo = False, epsFromPdf = False):

    # Start new figure
    fig = plot_setup(figNumber = figNumber, plotType = plotType, 
                     fig_width_pt = 1.2*textwidth, aspect = 0.25, lw = 1., fontsize = 10)

    if iSource is None:
        iSource = 0
    
    # Plot
    pl.plots_fittingPDFs_example(fit, iSource, drow=0, icol = -1, subp = [1, 4], zorder=0)

def lix():
    pl.adjust_subplots(fig)
    fig.set_tight_layout(True)
    pl.save_figs_paper(fig, 'fitting_%s_s%04i' % (fit.baseName, iSource), savePDF = savePDF, saveEPS = saveEPS, pdftocairo = pdftocairo, epsFromPdf = epsFromPdf)

#=========================================================================
# EOF
