'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2', 'noSSL_chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

flag_sampleB = p1.sampleB(S)

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

##########################################################################
# N/O vs O/H for SL+4363 and SL+SSL
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1.5, fontsize=20)
fig = plt.figure(11)
plt.clf()

# Plots
subp = 1, 2

ftype = 'jmod'
branch = 'all'

# O/H BOND vs gNO
f = Fs['chi2']
fSL = Fs['noSSL_chi2']

x = f.PDFSummaries__tcps[ftype]['all']['OXYGEN'][..., 0]
y = fSL.PDFSummaries__tcps[ftype][branch]['OXYGEN'][..., 0]

ff = flag_sampleB
cS = pl.colour('SL')
cB = pl.colour('BOND')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], c=cS, s=10, marker='o', lw = 0)
#plt.errorbar(12+x[ff], 12+y[ff], xerr=xerr[..., ff], yerr=yerr[..., ff], fmt='none', ecolor='#999999', elinewidth=1, capsize=0, alpha=0.2, zorder=0)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{SL}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)


# N/O vs O/H BONd vs gNO
ax = plt.subplot2grid(subp, (0, 1)) 

xSL  = fSL.PDFSummaries__tcps[ftype][branch]['OXYGEN'][ff][..., 0] + 12
ySL  = fSL.PDFSummaries__tcps[ftype][branch]['logN2O'][ff][..., 0]
xfit = f.PDFSummaries__tcps[ftype]['all']['OXYGEN'][ff][..., 0] + 12
yfit = f.PDFSummaries__tcps[ftype]['all']['logN2O'][ff][..., 0]

#plt.scatter(xSL, ySL, s=10, marker = 'o', c='k', lw=0, zorder=11)
#plt.scatter(xfit, yfit, s=40, marker = 'o', c='b', lw=0, zorder=10)
pl.plot_fit(fSL, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch,  flag = (ff),  pltTitle=False, pltLegend=False, colours=cS, s=10, marker='o', zorder = 8)
pl.plot_fit(f,   xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'all',       flag = (ff),  pltTitle=False, pltLegend=False, colours=cB, s=30, marker='o', zorder = 9, alpha = 0.7)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax)

for xo, yo, xf, yf in zip(xSL, ySL, xfit, yfit):
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 2, zorder=1)

pl.label_panels(fig)
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'compare_fitSL_fitAll')

#=========================================================================
# EOF

'''
'''
