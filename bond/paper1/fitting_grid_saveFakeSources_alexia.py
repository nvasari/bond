'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import bond.strongLinesHelper as sl

# Clever reader
import bond.strongLinesReader as slr

# Some useful functions
import bond.natastro.plotutils as nplot
import bond.utils as utils
import bond.plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
#dataDir = os.path.join(home, 'data/bond/')
dataDir = '/Volumes/Ultra/snoopy/data/bond/'

#=========================================================================
# ===> Save fake sources
GTs = slr.read_OurGrids('test', 'all', forceRead = False)
Ss = {}
debug = False
save_sources = True
seed = 42

for scenario in GTs.keys():

    # Perturb grid
    t = GTs[scenario].tabGrid
    Ng = (len(t))

    np.random.seed(seed)    
    inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age']
    pertVars = ['OXYGEN', 'logN2O', 'logUin']

    pert = np.random.rand(len(inputVars), Ng)

    newTable = {}
    for i, var in enumerate(inputVars):
        if var in pertVars:
            dv = GTs[scenario].inputVars__u[var][1] - GTs[scenario].inputVars__u[var][0]
            var_new = t[var] + dv * (pert[i] - 0.5)
            newTable[var] = var_new
        else:
            newTable[var] = t[var]
            
    newGrid, flagInGrid = GTs[scenario].multipleValues_interp_regularGrid(inputParams = newTable, outputVars = None)


    # Create fake sources
    print "@@ Creating fake sources for scenario %s." % scenario

    # Get some parameters from grid
    #tg = GTs[scenario].tabGrid # To use unperturbed grid
    tg = newGrid[flagInGrid] 
    inp_OH = 12 + tg['OXYGEN']
    inp_NO = tg['logN2O']
    inp_U  = tg['logUin']
    inp_O3Hb = tg['F5007']
    inp_N2Ha = tg['F6584'] - tg['F6563']

    
    # Select range in N/O vs O/H using the relation from Pilyugin et al 2012 (Eq 2, 2012MNRAS.421.1624P)
    def P_NO(P_OH, dy):
        t = np.arctan(1.489)
        dp = dy / np.cos(t)
        dx = dy * (1./np.sin(t) - 1./np.tan(t))
        return np.where(P_OH < (8.14 - dx), -1.493 + dy, 1.489 * P_OH - 13.613 + dp)
        
    dlow = -0.2
    dupp = 0.4

    flag_P12 = (inp_NO > P_NO(inp_OH, dlow)) & (inp_NO < P_NO(inp_OH, dupp))


    # Select fake sources according to BPT 
    dN2Ha_low = 0.6
    dN2Ha_upp = 0.3
    dO3Hb_low = 0.7
    dO3Hb_upp = 0.1

    O3Hb_S06 = lambda N2Ha: (-30.787 + 1.1358 * N2Ha + 0.27297 * N2Ha**2) * np.tanh(5.7409 * N2Ha) - 31.093
    inp_O3Hb_upp = O3Hb_S06(inp_N2Ha - dN2Ha_upp) + dO3Hb_upp
    inp_O3Hb_low = O3Hb_S06(inp_N2Ha + dN2Ha_low) - dO3Hb_low
    
    flag_S06 = (inp_O3Hb >= inp_O3Hb_low) & (inp_O3Hb <= inp_O3Hb_upp) & (inp_N2Ha > -3.) & (inp_O3Hb > -1.5) & (inp_N2Ha > -2.5)


    # Select fake sources from log O/H vs log U (Perez-Montero 2014)
    b_PM14 = -1.25
    a1_PM14 = 6.625
    a2_PM14 = 8.625

    U_low = a1_PM14 + b_PM14 * inp_OH 
    U_upp = a2_PM14 + b_PM14 * inp_OH 
   
    flag_PM14 = (inp_U >= U_low) & (inp_U <= U_upp)
    
    # Combine flags
    flag_T = flag_S06 & flag_PM14 & flag_P12

    # Create fake sources
    largerError_red = False
    if largerError_red:
        fakeErr = 0.05
    else:
        fakeErr = 0.10


    S = GTs[scenario].toSources(tabGrid = tg, fakeErr = fakeErr, useLog = True, tableIsInLog = True, tableHasLineRatios = True, selectTableRows = flag_T)
    Ss[scenario] = S
    print '@@ Created %s fake sources.' % np.sum(flag_T)


    # Increase the uncertainties in the red part of the spectra (i.e. for [NII] and [ArIII]) -- S/N = 10 instead of 20.
    if largerError_red:
        for line in ['eF6584', 'eF7135']:
            S.tabData[line] *= 2.
        sl.calcLineIndices(S.tabData, tableIsInLog = S.tableIsInLog, calcErrors = True, force = True)
    
    # Add limit to 4363 based on 2x the lowest uncertainty in the flux of [OII], [OIII] and [NII]
    dFfactor = 2.
    lines = ['F3727', 'F5007', 'F6584']
    limF = np.full(S.nSources, 1e30)
    
    for il, line in enumerate(lines):
        # Calc F, dF for this line
        F = utils.safe_pow(10, S.tabData[line])
        dF = dFfactor * np.log(10.) * S.tabData['e%s' % line] * F

        # Save upper limit
        flagLim = (dF < limF)
        limF[flagLim] = dF[flagLim]

    ff = (10**S.tabData['F4363'] < limF)
    print '@@> %s/%s objects with upper limits for [OIII]4363.' % (ff.sum(), S.nSources)
    S.tabData['limF4363'] = np.full(S.nSources, -999.)    
    S.tabData['limF4363'][ff] = utils.safe_log10(limF[ff])
    #print zip(np.array(S.tabData[['F4363', 'limF4363']]), utils.safe_log10(limF))[:10]
    


    if True:
        
        # Draw Pilyugin's relations
        plt.figure(1, figsize=(10,10))
        plt.clf()
        P_OH = np.arange(6.5, 9.5, .01)

        plt.plot(P_OH, P_NO(P_OH, 0), 'y-')
        plt.plot(P_OH, P_NO(P_OH, dupp), 'y-')
        plt.plot(P_OH, P_NO(P_OH, dlow), 'y-')
        plt.xlim(6.5,9.5)
        plt.ylim(-2,1)
        
        plt.xlabel('12 + log O/H')
        plt.ylabel('log N/O')

        # Plot grid
        plt.plot(12+t['OXYGEN'], t['logN2O'], 'b.')
        plt.plot(12+newGrid['OXYGEN'][flagInGrid], newGrid['logN2O'][flagInGrid], 'c.')
    
        plt.plot(12 + Ss[scenario].tabData['OXYGEN'], Ss[scenario].tabData['logN2O'], 'r.')

        
        # Plot BPT
        plt.figure(2)
        plt.clf()
        plt.plot(inp_N2Ha, inp_O3Hb, 'b.', alpha = 0.1, label='grid')
        plt.plot(Ss[scenario].tabData['F6584'] - Ss[scenario].tabData['F6563'], Ss[scenario].tabData['F5007'], 'r.')

        plt.xlabel(r'log [N II]6584/H$\alpha$')
        plt.ylabel(r'log [O III]5007/H$\beta$')
    
        line_N2Ha = np.linspace(-2.5, -0.2, 100)
        plt.plot(line_N2Ha, O3Hb_S06(line_N2Ha), 'y-')
        plt.plot(line_N2Ha, O3Hb_S06(line_N2Ha - dN2Ha_upp) + dO3Hb_upp, 'y-')
        plt.plot(line_N2Ha, O3Hb_S06(line_N2Ha + dN2Ha_low) - dO3Hb_low, 'y-')
        plt.ylim(-6, 2)

        # Plot real data
        #S = sl.sources(sourcesFile = 'sources/ALLGAL-0-COR.DAT.CID', lines2Clean__f = obsFit__f , fixHb = 100., efixHb = 0.005)
        #F1F2, dF1F2, logF1F2, dlogF1F2 = utils.calcLineRatio(S.tabData['F5007'], 1.)
        #S_O3Hb = logF1F2
        #F1F2, dF1F2, logF1F2, dlogF1F2 = utils.calcLineRatio(S.tabData['F6584'], S.tabData['F6563'])
        #S_N2Ha = logF1F2
        #plt.plot(S_N2Ha, S_O3Hb, 'g.', alpha = 0.5, label = 'ALLGAL-0-COR.DAT')

        plt.legend(loc = 'lower left')

        
        # Plot O/H vs U
        plt.figure(3)
        plt.clf()
        plt.plot(inp_OH, inp_U, 'b.', alpha = 0.1)
        plt.plot(12 + Ss[scenario].tabData['OXYGEN'], Ss[scenario].tabData['logUin'], 'r.')

        OH = np.arange(6.5, 10, 0.5)
        plt.plot(OH, a1_PM14 + b_PM14 * OH, 'y-')
        plt.plot(OH, a2_PM14 + b_PM14 * OH, 'y-')
    

#=========================================================================
# EOF

