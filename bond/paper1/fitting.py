'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import fittingHelper as fh
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

if grid == 'octr':
    gScenario = 'Goctree'
else:
    gScenario = 'Gorigin'

#=========================================================================
# ===> Fitting

# ===> Naming fit
G = slr.f[gScenario]
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
print "@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario)

# ===> Output file & dir
suffix = '_'.join(obsFit__f) + '_chi2'
dirName =  os.path.join(dataDir, 'fits/')
baseName = 'fit_%s_%s' % (Fscenario, suffix)

# ===> Choose sources
flag_sampleB = p1.sampleB(S)
ff = flag_sampleB
_iSources = np.where(ff)[0]
iSources = _iSources
#iSources = [3]
#iSources = [75, 74, 68, 8, 18, 41, 23, 24, 9, 802, 26, 50, 27]
#iSources = [75]

# ===> Fit sources 
print "@@> Starting %s" % baseName

f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = iSources, setupParBins = True)
fh.run(nProc = 3)

# Consolidate results into a single file
f.combineSources_singleFile(iSources = iSources, reAddSources = False)

# Fit one source
'''
f_source = f.fitOneSource(3)
f_source = fh.fit_constraints(f_source)
f_source.octree_grid(fh.BONDfit_newFit_octree)
f_source.calcCrossValidation(fh.BONDfit_newFit_octree)
f_source.calcBinnedPDFs()
f_source.calcPDFSummaries(debug = True)
#f_source.saveToHDF5(fileName = 'lix.hdf5', overwrite = True)
'''
#=========================================================================
# EOF


