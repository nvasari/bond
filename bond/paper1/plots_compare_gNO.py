'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

G2s = slr.read_OurGrids('NO', scen, forceRead = False)
Gs['GNO'] = G2s.values()[0]

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[gScenario] = f
      f.dirName = dirName
      f.baseName = baseName

flag_sampleB = p1.sampleB(S)
flag_sampleA = p1.sampleA(S)
flag_sampleBinA = flag_sampleB[flag_sampleA]

#=========================================================================
# ===> Read IZI results

iziFile = os.path.join(dataDir, 'izi/', 'bond_outizi.txt')
tizi = astropy.table.Table.read(iziFile, format='ascii')
tizi['OXYGEN'] = tizi['Z'] - 12

# iziGrid = os.path.join(home, 'software/izi/grids/', 'l09_high_csf_n1e2_6.0Myr.fits')
# gizi = astropy.table.Table.read(iziGrid)

# Calc N/H for the Levesque grid
logO2H = tizi['OXYGEN']
logZ = logO2H + 3.07
tizi['logN2O'] = - logO2H + np.where(logZ <= -0.63, -4.57 + logZ, -3.94 + 2. * logZ)


#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

'''
##########################################################################
# N/O vs O/H for gNO, compare O/H BOND and gNO
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1.5, fontsize=20)
#psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=16)
fig = plt.figure(1)
plt.clf()

# Plots
subp = 1, 2

ftype = 'jc05'
branch = 'all'

# O/H BOND vs gNO
f = Fs['Goctree']
fNO = Fs['GNO']

x = f.PDFSummaries__tcps[ftype][branch]['OXYGEN'][..., 0]
y = fNO.PDFSummaries__tcps[ftype]['all']['OXYGEN'][..., 0]

ff = flag_sampleB
cS = pl.colour('SL')
cB = pl.colour('BOND')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], c=cS, s=10, marker='o', lw = 0, alpha = 0.7)
#plt.errorbar(12+x[ff], 12+y[ff], xerr=xerr[..., ff], yerr=yerr[..., ff], fmt='none', ecolor='#999999', elinewidth=1, capsize=0, alpha=0.2, zorder=0)
#nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{gNO}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)


# N/O vs O/H BONd vs gNO
OH_min, OH_max =  6.81, 9.19
NO_min, NO_max = -1.9, 0.49

ax = plt.subplot2grid(subp, (0, 1)) 

xgNO = fNO.PDFSummaries__tcps[ftype]['all']['OXYGEN'][ff][..., 0] + 12
ygNO = fNO.PDFSummaries__tcps[ftype]['all']['logN2O'][ff][..., 0]
xfit = f.PDFSummaries__tcps[ftype][branch]['OXYGEN'][ff][..., 0] + 12
yfit = f.PDFSummaries__tcps[ftype][branch]['logN2O'][ff][..., 0]

pl.plot_fit(fNO, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'all',  flag = (ff),  pltTitle=False, pltLegend=False, colours=cS, s=10, marker='o', zorder = 8, alpha = 0.7)
pl.plot_fit(f,   xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = (ff),  pltTitle=False, pltLegend=False, colours=cB, s=30, marker='o', zorder = 9, alpha = 0.7)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax)

for xo, yo, xf, yf in zip(xgNO, ygNO, xfit, yfit):
    #plt.plot([xo, xf], [yo, yf], color='#999999', lw = 1, zorder=1)
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 1, zorder=2)

pl.label_panels(fig)
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'compare_gNO')
'''

##########################################################################
# Compare O/H BOND and IZI
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1.5, fontsize=20)
fig = plt.figure(2)
plt.clf()

# Plots
subp = 1, 2

ftype = 'jc05'
branch = 'all'

# O/H BOND vs IZI
f = Fs['Goctree']

x = f.PDFSummaries__tcps[ftype][branch]['OXYGEN'][..., 0]
xerr = np.abs(x[..., np.newaxis] - f.PDFSummaries__tcps['mp68']['all']['OXYGEN'])
y = tizi['OXYGEN']
yerr = np.array([tizi['eZdown'], tizi['eZup']])

# Check IZI PDFs
_ff = (tizi['npeakZ'] == 1) & (tizi['limZ'] == 1) & (tizi['npeakq'] == 1) & (tizi['limq'] == 1)
_fc = (flag_sampleBinA) & (~_ff)
#print _fc.sum()
#print np.where(_fc)[0]
#print (np.where(_fc)[0]+1)*3



#ff = flag_sampleB
ff2 = (flag_sampleBinA) & (tizi['npeakZ'] == 1) & (tizi['limZ'] == 1) #& (tizi['npeakq'] == 1) & (tizi['limq'] == 1)
ff3 = np.where(flag_sampleA)[0]
ff  = utils.ind2flag(S.tabData, ff3[ff2])
cS = pl.colour('SL')
cB = pl.colour('BOND')


# Check IZI sigma for the ones with flag == 0
_ff = (tizi['npeakZ'] == 1) & (tizi['limZ'] == 1) & (tizi['npeakq'] == 1) & (tizi['limq'] == 1)
_fc = (flag_sampleBinA) & (~_ff)
aux = tizi['eZup'] + tizi['eZdown']
print np.average(aux[_fc]), np.median(aux[_fc])
print np.average(aux[ff2]), np.median(aux[ff2])


# Plots
ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff2], c=cS, s=10, marker='o', lw = 0, alpha = 0.7)
#plt.errorbar(12+x[ff], 12+y[ff2], xerr=xerr[ff].T, yerr=yerr[..., ff2], fmt='none', ecolor='#999999', elinewidth=1, capsize=0, alpha=0.2, zorder=0)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{IZI}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)

# N/O vs O/H BONd vs gNO
OH_min, OH_max =  6.81, 9.19
NO_min, NO_max = -1.9, 0.49

ax = plt.subplot2grid(subp, (0, 1)) 

xizi = tizi['OXYGEN'][ff2] + 12
yizi = tizi['logN2O'][ff2]
xfit = f.PDFSummaries__tcps[ftype][branch]['OXYGEN'][ff][..., 0] + 12
yfit = f.PDFSummaries__tcps[ftype][branch]['logN2O'][ff][..., 0]

pl.plot_table_xy(tizi, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff2), pltTitle = False, colours=cS, s=20, marker='o', zorder = 8, alpha = 0.7)
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = (ff), pltNpoi = False, pltTitle=False, pltLegend=False, colours=cB, s=30, marker='o', zorder = 9, alpha = 0.7)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax)

for xo, yo, xf, yf in zip(xizi, yizi, xfit, yfit):
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 1, zorder=2)
    
pl.label_panels(fig)
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'compare_izi')


#=========================================================================
# EOF
