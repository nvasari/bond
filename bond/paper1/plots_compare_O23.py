'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns
import scipy

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

fchi = Fs['chi2']
f = fchi

flag_sampleB = p1.sampleB(S)

##########################################################################
# R23 from Maiolino
psetup(fig_width_pt=textwidth, aspect=0.7, lw=1.5, fontsize=20, override_params={'figure.subplot.wspace': 0.0})
fig = plt.figure(1)
plt.clf()

# Maiolino et al Table 4 - for metallicities > 8.35
OH = {}

x = np.linspace(7.0, 9.5, 1000) - 8.69
R23  =  0.7462 -0.7149 * x -0.9401 * x**2 -0.6154 * x**3 -0.2524 * x**4

# Separate in high and low Z branches
ind_max = R23.argmax()
inds = np.arange(len(x))
flag_low = (inds < ind_max)
x_low = x[flag_low]
R23_low = R23[flag_low]
x_upp = x[~flag_low]
R23_upp = R23[~flag_low]

# Reverse equations
obs_R23 = S.tabData['O23']

interp_low = scipy.interpolate.interp1d(R23_low, x_low, kind = 'linear', bounds_error = False)
R23_OH_low = interp_low(obs_R23) + 8.69

interp_upp = scipy.interpolate.interp1d(R23_upp, x_upp, kind = 'linear', bounds_error = False)
R23_OH_upp = interp_upp(obs_R23) + 8.69

R23_OH = np.where( (S.tabData['N2O2'] < -1.2), R23_OH_low, R23_OH_upp)
OH['MR23_OH_-1.2'] = R23_OH - 12

R23_OH = np.where( (S.tabData['N2O2'] < -1.0), R23_OH_low, R23_OH_upp)
OH['MR23_OH_-1.0'] = R23_OH - 12

# Check reversed equation
# plt.plot(8.69+x, R23)
# plt.plot(R23_OH, obs_R23, '.')


# Plots
subp = 1, 2

# BOND vs R23
x = f.PDFSummaries__tcps['jmod']['all']['OXYGEN'][..., 0]
y = OH['MR23_OH_-1.2']

ff = flag_sampleB
c = pl.colour('SL')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{O23}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)


y = OH['MR23_OH_-1.0']

ax = plt.subplot2grid(subp, (0, 1)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)
plt.setp(ax.get_yticklabels(), visible=False)


pl.adjust_subplots(fig)
pl.label_panels(fig, addToLabel=[r'N2O2 $< -1.2$', r'N2O2 $< -1.0$'])
pl.save_figs_paper(fig, 'compare_R23_Maiolino')


##########################################################################
# R23 from P05 - Kewley & Ellison eq A11, A12
psetup(fig_width_pt=textwidth, aspect=0.7, lw=1.5, fontsize=20, override_params={'figure.subplot.wspace': 0.0})
fig = plt.figure(2)
plt.clf()

# Maiolino et al Table 4 - for metallicities > 8.35
R23 = 10**S.tabData['O23']
P = 10**(S.tabData['F5007'] + np.log10(1 + 1/2.97)) / R23

# Separate in high and low Z branches
OH_upp = (R23 + 726.1 + 842.2 * P + 337.5 * P**2) / (85.96 + 82.76 * P + 43.98 * P**2 + 1.793 * R23)
OH_low = (R23 + 106.4 + 106.8 * P -  3.40 * P**2) / (17.72 +  6.60 * P +  6.95 * P**2 - 0.302 * R23)

OH_P05 = np.where( (S.tabData['N2O2'] < -1.2), OH_low, OH_upp)
OH['PR23_OH_-1.2'] = OH_P05 - 12

OH_P05 = np.where( (S.tabData['N2O2'] < -1.0), OH_low, OH_upp)
OH['PR23_OH_-1.0'] = OH_P05 - 12

# Check reversed equation
# plt.plot(8.69+x, R23)
# plt.plot(R23_OH, obs_R23, '.')


# Plots
subp = 1, 2

# BOND vs R23
x = f.PDFSummaries__tcps['jmod']['all']['OXYGEN'][..., 0]
y = OH['PR23_OH_-1.2']

ff = flag_sampleB
c = pl.colour('SL')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{O23}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)


y = OH['PR23_OH_-1.0']

ax = plt.subplot2grid(subp, (0, 1)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)
plt.setp(ax.get_yticklabels(), visible=False)


pl.adjust_subplots(fig)
pl.label_panels(fig, addToLabel=[r'N2O2 $< -1.2$', r'N2O2 $< -1.0$'])
pl.save_figs_paper(fig, 'compare_R23_P05')
#=========================================================================
# EOF
