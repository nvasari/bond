'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dirName = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dirName, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

fchi = Fs['chi2']
f = fchi

flag_sampleB = p1.sampleB(S)

##########################################################################
# O/H - compare
psetup(fig_width_pt=columnwidth, aspect=1, lw=1., fontsize=16)
fig = plt.figure(12)
plt.clf()

subp = 1, 1


# BOND jpdf vs median
x = f.PDFSummaries__tcps['jmod']['all']['OXYGEN'][..., 0]
y = f.PDFSummaries__tcps['mmed']['all']['OXYGEN']#[..., 0]
xerr = f.PDFSummaries__tcps['mp68']['all']['OXYGEN']
yerr = xerr
dx = 12

ff = flag_sampleB
c = pl.colour('BOND')

ax = plt.subplot2grid(subp, (0, 0))
plt.scatter(dx+x[ff], dx+y[ff], marker='.', s=30, c=c, lw = 0, zorder = 10)
nplot.plot_identity(dx+x[ff], dx+x[ff], color = '#999999', alpha = 0.5, lw = 1, zorder = 9)
#plt.xlim(pl.lims('OXYGEN'))
#plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('%s + %s$_\mathrm{MAP}$'  % (dx, pl.label('OXYGEN')))
plt.ylabel('%s + %s$_\mathrm{med}$' % (dx, pl.label('OXYGEN')))
nplot.fix_ticks(ax)

diff = y[ff] - x[ff]
print np.average(diff), np.median(diff), np.std(diff), np.max(diff), np.min(diff)

fig.set_tight_layout(True)
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'BOND_MAP_vs_median')

#=========================================================================
# EOF
