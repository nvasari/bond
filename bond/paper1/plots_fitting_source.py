'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import fittingHelper as fh

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)


suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

if grid == 'octr':
    gScenario = 'Goctree'
else:
    gScenario = 'Gorigin'


#=========================================================================
# ===> Fitting

# ===> Naming fit
G = Gs[gScenario]
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
print "@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario)


# ===> Output file & dir
suffix = '_'.join(obsFit__f) + '_chi2'
dirName = os.path.join(dataDir, 'fits/')
baseName = 'fit_%s_%s' % (Fscenario, suffix)


# ===> Choose sources
ff = (S.tabData['F6584'] > -999) & (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['F5876'] > -999)
ff2 = (S.tabData['limF4363'] > -999).data | (S.tabData['limF5755'] > -999).data | (S.tabData['limF6312'] > -999).data | (S.tabData['limF7135'] > -999).data | (S.tabData['limF7325'] > -999).data
ff3 = ff & ff2
_iSources = np.where(ff3)[0]
print _iSources

# ===> Fit sources 
print "@@> Starting %s" % baseName
    
    
#=========================================================================
# Plots
columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup


#=========================================================================
# ==> Paper plots
psetup(fig_width_pt=textwidth*1.2, aspect=0.95, lw=1., fontsize=10)
fig = plt.figure(1)
plt.clf()

fig.set_tight_layout(True)


iSource = 3

# Fit
f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = [iSource], setupParBins = True)
f_source_003_1 = fh.fit_source(iSource, constraints = ['SL'], recalc = True, saveFile = False)

f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = [iSource], setupParBins = True)
f_source_003_2 = fh.fit_source(iSource, constraints = ['SL', 'upperLim'], recalc = True, saveFile = False)

f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = [iSource], setupParBins = True)
f_source_003_3 = fh.fit_source(iSource, constraints = ['SL', 'upperLim', 'Ar3Ne3'], recalc = True, saveFile = False)

f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = [iSource], setupParBins = True)
f_source_003_4 = fh.fit_source(iSource, constraints = ['SL', 'upperLim', 'Ar3Ne3', 'HeI'], recalc = True, saveFile = False)
f_source_003 = f_source_003_4

# Plot
axes = pl.plots_fittingPDFs_paper1(f_source_003_1, 0, drow=0, title=r'\hspace{1em}\\ Gaussian constraints: \\%s \\%s \\%s' % (pl.label('F3727'), pl.label('F5007'), pl.label('F6584')), zorder=0)
pl.plots_fittingPDFs_paper1(f_source_003_2, 0, drow=1, title=r'\hspace{1em}\\+ upper limit: \\%s \\%s' % (pl.label('F4363'), pl.label('F5755')), zorder=0)
pl.plots_fittingPDFs_paper1(f_source_003_3, 0, drow=2, title=r'\hspace{1em}\\+ gaussian constraints \\(margin. uncertainties): \\%s \\%s' % (pl.label('F7135'), pl.label('F3869'),), zorder=0)
pl.plots_fittingPDFs_paper1(f_source_003_4, 0, drow=3, title=r'\hspace{1em}\\+ gaussian constraints \\(margin. uncertainties): \\%s' % (pl.label('F5876'),), zorder=0)


pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'fitting_%s_s%04i' % (baseName, iSource))



#=========================================================================
# Checking the multiple solutions
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=24)
fig = plt.figure(2)
plt.clf()

ax = plt.subplot(111)

# Plot results for each scenario
try:
    for scen in f_source_003.grid.scenarios.keys():
        f_source_003.PDFSummaries__tcps['jmod'][scen]
except:
    f_source_003.calcBinnedPDFs(gridParts = f_source_003.grid.scenarios.keys())
    f_source_003.calcPDFSummaries(gridParts = f_source_003.grid.scenarios.keys())

ax = pl.plot_jointMargPDF(f_source_003, 0, 'OXYGEN', 'logN2O', dx=12, pltEllipse = True, pltScens = True, zorder = 0)
ax.set_xlim(8.0, 9.4)
ax.set_ylim(-1.25, -0.62)
        
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'fitting_%s_s%04i_zoom_margPDFs' % (baseName, iSource))
#=========================================================================


#=========================================================================
# Plotting octree grid

iSource2 = 709
f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = [iSource], setupParBins = True)
f_source_709 = fh.fit_source(iSource2, constraints = ['SL', 'upperLim', 'Ar3Ne3', 'HeI'], recalc = True, saveFile = False)

def calcHistOctree(tabGrid):
     n = 1
     nBins_Xs = [28*n+1, 20*n+1]
     Xs = ['OXYGEN', 'logN2O']
     bins   = [0] * len(Xs)
     Rs__ro = [0] * len(Xs)
             
     for i, X in enumerate(Xs):
         bins_cen = np.linspace(f_source_003.bParCen__pb[X][0], f_source_003.bParCen__pb[X][-1], nBins_Xs[i])
         db = bins_cen[1] - bins_cen[0]
         bins_resam = np.empty(len(bins_cen)+1)
         bins_resam[:-1] = bins_cen - 0.5 *db
         bins_resam[-1]  = bins_cen[-1] + 0.5 *db
         bins[i] = bins_resam
     
     Xgrid = np.meshgrid(bins[0], bins[1], indexing='ij')
     N = len(tabGrid)
     xy = np.array(tabGrid[Xs]).view(np.float64).reshape(N, -1)
     values = np.histogramdd(xy, bins=bins)
     histGrid = values[0]
     
     h = utils.safe_log10(histGrid)

     return Xgrid, h

def pltMap(fit_source, ax, vmax = None):
     tabGrid = fit_source.tabGrids__si[0]
     Xgrid, h = calcHistOctree(tabGrid)
     if vmax is None:
         vmax = h.max()
     
     # Plot maps
     light = sns.set_hls_values(pl.colour('fake'), l=1.)
     colors = [sns.set_hls_values(pl.colour('fake'), l=l) for l in np.linspace(0.85, 0., 12)]
     cmap = sns.blend_palette(colors, as_cmap=True)
     im = ax.pcolormesh(12 + Xgrid[0], Xgrid[1], h, cmap = cmap, zorder = 0, vmax = vmax)
     
     # Plot color bar
     cb = nplot.tight_colorbar(im, pad = "3%", zorder = 0, ax = ax)
     cb.set_label('$\log \, N_\mathrm{points}$')
     cbarytks = plt.getp(cb.ax.axes, 'yticklines')
     plt.setp(cbarytks, visible=False)
     
     cb.set_alpha(1)
     cb.solids.set_edgecolor("face")
     nplot.fix_colorbar_ticks(cb, 5, steps=[0.1, 0.2, 0.5])

     
psetup(fig_width_pt=columnwidth, aspect=1.5, lw=1., fontsize=12, override_params={'figure.subplot.hspace': 0.04})
fig = plt.figure(3)
plt.clf()

# Plot grid points
ax1 = plt.subplot(211)
ax2 = plt.subplot(212, sharex = ax1, sharey = ax1)
ff = G.flagGrid(inputVar = 'age', value = 2.) & G.flagGrid(inputVar = 'fr', value = 0.03) & G.flagGrid(inputVar = 'logUin', value = -1.)
ax1.scatter(12+G.tabGrid['OXYGEN'][ff], G.tabGrid['logN2O'][ff], c='#666666', marker = '.', s=5, lw = 0, zorder = 10)
ax2.scatter(12+G.tabGrid['OXYGEN'][ff], G.tabGrid['logN2O'][ff], c='#666666', marker = '.', s=5, lw = 0, zorder = 10)
#ax.set_xlim(6.55, 9.45)
ax2.set_xlim(7.35, 9.45)
ax1.set_ylim(-2.05, 0.05)
nplot.fix_ticks(ax1)

ax2.set_xlabel('12 + %s' % pl.label('OXYGEN'))
ax1.set_ylabel(pl.label('logN2O'))
ax2.set_ylabel(pl.label('logN2O'))

# Calc number of grid points spawned by octree
pltMap(f_source_003, ax1)
pltMap(f_source_709, ax2)

# Adjust & save
pl.label_panels(fig)
pl.adjust_subplots(fig, exclude_subp = [3, 4])
pl.save_figs_paper(fig, 'octree_s%04i_s%04i' % (iSource, iSource2))
#=========================================================================




#=========================================================================
# Checking the variation of Ar3Ne3 in Izotov's data --> add to delta
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=12)
plt.figure(4)
plt.clf()

#ff = (G.scenarios['Sage2.00fr0.03']) & (G.flagGrid('logN2O', -1.))
#ff = (G.scenarios['Sage2.00fr0.03']) & (G.flagGrid('logUin', -2.))
ff = (G.scenarios['Sage2.00fr0.03'])
plt.plot(G.tabGrid['OXYGEN'][ff]+12, G.tabGrid['Ar3Ne3'][ff], marker = ',', color = '#999999', zorder=-10)

ff = (S.tabData['r'] == 'z') & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['OXYGEN'] > -999)
plt.plot(S.tabData['OXYGEN'][ff]+12, S.tabData['Ar3Ne3'][ff], 'ko', ms=5)
plt.errorbar(S.tabData['OXYGEN'][ff]+12, S.tabData['Ar3Ne3'][ff], yerr=S.tabData['eAr3Ne3'][ff], fmt='none', ecolor='k', elinewidth=1, capsize=0)

ff = (S.tabData['r'] != 'z') & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['OXYGEN'] > -999)
plt.plot(S.tabData['OXYGEN'][ff]+12, S.tabData['Ar3Ne3'][ff], 'bo', ms=5)
plt.errorbar(S.tabData['OXYGEN'][ff]+12, S.tabData['Ar3Ne3'][ff], yerr=S.tabData['eAr3Ne3'][ff], fmt='none', ecolor='b', elinewidth=1, capsize=0)

# Calc rms fitting a line
_ff = (S.tabData['Ar3Ne3'] > -999) & (S.tabData['OXYGEN'] > -999)
ff1 = (S.tabData['r'] == 'z') & _ff
ff2 = (S.tabData['r'] != 'z') & _ff

for ff in [_ff, ff1, ff2]:
    regression = np.polyfit(S.tabData['OXYGEN'][ff]+12, S.tabData['Ar3Ne3'][ff], 1)
    r_x = np.linspace(6.5, 9.5)
    r_y = r_x * regression[0] + regression[1]
    plt.plot(r_x, r_y)
    
    yy = (12+S.tabData['OXYGEN'][ff]) * regression[0] + regression[1]
    dy = S.tabData['Ar3Ne3'][ff] - yy
    print np.mean(dy), np.median(dy), np.std(dy)


#=========================================================================
# Checking the lower limits for the uncertainities of F6584, F5007, F3727 --> add to delta
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=12)
plt.figure(5)
plt.clf()

for line in ['F3727', 'F5007', 'F6584']:
    aux = G.tabGrid[line]
    aux2 = np.sort(aux)
    aux3 = aux2[:-1] - aux2[1:]
    plt.hist(np.log10(np.abs(aux3)))

#=========================================================================
# EOF
