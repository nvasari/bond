'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re
import itertools

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplcolors
import astropy.table
import seaborn as sns

# Clever reader
import strongLinesHelper as sl
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids('octr', 'all', forceRead = False)
Gs = slr.read_OurGrids('orig', 'all', forceRead = False)
Gs = slr.read_OurGrids('test', 'all', forceRead = False)
Gs = slr.read_OurGrids('full', 'all', forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ==> Plots
columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

def plot_grid_sources(x, y, ax, flag = None, pltSources=True, lwU=0.1, sPoi=1, 
                      axisU=False, axisOH=False, axisNO=False, ptsOnlyNO=False,
                      labelU=False, labelOH=False, labelNO=False):
    
    # Plot lines
    nU  = len(G.inputVars__u['logUin'])
    nOH = len(G.inputVars__u['OXYGEN'])
    nNO = len(G.inputVars__u['logN2O'])

    colours = sns.hls_palette(nOH+3, l=.5, s=.8)[::-1][3:]
    for OH, colour in zip(G.inputVars__u['OXYGEN'], colours):
        for NO in G.inputVars__u['logN2O']:
            newGrid = G.maskGrid(inputVars = {'OXYGEN': OH, 'logN2O': NO}, flag=flag)
            if (newGrid is not None):
                plt.plot(newGrid.tabGrid[x], newGrid.tabGrid[y], color=colour, lw=1, zorder=2)
                
    for NO in G.inputVars__u['logN2O']:
        for U in G.inputVars__u['logUin']:
            newGrid = G.maskGrid(inputVars = {'logN2O': NO, 'logUin': U }, flag=flag)
            if (newGrid is not None):
                plt.plot(newGrid.tabGrid[x], newGrid.tabGrid[y], color='#999999', lw=lwU, zorder=1)

    if pltSources:
        f = (S.tabData[x] > -999) & (S.tabData[y] > -999)
        plt.scatter(S.tabData[x][f], S.tabData[y][f], color=pl.colour('obs'), marker='.', s=sPoi, zorder=0)
    
    if axisU:
        OH = G.inputVars__u['OXYGEN'][0]
        NO = G.inputVars__u['logN2O'][0]
        Us = G.inputVars__u['logUin']
        newGrid = G.maskGrid(inputVars = {'OXYGEN': OH, 'logN2O': NO}, flag=flag)
        if (newGrid is not None):
            labels = ['%.1f' % U for U in Us]
            dx, dy = -0.1, -0.1
            xs, ys = dx + newGrid.tabGrid[x], dy + newGrid.tabGrid[y]
            for xl, yl, lab in zip(xs, ys, labels):
                ax.text(xl, yl, lab, ha='right', va='center', fontdict = {'size': 'x-small', 'color': '#666666'}, zorder=30).set_clip_on(True)
                plt.scatter(xl-dx, yl-dy, color='#666666', marker='s', s=10, zorder=20)
            if labelU:
                xm, ym = xs.mean(), ys.mean()
                ax.text(xm-0.4, ym-0.4, '%s' % pl.label('logUin'), ha='right', va='center', fontdict = {'size': 'x-small', 'color': '#666666'})
                

                
    if axisOH:
        NO = G.inputVars__u['logN2O'][0]
        U  = G.inputVars__u['logUin'][-1]
        Os = G.inputVars__u['OXYGEN']
        newGrid = G.maskGrid(inputVars = {'logUin': U, 'logN2O': NO}, flag=flag)
        if (newGrid is not None):
            labels = ['%.1f' % (12+O) for O in Os]
            dx, dy = 0, 0.5
            xs, ys = dx + newGrid.tabGrid[x], dy + newGrid.tabGrid[y]
            for xl, yl, lab, c in zip(xs, ys, labels, colours)[::2]:
                ax.text(xl, yl, lab, ha='center', va='top', fontdict = {'size': 'x-small', 'color': c}, zorder=30).set_clip_on(True)
                plt.scatter(xl-dx, yl-dy, color=c, marker='^', s=20, zorder=20)
            if labelOH:
                xm, ym = xs.mean(), ys.mean()
                ax.text(xm-0.5, ym-0.2, '12+%s' % pl.label('OXYGEN'), rotation=40, ha='right', va='center', fontdict = {'size': 'x-small', 'color': colours[3]})
                
    if axisNO:
        for i in [0, -1]:
            OH = G.inputVars__u['OXYGEN'][i]
            U  = G.inputVars__u['logUin'][0]
            Ns = G.inputVars__u['logN2O']
            newGrid = G.maskGrid(inputVars = {'OXYGEN': OH, 'logUin': U}, flag=flag)
            if (newGrid is not None):
                labels = ['%.1f' % N for N in Ns]
                dx, dy = 0, -0.45
                xs, ys = dx + newGrid.tabGrid[x], dy + newGrid.tabGrid[y]
                for xl, yl, lab in zip(xs, ys, labels):
                    c = colours[i]
                    if not ptsOnlyNO:
                        ax.text(xl, yl, lab, ha='center', va='bottom', fontdict = {'size': 'x-small'}, zorder=30).set_clip_on(True)               
                    plt.scatter(xl-dx, yl-dy, color=c, marker='v', s=20, zorder=20)
    if labelNO:
        xm, ym = xs.mean(), ys.mean()
        ax.text(xm, ym+0.1, pl.label('logN2O'), ha='right', va='center', fontdict = {'size': 'x-small'})


                
    plt.xlabel(pl.label(x, short=True))
    plt.ylabel(pl.label(y, short=True), labelpad=-1)

                
    
 
#=========================================================================
psetup(fig_width_pt=textwidth, aspect=0.6, lw=1., fontsize=14)
fig = plt.figure(11)
plt.clf()

subp = 5, 8

addToLabel = []

for iage, age in enumerate(range(1, 7)[:1]):
    for ifr, fr in enumerate([0.03]):
        ix = iage
        iy = ifr

        Gs = slr.read_OurGrids('orig', 'all', forceRead = False)
        G = Gs['Sage%.2ffr%.2f' % (age, fr)]

        if fr == 0.03:
            geom = 'Filled sphere'
        else:
            geom = 'Spherical shell'
        
        mx = 4
        dy = subp[-1]/2
        my = 1 + subp[-1]/2
        
        ax1 = plt.subplot2grid(subp, (1+mx*ix, my*iy), colspan=4, rowspan=4)
        plot_grid_sources('N2Ha', 'F5007', ax1, lwU=1, sPoi=3, axisNO=True, axisU=True, axisOH=True, labelNO=True, labelU=True, labelOH=True)
        #plt.xlim(-4.4, 0.7)
        #plt.ylim(-4.0, 2.3)
        plt.xlim(-5.2, 0.7)
        plt.ylim(-4.0, 1.9)
        nplot.fix_ticks(ax1, 5, 4, steps=[0, 2, 5, 10])
        addToLabel.append('')

        ax2 = plt.subplot2grid(subp, (1+mx*ix, my*iy+dy), colspan=4, rowspan=4)
        plot_grid_sources('O23', 'O3O2', ax2, lwU=1, sPoi=3, axisNO=True, axisU=True, axisOH=True, ptsOnlyNO=True)
        #plt.xlim(-2.0, 1.2)
        #plt.ylim(-3.9, 2.9)
        plt.xlim(-1.6, 1.3)
        plt.ylim(-3.9, 2.9)
        nplot.fix_ticks(ax2, 5, 4, steps=[0, 1, 2, 10])
        addToLabel.append('%i Myr' % age)

        if iage == 0:
            ax1.text(1.14, 1.06, geom, ha='center', va='bottom', transform=ax1.transAxes, fontdict = {'size': 'x-large'})

fig.set_tight_layout(True)
pl.label_panels(fig, labels=addToLabel, fontdict = {'size': 'small'}, x = 0.08, y = 0.95)
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'grid_scenarios_zoom')

#=========================================================================
psetup(fig_width_pt=textwidth*1.1, aspect=1.5, lw=1., fontsize=12)
fig = plt.figure(1)
plt.clf()

subp = 25, 17

addToLabel = []

#for iage, age in enumerate(range(1, 7)[:1]):
#    for ifr, fr in enumerate([0.03, 3.00]):

for iage, age in enumerate(range(1, 7)):
    for ifr, fr in enumerate([0.03, 3.00]):
        ix = iage
        iy = ifr

        Gs = slr.read_OurGrids('orig', 'all', forceRead = False)
        G = Gs['Sage%.2ffr%.2f' % (age, fr)]

        if fr == 0.03:
            geom = 'Filled sphere'
        else:
            geom = 'Spherical shell'
        
        mx = 4
        dy = subp[-1]/4
        my = 1 + subp[-1]/2

        ax1 = plt.subplot2grid(subp, (1+mx*ix, my*iy), colspan=4, rowspan=4)
        plot_grid_sources('N2Ha', 'F5007', ax1)
        plt.xlim(-4.4, 0.7)
        plt.ylim(-4.0, 2.3)
        nplot.fix_ticks(ax1, 5, 4, steps=[0, 2, 5, 10])
        addToLabel.append('%i Myr' % age)

        ax2 = plt.subplot2grid(subp, (1+mx*ix, my*iy+dy), colspan=4, rowspan=4)
        plot_grid_sources('O23', 'O3O2', ax2)
        plt.xlim(-2.0, 1.2)
        plt.ylim(-3.9, 2.9)
        nplot.fix_ticks(ax2, 5, 4, steps=[0, 1, 2, 10])
        addToLabel.append('%i Myr' % age)

        if iage == 0:
            ax1.text(1.22, 1.15, geom, ha='center', va='bottom', transform=ax1.transAxes, fontdict = {'size': 'x-large'})

fig.set_tight_layout(True)
pl.label_panels(fig, labels=addToLabel, fontdict = {'size': 'small'}, x = 0.08, y = 0.95)
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'grid_scenarios')


#=========================================================================
# ==> QHe/QH Popstar
psetup(fig_width_pt=columnwidth, aspect=1, lw=1., fontsize=14)
fig = plt.figure(2)
plt.clf()
 
subp = 1, 1
ax = plt.subplot2grid(subp, (0, 0))

sns.set_palette("husl")

ax.set_color_cycle(sns.color_palette("coolwarm", 6))

E, Enorm = 1.8, 1
for met in ('0001', '0004', '0040', '0080', '0200', '0500'):
    inFile = '/Users/natalia/projects/strong_lines/Q-age/QHeQH_popstar/popstar_Q%3.1fQ%3.1f_Z%s.txt' % (E, Enorm, met)
    aux = astropy.table.Table.read(inFile, format='ascii.fixed_width_two_line')
    x, y = aux['log_age'], aux['QaQb']
    l = r'%s Z$_\odot$' % (np.float(met)/0200.)
    ax.plot(10**x/1e6, np.log10(y), label=l, lw = 2)

ax.legend(loc = 'lower left', frameon = False, bbox_to_anchor=(0.07, 0.05), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'small')
ax.set_xlim(0, 10)
ax.set_ylim(-5.5, 0)
ax.set_xlabel('age [Myr]')
ax.set_ylabel(r'log %s' % pl.label('QHe_QH'))
nplot.fix_ticks(ax, 5, 4, steps=[0, 2, 0.5, 2])

fig.set_tight_layout(True) 
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'popstar_QHeQH')

#=========================================================================
# ==> O/H vs O23, coloured by QHe/QH
psetup(fig_width_pt=columnwidth, aspect=0.8, lw=1., fontsize=13)
fig = plt.figure(3)
plt.clf()

subp = 1, 1

G = slr.f['Ginterp']
#G = slr.f['Gorigin']
flag_NO = G.flagGrid('logN2O', -1)
flag_U = G.flagGrid('logUin', -2.)

colours = sns.hls_palette(256, l=.5, s=.8)[:210]
cmap = mplcolors.ListedColormap(colours)


# Calc QHe/QH
# https://sites.google.com/site/mexicanmillionmodels/the-different-tables/tab
# logQ	log(Q), where Q in the number of ionizing photons emitted/s [log s^-1]
# logQ0	Q0 is the number of ionizing photons between 1 and 1.807 Ryd [log s^-1] = 13.6--24.6 eV
# logQ1	Q1 is the number of ionizing photons between 1.807 and 4 Ryd [log s^-1] = 24.6--54.4 eV
# logQ2	Q2 is the number of ionizing photons between 4 and 20.6 eV [log s^-1] = 24.6--280.6 eV

aux = utils.calcLineSum(G.tabGrid['logQ1'], G.tabGrid['logQ2'], fluxInLog = True, returnAll = False)
logQHe = aux[0]
logQH = G.tabGrid['logQ']
G.tabGrid['QHe_QH'] = 10**(logQHe - logQH)


#ff = flag_NO & flag_U 
#ff = np.ones(G.nGrid, 'bool')
ff = flag_U
ax = plt.subplot2grid(subp, (0, 0)) 
a = pl.plot_table_xy(G.tabGrid, xlab = 'OXYGEN', ylab = 'O23', dx = 12, flag = (ff), pltTitle = False, colours='b', s=1, marker = 'o', c=G.tabGrid['QHe_QH'][ff], cmap=cmap, lw=0, zorder=0)

# Colorbar
cb = nplot.tight_colorbar(a, zorder = 0)
cb.set_label(pl.label('QHe_QH'))
cbarytks = plt.getp(cb.ax.axes, 'yticklines')
plt.setp(cbarytks, visible=False)

cb.set_alpha(1)
cb.solids.set_edgecolor("face")
nplot.fix_colorbar_ticks(cb, 5, steps=[0.1, 0.2, 0.5])
nplot.fix_ticks(ax)

fig.set_tight_layout(True)
pl.adjust_subplots(fig, exclude_subp = [2])
pl.save_figs_paper(fig, 'grid_OHvsO23_QHeQH')


#=========================================================================
# ==> Ar3Ne3 vs O23, coloured by branch 
psetup(fig_width_pt=columnwidth, aspect=1, lw=1., fontsize=14)
fig = plt.figure(4)
plt.clf()

subp = 1, 1

G = slr.f['Ginterp_scen']['Sage2.00fr0.03']
G.findBimodality()

ax = plt.subplot2grid(subp, (0, 0))

colours = sns.color_palette("coolwarm", 10)
cupp, clow = colours[0], colours[-1]

f = G.branches['upp']
plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c=clow, lw=0, s=1, alpha=0.1, zorder=0)

f = G.branches['low']
plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c=cupp, lw=0, s=1, alpha=0.1, zorder=0)

plt.xlabel(pl.label('O3O2', short = True))
plt.ylabel(pl.label('Ar3Ne3', short = True))

plt.xlim(-2.5, 2.5)
plt.ylim(-2, 2)
nplot.fix_ticks(ax, 5, 5)

G = slr.f['Ginterp']

fig.set_tight_layout(True)
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'grid_Ar3Ne3_O23_branches')
#=========================================================================
# ==> O/H vs HeI, coloured by QHe/QH
psetup(fig_width_pt=columnwidth, aspect=0.80, lw=1., fontsize=14)
fig = plt.figure(5)
plt.clf()

subp = 1, 1

colours = sns.hls_palette(256, l=.5, s=.8)[:210]
cmap = mplcolors.ListedColormap(colours)

G = slr.f['Ginterp']
flag_NO = G.flagGrid('logN2O', -1)
flag_U = G.flagGrid('logUin', -2.)

G.tabGrid['linF5876'] = 10**(G.tabGrid['F5876'])

ff = flag_U
ax = plt.subplot2grid(subp, (0, 0))
a = pl.plot_table_xy(G.tabGrid, xlab = 'OXYGEN', ylab = 'linF5876', dx = 12, flag = (ff), pltTitle = False, colours='b', s=1, marker = 'o', c=G.tabGrid['QHe_QH'][ff], cmap=cmap, lw=0, zorder = 0)
nplot.fix_ticks(ax, 3, 4)

# Colorbar
cb = nplot.tight_colorbar(a, ax = ax, zorder = 0)
cb.set_label(pl.label('QHe_QH'))
cbarytks = plt.getp(cb.ax.axes, 'yticklines')
plt.setp(cbarytks, visible=False)

cb.set_alpha(1)
cb.solids.set_edgecolor("face")
nplot.fix_colorbar_ticks(cb, 5, steps=[0.1, 0.2, 0.5])

# Adjust & save
fig.set_tight_layout(True)
# This mysteriously does not work.
#pl.adjust_subplots(fig, exclude_subp = [2])
pl.save_figs_paper(fig, 'grid_OHvs5876_QHeQH')
#=========================================================================
# ==> QHe/QH vs HeI, coloured by O/H for Cid
psetup(fig_width_pt=columnwidth, aspect=0.75, lw=1., fontsize=14)
fig = plt.figure(51)
plt.clf()

subp = 1, 1

colours = sns.hls_palette(256, l=.5, s=.8)[:210]
cmap = mplcolors.ListedColormap(colours)

G = slr.f['Ginterp']
flag_NO = G.flagGrid('logN2O', -1)
flag_U = G.flagGrid('logUin', -2.)

G.tabGrid['linF5876'] = 10**(G.tabGrid['F5876'])

#ff = flag_NO & flag_U 
#ff = np.ones(G.nGrid, 'bool')
ff = flag_U
ax = plt.subplot2grid(subp, (0, 0))
a = pl.plot_table_xy(G.tabGrid, ylab = 'QHe_QH', xlab = 'linF5876', flag = (ff), pltTitle = False, colours='b', s=1, marker = 'o', c=(12+G.tabGrid['OXYGEN'])[ff], cmap=cmap, lw=0, zorder = 0)

# Colorbar
cb = nplot.tight_colorbar(a, zorder = 0)
cb.set_label('12 + %s' % pl.label('OXYGEN'))
cbarytks = plt.getp(cb.ax.axes, 'yticklines')
plt.setp(cbarytks, visible=False)

cb.set_alpha(1)
cb.solids.set_edgecolor("face")
nplot.fix_colorbar_ticks(cb, 5, steps=[0.1, 0.2, 0.5])
nplot.fix_ticks(ax, 3, 4)

fig.set_tight_layout(True)
#pl.save_figs_paper(fig, 'grid_QHeQHvs5876_OH')



#=========================================================================
# ==> T NII vs T OIII & N+/N vs O+/O for fake samples
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=18)
fig = plt.figure(6)
plt.clf()

# Plots for Appendix A
scenVars = {'age': np.arange(1, 7), 'fr': [0.03, 3.00]}
scens = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]
Ss = {}

# Create a list of fake sources scenarios
sScenarios = []
for scen in scens:
    s = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scen.items()])
    sScenarios.append(s)

makeSTable = False
    
try:
    allS
except:
    allS = astropy.table.Table()
    makeSTable = True
    
for sScenario in sScenarios:
    fileName = os.path.join(dataDir, 'sources/fakeSources_tab_HII_15_All_interpTests_s%s.txt' % sScenario)
    S = slr.readSources(os.path.join(dataDir, 'fakeSources %s' % sScenario), fileName = fileName, tableIsInLog = True, useLog = True, tableHasLineRatios = True, debug = False, forceRead = False)
    Ss[sScenario] = S
    if makeSTable:
        allS = astropy.table.vstack([allS, S.tabData])

makeSTable = False

subp = 1, 2

for T in ['T_OXYGEN_vol_2', 'T_OXYGEN_vol_1']:
    allS['%s/1e3' % T] = allS[T] * 1e-3

for ionAb in ['A_OXYGEN_vol_1', 'A_NITROGEN_vol_1']:
    allS['lg%s' % ionAb] = utils.safe_log10(allS[ionAb])
    
c = pl.colour('fake')

ax1 = plt.subplot2grid(subp, (0, 0))
pl.plot_table_xy(allS, 'T_OXYGEN_vol_2/1e3', 'T_OXYGEN_vol_1/1e3', colours=c, marker='o', s=10, alpha=0.3, pltTitle=False, zorder = 0)
nplot.fix_ticks(ax1)

ax2 = plt.subplot2grid(subp, (0, 1))
pl.plot_table_xy(allS, 'lgA_OXYGEN_vol_1', 'lgA_NITROGEN_vol_1', colours=c, marker='o', s=10, alpha=0.3, pltTitle=False, zorder = 0)
nplot.fix_ticks(ax2)
nplot.plot_identity(allS['lgA_OXYGEN_vol_1'], allS['lgA_NITROGEN_vol_1'], c='#999999', zorder = 1, lw = 2)
plt.xlim(-1.8, 0.05)
plt.ylim(-1.8, 0.05)

# Dispersions in TN, TO
plt.sca(ax1)
TO3_min, TO3_max = 0, 25000.

TO3 = np.arange(TO3_min, TO3_max, 100.)
TN2 = (0.70 * TO3 + 3000.)

plt.plot(TO3*1e-3, TN2*1e-3, c='#999999', zorder = 1, lw = 2)
#plt.xlim(TO3_min*1e-3, TO3_max*1e-3)
plt.xlim(0, 20)
plt.ylim(0, 20)

TO3_g = allS['T_OXYGEN_vol_2']
TN2_g = allS['T_OXYGEN_vol_1']
TN2_l = (0.70 * TO3_g + 3000.)
diff = TN2_g - TN2_l
print 'Diff TN2: mean +- rms:', np.mean(diff), np.std(diff)

TO3_l = ((TN2_g - 3000.) / 0.70)
diff = TO3_g - TO3_l
print 'Diff TO3: mean +- rms:', np.mean(diff), np.std(diff)


# Dispersions in ionic abundances
aux = allS['lgA_OXYGEN_vol_1'] - allS['lgA_NITROGEN_vol_1']
print 'Diff N+/N and O+/O:', np.mean(aux), np.std(aux)

pl.label_panels(fig)
fig.set_tight_layout(True) 
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'TN2vsTN3_NIINvsOIIO')


#=========================================================================
# EOF


