'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

fchi = Fs['chi2']
f = fchi

flag_sampleB = p1.sampleB(S)

##########################################################################
# N/O vs O/H
psetup(fig_width_pt=textwidth, aspect=0.7, lw=1.5, fontsize=20, override_params={'figure.subplot.wspace': 0.0})
fig = plt.figure(1)
plt.clf()

# O/H from MacGaugh 91 (Kewley & Ellison 2008, A1, A2)
OH = {}
ylab = S.tabData['O3O2']
xlab = S.tabData['O23']
M_OH_low = -4.944 + 0.767 * xlab + 0.602 * xlab**2 - ylab * (0.29 + 0.332 * xlab - 0.331 * xlab**2)
M_OH_upp = -2.939 - 0.2 * xlab - 0.237 * xlab**2 - 0.305 * xlab**3 - 0.0283 * xlab**4 - ylab * (0.0047 - 0.0221 * xlab - 0.102 * xlab**2 - 0.0817 * xlab**3 - 0.00717 * xlab**4)

M_OH = np.where( (S.tabData['N2O2'] < -1.2), M_OH_low, M_OH_upp)
M_OH = np.where( (S.tabData['O3O2'] > -999) & (S.tabData['O23'] > -999), M_OH, -999)
OH['M91_OH_-1.2'] = M_OH

M_OH = np.where( (S.tabData['N2O2'] < -1.0), M_OH_low, M_OH_upp)
M_OH = np.where( (S.tabData['O3O2'] > -999) & (S.tabData['O23'] > -999), M_OH, -999)
OH['M91_OH_-1.0'] = M_OH

# Plots
subp = 1, 2

# BOND vs M91
x = f.PDFSummaries__tcps['jmod']['all']['OXYGEN'][..., 0]
y = OH['M91_OH_-1.2']

ff = flag_sampleB
c = pl.colour('SL')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{M91}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)

y = OH['M91_OH_-1.0']

ax = plt.subplot2grid(subp, (0, 1)) 
plt.scatter(12+x[ff], 12+y[ff], marker='.', s=60, c=c, lw = 0, zorder = 10, alpha=0.7)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)
plt.setp(ax.get_yticklabels(), visible=False)


pl.adjust_subplots(fig)
pl.label_panels(fig, addToLabel=[r'N2O2 $< -1.2$', r'N2O2 $< -1.0$'])
pl.save_figs_paper(fig, 'compare_M91')

#=========================================================================
# EOF
