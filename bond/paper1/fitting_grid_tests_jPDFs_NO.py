'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re
import itertools

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dirName = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
G1s = slr.read_OurGrids(grid, scen, forceRead = False)
G2s = slr.gridScenarios('Ginterp_scen', gridName = 'Ginterp', forceRead = False, scenVars = {'age': [2], 'fr': [0.03, 3.00]})

suffix = '_'.join(obsFit__f)
Gs = G2s
Gs.update(G1s)
gScenarios = Gs.keys()
    
#=========================================================================
# ===> Def pars & fit sources
errCookingFactor = 1.
physPars4PDF = ['logUin', 'OXYGEN', 'logN2O', 'fr', 'age']
extraLines4PDF = ['F3727', 'F4074', 'F4363', 'F5007', 'F5755', 'F5876', 'F6312', 'F6584', 'F6724', 'F7135', 'F7325', 'F9069', 'O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RN2', 'Ar3Ne3', 'F5876']
parName__p = np.unique(physPars4PDF + obsFit__f + extraLines4PDF)

overwrite = True
debug = True
errCookingFactor = 1.

#=========================================================================
# ===> Read fake sources
#scenVars = {'age': [1, 2, 3, 4, 5, 6], 'fr': [0.03, 3.00]}
scenVars = {'age': [4, 2], 'fr': [0.03]}
scens = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]
Ss = {}

# Create a list of fake sources scenarios
sScenarios = []
for scen in scens:
    s = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scen.items()])
    sScenarios.append(s)

# Create a list of fits
fScenarios = []
for sScenario in sScenarios:
    for gScenario in gScenarios:
        fScenarios.append( [sScenario, gScenario] )
        if (sScenario == 'Sage4.00fr0.03') & (gScenario != 'Sage2.00fr0.03'):
            fScenarios.pop()
# Fit
for sScenario, gScenario in fScenarios[-2:-1]:
#++for sScenario, gScenario in fScenarios[-1:]:
#++for sScenario, gScenario in fScenarios:
    print "@@ Reading fake sources for scenario %s." % sScenario

    fileName = os.path.join(dirName, 'sources/fakeSources_tab_HII_15_All_interpTests_s%s.txt' % sScenario)
    S = slr.readSources(os.path.join(dirName, 'fakeSources %s' % sScenario), fileName = fileName, tableIsInLog = True, useLog = True, tableHasLineRatios = True, debug = False, forceRead = True)
    Ss[sScenario] = S

    
    #=========================================================================
    # ===> Fitting
    
    # ===> Naming fit
    G = Gs[gScenario]
    Fscenario = 's%s_g%s' % (sScenario, gScenario)
    print "@@> Fitting sources %s with grid %s (%s)" % (sScenario, gScenario, Fscenario)
    
    
    # ===> Output file & dir
    suffix = '_'.join(obsFit__f) + '_chi2'
    dirName = os.path.join(dirName, 'fits_fs/')
    baseName = 'fit_%s_%s' % (Fscenario, suffix)
    
    
    # ===> Choose sources
    #iSources = np.arange(S.nSources)
    #iSources = [250, 269]
    #iSources = np.arange(0, 100)
    iSources = np.random.randint(0, S.nSources, 100)
    
    # ===> Fit sources 
    print "@@> Starting %s" % baseName


    # ==> Tests for one U
    #G1 = G.selectScenarios(scenVars = {'logUin': [-2]})
    #G = G1['SlogUin-2.00']

    
    iSources = [131]
    err = S.tabData['eF5007'][iSources].copy()
    S.tabData['eF6584'][iSources] = err #* 0.5
    S.tabData['eF3727'][iSources] = err #* 0.5
    S.tabData['eF5007'][iSources] = err #* 0.25
    
    # ===> Start fitting
    f = sl.fit(grid = G, sources = S, gridParts = ['all'], sourcesRows = iSources, errCookingFactor = errCookingFactor, obsFit__f = obsFit__f, parName__p = parName__p, calcAll = False, baseName = baseName, dirName = dirName, removePars = True, debug = debug)

    f.calcLikelihood(mode='chi2')
    #f.normalizeLikelihood()
    
    f.stepProb()
    f.gaussianProb(obs = ['Ar3Ne3'], nSigma = 1.)
    f.gaussianProb(obs = ['F5876'], nSigma = 1.)
    f.calcNSolutions()
            
    # Calc PDFs
    f.calcPDFsAll()
    f.calcPDFSummaries()
    
    
    jPDF__j, Xgrid = f.calcJointPDF(['OXYGEN', 'logN2O'], branch='all', iSource=iSources[0]) 
    #jPDF__j = f.jPDF__cjs['all'][..., iSources[0]]
    #Xgrid = f.bins__j
    
    import seaborn as sns
    plt.figure()
    plt.clf()
    plt.title(iSources[0])
    #plt.xlim(7, 7.5)
    #plt.ylim(-1.5, -1)
    im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = sns.light_palette('b', as_cmap=True), vmin=0, vmax=0.5)
    cb = plt.colorbar()
    plt.plot(S.tabData['OXYGEN'][iSources[0]] + 12, S.tabData['logN2O'][iSources[0]], 'r*')

    '''
    f_sources = [f.fitOneSource(iSource) for iSource in np.arange(S.nSources)]
    fileNames = [f.sourceFileName(iSource, baseName, nChars = 4, dirName = dirName) for iSource in np.arange(S.nSources)]
    
    def fit_source(iSource):
        print '@@> Fitting source %s' % iSource

        import time
        t1 = time.time()
        
        # Temp file name for this source
        fileName = fileNames[iSource]
        
        if not (os.path.exists(fileName)):
            f_source = f_sources[iSource]
            
            # Calc likelihood for strong lines
            f_source.calcLikelihood(mode='chi2')
            
            # Calc likelihood for semi-sstrong lines
            f_source.stepProb()
            f_source.gaussianProb(obs = ['Ar3Ne3'], nSigma = 1.)
            f_source.gaussianProb(obs = ['F5876'], nSigma = 1., linear=True)
            f_source.calcNSolutions()
            
            # Calc PDFs
            f_source.calcPDFsAll()
            f_source.calcPDFSummaries()

            # Save temp file for source
            #f_source.saveToHDF5(overwrite = overwrite, fileName=fileName, saveSourceFit=True, saveProb=True)

        print '@@> Fitted source %s in %s s' % (iSource, time.time()-t1)

    # No parallel computing
    #for iSource in f.sourcesRowsFitted:
    #    fit_source(iSource)

    # https://pythonhosted.org/joblib/parallel.html
    #import tempfile
    #import os
    #from joblib import load, dump
    #
    #temp_folder = tempfile.mkdtemp()
    #filename = os.path.join(temp_folder, 'joblib_test.mmap')
    #if os.path.exists(filename): os.unlink(filename)
    #_ = dump(G.tabGrid, filename)
    #large_memmap = load(filename, mmap_mode='r+')

    # Parallel computing
    from joblib import Parallel, delayed
    Parallel(n_jobs=3)(delayed(fit_source)(iSource) for iSource in f.sourcesRowsFitted)

    #from multiprocessing import Pool
    #p = Pool(processes=4, maxtasksperchild=9)
    #p.map(fit_source, np.arange(65,71))
    #p.close()
    #p.join()


    # Consolidate results into a single file
    #++f.saveSourcesToSingleFiles()

    # Test file reading
    #f2 = slr.readFit('lix', fileName = 'fits_fs/fit_sSage2.00fr0.03_gGinterp_F3727_F5007_F6584_chi2_summary.hdf5', forceRead = True)
    #f2.loadProbData(baseName=baseName, dirName=dirName)
    #f2.loadDataSources([250, 269], debug=True)

    # Calc likelihoods and apply priors
    f.calcLikelihood(mode='chi2')
    f.stepProb()
    f.gaussianProb(obs = ['Ar3Ne3'], nSigma = 1.)
    f.gaussianProb(obs = ['F5876'], nSigma = 1., linear=True)
    f.calcNSolutions()

    ## Calc posterior and PDFs
    #f.calcPosterior()
    f.calcPDFsAll()
    f.calcPDFSummaries()
            
    # Save fits
    f.saveToHDF5(overwrite = overwrite)
    tab = f.saveToTxt(overwrite = overwrite)
    f.saveFittedSourcesToHDF5(overwrite = overwrite)
    f.saveProbToHDF5(overwrite = overwrite)
    '''
    
'''
    # ===> Plots

    columnwidth = 240.
    textwidth = 504.
    screenwidth = 1024.
    psetup = nplot.plotSetupMinion
    #psetup = nplot.plotSetup

    psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=15)
    fig = plt.figure(1)
    plt.clf()
    nplot.title_date('grid vs grid %s' % (suffix.replace('_', ' ')))

    subp = 4, 5

    f = sl.fit(grid = G, sources = S, gridParts = ['all'], sourcesRows = iSources, errCookingFactor = errCookingFactor, obsFit__f = obsFit__f, parName__p = parName__p, calcAll = False, baseName = baseName, dirName = dirName, debug = debug)

    f.calcLikelihood(mode='chi2')
    pl.plots_fittingPDFs_check(f, iSources[0], branches = ['all'], drow=0, title=r'Fitting log [NII]/H$\beta$, log [OII]/H$\beta$, log [OIII]/H$\beta$', recalc=True)
    
    f.stepPrior()
    pl.plots_fittingPDFs_check(f, iSources[0], branches = ['all'], drow=1, title=r'+ upper-limit step prior for F4363, F5755, F6312, F7135, F7325', recalc=True)

    f.gaussianPrior(obs = ['Ar3Ne3'], nSigma = 1.)
    pl.plots_fittingPDFs_check(f, iSources[0], branches = ['all'], drow=2, title=r'+ 1$\sigma$ gaussian prior in log [ArIII]/[NeIII]', recalc=True)
    
    f.gaussianPrior(obs = ['F5876'], nSigma = 1.)
    pl.plots_fittingPDFs_check(f, iSources[0], branches = ['all'], drow=3, title=r'+ 1$\sigma$ gaussian prior in HeI/H$\beta$', recalc=True)

    nplot.save(fig, 'fitting_%s_s%04i.png' % (baseName, iSources[0]))
    '''

    
#=========================================================================
# EOF

