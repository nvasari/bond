'''
N/O vs O/H for Te and Pilyugin ON.

Natalia@Corrego - 03/Nov/2015
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
S = slr.readSources(sources, debug = False, forceRead = False)

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
#psetup = nplot.plotSetupMinion
psetup = nplot.plotSetup

'''+++ 
##########################################################################
# Calc S/N for the objects in our sample
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(1)
plt.clf()

# Plots
subp = 2, 3

lines = ['F3727', 'F5007', 'F6584']
SN_strongLines = []

for il, line in enumerate(lines):

    SN = utils.mask_minus999( utils.safe_pow(np.log(10.) * S.tabData['e%s' % line], -1) )
    SN_strongLines.append(SN)

    ax = plt.subplot2grid(subp, (0, il), aspect='equal')
    plt.hist(SN[~SN.mask], bins= 20)
    plt.title(line)
    plt.xlim(0, 100)
    nplot.fix_ticks(ax, nx = 5)

SN_strongLines = utils.mask_minus999(SN_strongLines)
ax = plt.subplot2grid(subp, (1, 0), aspect='equal')
plt.hist(SN_strongLines[~SN_strongLines.mask], bins = 20)
plt.title("O2, O3, N2")
#plt.xlim(0, 100)
#nplot.fix_ticks(ax, nx = 5)

   
fig.set_tight_layout(True)
+++'''

'''+++
##########################################################################
# Calc S/N for the objects in our sample
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(2)
plt.clf()

# Plots
subp = 2, 3

lines = ['F3727', 'F5007', 'F6584']

for il, line in enumerate(lines):

    F = utils.safe_pow(10, S.tabData[line])
    dF = np.log(10.) * S.tabData['e%s' % line] * F
    print zip(F, dF)

    ax = plt.subplot2grid(subp, (0, il)) #, aspect='equal')
    plt.scatter(F, dF)
    plt.title(line)
    nplot.fix_ticks(ax, nx = 5)
    
    ax = plt.subplot2grid(subp, (1, il)) #, aspect='equal')
    plt.scatter(S.tabData[line], S.tabData['e%s' % line])
    plt.title(line)
    nplot.fix_ticks(ax, nx = 5)

fig.set_tight_layout(True)
+++'''

##########################################################################
# Check linear uncertainties
psetup(fig_width_pt=textwidth, aspect=1.5, lw=1., fontsize=15)

Slin = slr.readSources('%s-lin' % sources, debug = False, forceRead = False, useLog = True)

flag_sampleA = p1.sampleA(S)
flag_sampleB = p1.sampleB(S)

ff = flag_sampleB

F3727  = Slin.tabData['F3727']
eF3727 = Slin.tabData['eF3727']
SNF3727 = F3727 / eF3727

F3869  = Slin.tabData['F3869']
eF3869 = Slin.tabData['eF3869']
SNF3869 = F3869 / eF3869

F5007  = Slin.tabData['F5007']
eF5007 = Slin.tabData['eF5007']
SNF5007 = F5007 / eF5007

F5876  = Slin.tabData['F5876']
eF5876 = Slin.tabData['eF5876']
SNF5876 = F5876 / eF5876

F6584  = Slin.tabData['F6584']
eF6584 = Slin.tabData['eF6584']
SNF6584 = F6584 / eF6584

F7135  = Slin.tabData['F7135']
eF7135 = Slin.tabData['eF7135']
SNF7135 = F7135 / eF7135

Ar3Ne3  = Slin.tabData['Ar3Ne3']
eAr3Ne3 = Slin.tabData['eAr3Ne3']
SNAr3Ne3 = Ar3Ne3 / eAr3Ne3

fig = plt.figure(3)
plt.clf()
subp = 3, 2

ax = plt.subplot2grid(subp, (0, 0))
plt.plot(SNF5007[ff], SNF3727[ff], '.')
nplot.plot_identity(SNF5007[ff], SNF3727[ff])
plt.xlabel('SNF5007')
plt.ylabel('SNF3727')

ax = plt.subplot2grid(subp, (0, 1))
plt.plot(SNF5007[ff], SNF6584[ff], '.')
nplot.plot_identity(SNF5007[ff], SNF6584[ff])
plt.xlabel('SNF5007')
plt.ylabel('SNF6584')

ax = plt.subplot2grid(subp, (1, 0))
plt.plot(eF5007[ff], eF3727[ff], '.')
nplot.plot_identity(eF5007[ff], eF3727[ff])
plt.xlabel('eF5007')
plt.ylabel('eF3727')

ax = plt.subplot2grid(subp, (1, 1))
plt.plot(eF5007[ff], eF6584[ff], '.')
nplot.plot_identity(eF5007[ff], eF6584[ff])
plt.xlabel('eF5007')
plt.ylabel('eF6584')

ax = plt.subplot2grid(subp, (2, 0))
plt.plot(F5007[ff], F3727[ff], '.')
nplot.plot_identity(F5007[ff], F3727[ff])
plt.xlabel('F5007')
plt.ylabel('F3727')

ax = plt.subplot2grid(subp, (2, 1))
plt.plot(F5007[ff], F6584[ff], '.')
nplot.plot_identity(F5007[ff], F6584[ff])
plt.xlabel('F5007')
plt.ylabel('F6584')

fig.set_tight_layout(True)



fig = plt.figure(4)
plt.clf()
subp = 3, 2

ff = flag_sampleB

xlow, xupp = 0, 50

ax = plt.subplot2grid(subp, (0, 0))
plt.hist(SNF3727[ff], histtype='step')
plt.xlabel('SNF3727')
plt.axvline(np.percentile(SNF3727[ff], 25), color='k')
plt.axvline(np.percentile(SNF3727[ff], 50), color='k')
plt.xlim(xlow, xupp)

ax = plt.subplot2grid(subp, (0, 1))
plt.hist(SNF3869[ff], histtype='step')
plt.xlabel('SNF3869')
plt.axvline(np.percentile(SNF3869[ff], 25), color='k')
plt.axvline(np.percentile(SNF3869[ff], 50), color='k')
plt.xlim(xlow, xupp)

ax = plt.subplot2grid(subp, (1, 0))
plt.hist(SNF5007[ff], histtype='step')
plt.xlabel('SNF5007')
plt.axvline(np.percentile(SNF5007[ff], 25), color='k')
plt.axvline(np.percentile(SNF5007[ff], 50), color='k')
plt.xlim(xlow, xupp)

ax = plt.subplot2grid(subp, (1, 1))
plt.hist(SNF5876[ff], histtype='step')
plt.xlabel('SNF5876')
plt.axvline(np.percentile(SNF5876[ff], 25), color='k')
plt.axvline(np.percentile(SNF5876[ff], 50), color='k')
plt.xlim(xlow, xupp)

ax = plt.subplot2grid(subp, (2, 0))
plt.hist(SNF6584[ff], histtype='step')
plt.xlabel('SNF6584')
plt.axvline(np.percentile(SNF6584[ff], 25), color='k')
plt.axvline(np.percentile(SNF6584[ff], 50), color='k')
plt.xlim(xlow, xupp)

ax = plt.subplot2grid(subp, (2, 1))
plt.hist(SNF7135[ff], histtype='step')
plt.xlabel('SNF7135')
plt.axvline(np.percentile(SNF7135[ff], 25), color='k')
plt.axvline(np.percentile(SNF7135[ff], 50), color='k')
plt.xlim(xlow, xupp)


fig = plt.figure(5)
plt.clf()
subp = 3, 2

ff = flag_sampleB

ax = plt.subplot2grid(subp, (0, 0))
plt.plot(F7135[ff], eF7135[ff], '.')
