'''
N/O vs O/H for Te and Pilyugin ON.

Natalia@Corrego - 03/Nov/2015
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

import paper1_stats as p1

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
S = slr.readSources(sources, debug = False, forceRead = False)
S_MC = slr.readSources('SPIRALS-HEBCD-MC', debug = False, forceRead = False)

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

##########################################################################
# N/O vs O/H for Te and Pilyugin ON
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=15)
fig = plt.figure(1)
plt.clf()

# Plots
subp = 1, 2


# ********** Calc Piliyugin **********
# Start aux table
tabAux = astropy.table.Table()

# Calc Pilyugin O/H and N/O
# ON calibration from Pilyugin Vilchez Thuan 2010 

# Page 2: Defining line ratios
r2 = S.tabData['F3727']
r3 = utils.safe_sum(S.tabData['F5007'], np.log10(1.34))
n2 = utils.safe_sum(S.tabData['F6584'], np.log10(1.34))
F6716 = np.where(S.tabData['F6716'] > -999, 10**S.tabData['F6716'], 0)
F6731 = np.where(S.tabData['F6731'] > -999, 10**S.tabData['F6716'], 0)
s2 = utils.safe_log10(F6716 + F6731)

n2r2 = utils.safe_sub(n2, r2)
n2s2 = utils.safe_sub(n2, s2)

# Sec 3.2: ON calibration - 3.2, eq 19 & 20
AO_ON = np.full_like(r3, -999.)
AN_ON = np.full_like(r3, -999.)

_f = (n2 > -0.1)
AO_ON[_f] = (8.606 - 0.105*r3 - 0.410*r2 - 0.150*n2r2)[_f]
AN_ON[_f] = (7.955 + 0.048*r3 - 0.171*n2 + 1.015*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 > -0.25)
AO_ON[_f] = (8.642 + 0.077*r3 + 0.411*r2 + 0.601*n2r2)[_f]
AN_ON[_f] = (7.928 + 0.291*r3 + 0.454*n2 + 0.953*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 <= -0.25) #& (n2s2 > -999)
AO_ON[_f] = (8.013 + 0.905*r3 + 0.602*r2 + 0.751*n2r2)[_f]
AN_ON[_f] = (7.505 + 0.839*r3 + 0.492*n2 + 0.970*n2r2)[_f]

tabAux['AO_ON'] = utils.safe_sub(AO_ON, 12)
tabAux['AN_ON'] = utils.safe_sub(AN_ON, AO_ON)


# Get number of objects
flag_sampleON = (tabAux['AO_ON'] > -999) & (tabAux['AN_ON'] > -999)
flag_sampleT = p1.sampleT(S)
flag_sampleT_ON = flag_sampleT & flag_sampleON
flag_sampleT_ON_S2 = flag_sampleT & flag_sampleON & (s2 > -999)

print 'Sample ON: N = %s' % np.sum(flag_sampleON)
print 'Sample T: N = %s' % np.sum(flag_sampleT.data)
print 'Sample T & ON: N = %s' % np.sum(flag_sampleT_ON)
print 'Sample T & ON & S2: N = %s' % np.sum(flag_sampleT_ON_S2)

# ********** Calcs for Monte Carlo table **********
# Add galaxy number
S_MC.tabData['n_orig'] = np.arange(len(S.tabData)).repeat(200)

# ********** Flags **********
ff = flag_sampleT_ON_S2

flag_sampleT_MC = ff.data.repeat(200)

flag_sampleT_MC_nominal = np.zeros_like(flag_sampleT_MC)
flag_sampleT_MC_nominal[::200] = flag_sampleT_MC[::200]

f_MC = flag_sampleT_MC
ff_MC = flag_sampleT_MC_nominal

# Check one object
#ff = utils.ind2flag(ff, [5])
#f_MC = utils.ind2flag(f_MC, np.arange(1000, 1200))
# ********** Uncertainties from Monte Carlo **********

saveMCtable = False
if saveMCtable:

    # Create new table to save the dispersion and rho (off-diagonal factor on the covariance matrix)
    tab = {}
    Ps = ['OXYGEN', 'logN2O']
    for P in Ps:
        tab['ave_' + P] = np.full(S.nSources, -999.)
        tab['e'    + P] = np.full(S.nSources, -999.)
    tab['rho_' + '_'.join(Ps)] = np.full(S.nSources, -999.)

    # Calc the dispersions
    for _iMC, _i in zip(np.where(ff_MC)[0], np.where(ff)[0]):

        Xs  = [0] * len(Ps)
        ave = [0] * len(Ps)
        sig = [0] * len(Ps)
        
        for iP, P in enumerate(Ps):
            
            # Get MC results for this object
            values = S_MC.tabData[P][_iMC:_iMC+200]
            _f = (values > -990.)
            N = _f.sum()
            Xs[iP] = values[_f]
            
            # Calculate average
            average = Xs[iP].sum() / N
            ave[iP] = average

            # Calculate dispersion
            variance = ( (Xs[iP] - ave[iP])**2 ).sum() / N
            sig[iP] = np.sqrt(variance)

            # Save average & dispersion to table
            tab['ave_' + P][_i] = ave[iP]
            tab['e'    + P][_i] = sig[iP]
            print P, S.tabData[P][_i], ave[iP], sig[iP], np.std(values[_f])

        # Calc off-diagonal term for covariance matrix & save it
        var_xy = ( (Xs[0] - ave[0]) * (Xs[1] - ave[1]) ).sum() / N
        rho_xy = var_xy / (sig[0] * sig[1])
        tab['rho_' + '_'.join(Ps)][_i] = rho_xy


    # Save to txt file
    fileMC_unc = os.path.join(dataDir, 'sources/ABRESU.ALLm.uncert.txt')            
    astropy.table.Table(tab).write(fileMC_unc, format="ascii.fixed_width_two_line")

# ********** Plots **********
cTe = pl.colour('Te')
cSL = pl.colour('SL')
cTel = pl.colour('Te', 0.50)

ax = plt.subplot2grid(subp, (0, 0))
pl.plot_table_xy(S_MC.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = f_MC, colours = cTel, pltTitle = False, alpha = 0.20, marker='.', zorder = 0)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = ff, colours = cTe, pltTitle = False, ax=ax, pltNpoi = True, marker='.', s=30, zorder = 10)
pl.plot_table_ellipse_covMatrix(S.tabData, dx = 12, flag = ff, edgecolor = cTel, alpha = 0.5, zorder = 9)

# Check error bars
#plt.errorbar(12+S.tabData['OXYGEN'][ff], S.tabData['logN2O'][ff], xerr=S.tabData['eOXYGEN'][ff], yerr=S.tabData['elogN2O'][ff], fmt='none', ecolor=cl, elinewidth=2, capsize=0, alpha=0.2, zorder=1)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax, nx = 5)

ax = plt.subplot2grid(subp, (0, 1))
pl.plot_table_xy(tabAux, xlab = 'AO_ON', ylab = 'AN_ON', dx = 12, flag = ff, colours = cSL, pltTitle = False, ax=ax, pltNpoi = True, marker='.', s=30, zorder = 10)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax, nx = 5)

# Check that the flags for the MC file and the nominal file are the same
#ax = plt.subplot2grid(subp, (1, 0)) 
#pl.plot_table_xy(S_MC.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = ff_MC, colours = 'k', addToTitle = 'Te | ')
#plt.xlim(OH_min, OH_max)
#plt.ylim(NO_min, NO_max)
#nplot.fix_ticks(ax)

pl.label_panels(fig, addToLabel=[r'$T_e$', r'ON'])
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'NO_vs_OH_Te_P10')
##########################################################################
'''
'''
