'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import fittingHelper as fh
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

if grid == 'octr':
    gScenario = 'Goctree'
else:
    gScenario = 'Gorigin'
    
#=========================================================================
# ===> Fitting

# ===> Naming fit
G = slr.f[gScenario]
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
print "@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario)

# ===> Output file & dir
suffix = '_'.join(obsFit__f) + '_noSSL_chi2'
dirName =  os.path.join(dataDir, 'fits/')
baseName = 'fit_%s_%s' % (Fscenario, suffix)

# ===> Check these sources (they have Te measures, but BOND could not be applied)
print np.where(p1.sampleT(S) & ~p1.sampleB(S))

# ==> Create limits for those sources
f_lim = (S.tabData['limF5755'] <= -999) & (S.tabData['limF4363'] <= -999) & (S.tabData['F4363'] <= -999) & (S.tabData['F5755'] <= -999) & p1.sampleA(S)

# Add limit to 4363 based on 2x the lowest uncertainty in the flux of [OII], [OIII] and [NII]
dFfactor = 2.
lines = ['F3727', 'F5007', 'F6584']
limF = np.full(S.nSources, 1e30)
    
for il, line in enumerate(lines):
    # Calc F, dF for this line
    F = utils.safe_pow(10, S.tabData[line])
    dF = dFfactor * np.log(10.) * S.tabData['e%s' % line] * F

    # Save upper limit
    flagLim = (dF < limF)
    limF[flagLim] = dF[flagLim]

print '@@> Created upper limits for %s/%s objects.' % (f_lim.sum(), S.nSources)

aux = utils.safe_log10(limF[f_lim])
S.tabData['limF4363'][f_lim] = aux
S.tabData['limF5755'][f_lim] = aux

# ===> Choose sources
flag_sampleA = p1.sampleA(S)
ff = flag_sampleA
_iSources = np.where(ff)[0]
iSources = _iSources
#iSources = np.where(p1.sampleB(S))[0]
#iSources = [4]

# ===> Fit sources 
print "@@> Starting %s" % baseName

f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = iSources, setupParBins = True)
fh.run(nProc = 1, wait = 0.1, fitAll = False, constraints = ['SL', 'upperLim'])

# Consolidate results into a single file
f.combineSources_singleFile(iSources = iSources, reAddSources = False)

# Add sources after a bug
#iSources_done = iSources[iSources < 701]
#f.combineSources_singleFile(iSources = [701], iSources_done = iSources_done, reAddSources = False)


# Fit one source
'''
f_source = f.fitOneSource(18)
f_source = fh.fit_constraints(f_source, constraints = ['SL', 'upperLim'])
f_source.octree_grid(fh.BONDfit_newFit_octree)
f_source.calcCrossValidation(fh.BONDfit_newFit_octree)
f_source.calcBinnedPDFs()
f_source.calcPDFSummaries(debug = True)

f_source = f.fitOneSource(18)
f_source = fh.fit_constraints(f_source, constraints = ['SL', 'upperLim', 'Ar3Ne3', 'HeI'])
f_source.octree_grid(fh.BONDfit_newFit_octree)
f_source.calcCrossValidation(fh.BONDfit_newFit_octree)
f_source.calcBinnedPDFs()
f_source.calcPDFSummaries(debug = True)
#f_source.saveToHDF5(fileName = 'lix.hdf5', overwrite = True)
'''

#=========================================================================
# EOF


