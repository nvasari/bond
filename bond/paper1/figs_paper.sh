#!/bin/bash

# Natalia@Corrego - 20/Dec/2015

# ** Fig 1
python plots_Te_ON.py

# ** Fig 2--7, A2
python plots_grid.py

# ** Fig 8--11
python plots_fitting_source.py
python plots_marg_uncertainties.py

# ** Fig 12
python plots_NO_vs_OH.py

# ** Fig 13
python plots_compare_solutions.py

# ** Fig 14
python plots_compare_Te.py

# ** Fig A1, B1, C5
python plots_fitting_grid.py

# ** Fig C1
python plots_compare_M91.py

# ** Fig C2
python plots_compare_O23.py

# ** Fig C3
python plots_compare_gNO.py

# ** Fig C4
python plots_compare_PM14.py

