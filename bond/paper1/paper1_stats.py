'''
Get stats for paper1.

Natalia@Corrego - 03/Nov/2015
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import bond.strongLinesHelper as sl

# Clever reader
import bond.strongLinesReader as slr

# Some useful functions
import bond.natastro.plotutils as nplot
import bond.natastro.utils as utils
import bond.plotHelpers as pl

#=========================================================================
# Subsamples

def sampleA(S):
    # All (entire sample)
    flag = (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['F6584'] > -999)
    return flag.data

def sampleT(S):
    # Te sample
    flag = S.tabData['flagOk_ab']
    return flag.data

def sampleB(S):
    # BOND sample
    flag = (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['F6584'] > -999) & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['F5876'] > -999)
    return flag.data

if __name__ == "__main__":
    #=========================================================================
    # ==> Read sources & grid
    obsFit__f, grid, scen, sources = slr.options(*sys.argv)
    S = slr.readSources(sources, debug = False, forceRead = False)

    flag_sampleA = sampleA(S)
    flag_sampleT = sampleT(S)
    flag_sampleB = sampleB(S)

    # Print stats for samples
    print ('Sample All: N = %s' % len(S.tabData))
    print ('Sample A: N = %s' % np.sum(flag_sampleA))
    print ('Sample T: N = %s' % np.sum(flag_sampleT))
    print ('Sample B: N = %s' % np.sum(flag_sampleB))
    print ('Sample T & B: N = %s' % np.sum(flag_sampleT & flag_sampleB))
