'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

fchi = Fs['chi2']
f = fchi

##########################################################################
aux = S.tabData[['F3727', 'F4363', 'F5007', 'F6584', 'F6724']].copy()
#del aux.meta['comments']
for k in aux.keys():
    aux[k] = 10.**aux[k]

saveInput = False

if saveInput:
    aux.write("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/input_SPIRALS-HEBCD.txt", format='ascii.fixed_width_no_header', delimiter=None, fill_values=[(astropy.io.ascii.masked, 0)], formats={k: '%10.5e' for k in aux.keys()})
    
    aux['F4363'] = 0
    aux.write("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/input_SPIRALS-HEBCD_noF4363.txt", format='ascii.fixed_width_no_header', delimiter=None, fill_values=[(astropy.io.ascii.masked, 0)], formats={k: '%10.5e' for k in aux.keys()})

# Terminal:
#cd ~/projects/strong_lines/HII-CHI-mistry_v01
#python HII-CHI-mistry_v01.py 

flag_sampleB = p1.sampleB(S)


# Read PM14 results
#p1 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_noF4363_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
#p2 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p3 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_noF4363_v01.2.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p4 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_v01.2.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p5 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_muestra_4363_v01.2.dat", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'OXYGENTe', 'logN2OTe', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')

#p1['OXYGEN'] -= 12.
#p2['OXYGEN'] -= 12.
p3['OXYGEN'] -= 12.
p4['OXYGEN'] -= 12.
p5['OXYGEN'] -= 12.



'''+++
##########################################################################
# N/O vs O/H - all comparisons
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=1., fontsize=15)
fig = plt.figure(1)
plt.clf()
nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))

subp = 4, 5

# N/O vs O/H - Te
ff = S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = r'$T_\mathrm{e}$', colours='b')
plt.xlim(6.9, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

# N/O vs O/H - BOND
ff2 = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (1, 0)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'jpdf', flag = (ff2),  addToTitle = r'BOND $\chi^2$', pltLegend=False, colours='r')
plt.xlim(6.9, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 0)) 
pl.plot_table_xy(p3, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff2), addToTitle = 'PMv1.2-noF4363 ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 1)) 
pl.plot_table_xy(p4, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff2), addToTitle = 'PMv1.2-F4363 ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ff3 = (p4['grid'] != 2) & (p4['grid'] != 3)
ax = plt.subplot2grid(subp, (2, 2)) 
pl.plot_table_xy(p4, xlab = 'OXYGEN', ylab = 'logN2O', flag = (ff3), dx = 12, addToTitle = 'PMv1.2-F4363 ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 3)) 
pl.plot_table_xy(p4, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1.2-F4363 ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 0)) 
pl.plot_table_xy(p5, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1.2-muestraCM ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)


fig.set_tight_layout(True)
nplot.save(fig, 'compare_Te_PM14_check.pdf')
+++'''

##########################################################################
# N/O vs O/H - paper
psetup(fig_width_pt=textwidth, aspect=0.6, lw=1., fontsize=12, override_params={'figure.subplot.wspace': 0.4, 'figure.subplot.hspace': 0.4, 'figure.subplot.top': 0.95, 'figure.subplot.bottom': 0.2})

ff = (p4['grid'] != 2) & (p4['grid'] != 3)
cS = pl.colour('SL')
cB = pl.colour('BOND')


def fig_pm14(pm14, ix=0):
    subp = 2, 3

    flag = flag_sampleB & ff

    print 'Showing %s objects; from sample B and where grid != 2 or 3 for PM14' % flag.sum()

    ax = plt.subplot2grid(subp, (ix, 0)) 
    pl.plot_table_xy(pm14, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (flag), pltTitle = False, colours=cS, s=10, marker='o', alpha=0.7)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    nplot.fix_ticks(ax)

    
    # BOND vs PM14
    x = f.PDFSummaries__tcps['jmod']['all']['OXYGEN'][..., 0]
    y = pm14['OXYGEN']
    #xerr = np.array([x - f.PDFSummaries__tcps['p16']['all']['OXYGEN'], f.PDFSummaries__tcps['p84']['all']['OXYGEN'] - x])
    yerr = pm14['eOXYGEN']
    
    ax = plt.subplot2grid(subp, (ix, 1)) 
    plt.scatter(12+x[flag], 12+y[flag], c=cS, s=10, marker='o', lw = 0, alpha=0.7)
    nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('OXYGEN'))
    plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
    plt.ylabel('12 + %s$_\mathrm{PM14}$' % pl.label('OXYGEN'))
    nplot.fix_ticks(ax)
    
    
    x = f.PDFSummaries__tcps['jmod']['all']['logN2O'][..., 0]
    y = pm14['logN2O']
    #xerr = np.array([x - f.PDFSummaries__tcps['p16']['all']['logN2O'], f.PDFSummaries__tcps['p84']['all']['logN2O'] - x])
    yerr = pm14['elogN2O']
    
    ax = plt.subplot2grid(subp, (ix, 2)) 
    plt.scatter(x[flag], y[flag], c=cS, s=10, marker='o', lw = 0, alpha=0.7)
    nplot.plot_identity(np.array([-2.2, -0.2]), np.array([-2.2, -0.2]), color = '#666666', lw = 1)
    plt.xlabel('%s$_\mathrm{BOND}$' % pl.label('logN2O'))
    plt.ylabel('%s$_\mathrm{PM14}$' % pl.label('logN2O'))
    plt.xlim(pl.lims('logN2O'))
    plt.ylim(pl.lims('logN2O'))
    plt.xlim(-2.2, -0.2)
    plt.ylim(-2.2, -0.2)
    nplot.fix_ticks(ax)


fig = plt.figure(2)
plt.clf()
fig_pm14(p3, 0)
fig_pm14(p4, 1)

pl.label_panels(fig, x = 0.07)
pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'compare_PM14')

#=========================================================================
# EOF
