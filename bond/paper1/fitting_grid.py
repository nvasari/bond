'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re
import itertools

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import fittingHelper as fh

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
G1s = slr.read_OurGrids(grid, scen, forceRead = False)
G2s = slr.gridScenarios('Goctr_scen', gridName = 'Goctree', forceRead = False, scenVars = {'age': [2], 'fr': [0.03, 3.00]})

suffix = '_'.join(obsFit__f)
Gs = G2s
Gs.update(G1s)
gScenarios = Gs.keys()
    
#=========================================================================
# ===> Read fake sources
#scenVars = {'age': [1, 2, 3, 4, 5, 6], 'fr': [0.03, 3.00]}
scenVars = {'age': [4, 2], 'fr': [0.03]}
scens = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]
Ss = {}

# Create a list of fake sources scenarios
sScenarios = []
for scen in scens:
    s = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scen.items()])
    sScenarios.append(s)

# Create a list of fits
fScenarios = []
for sScenario in sScenarios:
    for gScenario in gScenarios:
        fScenarios.append( [sScenario, gScenario] )
        if (sScenario == 'Sage4.00fr0.03') & (gScenario != 'Sage2.00fr0.03'):
            fScenarios.pop()

# Fit
#++for sScenario, gScenario in fScenarios[::-1]:
for sScenario, gScenario in fScenarios[-1:]:
    
    print "@@> Reading fake sources for scenario %s." % sScenario

    fileName = os.path.join(dataDir, 'sources/fakeSources_tab_HII_15_All_interpTests_s%s.txt' % sScenario)
    S = slr.readSources('fakeSources %s' % sScenario, fileName = fileName, tableIsInLog = True, useLog = True, tableHasLineRatios = True, debug = False, forceRead = True)
    Ss[sScenario] = S
    
    #=========================================================================
    # ===> Fitting
    
    # ===> Naming fit
    G = Gs[gScenario]
    Fscenario = 's%s_g%s' % (sScenario, gScenario)
    print "@@> Fitting sources %s with grid %s (%s)" % (sScenario, gScenario, Fscenario)
    
    # ===> Output file & dir
    suffix = '_'.join(obsFit__f) + '_chi2'
    dirName = os.path.join(dataDir, 'fits_fs/')
    baseName = 'fit_%s_%s' % (Fscenario, suffix)
    
    
    # ===> Choose sources
    _iSources = np.arange(S.nSources)
    iSources = _iSources

    #iSources = np.random.randint(0, S.nSources+1, 20)
    #iSources = [250, 269]
    #iSources = [279, 250]
    #iSources = [292, 200, 294, 0, 1, 2, 4]
    #iSources = [  1,   5,  29,  30,  42,  45,  56,  65,  76,  96, 111, 135, 138, 141, 143, 206, 210, 211, 216, 229, 234, 250, 253, 266, 272, 277, 279, 282, 289, 290, 305, 328, 343, 349, 361, 376, 411, 414, 429, 438]
    
    # ===> Fit sources 
    print "@@> Starting %s" % baseName
    
    # ===> Start fitting
    f = fh.BONDfit_paper1(G, S, obsFit__f, baseName = baseName, dirName = dirName, sourcesRows = iSources, setupParBins = True)
    fh.run(nProc = 1, wait = 0.1)

    # Consolidate results into a single file
    f.combineSources_singleFile(iSources = iSources, reAddSources = False)

    # Fit one source
    '''
    f_source = f.fitOneSource(292)
    f_source = fh.fit_constraints(f_source)
    f_source.octree_grid(fh.BONDfit_newFit_octree, debug=True, fracMax = 1e-4)
    f_source.calcCrossValidation(fh.BONDfit_newFit_octree, debug=True, fracMax = 1e-4)
    f_source.calcBinnedPDFs()
    f_source.calcPDFSummaries(debug = True)
    #f_source.saveToHDF5(fileName = 'lix.hdf5', overwrite = True)
    '''
#=========================================================================
# EOF
'''
'''
