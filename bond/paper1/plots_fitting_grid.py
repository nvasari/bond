'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re
import itertools

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import fittingHelper as fh

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
G1s = slr.read_OurGrids(grid, scen, forceRead = False)
G2s = slr.gridScenarios('Goctr_scen', gridName = 'Goctree', forceRead = False, scenVars = {'age': [2], 'fr': [0.03, 3.00]})

suffix = '_'.join(obsFit__f)
Gs = G2s
Gs.update(G1s)
gScenarios = Gs.keys()

gScenarios = ['Sage2.00fr0.03', 'Sage2.00fr3.00', 'Goctree']

#=========================================================================
# ===> Read fake sources
#scenVars = {'age': [1, 2, 3, 4, 5, 6], 'fr': [0.03, 3.00]}
scenVars = {'age': [2, 4], 'fr': [0.03]}
scens = [dict(zip(scenVars, v)) for v in itertools.product(*scenVars.values())]
Ss = {}

# Create a list of fake sources scenarios
sScenarios = []
for scen in scens:
    s = 'S' + ''.join(['%s%.2f' % (key, value) for (key, value) in scen.items()])
    sScenarios.append(s)

# Create a list of fits
fScenarios = []
for sScenario in sScenarios:
    for gScenario in gScenarios:
        fScenarios.append( [sScenario, gScenario] )
        if (sScenario == 'Sage4.00fr0.03') & (gScenario != 'Sage2.00fr0.03'):
            fScenarios.pop()

# Fixing the order of the plots
aux = fScenarios[:2] + fScenarios[3:4] + fScenarios[2:3]
fScenarios = aux

Fs = {}

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

# Read fits & plots
for i, (sScenario, gScenario) in enumerate(fScenarios):

    print "@@ Reading fake sources for scenario %s." % sScenario

    fileName = os.path.join(dataDir, 'sources/fakeSources_tab_HII_15_All_interpTests_s%s.txt' % sScenario)
    S = slr.readSources('fakeSources %s' % sScenario, fileName = fileName, tableIsInLog = True, useLog = True, tableHasLineRatios = True, debug = False, forceRead = False)
    Ss[sScenario] = S



##########################################################################
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=10, override_params={'figure.subplot.wspace': 0.5, 'figure.subplot.hspace': 1, 'figure.subplot.top': 0.95, 'figure.subplot.bottom': 0.1})
fig = plt.figure(1)
plt.clf()
#nplot.title_date()

subp = 4, 4

c = pl.colour('fake')

import string
labels = list(string.ascii_lowercase)

# ===> Read fit   
for i, (sScenario, gScenario) in enumerate(fScenarios):
#for i, (sScenario, gScenario) in enumerate(fScenarios[-1:]):
    # ===> Naming fit
    G = Gs[gScenario]
    Fscenario = 's%s_g%s' % (sScenario, gScenario)
    print "@@> Fitting sources %s with grid %s (%s)" % (sScenario, gScenario, Fscenario)

    S = Ss[sScenario]
    
    # ===> Output file & dir
    suffix = '_'.join(obsFit__f) + '_chi2'
    dirName = os.path.join(dataDir, 'fits_fs/')
    baseName = 'fit_%s_%s' % (Fscenario, suffix)
    

    # ===> Read fit
    fitFile = os.path.join(dirName, '%s' % baseName)
    f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

    f.sources = S
    Fs[baseName] = f
    f.dirName = dirName
    f.baseName = baseName

    ftype = 'jmod'
    branch = 'all'

    def stats(f, ftype, ylab, branch = branch, ff = None):
        fresult = f.PDFSummaries__tcps[ftype][branch]
        if ff is None:
            ff = f.sourcesRows
        yf = fresult[ylab][ff][..., 0]
        y = utils.safe_sub(yf, f.sources.tabData[ylab][ff])
        print '%s: %s +- %s' % (ylab, np.average(y), np.std(y))
        return y
    
    # ===> Plots    
    #ax = plt.subplot2grid(subp, (i+0, 0))
    #ax.set_axis_off()
    sage, sfr = np.float_(sScenario.split('age')[-1].split('fr'))
    sage = '%i Myr' % sage
    sfr = np.where(sfr <= 0.04, 'sphere', 'shell')
    if gScenario == 'Goctree':
        gage = '1--6 Myr'
        gfr = 'all geom.'
    else:
        gage, gfr = np.float_(gScenario.split('age')[-1].split('fr'))
        gage = '%i Myr' % gage
        gfr = np.where(gfr <= 0.04, 'sphere', 'shell')

    ax = plt.subplot2grid(subp, (i, 0))
    ax.text(-0.6, 1.11, '(%s) Sources: %s, %s / Grid: %s, %s' % (labels[i], sage, sfr, gage, gfr), ha='left', va='bottom', transform=ax.transAxes) #, fontdict = {'size': 'large'})
    
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, pltTitle = False, ftype=ftype, branch = branch, colours=c, s=20, marker ='.', alpha=0.2, zorder=0, diff_y = True, pltLegend = False, pltZero = False, labelObs = False, annotatePts=False)
    plt.xlim(6.6, 9.4)
    plt.ylim(-0.5, 0.5)
    nplot.fix_ticks(ax, 3, 3)
    stats(f, ftype = ftype, ylab = 'OXYGEN' )
    stats(f, ftype = ftype, ylab = 'OXYGEN', ff = (S.tabData['OXYGEN'] > -3.8) )
    ax.yaxis.labelpad = 0
    
    ax = plt.subplot2grid(subp, (i, 1))
    pl.plot_fit_obsAndFit(f, xlab = 'logN2O', ylab = 'logN2O', pltTitle = False, ftype=ftype, branch = branch, colours=c, s=20, marker ='.', alpha=0.2, zorder=0, diff_y = True, pltLegend = False, pltZero = False, labelObs = False, annotatePts=False)
    plt.xlim(-2.2, 0.4)
    plt.ylim(-0.5, 0.5)
    nplot.fix_ticks(ax, 3, 3)
    stats(f, ftype = ftype, ylab = 'logN2O' )
    stats(f, ftype = ftype, ylab = 'logN2O', ff = (S.tabData['OXYGEN'] > -3.8) )
    ax.yaxis.labelpad = 0

pl.adjust_subplots(fig)
pl.save_figs_paper(fig, 'fitting_grid_vs_grid')


#=========================================================================
# ==> Plot fake sources
psetup(fig_width_pt=textwidth, aspect=0.4, lw=1., fontsize=14)
fig = plt.figure(2)
plt.clf()

subp = 1, 3

S = Ss['Sage2.00fr0.03']
c = pl.colour('fake')

ax = plt.subplot2grid(subp, (0, 0))
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, colours=c, pltTitle = False, zorder = 0, s=30, marker='.', alpha=0.7)
plt.xlim(6.9, 9.2)
nplot.fix_ticks(ax, 3, 3)

ax = plt.subplot2grid(subp, (0, 1))
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logUin', dx = 12, colours=c, pltTitle = False, zorder = 0, s=30, marker='.', alpha=0.7)
plt.xlim(6.9, 9.2)
nplot.fix_ticks(ax, 3, 4)

ax = plt.subplot2grid(subp, (0, 2))
pl.plot_table_xy(S.tabData, xlab = 'F6584', ylab = 'F5007', colours=c, pltTitle = False, zorder = 0, s=30, marker='.', alpha=0.7)
plt.xlim(-2.7, 0.6)
nplot.fix_ticks(ax, 3, 3)
plt.xlabel(pl.label('F6584', short=True))
plt.ylabel(pl.label('F5007', short=True))

fig.set_tight_layout(True)
pl.adjust_subplots(fig)
pl.label_panels(fig, x=0.07)
pl.save_figs_paper(fig, 'fakeSources')


#=========================================================================
# ==> Plot fake sources with Pilyugin formula
psetup(fig_width_pt=columnwidth, aspect=0.5, lw=1., fontsize=10)
fig = plt.figure(3)
plt.clf()

S = Ss['Sage2.00fr0.03']

# Calc Pilyugin O/H and N/O
# ON calibration from Pilyugin Vilchez Thuan 2010 
tabAux = {}

# Page 2: Defining line ratios
r2 = S.tabData['F3727']
r3 = utils.safe_sum(S.tabData['F5007'], np.log10(1.34))
n2 = utils.safe_sum(S.tabData['F6584'], np.log10(1.34))
F6716 = np.where(S.tabData['F6716'] > -999, 10**S.tabData['F6716'], 0)
F6731 = np.where(S.tabData['F6731'] > -999, 10**S.tabData['F6716'], 0)
s2 = utils.safe_log10(F6716 + F6731)

n2r2 = utils.safe_sub(n2, r2)
n2s2 = utils.safe_sub(n2, s2)

# Sec 3.2: ON calibration
AO_ON = np.full_like(r3, -999.)
AN_ON = np.full_like(r3, -999.)

_f = (n2 > -0.1)
AO_ON[_f] = (8.606 - 0.105*r3 - 0.410*r2 - 0.150*n2r2)[_f]
AN_ON[_f] = (7.955 + 0.048*r3 - 0.171*n2 + 1.015*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 > -0.25)
AO_ON[_f] = (8.642 + 0.077*r3 + 0.411*r2 + 0.601*n2r2)[_f]
AN_ON[_f] = (7.928 + 0.291*r3 + 0.454*n2 + 0.953*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 <= -0.25) #& (n2s2 > -999)
AO_ON[_f] = (8.013 + 0.905*r3 + 0.602*r2 + 0.751*n2r2)[_f]
AN_ON[_f] = (7.505 + 0.839*r3 + 0.492*n2 + 0.970*n2r2)[_f]

tabAux['AO_ON'] = utils.safe_sub(AO_ON, 12)
tabAux['AN_ON'] = utils.safe_sub(AN_ON, AO_ON)



subp = 1, 2

cf = pl.colour('fake')
cs = pl.colour('SL')

ax = plt.subplot2grid(subp, (0, 0))
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, colours = cf, pltTitle = False, s=30, marker='.', alpha=0.5, zorder=0)
nplot.fix_ticks(ax, 3, 3)
plt.xlim(6.9, 9.2)

ax = plt.subplot2grid(subp, (0, 1))
pl.plot_table_xy(tabAux, xlab = 'AO_ON', ylab = 'AN_ON', dx = 12, colours = cs, pltTitle = False, s=30, marker='.', alpha=0.5, zorder=0)
nplot.fix_ticks(ax, 3, 3)
plt.xlim(6.9, 9.2)

fig.set_tight_layout(True)
pl.adjust_subplots(fig)
pl.label_panels(fig, x=0.1)
pl.save_figs_paper(fig, 'fakeSources_P10')

#=========================================================================
# EOF

