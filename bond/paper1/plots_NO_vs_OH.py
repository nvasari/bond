'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplcolors
import astropy.table
import seaborn as sns
import scipy

import bond.strongLinesHelper as sl

# Clever reader
import bond.strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import bond.natastro.plotutils as nplot
import bond.utils as utils
import bond.plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dirName = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
#suffixes = ['chi2', 'chebyshev', 'upperLims1']
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dirName, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

fchi = Fs['chi2']
f = fchi

flag_sampleB = p1.sampleB(S)

##########################################################################
# Cid's sample: points outside the grid

# Find the envelope of the O3O2 vs O23 from the grid
O3O2_env = np.linspace(-2, 2, 51)
O3O2_cen = (O3O2_env[:-1] + O3O2_env[1:]) / 2.
O23_cen  = np.zeros_like(O3O2_cen)
ind_bin = np.digitize(G.tabGrid['O3O2'], O3O2_env)

for i_bin, (bin_low, bin_upp) in enumerate( zip(O3O2_env[:-1], O3O2_env[1:]) ):
    _f = (ind_bin == i_bin) & (G.scenarios['Sage1.00fr0.03'])
    O23_max = (G.tabGrid['O23'][_f]).max()
    O23_cen[i_bin] = O23_max


# Find the points outside the O23 vs O3O2 plane
O23_max_data = scipy.interpolate.griddata(O3O2_cen, O23_cen, S.tabData['O3O2'].data.data, method='linear')
flag_outsiders = (O23_max_data < S.tabData['O23'])
flag_sampleCid = flag_sampleB & ~flag_outsiders

# Check the envelope
#++plt.figure(2)
#++plt.clf()
#++
#++plt.plot(G.tabGrid['O3O2'], G.tabGrid['O23'], ',')
#++plt.plot(O3O2_cen, O23_cen)
#++
#++plt.plot(S.tabData['O3O2'][flag_sampleB], S.tabData['O23'][flag_sampleB], 'k.', lw = 0, ms = 10)
#++plt.plot(S.tabData['O3O2'][flag_sampleCid], S.tabData['O23'][flag_sampleCid], 'r.', lw = 0, ms = 10)
#++ind_sort = np.argsort(S.tabData['O3O2'])
#++plt.plot(S.tabData['O3O2'][ind_sort], O23_max_data[ind_sort], 'k.-', ms=5)
#++
#++plt.xlim(-2, 2)
#++plt.ylim(0.5, 1.5)


##########################################################################
# N/O vs O/H
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=15)
fig = plt.figure(1)
plt.clf()

subp = 1, 2

ff = flag_sampleB #& (S.tabData['r'] == 'g')
#ff = flag_sampleCid
c = pl.colour('BOND')

ftype = 'jmod'
branch = 'all'

light = sns.set_hls_values(c, l=1.)
colours = [sns.set_hls_values(c, l=l) for l in np.linspace(1, 0.0, 12)]
cmap = sns.blend_palette(colours, as_cmap=True)

# N/O vs O/H joint PDF - BOND
ax = plt.subplot2grid(subp, (0, 0)) 
#norm__s = np.max(f.jPDF__csj[branch][ff], axis = (1,2))
jPDF__j = f.jPDF__csj[branch][ff].sum(axis = 0)
Xgrid = f.jPDF_Xgrid
im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = cmap, zorder = 0)


plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
plt.xlabel('12 + %s' % pl.label('OXYGEN'))
plt.ylabel(pl.label('logN2O'))
nplot.fix_ticks(ax, 5, 4)
pl.plot_fit(fchi, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = ff, ax=ax, pltNpoi = True, labSize = 'small', pltTitle = False, pltLegend=False, marker='.', s=30, colours=c)


# N/O vs O/H peak of joint PDF - BOND
ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_fit_confEllipse(fchi, jtype = 'jc68', branch = branch, flag = ff, facecolor = colours[3], alpha = 0.2, edgecolor = 'none', zorder = 0)
pl.plot_fit(fchi, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = ff, ax=ax, pltNpoi = True, labSize = 'small', pltTitle = False, pltLegend=False, marker='.', s=30, colours=c, annotatePts = False, zorder=10)
ff = utils.ind2flag(S.tabData, [725, 802])
pl.plot_fit(fchi, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch, flag = ff, ax=ax, pltNpoi = False, labSize = 'small', pltTitle = False, pltLegend=False, marker='o', s=35, colours=sns.xkcd_rgb["dark red"], alpha=0.5, annotatePts = False, zorder=9)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax, 5, 4)


pl.label_panels(fig, addToLabel=[r'Joint PDF + MAP', r'68\% credibility + MAP'], fontdict = {'size': 'small'})
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'NO_Vs_OH_BOND')


##########################################################################
# Stats for octree

len_tabGrid = np.zeros(S.nSources, 'int')
cells       = np.zeros((S.nSources, 10), 'int')
sub_div     = np.zeros(S.nSources, 'int')
subDiv_max = 0

for iSrc in f.sourcesRowsFitted:
    fileName = f.sourceFileName(iSrc, f.baseName, dirName = f.dirName, nChars = 4)

    sFile = h5py.File(fileName, 'r')
    dO = sFile['data/tabGrids__si/0/']['dOXYGEN']    
    sFile.close()

    # Save info
    len_tabGrid[iSrc] = len(dO)
    
    nO = np.int_(0.1 / np.round(dO, 8))
    unique, counts = np.unique(nO, return_counts=True)
    print iSrc, unique, counts

    for u, c in zip(unique, counts):
        icell = int(np.log(u) / np.log(2))
        cells[iSrc, icell] = c
        if icell > subDiv_max:
            subDiv_max = icell

    sub_div[iSrc] = icell

_ff = f.sourcesRowsFitted
print '@@> Max subdivisions = %s' % subDiv_max
print '@@> Median subdvisions = %s' % np.median(sub_div[_ff])
print '@@> Median, min, max number of cells = %s %s %s' % (np.median(len_tabGrid[_ff]), np.min(len_tabGrid[_ff]), np.max(len_tabGrid[_ff]))

##########################################################################

    
'''
##########################################################################
# N/O vs O/H - Tests 05/Jan/2016
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=15)
fig = plt.figure(11)
plt.clf()

subp = 1, 2

ff = flag_sampleB
c = pl.colour('BOND')

light = sns.set_hls_values(c, l=1.)
colours = [sns.set_hls_values(c, l=l) for l in np.linspace(1., 0.2, 12)]
cmap = sns.blend_palette(colours, as_cmap=True)

# N/O vs O/H joint PDF - BOND
ax = plt.subplot2grid(subp, (0, 0)) 
jPDF__j = f.jPDF__csj['all'][ff].sum(axis = 0)
Xgrid = f.jPDF_Xgrid
im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = cmap, zorder = 0)
#cb = plt.colorbar()
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
plt.xlabel('12 + %s' % pl.label('OXYGEN'))
plt.ylabel(pl.label('logN2O'))
nplot.fix_ticks(ax, 5, 4)


# N/O vs O/H peak of joint PDF - BOND
ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_fit(fchi, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'jmod', flag = ff, ax=ax, pltNpoi = True, pltTitle = False, pltLegend=False, marker='.', s=30, colours=c, annotatePts=True)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax, 5, 4)


pl.label_panels(fig, addToLabel=[r') BOND joint PDF', r')  BOND solution'])
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
#pl.save_figs_paper(fig, 'NO_Vs_OH_BOND_numbered')

##########################################################################
# N/O vs O/H PDFs
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(3)
plt.clf()

ff = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) 

jPDF__j = f.jPDF__cjs['all'][..., ff].sum(axis=-1)
Xgrid = f.bins__j
im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = sns.light_palette('b', as_cmap=True))
cb = plt.colorbar()

fig.set_tight_layout(True)
nplot.save(fig, 'NO_Vs_OH_BOND_jPDF%s.eps' % suffix, epsFromPdf=True)


for i in f.sourcesRowsFitted:
#for i in [719, 720]:
    jPDF__j = f.jPDF__cjs['all'][..., i]
    print jPDF__j.sum()
    Xgrid = f.bins__j

    plt.clf()
    plt.title(i)
    im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = sns.light_palette('b', as_cmap=True), vmin=0, vmax=1)
    cb = plt.colorbar()

    fig.set_tight_layout(True)
    nplot.save(fig, 'NO_Vs_OH_BOND_jPDF%s_%04i.eps' % (suffix, i), epsFromPdf=True)
    
    #raw_input()




##########################################################################
# sig N/O vs O/H
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(4)
plt.clf()

ff = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) 

sigOH = f.PDFSummaries__tcps['p84']['all']['OXYGEN'] - f.PDFSummaries__tcps['p16']['all']['OXYGEN']
sigNO = f.PDFSummaries__tcps['p84']['all']['logN2O'] - f.PDFSummaries__tcps['p16']['all']['logN2O']

plt.clf()
plt.plot(sigOH, sigNO, '.')
nplot.plot_identity(sigOH, sigNO)
plt.xlabel(r'$\sigma$ O/H')
plt.ylabel(r'$\sigma$ N/O')

fit = np.polyfit(sigOH, sigNO, 1)
fit_fn = np.poly1d(fit)
plt.plot(sigOH, fit_fn(sigOH))

fig.set_tight_layout(True)

##########################################################################
# sig N/O vs O/H
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(5)
plt.clf()

ff = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) 

sigOH = f.PDFSummaries__tcps['p84']['all']['OXYGEN'] - f.PDFSummaries__tcps['p16']['all']['OXYGEN']
sigNO = f.PDFSummaries__tcps['p84']['all']['logN2O'] - f.PDFSummaries__tcps['p16']['all']['logN2O']

subp = 2, 2

ax = plt.subplot2grid(subp, (0, 0)) 
plt.plot(f.sources.tabData['eO23'][ff], sigOH[ff], '.')
nplot.plot_identity(f.sources.tabData['eO23'][ff], sigOH[ff])
plt.xlabel(r'$\sigma$ O23')
plt.ylabel(r'$\sigma$ O/H')

ax = plt.subplot2grid(subp, (0, 1)) 
plt.plot(f.sources.tabData['eN2O2'][ff], sigNO[ff], '.')
nplot.plot_identity(f.sources.tabData['eN2O2'][ff], sigNO[ff])
plt.xlabel(r'$\sigma$ N2O2')
plt.ylabel(r'$\sigma$ N/O')

ax = plt.subplot2grid(subp, (1, 0)) 
plt.plot(sigOH[ff], sigNO[ff], '.')
nplot.plot_identity(sigOH[ff], sigNO[ff])
plt.xlabel(r'$\sigma$ O/H')
plt.ylabel(r'$\sigma$ N/O')

ax = plt.subplot2grid(subp, (1, 1)) 
plt.plot(sigOH[ff]/f.sources.tabData['eO23'][ff], sigNO[ff]/f.sources.tabData['eN2O2'][ff], '.')
nplot.plot_identity(sigOH[ff]/f.sources.tabData['eO23'][ff], sigNO[ff]/f.sources.tabData['eN2O2'][ff])
plt.xlabel(r'$\sigma$ O/H / $\sigma$ O23')
plt.ylabel(r'$\sigma$ N/O / $\sigma$ N2O2')

fig.set_tight_layout(True)

##########################################################################
# MC results
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=1., fontsize=15)
fig = plt.figure(6)
plt.clf()

subp = 2, 3

# Plot original PDF
iSrc = 39
xlow, xupp = 8.3, 9.0
ylow, yupp = -1.2, -0.5

ax = plt.subplot2grid(subp, (0, 0)) 
jPDF__j = f.jPDF__cjs['all'][..., iSrc]
Xgrid = f.bins__j
im = plt.pcolormesh(12+Xgrid[0], Xgrid[1], jPDF__j, cmap = sns.light_palette('b', as_cmap=True), vmin=0, vmax=1)
cb = plt.colorbar()
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)


# Read MC calculations
baseName = 'fitMC_%s_%s' % (Fscenario, suffix)
fitFile = os.path.join(dirName, '%s_summary' % baseName)
fMC = slr.readFit(fitFile, fileName = '%s.hdf5' % fitFile, forceRead = False)

ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_fit(fMC, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'best', addToTitle = r'best', pltLegend=False, colours='r', alpha=0.1, marker='o', s=10)
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2)) 
pl.plot_fit(fMC, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'jpdf', addToTitle = r'jPDF', pltLegend=False, colours='r', alpha=0.1, marker='o', s=10)
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 0)) 
pl.plot_fit(fMC, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'peak', addToTitle = r'peak', pltLegend=False, colours='r', alpha=0.1, marker='o', s=10)
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1)) 
pl.plot_fit(fMC, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', addToTitle = r'ave', pltLegend=False, colours='r', alpha=0.2, marker='o', s=10)
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2)) 
pl.plot_fit(fMC, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'med', addToTitle = r'med', pltLegend=False, colours='r', alpha=0.2, marker='o', s=10)
plt.xlim(xlow, xupp)
plt.ylim(ylow, yupp)
nplot.fix_ticks(ax)


fig.set_tight_layout(True)
nplot.save(fig, 'fitMC_s%04i_NO_Vs_OH.eps' % iSrc, epsFromPdf=True)
'''
#=========================================================================
# EOF
