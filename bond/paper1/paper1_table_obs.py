'''
Get stats for paper1.

Natalia@Corrego - 03/Nov/2015
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

import paper1_stats as p1

#=========================================================================
# ==> Read sources
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
S = slr.readSources(sources, debug = False, useLog = True, forceRead = False)
S_linear = slr.readSources('%s-linear' % sources, debug = False, useLog = False, forceRead = False)

#=========================================================================
# Subsamples

# All (entire sample)
flag_sampleA = p1.sampleA(S)

# Print stats for samples
print 'Sample A: N = %s' % np.sum(flag_sampleA)

# Keys to save
ids = ['id']
info = ['r', 'name']
lines = ['F3727', 'F3869', 'F4363', 'F5007', 'F5755', 'F5876', 'F6584', 'F7135']
uncer = ['e%s' % l for l in lines]
lims = ['limF4363', 'limF5755']

keys = ids + info + [item for sublist in zip(lines, uncer) for item in sublist] + lims
print '@@> Saving table for sample A. Selecting columns: \n   ', keys

formats = {}
for k in keys:
    if k in ids:
        formats[k] = '%03i'        
    elif k in info:
        formats[k] = '%s'
    else:
        formats[k] = '%12.4e'

# ****** ASCII table

# Create ids (only for sample A)
S_linear.tabData['id']  = np.zeros(S.nSources, 'int')
S_linear.tabData['id'][flag_sampleA] = np.arange(1, flag_sampleA.sum()+1)

# Select columns and rows for output table
paper1_table = S_linear.tabData[keys][flag_sampleA]

# Mask
for k in paper1_table.keys():
    paper1_table[k] = utils.mask_minus999(paper1_table[k], fill=0.)

# Save
#print np.unique(paper1_table['r'])
paper1_table.write('BOND_sources%s.txt' % S_linear.tabDataName, format='ascii.fixed_width_two_line', formats=formats)


# ****** Latex table

# Select first and last 4 lines for the paper
ii = np.r_[0:4,-4:0]
paper1_tex = paper1_table[ii]
paper1_tex['\dots'] = '\dots'
#del paper1_tex.meta['comments']

# Select keys to print
keys = paper1_tex.keys()[:7] + paper1_tex.keys()[-1:] + paper1_tex.keys()[-5:-1]
paper1_tex = paper1_tex[keys]

#info = ['r', 'namegal', 'name']
#lines = ['F3727', 'F3869', 'F4363', 'F5007', 'F5755', 'F5876', 'F6584', 'F7135']
#uncer = ['eF3727']
#keys = info + lines + uncer


# Masks
for k in paper1_tex.keys():
    paper1_tex[k] = utils.mask_minus999(paper1_tex[k], thereshold=0.)
    if k in info:
        paper1_tex[k] = [s.replace('_' , ' ') for s in paper1_tex[k]]

        
formats = {}
for k in keys:
    if k in ids:
        formats[k] = '%03i'        
    elif (k in info) | (k == '\dots'):
        formats[k] = '%s'
    else:
        formats[k] = '%.4f'


paper1_tex.write('BOND_sources%s.tex' % S_linear.tabDataName, format='ascii.latex', formats=formats) 

# To create the dots on the table
print
aux = ' '.join(['\dots &'] * len(paper1_tex.keys()))
aux = '\\\\'.join(aux.rsplit('&', 1))
print aux

#=========================================================================
# EOF
