'''
Fitting sources

Natalia@UFSC - 20/May/2014

Final version for paper: Natalia@Meudon - 03/Aug/2015

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dirName = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

if grid == 'orig':
    gScenario = 'Gorigin'
else:
    gScenario = 'Ginterp'
    
#=========================================================================
# ===> Def pars & fit sources
errCookingFactor = 1.
physPars4PDF = ['logUin', 'OXYGEN', 'logN2O', 'fr', 'age']
extraLines4PDF = ['F3727', 'F4074', 'F4363', 'F5007', 'F5755', 'F5876', 'F6312', 'F6584', 'F6724', 'F7135', 'F7325', 'F9069', 'O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RN2', 'Ar3Ne3', 'F5876']
parName__p = np.unique(physPars4PDF + obsFit__f + extraLines4PDF)

overwrite = True
debug = True
errCookingFactor = 1.

#=========================================================================
# ===> Fitting

# ===> Naming fit
G = slr.f[gScenario]
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
print "@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario)


# ===> Output file & dir
suffix = '_'.join(obsFit__f) + '_chi2'
dirName =  os.path.join(dirName, 'fits/')
baseName = 'fitMC_%s_%s' % (Fscenario, suffix)

# ===> Choose sources
flag_sampleB = (S.tabData['F6584'] > -999) & (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['F5876'] > -999)
ff = flag_sampleB
_iSources = np.where(ff)[0]
iSources = _iSources
#iSources = [269]
#iSources = [719]


# ==> Perturb source for Monte Carlo
iSrc = 39
nMC = 1000
tab = astropy.table.Table(data = np.array(S.tabData[iSrc]).repeat(nMC))
S2 = sl.sources(tabData = tab, useLog = S.useLog, tableIsInLog = S.tableIsInLog)
for i, line in enumerate(['F3727', 'F5007', 'F6584']):
    sigma = S.tabData['e'+line][iSrc]
    ave = S.tabData[line][iSrc]
    print line, sigma, ave
    np.random.seed(2739845+i)
    newLine = sigma * np.random.randn(nMC) + ave
    S2.tabData[line] = newLine

#plt.clf()
#plt.hist(S2.tabData['F6584'])
#plt.hist(S2.tabData['F5007'])
#plt.hist(S2.tabData['F3727'])

# ===> Fit sources 
print "@@> Starting %s" % baseName


# ===> Start fitting
f = sl.fit(grid = G, sources = S2, gridParts = ['all'], sourcesRows = np.arange(S2.nSources), errCookingFactor = errCookingFactor, obsFit__f = obsFit__f, parName__p = parName__p, calcAll = False, baseName = baseName, dirName = dirName, debug = debug)

f_sources = [f.fitOneSource(iSource) for iSource in np.arange(S2.nSources)]
fileNames = [f.sourceFileName(iSource, baseName, nChars = 4, dirName = dirName) for iSource in np.arange(S2.nSources)]

def fit_source(iSource):
    print '@@> Fitting source %s' % iSource

    import time
    t1 = time.time()
    
    # Temp file name for this source
    fileName = fileNames[iSource]
    
    if not (os.path.exists(fileName)):
        f_source = f_sources[iSource]
        
        # Calc likelihood for strong lines
        f_source.calcLikelihood(mode='chi2')
        
        # Calc likelihood for semi-sstrong lines
        f_source.stepProb()
        f_source.gaussianProb(obs = ['Ar3Ne3'], nSigma = 1.)
        #++f_source.gaussianProb(obs = ['F5876'], nSigma = 1., linear=True)
        f_source.gaussianProb(obs = ['F5876'], nSigma = 1.)
        f_source.calcNSolutions()
        
        # Calc PDFs
        f_source.calcPDFsAll()
        f_source.calcPDFSummaries()

        # Save temp file for source
        f_source.saveToHDF5(overwrite = overwrite, fileName=fileName, saveSourceFit=True, saveProb=True)

    print '@@> Fitted source %s in %s s' % (iSource, time.time()-t1)

# Parallel computing
#from joblib import Parallel, delayed
#Parallel(n_jobs=2)(delayed(fit_source)(iSource) for iSource in f.sourcesRowsFitted)

f.sourcesRows = np.arange(500)
f.sourcesRowsFitted = None
f.flagSourcesFitted()

from multiprocessing import Pool
p = Pool(processes=2, maxtasksperchild=5)
p.map_async(fit_source, f.sourcesRowsFitted)
p.close()
p.join()
    


# Consolidate results into a single file
#f.sourcesRows = np.arange(1)
#f.sourcesRowsFitted = None
#f.flagSourcesFitted()
f.saveSourcesToSingleFiles(iSources = np.arange(500), nChars = 4)

#f.sourcesRows = np.arange(500)
#f.sourcesRowsFitted = None
#f.flagSourcesFitted()
f.saveSourcesToSingleFiles(iSources = np.arange(500), nChars = 4, data_types=['all_sources'])

S2.saveTableToFile(os.path.join(dirName, '../sources/fitMC_HEBCD-SPIRALS_s%04i.txt' % iSrc), fileType='ascii.fixed_width_two_line', overwrite=True)
#=========================================================================
# EOF
'''
'''
