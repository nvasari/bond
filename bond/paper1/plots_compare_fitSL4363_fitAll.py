'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dataDir = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
suffixes = ['chi2', 'F4363_chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dataDir, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

flag_sampleB = p1.sampleB(S)
flag_sampleT = p1.sampleT(S)

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

##########################################################################
# Compare fits with all lines + all lines & 4363
psetup(fig_width_pt=textwidth, aspect=0.5, lw=1.5, fontsize=20)
fig = plt.figure(11)
plt.clf()

# Plots
subp = 1, 2

ftype = 'jmod'
branch = 'all'

# O/H BOND vs gNO
f = Fs['chi2']
f4363 = Fs['F4363_chi2']

x = f.PDFSummaries__tcps[ftype]['all']['OXYGEN'][..., 0]
y = f4363.PDFSummaries__tcps[ftype][branch]['OXYGEN'][..., 0]

ff = flag_sampleB & (S.tabData['F4363'] > -999)
cS = pl.colour('SL')
cB = pl.colour('BOND')

ax = plt.subplot2grid(subp, (0, 0)) 
plt.scatter(12+x[ff], 12+y[ff], c=cS, s=10, marker='o', lw = 0)
#plt.errorbar(12+x[ff], 12+y[ff], xerr=xerr[..., ff], yerr=yerr[..., ff], fmt='none', ecolor='#999999', elinewidth=1, capsize=0, alpha=0.2, zorder=0)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 2)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('OXYGEN'))
plt.xlabel('12 + %s$_\mathrm{BOND}$' % pl.label('OXYGEN'))
plt.ylabel('12 + %s$_\mathrm{+4363}$' % pl.label('OXYGEN'))
nplot.fix_ticks(ax)


# N/O vs O/H BONd vs gNO
ax = plt.subplot2grid(subp, (0, 1)) 

xSL  = f4363.PDFSummaries__tcps[ftype][branch]['OXYGEN'][ff][..., 0] + 12
ySL  = f4363.PDFSummaries__tcps[ftype][branch]['logN2O'][ff][..., 0]
xfit = f.PDFSummaries__tcps[ftype]['all']['OXYGEN'][ff][..., 0] + 12
yfit = f.PDFSummaries__tcps[ftype]['all']['logN2O'][ff][..., 0]

#plt.scatter(xSL, ySL, s=10, marker = 'o', c='k', lw=0, zorder=11)
#plt.scatter(xfit, yfit, s=40, marker = 'o', c='b', lw=0, zorder=10)
pl.plot_fit(f4363, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = branch,  flag = (ff),  pltTitle=False, pltLegend=False, colours=cS, s=10, marker='o', zorder = 8)
pl.plot_fit(f,   xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'all',       flag = (ff),  pltTitle=False, pltLegend=False, colours=cB, s=30, marker='o', zorder = 9, alpha = 0.7)
plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax)

for xo, yo, xf, yf in zip(xSL, ySL, xfit, yfit):
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 2, zorder=1)

pl.label_panels(fig)
pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'compare_fitSL4363_fitAll')

##########################################################################
# N/O vs O/H: BOND F4363 vs Te
psetup(fig_width_pt=textwidth, aspect=0.83, lw=1., fontsize=16)
fig = plt.figure(21)
plt.clf()
#nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))

ftype = 'jmod'
branch = 'bestScen'

out_RO3 = f.PDFSummaries__tcps[ftype][branch]['RO3']
if len(out_RO3.shape) > 1:
    out_RO3 = out_RO3[..., 0]    
dRO3 = utils.safe_sub(out_RO3, S.tabData['RO3'])

subp = 1, 1

# Get color pallete
c1 = sns.color_palette("coolwarm_r", 256)[:140]
c2 = sns.color_palette("coolwarm_r", 256)[140:-20:4]
c1.extend(c2)
cmap = sns.blend_palette(c1, as_cmap=True)


ff = flag_sampleB & flag_sampleT &  (S.tabData['F4363'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 

xobs = S.tabData['OXYGEN'][ff] + 12
yobs = S.tabData['logN2O'][ff]
xfit = f4363.PDFSummaries__tcps[ftype][branch]['OXYGEN'][ff] + 12
yfit = f4363.PDFSummaries__tcps[ftype][branch]['logN2O'][ff]
if len(xfit.shape) > 1:
    xfit = xfit[..., 0]
    yfit = yfit[..., 0]
    
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), pltTitle = False, colours=pl.colour('Te'), s=10, marker = 'o', zorder=11)
#pl.plot_table_ellipse_covMatrix(S.tabData, dx = 12, flag = ff, edgecolor = pl.colour('Te'), alpha = 0.5, zorder = 9)
a = plt.scatter(xfit, yfit, s=40, marker = 'o', c=dRO3[ff], cmap=cmap, lw=0, zorder=10)

cb = nplot.tight_colorbar(a, size = "5%", pad = '3%', zorder = 0)
RO3_lab = pl.label('RO3', short = True)
cb.set_label('%s$_\mathrm{BOND} -$ %s$_\mathrm{obs}$' % (RO3_lab, RO3_lab))

cbarytks = plt.getp(cb.ax.axes, 'yticklines')
plt.setp(cbarytks, visible=False)
cb.set_alpha(1)
cb.solids.set_edgecolor("face")
nplot.fix_colorbar_ticks(cb, 8, steps=[0, 1, 2, 4, 5, 10])

plt.sca(ax)
for xo, yo, xf, yf in zip(xobs, yobs, xfit, yfit):
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 1, zorder=1)

plt.xlim(pl.lims('OXYGEN'))
plt.ylim(pl.lims('logN2O'))
nplot.fix_ticks(ax, 5, 4)

fig.set_tight_layout(True)

#=========================================================================

#=========================================================================
# EOF

'''
'''
