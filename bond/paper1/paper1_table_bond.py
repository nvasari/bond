'''
Get stats for paper1.

Natalia@Corrego - 03/Nov/2015
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import bond.strongLinesHelper as sl

# Clever reader
import bond.strongLinesReader as slr

# Some useful functions
import bond.natastro.plotutils as nplot
import bond.utils as utils
import bond.plotHelpers as pl

import paper1_stats as p1

#=========================================================================
# ==> Data dir
home = os.environ['HOME']
dirName = os.path.join(home, 'data/bond/')

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots
Fs = {}
#suffixes = ['chi2', 'chebyshev', 'upperLims1']
suffixes = ['chi2']

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suf in suffixes:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)

      suffix = '_'.join(obsFit__f) + '_%s' % suf
      dirName = os.path.join(dirName, 'fits/')
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      # ===> Read fit
      fitFile = os.path.join(dirName, '%s' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[suf] = f
      f.dirName = dirName
      f.baseName = baseName

#=========================================================================
# Subsamples

fchi = Fs['chi2']
f = fchi

# BOND sample
flag_sampleB = p1.sampleB(S)
flag_sampleA = p1.sampleA(S)

# Print stats for samples
print 'Sample B: N = %s' % np.sum(flag_sampleB)

# Keys to save
ids = ['id']
info = ['name']

# Create ids for sample A only
table = astropy.table.Table({'id' : np.zeros(S.nSources, 'int')})
table['id'][flag_sampleA] = np.arange(1, flag_sampleA.sum()+1)
for k in info:
    table[k] = S.tabData[k]

# Summaries to save
ts = ['jmod', 'jc68', 'mmed', 'mp68']
branch = 'all'
ps = ['OXYGEN', 'logN2O']
fs = flag_sampleB

# Save for Grazyna
#fs = np.ones_like(flag_sampleB)
#table['id'] = np.arange(1, fs.sum()+1)


ps_outLabels = {
  'OXYGEN' : 'logO/H' , 
  'logN2O' : 'logN/O' , 
}

ts_outLabels = {
    'jmod' : ['jmod'] ,
    'jc68' : ['jc68_cen', 'jc68_sig', 'jc68_cov', 'jc68_scale'] ,
    'mmed' : ['mmed'] ,
    'mp68' : ['mp68_low', 'mp68_upp'],
    }

rename = {
    'logO/H_jc68_cov'   : None         ,
    'logO/H_jc68_scale' : None         ,
    'logN/O_jc68_cov'   : 'jc68_cov'   ,
    'logN/O_jc68_scale' : 'jc68_scale' , 
}

# Generate table
out_table = table[fs]

for t in ts:
    for p in ps:
        outVals = f.PDFSummaries__tcps[t][branch][p][fs]
        for i, key in enumerate(ts_outLabels[t]):
            outVal = outVals[..., i]
            outKey = '%s_%s' % (ps_outLabels[p], key)
            out_table[outKey] = outVal 

            if outKey in rename.keys():
                newKey = rename[outKey]
                if newKey is not None:
                    out_table[newKey] = out_table[outKey]
                del out_table[outKey]


                
# ****** ASCII table
# Formatting
formats = {}
for k in out_table.keys():
    if k == 'id':
        formats[k] = '%03i'
    elif (k in info) | (k.startswith('\dots')):
        formats[k] = '%s'
    else:
        formats[k] = '%12.4e'

# Masking
for k in out_table.keys():
    out_table[k] = utils.mask_minus999(out_table[k], fill=0.)

# Save table 
out_table.write('BOND_summaries%s.txt' % S.tabDataName, format='ascii.fixed_width_two_line', formats=formats)


# ****** Latex table

# Select first and last 4 lines for the paper
ii = np.r_[0:4,-4:0]
tex_table = out_table[ii]
tex_table['\dots' ] = '\dots'
tex_table['\dots '] = '\dots'


# Masks, formatting & trick to make the header in two lines for latez
new_tex_table = astropy.table.Table()

for k in tex_table.keys():
    # Reformat the columns to string
    aux1 = utils.mask_minus999(tex_table[k])
    if (k == 'id'):
        aux1 = ['%03i' % item for item in aux1.data]
    elif (k not in info) & (~k.startswith('\dots')):
        aux1 = ["$%.4f$" % item for item in aux1.data]
    else:
        aux1 = ["%s" % item for item in aux1.data]

    if k in info:
        aux1 = [s.replace('_' , ' ') for s in aux1]
        
    # Split the header into two parts, put the second part inside the array
    k1 = k.split('_')
    k_new = k1[0].replace('log', 'log ')
    aux2 = ' '.join(k1[1:])
    aux = [aux2] + aux1

    # Save to new table
    while k_new in new_tex_table.keys():
        k_new = '%s ' % k_new
    new_tex_table[k_new] = aux


# Select keys to print
keys = new_tex_table.keys()[:6] + new_tex_table.keys()[-1:] + new_tex_table.keys()[10:14] + new_tex_table.keys()[-2:-1]
new_tex_table = new_tex_table[keys]

new_tex_table.write('BOND_summaries%s.tex' % S.tabDataName, format='ascii.latex', formats='%s')
#print new_tex_table

# To create the dots on the table
print
aux = ' '.join(['\dots &'] * len(new_tex_table.keys()))
aux = '\\\\'.join(aux.rsplit('&', 1))
print aux


# ****** Plots

# Check if table is correct
debug = False

if debug:

    columnwidth = 240.
    textwidth = 504.
    screenwidth = 1024.
    #psetup = nplot.plotSetupMinion
    psetup = nplot.plotSetup

    psetup(fig_width_pt=textwidth, aspect=0.5, lw=1., fontsize=15)
    fig = plt.figure(1)
    plt.clf()

    c = pl.colour('BOND')

    ax = plt.subplot(121)
    plt.title('mmed')
    
    plt.scatter(12+out_table['logO/H_mmed'], out_table['logN/O_mmed'], lw=0, c=c, alpha=0.8, s=10)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    plt.xlabel('12 + %s' % pl.label('OXYGEN'))
    plt.ylabel(pl.label('logN2O'))
    nplot.fix_ticks(ax, 5, 4)

    OH_c68 = np.array([-out_table['logO/H_mp68_low'] + out_table['logO/H_mmed'], out_table['logO/H_mp68_upp'] - out_table['logO/H_mmed']])
    NO_c68 = np.array([-out_table['logN/O_mp68_low'] + out_table['logN/O_mmed'], out_table['logN/O_mp68_upp'] - out_table['logN/O_mmed']])
    plt.errorbar(12+out_table['logO/H_mmed'], out_table['logN/O_mmed'], xerr=OH_c68, yerr=NO_c68, fmt='none', ecolor=c, elinewidth=2, capsize=0, alpha=0.2, zorder=1)
    
    ax = plt.subplot(122)    
    plt.title('jmod')
    
    plt.scatter(12+out_table['logO/H_jmod'], out_table['logN/O_jmod'], lw=0, c=c, alpha=0.8, s=10)
    plt.xlim(pl.lims('OXYGEN'))
    plt.ylim(pl.lims('logN2O'))
    plt.xlabel('12 + %s' % pl.label('OXYGEN'))
    plt.ylabel(pl.label('logN2O'))
    nplot.fix_ticks(ax, 5, 4)

    cen_x, cen_y = 12+out_table['logO/H_jc68_cen'], out_table['logN/O_jc68_cen']
    sig_x, sig_y =    out_table['logO/H_jc68_sig'], out_table['logN/O_jc68_sig']
    rho_xy = out_table['jc68_cov']
    scale  = out_table['jc68_scale'] 
    pl.plot_ellipse_parameters(cen_x, cen_y, sig_x, sig_y, rho_xy, scale, facecolor = c, alpha = 0.2, edgecolor = 'none')
    

    pl.adjust_subplots(fig)
    fig.set_tight_layout(True)
#=========================================================================
# EOF
