'''
Natalia@Corrego - 08/Jan/2016
'''

import sys

import numpy as np
import matplotlib.pyplot as plt

# Clever reader
import strongLinesReader as slr

# Fitting for paper 1
import paper1_stats as p1


# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
#psetup = nplot.plotSetupMinion
psetup = nplot.plotSetup

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
S = slr.readSources(sources, debug = False, forceRead = False)

#=========================================================================
# Tests for uncertainties
psetup = nplot.plotSetup
psetup(fig_width_pt=textwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(1)
fig.set_tight_layout(True)
plt.clf()


def p(v, sigma, delta):
    sv2 = sigma**2 + v[..., np.newaxis]**2
    p = (sv2)**-0.5 * np.exp(-0.5 * delta[np.newaxis, ...]**2 / sv2)
    return p


# Check the function for each delta
sigma = 0.02
delta = 1. * sigma
v = np.linspace(0, 0.5, 100)

for i, d in enumerate(np.linspace(0, 5, 8)):

    delta = d * sigma
    
    ax = plt.subplot(4, 4, i+1)
    plt.plot(v, p(v, sigma, delta))
    ax.set_title(d)
    

# Check the integral for different dv's

def P(p, sigma, delta, dv = None, nv = None):
    vmin = 0.01
    vmax = 1./np.log(10)
    
    vs = np.arange(vmin, vmax, 1e-5)
    if dv is not None:
        vs = np.arange(vmin, vmax, dv)
    if nv is not None:
        vs = np.logspace(np.log10(vmin), np.log10(vmax), nv)
        dv = np.empty_like(vs)
        _d = vs[1:] - vs[:-1]
        dv[0]  = _d[0]
        dv[-1] = _d[-1]
        dv[1:-1] = _d[1:]/2. + _d[:-1]/2

    #print vs, dv
    P = (p(vs, sigma, delta) * dv[..., np.newaxis]).sum(axis = 0)
    # This does not work:
    #P = (p(vs, sigma, delta)).sum(axis = 0)
    P /= np.sum(P)
    nv = len(vs)
    return P, nv, dv

delta = np.linspace(-7.5*sigma, 7.5*sigma, 100)
#print P(p, sigma, delta, 0.1)

i += 1
ax = plt.subplot(4, 4, i+1)
P1 = np.zeros_like(delta)

for dv in np.logspace(-6, -1, 1):
    P2 = P1
    P1, nv, dv = P(p, sigma, delta, dv=dv)
    plt.plot(delta, P1, '.-', label = 'dv = %s' % dv)

    diff = np.abs(P1 - P2)
    print nv, np.median(diff), np.average(diff), np.min(diff), np.max(diff)

    
#i += 1
#ax = plt.subplot(4, 4, i+1)

for nv in [20]:
    P2 = P1
    P1, nv, dv = P(p, sigma, delta, nv=nv)
    plt.plot(delta, P1, '*-', label = 'nv = %s' % nv)

    diff = np.abs(P1 - P2)
    print nv, np.median(diff), np.average(diff), np.min(diff), np.max(diff)

#plt.legend()

##########################################################################
# Plot uncertainties
psetup = nplot.plotSetupMinion
psetup(fig_width_pt=columnwidth, aspect=1, lw=1., fontsize=15)
fig = plt.figure(2)
plt.clf()

ff = p1.sampleB(S)

print np.average(S.tabData['eF7135'][ff]), np.median(S.tabData['eF7135'][ff]), np.max(S.tabData['eF7135'][ff])
print np.average(S.tabData['eF3869'][ff]), np.median(S.tabData['eF3869'][ff]), np.max(S.tabData['eF3869'][ff])

sigma = 0.02

ax = plt.subplot(111)

# Deltas (cij - oi)
aux = np.logspace(-6, np.log10(50*sigma), 100)
delta = np.hstack([-aux[::-1], 0., aux])
dd = np.empty_like(delta)
_d = delta[1:] - delta[:-1]
dd[0]  = _d[0]
dd[-1] = _d[-1]
dd[1:-1] = _d[1:]/2. + _d[:-1]/2

# Simple gaussian
P3 = p(np.array([0.]), sigma, delta)
plt.plot(delta, P3[0] / np.sum(P3 * dd), color=pl.colour('SL'), zorder = 10, label = 'Gaussian\n($\sigma = 0.02$)')
plt.fill(delta, P3[0] / np.sum(P3 * dd), color=pl.colour('SL'), alpha = 0.3, zorder = 10)

plt.annotate( 'Gaussian',          xy=(4*sigma, 18.0), size='x-small', color=pl.colour('SL') )
plt.annotate( '($\sigma = 0.02$)', xy=(4*sigma, 16.5), size='x-small', color=pl.colour('SL') )

        
# Marginalized uncertainties - my fit
P1, nv, dv = P(p, sigma, delta, nv=20)
plt.plot(delta, P1)
plt.plot(delta, P1 / np.sum(P1 * dd), color=pl.colour('BOND'), zorder = 11)
plt.fill(delta, P1 / np.sum(P1 * dd), color=pl.colour('BOND'), alpha = 0.3, zorder = 11)

plt.annotate( 'Sum of gaussians',  xy=(4*sigma, 5.0), size='x-small', color=pl.colour('BOND') )
plt.annotate( '($\sigma = 0.02$;', xy=(4*sigma, 3.5), size='x-small', color=pl.colour('BOND') )
plt.annotate( '$e = 0.01..0.43$)', xy=(4*sigma, 2.0), size='x-small', color=pl.colour('BOND') )

# Marginalized uncertainties - fuller fit
P2, nv, dv = P(p, sigma, delta, dv=np.array([1e-6]))
diff = np.abs(P1 - P2)
print nv, np.median(diff), np.average(diff), np.min(diff), np.max(diff)

nplot.fix_ticks(ax)

plt.xlabel('$\Delta_{j}$')
plt.ylabel('${\cal L}_{j}$')

pl.adjust_subplots(fig)
fig.set_tight_layout(True)
pl.save_figs_paper(fig, 'uncertainties_marg')
##########################################################################

