'''
Fit sources - tests for one source

Natalia@UFSC - 20/May/2014

Running:
%time %run fitting.py lines      orig all  HIIBCD
%time %run fitting.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]


#=========================================================================
# ===> Def pars & fit sources
errCookingFactor = 1.
physPars4PDF = ['logUin', 'OXYGEN', 'logN2O']
extraLines4PDF = ['F3727', 'F4074', 'F4363', 'F5007', 'F5755', 'F5876', 'F6312', 'F6584', 'F6724', 'F7135', 'F7325', 'F9069', 'O23', 'O3O2', 'N2O2', 'O3N2', 'RO3', 'RN2']
parName__p = np.unique(physPars4PDF + obsFit__f + extraLines4PDF)



# =================================================================
# A single fit
def do_plots(f, iSource, baseName):    
    nplot.plotSetupMinion(fontsize=15)
    
    # Plots
    fig = plt.figure(1, figsize=(14,10))
    plt.clf()
    pl.plot_PDFs(f, iSource)
    nplot.save(fig, 'fitting_PDFs_%s_s%04i.png' % (baseName, iSource))

    fig = plt.figure(2, figsize=(14,10))
    plt.clf()
    pl.allPlots_gridSourceFit(f, iSource, psize = 3)
    fig.set_tight_layout(True)
    nplot.save(fig, 'fitting_chi2_%s_s%04i.png' % (baseName, iSource))

    fig = plt.figure(3, figsize=(14,10))
    plt.clf()
    pl.allPlots_jointPDF(f, iSource)
    plt.xlim(6, 9)
    nplot.title_date(' '.join(obsFit__f))
    nplot.save(fig, 'fitting_jointPDFs_%s_s%04i.png' % (baseName, iSource))

    fig = plt.figure(4, figsize=(14,10))
    plt.clf()
    pl.allPlots_jointPDF(f, iSource, zoom=True)
    plt.xlim(6, 9)
    nplot.title_date(' '.join(obsFit__f))
    nplot.save(fig, 'fitting_jointPDFs_zoom_%s_s%04i.png' % (baseName, iSource))
    
    nplot.plotSetup(fontsize=15)

    
def do_fit(iSources = None):
    
    # ===> Name scenario
    Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
    print "@@> Fitting sources %s with grid %s (%s)" % (S.tabDataName, gScenario, Fscenario)

    suffix = '_'.join(obsFit__f)
    if errCookingFactor != 1.:
        suffix = "%s_errC%05.1f" % (suffix, errCookingFactor)
    if fakeErr is not None:
        suffix = "%s_fErr%4.2f" % (suffix, fakeErr)

    # ===> Name output file
    dirName = 'fits/'
    baseName = 'fit_grid%s_%s_%s' % (grid, Fscenario, suffix)
    
    # ===> Fit the sources 
    G = Gs[gScenario]

    print "@@> Starting %s" % baseName

    if iSources is None:
        # Fit all sources at once
        f = sl.fit(grid = G, sources = S, errCookingFactor = errCookingFactor, obsFit__f = obsFit__f, parName__p = parName__p, debug = debug)
        f.saveFittedSourcesToHDF5(baseName, nChars = 4, dirName = dirName, overwrite = overwrite)

        # Save fits
        outFile = os.path.join(dirName, '%s_summary' % baseName)
        tab = f.saveToTxt('%s.txt' % outFile, overwrite = overwrite)
        f.saveToHDF5('%s.hdf5' % outFile, saveSources = False, saveGrid = False, saveSourceFit = False, overwrite = overwrite)
    
    else:
        # Fit a few sources
        f = sl.fit(grid = G, sources = S, sourcesRows = iSources, errCookingFactor = errCookingFactor, fakeErr = fakeErr, obsFit__f = obsFit__f, parName__p = parName__p, debug = debug)

    return baseName, f

    
#=========================================================================
# ===> Fit sources for each scenario
errCookingFactor = 1.
fakeErr = None
fitAll = True
debug = True
n_jobs = 1

# Checking one source
iSource = 675
#iSource = 287
gScenarios = ['Sage2.00fr3.00']
#obsFits__f = [['F4363', 'F3727', 'F5007', 'F6584']]
obsFits__f = [['F3727', 'F5007', 'F6584']]

for gScenario in gScenarios:
    for obsFit__f in obsFits__f:
        pass
        #baseName, f = do_fit([iSource])
        #do_plots(f, iSource, baseName)


# Test interpolation
G = Gs[gScenario]
#par = G.singleValue_interp_regularGrid(inputParams={'OXYGEN': -4.223409, 'logN2O':-2., 'logUin':-2.3})
#G.interpolate_regularGrid(inputVars = ['OXYGEN', 'logN2O', 'logUin', 'fr', 'age'], nInterp = [2, 5, 1, 1, 1], )
#G.plot_origVSinterp_grid(addParams=par)

import emcee

def inputLims(pars):
    OXYGEN, logUin, logN2O, sigma = pars
    smin, smax = 0, 0.5
    omin, omax = G.inputVars__u['OXYGEN'].min(), G.inputVars__u['OXYGEN'].max()
    umin, umax = G.inputVars__u['logUin'].min(), G.inputVars__u['logUin'].max()
    nmin, nmax = G.inputVars__u['logN2O'].min(), G.inputVars__u['logN2O'].max()
    f = (sigma >= smin) & (sigma <= smax) & (OXYGEN >= omin) & (OXYGEN <= omax) & (logUin >= umin) & (logUin <= umax) & (logN2O >= nmin) & (logN2O <= nmax)
    return f

def sourcePars(line, iSource):
    l_obs = S.tabData[line][iSource]
    e_obs = S.tabData['e'+line][iSource]
    return l_obs, e_obs


def ln_likelihood(pars, lines, iSource):
    OXYGEN, logUin, logN2O, sigma = pars
    
    if not inputLims(pars):
        #p, par_values = -np.inf, None
        p, par_values = -np.inf, [-10, -10, -10]
    else:
        par = G.singleValue_interp_regularGrid(inputParams={'OXYGEN': OXYGEN, 'logUin': logUin, 'logN2O': logN2O}, outputVars=lines)
        p = 0.
        trunc = False
        for line in lines:
            l_model = par[line]
            l_obs, e_obs = sourcePars(line, iSource)
            #p += -0.5 * ((10**l_obs - 10**l_model)**2 / 10**sigma**2)
            p += -0.5 * ((l_obs - l_model)**2 / sigma**2)
            if (abs(l_model - l_obs) > 3*sigma):
                trunc = True
        if trunc:
            p = -np.inf
        #par_values = np.array([par[l][0] for l in obsFit__f])
        par_values = [par[l][0] for l in obsFit__f]
        #print par
        #print par_values
        
    return p, par_values


def ln_prior(pars):
    OXYGEN, logUin, logN2O, sigma = pars
    if inputLims(pars):
        p = 0
        #p = -np.log(sigma)
    else:
        p = -np.inf
    return p


def ln_probability(pars, lines, iSource):
    p, par_values = ln_likelihood(pars, lines, iSource)
    return ln_prior(pars) + p, par_values


# Here we'll set up the computation. emcee combines multiple "walkers",
# each of which is its own MCMC chain. The number of trace results will
# be nwalkers * nsteps

ndim = 4  # number of parameters in the model
# Real
nwalkers = 10  # number of MCMC walkers
nburn = 50  # "burn-in" period to let chains stabilize
nsteps = 1000  # number of MCMC steps to take
# Quick
#nwalkers = 8  # number of MCMC walkers
#nburn = 1  # "burn-in" period to let chains stabilize
#nsteps = 10  # number of MCMC steps to take


# set initial parameters
np.random.seed(0)
mid_grid = [np.median(G.inputVars__u['OXYGEN']), np.median(G.inputVars__u['logUin']), np.median(G.inputVars__u['logN2O']), 0.1]
starting_guesses = mid_grid + np.random.random((nwalkers, ndim))

# Here's the function call where all the work happens:
print 'MCMC with', obsFit__f
sampler = emcee.EnsembleSampler(nwalkers, ndim, ln_probability, args=[obsFit__f, iSource], threads = 4)
a = sampler.run_mcmc(starting_guesses, nsteps)


flag_walkers = (sampler.acceptance_fraction > 0.1)

samples1__sw = sampler.chain[flag_walkers]
samples2__sw = np.swapaxes(np.array(sampler.blobs), 0, 1)[flag_walkers]

# Plot chains
for i in range(10): 
    plt.close()
    
nplot.plotSetupMinion(fontsize=15)
from matplotlib.ticker import MaxNLocator
fig1, axes = plt.subplots(4, 1, sharex=True, figsize=(8, 9))
axes[0].plot(samples1__sw[..., 0].T, color="k", alpha=0.4)
axes[0].yaxis.set_major_locator(MaxNLocator(5))
axes[0].axhline(S.tabData['OXYGEN'][iSource], color="#888888", lw=2)
axes[0].set_ylabel("OXYGEN")

axes[1].plot(samples1__sw[..., 1].T, color="k", alpha=0.4)
axes[1].yaxis.set_major_locator(MaxNLocator(5))
axes[1].set_ylabel("logUin")

axes[2].plot(samples1__sw[..., 2].T, color="k", alpha=0.4)
axes[2].yaxis.set_major_locator(MaxNLocator(5))
axes[2].axhline(S.tabData['logN2O'][iSource], color="#888888", lw=2)
axes[2].set_ylabel("logN2O")

axes[3].plot(samples1__sw[..., 3].T, color="k", alpha=0.4)
axes[3].yaxis.set_major_locator(MaxNLocator(5))
axes[3].set_ylabel("sigma")
axes[3].set_xlabel("step number")


nplot.plotSetupMinion(fontsize=15)
from matplotlib.ticker import MaxNLocator
fig2, axes = plt.subplots(4, 1, sharex=True, figsize=(8, 9))
if "F4363" in obsFit__f:
    axes[0].plot(samples2__sw[..., 0].T, color="k", alpha=0.4)
    axes[0].yaxis.set_major_locator(MaxNLocator(5))
    axes[0].axhline(S.tabData['F4363'][iSource], color="#888888", lw=2)
    axes[0].set_ylabel("F4363")

axes[1].plot(samples2__sw[..., -3].T, color="k", alpha=0.4)
axes[1].yaxis.set_major_locator(MaxNLocator(5))
axes[1].axhline(S.tabData['F3727'][iSource], color="#888888", lw=2)
axes[1].set_ylabel("F3727")

axes[2].plot(samples2__sw[..., -2].T, color="k", alpha=0.4)
axes[2].yaxis.set_major_locator(MaxNLocator(5))
axes[2].axhline(S.tabData['F6584'][iSource], color="#888888", lw=2)
axes[2].set_ylabel("F6584")

axes[3].plot(samples2__sw[..., -1].T, color="k", alpha=0.4)
axes[3].yaxis.set_major_locator(MaxNLocator(5))
axes[3].set_ylabel("F5007")
axes[3].axhline(S.tabData['F5007'][iSource], color="#888888", lw=2)
axes[3].set_xlabel("step number")


# Plot PDFs
samples1 = samples1__sw[:, nburn:, :].reshape((-1, ndim))
samples2 = samples2__sw[:, nburn:, :].reshape((-1, len(obsFit__f)))

O_mcmc, U_mcmc, N_mcmc, s_mcmc = map(lambda v: (v[1], v[2], v[0]),
                                     zip(*np.percentile(samples1, [16, 50, 84],
                                                        axis=0)))
#print O_mcmc, U_mcmc, N_mcmc, s_mcmc
O2_mcmc, O4363_mcmc, N2_mcmc, O3_mcmc = map(lambda v: (v[1], v[2], v[0]),
                                            zip(*np.percentile(samples1, [16, 50, 84],
                                                               axis=0)))
#print O2_mcmc, O4363_mcmc, N2_mcmc, O3_mcmc

import triangle2
fig3 = triangle2.corner(samples1, labels=["OXYGEN", "logUin", "logN2O", "sigma"],
                 truths=[S.tabData['OXYGEN'][iSource], 0, S.tabData['logN2O'][iSource], 0],
                 quantiles=[0.16, 0.50, 0.84])
fig4 = triangle2.corner(samples2, labels=obsFit__f,
                 truths=[S.tabData[l][iSource] for l in obsFit__f],
                 quantiles=[0.16, 0.50, 0.84])

# Save
Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
suffix = '_'.join(obsFit__f)
baseName = 'fit_grid%s_%s_%s' % (grid, Fscenario, suffix)

nplot.save(fig1, 'emceeChainsOut_%s_s%04i.png' % (baseName, iSource))
nplot.save(fig2, 'emceeChainsIn_%s_s%04i.png' % (baseName, iSource))
nplot.save(fig3, 'emceeChainsPDFsOut_%s_s%04i.png' % (baseName, iSource))
nplot.save(fig4, 'emceeChainsPDFsIn_%s_s%04i.png' % (baseName, iSource))


import h5py
h5f = h5py.File('emceeChains_%s_s%04i.hdf5' % (baseName, iSource), 'w')
h5f.create_dataset('samples1__sw', data=samples1__sw)
h5f.create_dataset('samples2__sw', data=samples2__sw)
h5f.close()


#h5f = h5py.File('lix.hdf5','r')
#b = h5f['dataset'][:]
#h5f.close()
#print np.allclose(sampler.chain,b)
'''
'''
