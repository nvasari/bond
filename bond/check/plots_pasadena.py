'''
Plots to fit sources with all scenarios

Natalia@UFSC - 22/May/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

# Read lines, all_lines and all_ratios
obsFit1__f, grid, scen, sources = slr.options('', 'lines', *sys.argv[2:])
suffix1 = '_'.join(obsFit1__f)
#obsFit2__f, grid, scen, sources = slr.options('', 'all_lines', *sys.argv[2:])
#suffix2 = '_'.join(obsFit2__f)
#suffixes = [suffix1, suffix2]
suffixes = [suffix1]

#=========================================================================
# ===> Read fits & do plots
gScenarios = ['Sage2.00fr0.03']
#gScenarios = ['Sage2.00fr0.03', 'Sage6.00fr3.00', 'Sage2.00fr3.00', 'Sage6.00fr0.03', 'Sage1.00fr0.03', 'Sage1.00fr3.00', 'Sage5.00fr3.00']
 
columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

dfig = 0

Fs = {}
OH = {}

for sScenario in sScenarios:
  for gScenario in gScenarios:
    for suffix in suffixes:
     if (suffix != 'F3727_F5007_F6584_F4363') | ((suffix == 'F3727_F5007_F6584_F4363') & (gScenario == 'Sage2.00fr0.03')):
       
          Fscenario = 's%s_g%s' % (sScenario, gScenario)
          dirName = 'fits/'
          baseName = 'fit_grid%s_%s_%s' % (grid, Fscenario, suffix)
          
          # ===> Read fit
          fitFile = os.path.join(dirName, '%s_summary' % baseName)
          f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = True)
          f.dirName = dirName
          f.baseName = baseName, 
          G = Gs[gScenario]
          f.sources = S
          f.grid = G
          Fs[Fscenario] = f
  
          # Check results for average 
          ftype = 'jpdf'
          fresult = f.PDFSummaries__tcps[ftype]     
  
          #### Choosing branches
          f.chooseBranch(temp_sumType=ftype, recalc=True)

        
#f1 = slr.f['gridfull_sSPIRALS-HEBCD_gSage1.00fr0.03_F3727_F5007_F6584']
f2 = slr.f['gridfull_sSPIRALS-HEBCD_gSage2.00fr0.03_F3727_F5007_F6584']
#f5 = slr.f['gridfull_sSPIRALS-HEBCD_gSage5.00fr3.00_F3727_F5007_F6584']
#f6 = slr.f['gridfull_sSPIRALS-HEBCD_gSage6.00fr3.00_F3727_F5007_F6584']
#f1a = slr.f['gridfull_sSPIRALS-HEBCD_gSage1.00fr3.00_F3727_F5007_F6584']
#f2a = slr.f['gridfull_sSPIRALS-HEBCD_gSage2.00fr3.00_F3727_F5007_F6584']
#f6a = slr.f['gridfull_sSPIRALS-HEBCD_gSage6.00fr0.03_F3727_F5007_F6584']
#fo3 = slr.f['gridfull_sSPIRALS-HEBCD_gSage2.00fr0.03_F3727_F5007_F6584_F4363']

'''
####################################################################################################
# Izotov sample
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=15)
fig = plt.figure(1)
plt.clf()
nplot.title_date('HEBCD %s' % (suffix.replace('_', ' ')))

dOH = f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - f5.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
OH['d_OH_2m6'] = dOH
dOH = f1.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - f5.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
OH['d_OH_1m6'] = dOH
dOH = f1.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
OH['d_OH_2m6'] = dOH
dOH = f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - f2a.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
OH['d_OH2_0.03m3.00'] = dOH
#dOH = f5a.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - f5.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
#OH['d_OH6_0.03m3.00'] = dOH

dOH = f2.PDFSummaries__tcps[ftype]['sol']['logN2O'] - f5.PDFSummaries__tcps[ftype]['sol']['logN2O']
OH['d_NO_2m6'] = dOH
dOH = f1.PDFSummaries__tcps[ftype]['sol']['logN2O'] - f5.PDFSummaries__tcps[ftype]['sol']['logN2O']
OH['d_NO_1m6'] = dOH
dOH = f1.PDFSummaries__tcps[ftype]['sol']['logN2O'] - f2.PDFSummaries__tcps[ftype]['sol']['logN2O']
OH['d_NO_2m6'] = dOH
dOH = f2.PDFSummaries__tcps[ftype]['sol']['logN2O'] - f2a.PDFSummaries__tcps[ftype]['sol']['logN2O']
OH['d_NO2_0.03m3.00'] = dOH
#dOH = f5a.PDFSummaries__tcps[ftype]['sol']['logN2O'] - f5.PDFSummaries__tcps[ftype]['sol']['logN2O']
#OH['d_NO6_0.03m3.00'] = dOH


flag_Izotov = (S.tabData['ref'] == 'z')
flag = (S.tabData['flagOk_OH']) & (flag_Izotov)

subp = 2, 3

ax = plt.subplot2grid(subp, (0, 0))
pl.plot_fit_obsAndFit(f2, xlab = 'ewHb', ylab = 'OXYGEN', dy = 12, flag = flag, addToTitle = 'BOND |', colours='b', diff_y = False, pltLegend = False)
plt.ylim(6.5, 9)
nplot.fix_ticks(ax)

lWHb = 100
flag2 = (S.tabData['ewHb'] >  lWHb) & flag_Izotov
flag5 = (S.tabData['ewHb'] <= lWHb) & flag_Izotov

ax = plt.subplot2grid(subp, (1, 0))
pl.plot_fit_obsAndFit(f2, xlab = 'ewHb', ylab = 'OXYGEN', dy = 12, flag = flag2, addToTitle = 'BOND |', colours='b', diff_y = False, pltLegend = False)
pl.plot_fit_obsAndFit(f5, xlab = 'ewHb', ylab = 'OXYGEN', dy = 12, flag = flag5, addToTitle = 'BOND |', colours='r', diff_y = False, pltLegend = False)
plt.ylim(6.5, 9)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 1))
plt.plot(S.tabData['ewHb'][flag], 12+S.tabData['OXYGEN'][flag], 'k.')
plt.xlabel(r'$W_{\mathrm{H\beta}}$ [$\AA$]')
plt.ylabel('12 + log O/H$_\mathrm{Te}$')
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1))
plt.plot(S.tabData['ewHb'][flag], S.tabData['O3O2'][flag], 'k.')
plt.xlabel('$W_{\mathrm{H\beta}}$ [$\AA$]')
plt.ylabel('O3O2')
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2))
plt.plot(12+S.tabData['OXYGEN'][flag], 12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][flag], 'k.')
plt.xlabel(r'12 + log O/H$_\mathrm{Te}$')
plt.ylabel(r'12 + log O/H$_\mathrm{2Myr}$')
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2))
plt.plot(12+S.tabData['OXYGEN'][flag], 12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][flag], 'b.', lw = 1)
plt.xlabel(r'12 + log O/H$_\mathrm{Te}$')
plt.ylabel(r'12 + log O/H$_\mathrm{2Myr}$')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2))
plt.plot(12+S.tabData['OXYGEN'][flag2], 12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][flag2], 'b.', lw = 1)
plt.plot(12+S.tabData['OXYGEN'][flag5], 12+f5.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][flag5], 'r.', lw = 1)
plt.xlabel(r'12 + log O/H$_\mathrm{Te}$')
plt.ylabel(r'12 + log O/H$_\mathrm{2,6Myr}$')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
nplot.fix_ticks(ax)

fig.set_tight_layout(True)
nplot.save(fig, 'Pasadena_HEBCD_WHb_dOH_2_6Myr.png')


####################################################################################################
# HII regions IMF sampling
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=15, override_params = {'figure.subplot.hspace': 0., 'figure.subplot.wspace': 0.,})
fig = plt.figure(2)
plt.clf()
nplot.title_date('Spirals %s' % (suffix.replace('_', ' ')))

# O/H from MacGaugh 91 (Kewley & Ellison 2008, A1, A2)
ylab = S.tabData['O3O2'] + np.log10(1 + 1/2.97)
xlab = S.tabData['O23']
M_OH_low = -4.944 + 0.767 * xlab + 0.602 * xlab**2 - ylab * (0.29 + 0.332 * xlab - 0.331 * xlab**2)
M_OH_upp = -2.939 - 0.2 * xlab - 0.237 * xlab**2 - 0.305 * xlab**3 - 0.0283 * xlab**4 - ylab * (0.0047 - 0.0221 * xlab - 0.102 * xlab**2 - 0.0817 * xlab**3 - 0.00717 * xlab**4)
M_OH = np.where( (S.tabData['N2O2'] < -1.2), M_OH_low, M_OH_upp)
M_OH = np.where( (S.tabData['O3O2'] > -999) & (S.tabData['O23'] > -999), M_OH, -999)
#OH['M91_OH'] = M_OH
OH['M91_OH'] = M_OH_upp

# Calc luminosities
log_4PId2 = np.log10(4 * np.pi * S.tabData['dist']**2) + 2 * np.log10(1e6 * 3.086e18)
log_Lsun  = np.log10(3.826e33)
FHb       = S.tabData['FHB']
log_LHb   = np.where((FHb > 0), utils.safe_log10(FHb) + log_4PId2 - log_Lsun, -999)
log_LHb_min = np.log10(5000.)

# Flag 
#flag = (S.tabData['flagOk_OH']) & 
flag = (~S.tabData['r/r25'].mask) & (log_LHb > -999)
a = S.tabData[flag][['VT', 'namegal']].copy()
a.sort(['VT'])
a = astropy.table.unique(a)
namegals = a['namegal']

# Fix distance for NGC 5236
#d = 6.806
#ff = (S.tabData['namegal'] == 'NGC_5236')
#S.tabData['dist'][ff] = d



# Plots
subp = 4, 4
fig, axes = plt.subplots(subp[0], subp[1], num = 2, sharex=True, sharey='row')
colours = sns.hls_palette(len(namegals), l=.3, s=.8)

#sns.color_palette("hls", )
#for iax, namegal in enumerate(namegals[::-1][:1]):
for iax, namegal in enumerate(namegals):
    pltYlab = False
    if iax == 0:
        pltYlab = True
        
    f = flag & (S.tabData['namegal'] == namegal)
    VT = S.tabData[f]['VT'][0]
    LHb = S.tabData[f]['FHB']

    ax = axes[0, iax]
    plt.sca(ax)
    flag2 = f & (log_LHb >= log_LHb_min) & (log_LHb > -999)
    flag6 = f & (log_LHb <  log_LHb_min) & (log_LHb > -999)
    #print f.sum(), flag2.sum(), flag6.sum() #, log_LHb[f], S.tabData['TOTLHB'][f]
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = flag2, colours=colours[iax], pltTitle=False, s=20, marker='o', lw = 2, label='BOND 2 Myr', pltYlab=pltYlab, pltXlab = False)
    pl.plot_table_xy(S.tabData, table_y = f6.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = flag6, colours=colours[iax], pltTitle=False, s=30, marker='o', lw = 2, set_facecolors=False, label='BOND 6 Myr', pltYlab=pltYlab, pltXlab = False)

    _f2 = flag2 & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] < 0)
    _f6 = flag6 & (f6.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] < 0)
    x = np.hstack((np.array(S.tabData['r/r25'][_f2]), np.array(S.tabData['r/r25'][_f6])))
    y = 12 + np.hstack((f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][_f2], f6.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][_f6]))
    regression = np.polyfit(x, y, 1)
    r_x = np.arange(-1, 5)
    r_y = r_x * regression[0] + regression[1]
    plt.plot(r_x, r_y, color=colours[iax], alpha=0.5)
    
    plt.title('%s (VT = %s)' % (namegal.replace('_', ''), VT))
    #ax.text(0.08, 0.92, '%s (VT = %s)' % (namegal.replace('_', ''), VT), ha='left', va='center', transform=ax.transAxes, fontsize='small')
    
    plt.ylim(7.6, 9.4)
    nplot.fix_ticks(ax, 4, 3)
    
    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')

        
    ftype = 'jpdf'
    f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2Ha': [-1.5, 'low']})
    ax = axes[1, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = f, colours=colours[iax], pltTitle=False, marker = 'o', s = 30., label='BOND 2 Myr', pltYlab=pltYlab, pltXlab = False)
    
    _f = f & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] < 0)
    regression = np.polyfit(S.tabData['r/r25'][_f], 12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][_f], 1)
    r_x = np.arange(-1, 5)
    r_y = r_x * regression[0] + regression[1]
    plt.plot(r_x, r_y, color=colours[iax], alpha=0.5)

    plt.ylim(7.6, 9.4)
    nplot.fix_ticks(ax, 4, 3)
    
    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')

        
    ax = axes[2, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, table_y = OH, xlab = 'r/r25', ylab = 'M91_OH', dy = 12, flag = f, pltTitle=False, colours=colours[iax], s=20, marker='x', lw = 2, label='M91', pltYlab=pltYlab, pltXlab = False)
    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')

    _f = f & (OH['M91_OH'] > -900)
    regression = np.polyfit(S.tabData['r/r25'][_f], 12+OH['M91_OH'][_f], 1)
    r_x = np.arange(-1, 5)
    r_y = r_x * regression[0] + regression[1]
    plt.plot(r_x, r_y, color=colours[iax], alpha=0.5)

    #iSrc = 569
    #plt.plot(S.tabData['r/r25'][iSrc], (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - OH['M91_OH'])[iSrc], 'bx')

    plt.ylim(7.6, 9.4)
    nplot.fix_ticks(ax, 4, 3)

    # Differences
    #ax = axes[3, iax]
    #plt.sca(ax)
    #plt.scatter(S.tabData['r/r25'][f], (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - OH['M91_OH'])[f], lw = 0)

    # F4363 limits
    ax = axes[3, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['low'], xlab = 'r/r25', ylab = 'F4363', flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, pltYlab=pltYlab, pltXlab = True)
    if iax == 0:
        ax.set_ylabel(r'log [OIII]4363/H$\beta_\mathrm{lower-b}$')
    
    #ax = axes[3, iax]
    #plt.sca(ax)
    #pl.plot_table_xy(S.tabData, xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, label='$T_e$', pltYlab=pltYlab, pltXlab = True)
    #if iax == 0:
    #    plt.legend(loc = 'lower left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.05, 0.05), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')
    #
    #_f = f & (S.tabData['OXYGEN'] > -999)
    #regression = np.polyfit(S.tabData['r/r25'][_f], 12+S.tabData['OXYGEN'][_f], 1)
    #r_x = np.arange(-1, 5)
    #r_y = r_x * regression[0] + regression[1]
    #plt.plot(r_x, r_y, color=colours[iax], alpha=0.5)
    
    #plt.ylim(7.6, 9.4)
    nplot.fix_ticks(ax, 4, 3)
    
    plt.xlim(-0.1, 3)
    
plt.subplots_adjust( hspace=0, wspace=0 )
nplot.save(fig, 'Pasadena_Spirals_gradients_2_6Myr.png')


fig = plt.figure(102)
plt.clf()

d = OH['M91_OH'] - f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
ff = (OH['M91_OH'] > -999) & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] > -999)

ax = plt.subplot(221)
plt.scatter(OH['M91_OH'][ff], d[ff], lw=0)
plt.xlabel('12 + log O/H$_\mathrm{M91}$')
plt.ylabel('log O/H$_\mathrm{M91}$ - log O/H$_\mathrm{grid}$')
plt.title('2 Myr')
plt.axhline(0, color = '#666666', lw = 1)
nplot.fix_ticks(ax)

d = OH['M91_OH'] - f1.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
ff = (OH['M91_OH'] > -999) & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] > -999)

ax = plt.subplot(222)
plt.scatter(OH['M91_OH'][ff], d[ff], lw=0)
plt.xlabel('12 + log O/H$_\mathrm{M91}$')
plt.ylabel('log O/H$_\mathrm{M91}$ - log O/H$_\mathrm{grid}$')
plt.title('1 Myr')
plt.axhline(0, color = '#666666', lw = 1)
nplot.fix_ticks(ax)

d = OH['M91_OH'] - f6.PDFSummaries__tcps[ftype]['sol']['OXYGEN']
ff = (OH['M91_OH'] > -999) & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] > -999)

ax = plt.subplot(223)
plt.scatter(OH['M91_OH'][ff], d[ff], lw=0)
plt.xlabel('12 + log O/H$_\mathrm{M91}$')
plt.ylabel('log O/H$_\mathrm{M91}$ - log O/H$_\mathrm{grid}$')
plt.title('6 Myr')
plt.axhline(0, color = '#666666', lw = 1)
nplot.fix_ticks(ax)


# Plots
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=13, override_params = {'figure.subplot.hspace': 0., 'figure.subplot.wspace': 0.,})
subp = 4, 4
fig, axes = plt.subplots(subp[0], subp[1], num = 3, sharex='col', sharey='row')
colours = sns.hls_palette(len(namegals), l=.3, s=.8)
#sns.color_palette("hls", )
for iax, namegal in enumerate(namegals):
    pltYlab = False
    if iax == 0:
        pltYlab = True
        
    f = flag & (S.tabData['namegal'] == namegal)
    VT = S.tabData[f]['VT'][0]
    LHb = S.tabData[f]['FHB']
    
    ax = axes[0, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, xlab = 'r/r25', ylab = 'N2O2', flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, pltYlab=pltYlab, pltXlab = False)
    nplot.fix_ticks(ax, 4, 3)

    ax = axes[1, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, xlab = 'r/r25', ylab = 'O23', flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, pltYlab=pltYlab, pltXlab = False)
    nplot.fix_ticks(ax, 4, 3)

    ax = axes[2, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, xlab = 'r/r25', ylab = 'O3O2', flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, pltYlab=pltYlab, pltXlab = False)
    nplot.fix_ticks(ax, 4, 3)
    
    ax = axes[3, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, xlab = 'r/r25', ylab = 'F6584', flag = f, pltTitle=False, colours=colours[iax], s=20, marker='^', lw = 2, pltYlab=pltYlab, pltXlab = True)
    nplot.fix_ticks(ax, 4, 3)
    
    plt.xlim(-0.1, 3)

    
plt.subplots_adjust( hspace=0, wspace=0 )
nplot.save(fig, 'Pasadena_Spirals_gradients_ELs.png')



# Plots - O/H gradient
psetup(fig_width_pt=screenwidth, aspect=0.55, lw=3., fontsize=25, override_params = {'figure.subplot.hspace': 0., 'figure.subplot.wspace': 0.,})
subp = 2, 4
fig, axes = plt.subplots(subp[0], subp[1], num = 102, sharex=True, sharey=True)

#sns.color_palette("hls", )
for iax, namegal in enumerate(namegals):
    pltYlab = False
    if iax == 0:
        pltYlab = True
        
    f = flag & (S.tabData['namegal'] == namegal) & (f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] != 0) & (f6.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] != 0)
    VT = S.tabData[f]['VT'][0]
    LHb = S.tabData[f]['FHB']
    
    ax = axes[-2, iax]
    plt.sca(ax)
    flag2 = f & (log_LHb >= log_LHb_min) & (log_LHb > -999)
    flag6 = f & (log_LHb <  log_LHb_min) & (log_LHb > -999)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = flag2, colours=colours[2], pltTitle=False, s=20, marker='o', label='2 Myr', pltYlab=pltYlab, pltXlab = False)
    pl.plot_table_xy(S.tabData, table_y = f6.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = flag6, colours=colours[0], pltTitle=False, s=20, marker='o', label='6 Myr', pltYlab=pltYlab, pltXlab = False)
    
    plt.title('%s (VT = %s)' % (namegal.replace('_', ''), VT), fontsize='small')

    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')
        
    ax = axes[-1, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'OXYGEN', dy = 12, flag = f, colours=colours[2], pltTitle=False, s=20, marker='o', label='2 Myr', pltYlab=pltYlab, pltXlab = True)
    
    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')        

    
plt.xlim(-0.1, 3)
plt.ylim(7.1, 9.5)
nplot.fix_ticks(ax, 4, 3)

plt.subplots_adjust( hspace=0, wspace=0 )
nplot.save(fig, 'Pasadena_Spirals_gradients_2_6Myr_1.png')



# Plots - N/O gradients
psetup(fig_width_pt=screenwidth, aspect=0.55, lw=3., fontsize=25, override_params = {'figure.subplot.hspace': 0., 'figure.subplot.wspace': 0.,})
subp = 2, 4
fig, axes = plt.subplots(subp[0], subp[1], num = 202, sharex=True, sharey=True)

#sns.color_palette("hls", )
for iax, namegal in enumerate(namegals):
    pltYlab = False
    if iax == 0:
        pltYlab = True
        
    f = flag & (S.tabData['namegal'] == namegal) & (f2.PDFSummaries__tcps[ftype]['sol']['logN2O'] != 0) & (f6.PDFSummaries__tcps[ftype]['sol']['logN2O'] != 0)
    VT = S.tabData[f]['VT'][0]
    LHb = S.tabData[f]['FHB']
    
    ax = axes[-2, iax]
    plt.sca(ax)
    flag2 = f & (log_LHb >= log_LHb_min) & (log_LHb > -999)
    flag6 = f & (log_LHb <  log_LHb_min) & (log_LHb > -999)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'logN2O', flag = flag2, colours=colours[2], pltTitle=False, s=20, marker='o', label='2 Myr', pltYlab=pltYlab, pltXlab = False)
    pl.plot_table_xy(S.tabData, table_y = f6.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'logN2O', flag = flag6, colours=colours[0], pltTitle=False, s=20, marker='o', label='6 Myr', pltYlab=pltYlab, pltXlab = False)
    
    plt.title('%s (VT = %s)' % (namegal.replace('_', ''), VT), fontsize='small')

    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')
        
    ax = axes[-1, iax]
    plt.sca(ax)
    pl.plot_table_xy(S.tabData, table_y = f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'r/r25', ylab = 'logN2O', flag = f, colours=colours[2], pltTitle=False, s=20, marker='o', label='2 Myr', pltYlab=pltYlab, pltXlab = True)
    
    if iax == 0:
        plt.legend(loc = 'upper left', frameon = False, numpoints = 1, scatterpoints = 1, bbox_to_anchor=(0.6, 0.98), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')        

    
plt.xlim(-0.1, 3)
#plt.ylim(7.1, 9.5)
nplot.fix_ticks(ax, 4, 3)

plt.subplots_adjust( hspace=0, wspace=0 )
nplot.save(fig, 'Pasadena_Spirals_gradients_2_6Myr_NO.png')


####################################################################################################
# N/O vs O/H
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=15)
fig = plt.figure(4)
plt.clf()
nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))

# Pilyugin O/H and N/O
# ON calibration from Pilyugin Vilchez Thuan 2010
r2 = S.tabData['F3727']
r3 = utils.safe_sum(S.tabData['F5007'], np.log10(1.34))
n2 = utils.safe_sum(S.tabData['F6584'], np.log10(1.34))
F6716 = np.where(S.tabData['F6716'] > -999, 10**S.tabData['F6716'], 0)
F6731 = np.where(S.tabData['F6731'] > -999, 10**S.tabData['F6716'], 0)
s2 = utils.safe_log10(F6716 + F6731)
#s2 = S.tabData['F6724']

n2r2 = utils.safe_sub(n2, r2)
n2s2 = utils.safe_sub(n2, s2)

AO_ON = np.full_like(r3, -999.)
AN_ON = np.full_like(r3, -999.)

_f = (n2 > -0.1)
AO_ON[_f] = (8.606 - 0.105*r3 - 0.410*r2 - 0.150*n2r2)[_f]
AN_ON[_f] = (7.955 + 0.048*r3 - 0.171*n2 + 1.015*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 > -0.25)
AO_ON[_f] = (8.642 + 0.077*r3 + 0.411*r2 + 0.601*n2r2)[_f]
AN_ON[_f] = (7.928 + 0.291*r3 + 0.454*n2 + 0.953*n2r2)[_f]

_f = (n2 <= -0.1) & (n2s2 <= -0.25) & (n2s2 > -999)
AO_ON[_f] = (8.013 + 0.905*r3 + 0.602*r2 + 0.751*n2r2)[_f]
AN_ON[_f] = (7.505 + 0.839*r3 + 0.492*n2 + 0.970*n2r2)[_f]

OH['AO_ON'] = utils.safe_sub(AO_ON, 12)
OH['AN_ON'] = utils.safe_sub(AN_ON, AO_ON)

# Plots
subp = 4, 5

fy = f2

ff = S.tabData['flagOk_ab'] & (OH['AO_ON'] > -999) & (OH['AN_ON'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = ff, colours = 'k', addToTitle = 'Te | ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 1)) 
plt.title('Pilyugin TO DO')
pl.plot_table_xy(OH, xlab = 'AO_ON', ylab = 'AN_ON', dx = 12, flag = ff, colours = 'k', addToTitle = 'P10,13 | ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

flag2 = (log_LHb >= log_LHb_min) | (log_LHb <= -999)
ax = plt.subplot2grid(subp, (1, 0)) 
pl.plot_fit_chooseBranch(fy, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag2), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1)) 
pl.plot_fit_chooseBranch(fy, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = flag2, addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2)) 
plt.title('BOND')
pl.plot_fit_chooseBranch(fy, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'wro', flag = flag2 & (fy.branchCrit__s == 'N2O2'), addToTitle = 'BOND 2Myr w | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

flag6 = (log_LHb <  log_LHb_min) | (log_LHb <= -999)
ax = plt.subplot2grid(subp, (2, 0)) 
pl.plot_fit_chooseBranch(f6, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag6), addToTitle = 'BOND 6 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 1)) 
pl.plot_fit_chooseBranch(f6, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = flag6, addToTitle = 'BOND 6 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 2)) 
plt.title('BOND')
pl.plot_fit_chooseBranch(f6, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'wro', flag = flag6 & (f6.branchCrit__s == 'N2O2'), addToTitle = 'BOND 6Myr w | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2)) 
plt.title('BOND')
pl.plot_fit_chooseBranch(f6, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'wro', flag = [1], addToTitle = 'BOND 6Myr w | ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

##

ax = plt.subplot2grid(subp, (3, 0)) 
pl.plot_table_xy(fy.PDFSummaries__tcps[ftype]['sol'], table_y = fo3.PDFSummaries__tcps[ftype]['sol'], dx = 12, dy = 12, xlab = 'OXYGEN', ylab = 'OXYGEN', flag = S.tabData['flagOk_ab'], addToTitle = '4363 BvB |', colours='k')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 1)) 
pl.plot_fit_obsAndFit(fo3, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, diff_y = False, addToTitle = 'Te vs B4363 | ', pltLegend=False)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

flag2 = (log_LHb >= log_LHb_min) | (log_LHb <= -999)
ax = plt.subplot2grid(subp, (3, 2)) 
pl.plot_fit_chooseBranch(fo3, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag2), addToTitle = 'B4363 | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)


ax = plt.subplot2grid(subp, (1, 3)) 
pl.plot_fit_obsAndFit(fy, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, diff_y = False, addToTitle = 'Te vs 2Myr | ', pltLegend=False)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 3)) 
pl.plot_fit_obsAndFit(f6, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, diff_y = False, addToTitle = 'Te vs 6Myr | ', pltLegend=False)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

        
ax = plt.subplot2grid(subp, (0, 3)) 
pl.plot_table_xy(S.tabData, table_y = OH, dx = 12, xlab = 'OXYGEN', ylab = 'd_OH_2m6', flag = S.tabData['flagOk_ab'], addToTitle = 'Te |', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 4)) 
pl.plot_table_xy(fy.PDFSummaries__tcps[ftype]['sol'], table_y = OH, dx = 12, xlab = 'OXYGEN', ylab = 'd_OH_2m6', addToTitle = 'BOND |', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 4)) 
pl.plot_fit_obsAndFit(fy, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, flag = (S.tabData['flagOk_ab']), addToTitle = 'Te vs 2Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-1.5, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 4)) 
pl.plot_fit_obsAndFit(f6, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, flag = (S.tabData['flagOk_ab']), addToTitle = 'Te vs 6Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-1.5, 1.5)
nplot.fix_ticks(ax)

#ax = plt.subplot2grid(subp, (2, 4)) 
#pl.plot_fit_obsAndFit(f6, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, x_data = False, addToTitle = 'Te vs 6Myr | ', pltLegend=False)
#plt.xlim(6.3, 9.7)
#plt.ylim(-1, 2.5)
#nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 3)) 
pl.plot_table_xy(S.tabData, table_y = OH, dx = 12, xlab = 'OXYGEN', ylab = 'd_OH2_0.03m3.00', flag = S.tabData['flagOk_ab'], addToTitle = '2 Myr', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 4)) 
pl.plot_table_xy(S.tabData, table_y = OH, dx = 12, xlab = 'OXYGEN', ylab = 'd_OH6_0.03m3.00', flag = S.tabData['flagOk_ab'], addToTitle = '6 Myr', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)


fig.set_tight_layout(True)

nplot.save(fig, 'Pasadena_Spirals_HEBCD_NO_OH.png')




# N/O plots
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=15)
fig = plt.figure(104)
plt.clf()
nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))


# Plots
subp = 4, 5

fy = f2

##

flag2 = (log_LHb <= -999)
ax = plt.subplot2grid(subp, (0, 0)) 
pl.plot_fit_chooseBranch(fy, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag2), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

f2.readChi2File(sourcesRows=np.where(flag2)[0])
ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_jpdf_chooseBranch(fy, X = 'OXYGEN', Y = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag2), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 0)) 
pl.plot_table_xy(fy.PDFSummaries__tcps[ftype]['sol'], table_y = fo3.PDFSummaries__tcps[ftype]['sol'], xlab = 'logN2O', ylab = 'logN2O', flag = S.tabData['flagOk_ab'], addToTitle = '4363 BvB |', colours='k')
nplot.plot_identity(np.array([-2.2, 0.4]), np.array([-2.2, 0.4]), color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 1)) 
pl.plot_fit_obsAndFit(fo3, xlab = 'logN2O', ylab = 'logN2O', diff_y = False, addToTitle = 'Te vs B4363 | ', pltLegend=False)
nplot.plot_identity(np.array([-2.2, 0.4]), np.array([-2.2, 0.4]), color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 3)) 
pl.plot_fit_obsAndFit(fy, xlab = 'logN2O', ylab = 'logN2O', diff_y = False, addToTitle = 'Te vs 2Myr | ', pltLegend=False)
nplot.plot_identity(np.array([-2.2, 0.4]), np.array([-2.2, 0.4]), color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 3)) 
pl.plot_fit_obsAndFit(f6, xlab = 'logN2O', ylab = 'logN2O',  diff_y = False, addToTitle = 'Te vs 6Myr | ', pltLegend=False)
nplot.plot_identity(np.array([-2.2, 0.4]), np.array([-2.2, 0.4]), color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

        
ax = plt.subplot2grid(subp, (0, 3)) 
pl.plot_table_xy(S.tabData, table_y = OH, xlab = 'logN2O', ylab = 'd_NO_2m6', flag = S.tabData['flagOk_ab'], addToTitle = 'Te |', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 4)) 
pl.plot_table_xy(fy.PDFSummaries__tcps[ftype]['sol'], table_y = OH, xlab = 'logN2O', ylab = 'd_NO_2m6', addToTitle = 'BOND |', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 4)) 
pl.plot_fit_obsAndFit(fy, xlab = 'logN2O', ylab = 'logN2O', flag = (S.tabData['flagOk_ab']), addToTitle = 'Te vs 2Myr | ', pltLegend=False)
plt.xlim(-2.2, 0.4)
plt.ylim(-1.5, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 4)) 
pl.plot_fit_obsAndFit(f6, xlab = 'logN2O', ylab = 'logN2O', flag = (S.tabData['flagOk_ab']), addToTitle = 'Te vs 6Myr | ', pltLegend=False)
plt.xlim(-2.2, 0.4)
plt.ylim(-1.5, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 3)) 
pl.plot_table_xy(S.tabData, table_y = OH, xlab = 'logN2O', ylab = 'd_NO2_0.03m3.00', flag = S.tabData['flagOk_ab'], addToTitle = '2 Myr', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 4)) 
pl.plot_table_xy(S.tabData, table_y = OH, xlab = 'logN2O', ylab = 'd_NO6_0.03m3.00', flag = S.tabData['flagOk_ab'], addToTitle = '6 Myr', colours='k')
plt.axhline(0, color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-1, 2.5)
nplot.fix_ticks(ax)


fig.set_tight_layout(True)

nplot.save(fig, 'Pasadena_Spirals_HEBCD_NO.png')



####################################################################################################
# Rotating grid
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=20)

iSource = 287
from rotanimate import rotanimate
angles = np.linspace(0,360,19)[1:] # A list of 20 angles between 0 and 360

fig = plt.figure(5)
plt.clf()
ax = pl.plot_grid_3D(d=0.2, fit=f2, iSource=iSource, fignumber=5, branch='all', alpha = 0.5)
nplot.title_date(r'$\Delta = 0.2$ for F3727 F5007 F6584')
w_in, h_in = fig.get_size_inches()
rotanimate(ax, angles, 'Pasadena_grid3D_N2O3O2_%s_s%04i.gif' % (baseName, iSource), delay=20, width=w_in, height=h_in)


####################################################################################################
# Joint PDF / bimodality
psetup(fig_width_pt=0.5*screenwidth, aspect=0.9, lw=3., fontsize=20, override_params = {'figure.subplot.left': 0.18, 'figure.subplot.bottom': 0.18})

f2.readChi2File(sourcesRows=[287])
f2.loadDataSources([287])

iSource = 287
fig = plt.figure(6)
pl.plot_jointPDF(f2, iSource, 'OXYGEN', 'logN2O', dx=12, zoom=True, pltFancyLabels=True)
plt.ylim(-2., -0.8)

nplot.save(fig, 'Pasadena_jointPDF_bimodality.png')



####################################################################################################
# Grid bimodality
psetup(fig_width_pt=screenwidth, aspect=0.3, lw=3., fontsize=25, override_params = {'figure.subplot.bottom': 0.18})
fig = plt.figure(7)
plt.clf()

f_low = f2.grid.branch['low']
f_upp = f2.grid.branch['upp']

ax1 = plt.subplot(131)
plt.plot(12+f2.grid.tabGrid['OXYGEN'], f2.grid.tabGrid['O23'], '#EEEEEE', marker=',')
plt.title('Full grid')
plt.ylabel(r'log ([OIII]+[OII])/H$\beta$')

ax2 = plt.subplot(132, sharey=ax1, sharex=ax1)
plt.plot(12+f2.grid.tabGrid['OXYGEN'][f_low], f2.grid.tabGrid['O23'][f_low], 'b,')
plt.title('Lower branch')
plt.xlabel(r'12 + log O/H')
plt.setp(ax2.get_yticklabels(), visible=False)

ax3 = plt.subplot(133, sharey=ax1, sharex=ax1)
plt.plot(12+f2.grid.tabGrid['OXYGEN'][f_upp], f2.grid.tabGrid['O23'][f_upp], 'r,')
plt.title('Upper branch')
nplot.fix_ticks(ax1)
plt.setp(ax3.get_yticklabels(), visible=False)

nplot.save(fig, 'Pasadena_grid_bimodality.png')


####################################################################################################
# McGaugh 91
Gs2 = slr.read_OurGrids('orig' , scen, forceRead = False)
GNO = slr.read_OurGrids('NO' , scen, forceRead = False)
G = GNO['Sage2.00fr0.03']
#G = Gs2['Sage2.00fr0.03']
 
# O/H from MacGaugh 91 (Kewley & Ellison 2008, A1, A2)
ylab = G.tabGrid['O3O2'] + np.log10(1 + 1/2.97)
xlab = G.tabGrid['O23']
M_OH_low = -4.944 + 0.767 * xlab + 0.602 * xlab**2 - ylab * (0.29 + 0.332 * xlab - 0.331 * xlab**2)
M_OH_upp = -2.939 - 0.2 * xlab - 0.237 * xlab**2 - 0.305 * xlab**3 - 0.0283 * xlab**4 - ylab * (0.0047 - 0.0221 * xlab - 0.102 * xlab**2 - 0.0817 * xlab**3 - 0.00717 * xlab**4)
M_OH = np.where( (G.tabGrid['N2O2'] < -1.), M_OH_low, M_OH_upp)
M_OH = np.where( (G.tabGrid['O3O2'] > -999) & (G.tabGrid['O23'] > -999), M_OH, -999)
d = M_OH - G.tabGrid['OXYGEN']

psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=25)

fig = plt.figure(8)
plt.clf()

ax = plt.subplot(221)
plt.scatter(G.tabGrid['logN2O'], d, c=12+G.tabGrid['OXYGEN'], lw=0)
cb = plt.colorbar()
cb.set_label('12 + log O/H')
plt.xlabel('log N/O')
plt.ylabel('log O/H$_\mathrm{M91}$ - log O/H$_\mathrm{grid}$')
nplot.fix_ticks(ax)

ax = plt.subplot(222)
plt.scatter(12+G.tabGrid['OXYGEN'], G.tabGrid['logN2O'], c=d, lw=0)
cb = plt.colorbar()
cb.set_label('log O/H$_\mathrm{M91}$ - log O/H$_\mathrm{grid}$')
plt.xlabel('12 + log O/H')
plt.ylabel('log N/O')
nplot.fix_ticks(ax)

fig.set_tight_layout(True)


####################################################################################################
# Weird sources
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=15)
fig = plt.figure(104)
plt.clf()

subp = 4, 5

ff = S.tabData['flagOk_ab']

flag2 = (log_LHb >= log_LHb_min) | (log_LHb <= -999)
ax = plt.subplot2grid(subp, (1, 0)) 
pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (ff & flag2), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1)) 
plt.title('BOND')
pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'sol', flag = flag2, addToTitle = 'BOND 2 Myr | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

#_f = (abs(f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'] - 8.07 + 12) < 0.1) & (abs(f2.PDFSummaries__tcps[ftype]['sol']['logN2O'] +1.14) < 0.1) & S.tabData['flagOk_ab']
iScrs = [81]
_f = utils.ind2flag(S.tabData, iScrs)
pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (_f), branch = 'sol', addToTitle = 'BOND 2 Myr | ', pltLegend=False, colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (_f), branch = 'wro', addToTitle = 'BOND 2 Myr | ', pltLegend=False, colours = 'r', marker = 'x', s = 50., lw = 2)
fig.set_tight_layout(True) 

'''

####################################################################################################
# Compare to Perez-Montero 2014
psetup(fig_width_pt=screenwidth, aspect=0.7, lw=3., fontsize=17)
fig = plt.figure(9)
plt.clf()

subp = 3, 5

aux = S.tabData[['F3727', 'F4363', 'F5007', 'F6584', 'F6724']].copy()
del aux.meta['comments']
for k in aux.keys():
    aux[k] = 10.**aux[k]

#aux.write("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/input_SPIRALS-HEBCD.txt", format='ascii.fast_no_header')

#aux['F4363'] = 0
#aux.write("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/input_SPIRALS-HEBCD_noF4363.txt", format='ascii.fast_no_header')

# Terminal:
#cd ~/projects/strong_lines/HII-CHI-mistry_v01
#python HII-CHI-mistry_v01.py 

p1 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_noF4363_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p2 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')

p1['OXYGEN'] -= 12.
p2['OXYGEN'] -= 12.

#ff = S.tabData['flagOk_ab']
ff = utils.ind2flag(S.tabData, f2.sourcesRowsFitted)

ftype = 'ave'
f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2Ha': [-1.3, 'low']})

ax = plt.subplot2grid(subp, (0, 0)) 
#pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype=ftype, recalc=False, flag = (ff), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'BOND 2 Myr | ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_table_xy(p1, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PM14-noF4363 | ', zorder=10)
#plt.errorbar(12+p1['OXYGEN'], p1['logN2O'], xerr=p1['eOXYGEN'], yerr=p1['elogN2O'], lw=1, linestyle="None", capsize=0)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2)) 
pl.plot_fit_obsAndFit(f2, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, flag=(ff), diff_y = False, addToTitle = 'Te vs 2Myr | ', pltLegend=False)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 3)) 
#plt.errorbar(12+S.tabData['OXYGEN'], 12+p1['OXYGEN'], yerr=p1['eOXYGEN'], lw=1, linestyle="None", capsize=0)
plt.scatter(12+S.tabData['OXYGEN'], 12+p1['OXYGEN'], c='k', lw=0, s=5, zorder=10)
plt.xlabel('12 + log O/H$_\mathrm{Te}$')
plt.ylabel('12 + log O/H$_\mathrm{PM14}$')
plt.title('PM14-noF4363')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 4)) 
#plt.errorbar(12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'], 12+p1['OXYGEN'], yerr=p1['eOXYGEN'], xerr=f2.PDFSummaries__tcps['sig']['sol']['OXYGEN'], lw=1, linestyle="None", capsize=0)
plt.scatter(12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'], 12+p1['OXYGEN'],  c='k', lw=0, s=5, zorder=10)
plt.xlabel('12 + log O/H$_\mathrm{BOND}$')
plt.ylabel('12 + log O/H$_\mathrm{PM14}$')
plt.title('PM14-noF4363')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)


ff = (p2['F4363'] > 0)

ftype = 'ave'
f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2Ha': [-1.3, 'low']}, debug=True)

ax = plt.subplot2grid(subp, (1, 0)) 
#pl.plot_fit_chooseBranch(f2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype=ftype, recalc=False, flag = (ff), addToTitle = 'BOND 2 Myr | ', pltLegend=False)
pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'BOND 2 Myr | ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1)) 
pl.plot_table_xy(p2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PM14-F4363 | ', zorder=10)
#plt.errorbar(12+p2['OXYGEN'], p2['logN2O'], xerr=p2['eOXYGEN'], yerr=p2['elogN2O'], lw=1, linestyle="None", capsize=0)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 1)) 
pl.plot_table_xy(p2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PM14-F4363 | ', zorder=10)
#plt.errorbar(12+p2['OXYGEN'], p2['logN2O'], xerr=p2['eOXYGEN'], yerr=p2['elogN2O'], lw=1, linestyle="None", capsize=0)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2)) 
pl.plot_fit_obsAndFit(f2, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, flag=(ff), diff_y = False, addToTitle = 'Te vs 2Myr | ', pltLegend=False)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 3)) 
#plt.errorbar(12+S.tabData['OXYGEN'], 12+p2['OXYGEN'], yerr=p2['eOXYGEN'], lw=1, linestyle="None", capsize=0)
plt.scatter(12+S.tabData['OXYGEN'][ff], 12+p2['OXYGEN'][ff], c='k', lw=0, s=5, zorder=10)
plt.xlabel('12 + log O/H$_\mathrm{Te}$')
plt.ylabel('12 + log O/H$_\mathrm{PM14}$')
plt.title('PM14-F4363')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 4)) 
#plt.errorbar(12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'], 12+p2['OXYGEN'], yerr=p2['eOXYGEN'], xerr=f2.PDFSummaries__tcps['sig']['sol']['OXYGEN'], lw=1, linestyle="None", capsize=0)
plt.scatter(12+f2.PDFSummaries__tcps[ftype]['sol']['OXYGEN'][ff], 12+p2['OXYGEN'][ff],  c='k', lw=0, s=5, zorder=10)
plt.xlabel('12 + log O/H$_\mathrm{BOND}$')
plt.ylabel('12 + log O/H$_\mathrm{PM14}$')
plt.title('PM14-F4363')
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

fig.set_tight_layout(True) 
nplot.save(fig, 'Pasadena_Spirals_HEBCD_NO_OH_astroCritN2Ha.png')

####################################################################################################
# Ar3Ne3
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(10)
plt.clf()

subp = 3, 6

fig.set_tight_layout(True) 

Gs = slr.read_OurGrids('orig', scen, forceRead = True)

ax = plt.subplot2grid(subp, (0, 0))
ip = 1

for fr in [0.03, 3.00]:
    for age in range(1, 7):

        Gscen = 'Sage%.2ffr%.2f' % (age, fr)
        G = Gs[Gscen]
        
        print G, Gscen

        ax = plt.subplot(subp[0], subp[1], ip, sharey=ax, sharex=ax)
        ip += 1

        f = G.branch['low']
        plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5, alpha=0.5)

        f = G.branch['upp']
        plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5, alpha=0.5)

        plt.xlabel('O3O2')
        plt.ylabel('Ar3Ne3')
        nplot.fix_ticks(ax)

        plt.xlim(-6, 3)
        plt.ylim(-2, 6)

        x = np.array([-9.0, 2.4])
        y = -0.4 * x - 0.4
        plt.plot(x, y, 'k', lw=1)

        plt.title(Gscen)



    
Gorig = slr.f['Gorigin']
aux = utils.calcLineSum(Gorig.tabGrid['logQ1'], Gorig.tabGrid['logQ2'], fluxInLog = True, returnAll = False)
logQHe = aux[0]
aux = utils.calcLineSum(Gorig.tabGrid['logQ0'], logQHe, fluxInLog = True, returnAll = False)
logQ = aux[0]
Gorig.tabGrid['QHe_QH'] = 10**(logQHe - Gorig.tabGrid['logQ'])

scenVars = {'age': [1, 2, 3, 4, 5, 6], 'fr': [0.03, 3.00]}
Gs = slr.gridScenarios('Gorigin_scen', gridName = 'Gorigin', forceRead = True, scenVars = scenVars)


ax = plt.subplot2grid(subp, (2, 0), colspan=2) 

for Gscen, G in Gs.items():
    print G, Gscen

    #print np.unique(G.tabGrid['age'])

    f = G.branch['low']
    plt.scatter(10**G.tabGrid['F5876'], G.tabGrid['Ar3Ne3'], c=G.tabGrid['QHe_QH'], lw=0, s=5, alpha=0.5, vmin = 0, vmax = 0.35)
    #plt.scatter(G.tabGrid['F5876'], G.tabGrid['Ar3Ne3'], c=G.tabGrid['age'], lw=0, s=5, alpha=0.5, vmin = 1, vmax = 6)
    #print G.tabGrid['age']
    
    plt.xlabel('F5876')
    plt.ylabel('log Ar3Ne3')
    nplot.fix_ticks(ax, 3, 3)

#plt.xlim(-1.6, -0.4)
plt.ylim(-4, 8)
cb = plt.colorbar()
cb.set_label(r'Q$_\mathrm{He}$/Q$_\mathrm{H}$')
#cb.set_label(r'age [Myr]')



ax = plt.subplot2grid(subp, (2, 2), colspan=2) 

for Gscen, G in Gs.items():
    print G, Gscen

    #print np.unique(G.tabGrid['age'])

    f = G.branch['low']
    plt.scatter(10**G.tabGrid['F5876'], G.tabGrid['Ar3Ne3'], c=G.tabGrid['OXYGEN'], lw=0, s=5, alpha=0.5, vmin = -5.5, vmax = -2.5)
    #plt.scatter(G.tabGrid['F5876'], G.tabGrid['Ar3Ne3'], c=G.tabGrid['age'], lw=0, s=5, alpha=0.5, vmin = 1, vmax = 6)
    #print G.tabGrid['age']
    
    plt.xlabel('F5876')
    plt.ylabel('log Ar3Ne3')
    nplot.fix_ticks(ax, 3, 3)

#plt.xlim(-1.6, -0.4)
plt.ylim(-2, 2)
cb = plt.colorbar()
cb.set_label(r'log O/H')
#cb.set_label(r'age [Myr]')


ax = plt.subplot2grid(subp, (2, 4), colspan=2)

for age in range(1, 7):

    f = Gorig.flagGrid('age', age)
    QHeQH = Gorig.tabGrid['QHe_QH'][f]            
    OH = Gorig.tabGrid['OXYGEN'][f]            
    t = np.full_like(QHeQH, age)

    plt.scatter(t, QHeQH, c=OH, lw=0, s=5, alpha=0.5)

cb = plt.colorbar()
cb.set_label('log O/H')
plt.xlabel('age [Myr]')
plt.ylabel(r'Q$_\mathrm{He}$/Q$_\mathrm{H}$')
nplot.fix_ticks(ax, 3, 3)


nplot.title_date()
nplot.save(fig, 'grid_QHe.png')


####################################################################################################
# Ar2Ne3 vs F5876
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(11)
plt.clf()

pl.plot_grid_3D(Gorig, fignumber=11, x1='F5876', x3='Ar3Ne3', x2='O3O2', color='OXYGEN')


####################################################################################################
# Hardness
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(13)
plt.clf()

subp = 3, 5

Gorig = slr.f['Gorigin']
Gorig.findBimodality()
aux = utils.calcLineSum(Gorig.tabGrid['logQ1'], Gorig.tabGrid['logQ2'], fluxInLog = True, returnAll = False)
logQHe = aux[0]
aux = utils.calcLineSum(Gorig.tabGrid['logQ0'], logQHe, fluxInLog = True, returnAll = False)
logQ = aux[0]
Gorig.tabGrid['QHe_QH'] = 10**(logQHe - Gorig.tabGrid['logQ'])

binsQHeQH = np.percentile(Gorig.tabGrid['QHe_QH'], [0, 30, 65, 100])

ax = plt.subplot2grid(subp, (0, 0))
ip = 1

for ib, b in enumerate(binsQHeQH[:-1]):

    ax = plt.subplot(subp[0], subp[1], ip, sharey=ax, sharex=ax)
    ip += 1

    f_bin = (Gorig.tabGrid['QHe_QH'] >= binsQHeQH[ib]) & (Gorig.tabGrid['QHe_QH'] <= binsQHeQH[ib+1])
    Nbin = np.sum(f_bin)
    
    f = Gorig.branch['low'] & f_bin
    Nlow = np.sum(f)
    plt.scatter(Gorig.tabGrid['O3O2'][f], Gorig.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5) #, alpha=0.5, label = 'N = %s' % Nlow)

    f = Gorig.branch['upp'] & f_bin
    Nupp = np.sum(f)
    plt.scatter(Gorig.tabGrid['O3O2'][f], Gorig.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5) #, alpha=0.5, label = 'N = %s' % Nupp)

    print ib, Nbin, Nlow+Nupp, Nlow, Nupp
    
    #plt.legend()
    plt.xlabel('O3O2')
    plt.ylabel('Ar3Ne3')
    nplot.fix_ticks(ax)

    plt.xlim(-6, 3)
    plt.ylim(-2, 6)

    x = np.array([-9.0, 3])
    y = -0.4 * x - 0.4
    plt.plot(x, y, 'k', lw=1)

    plt.title('QHeQH=%.2f-%.2f' % (binsQHeQH[ib], binsQHeQH[ib+1]))


fig.set_tight_layout(True) 

####################################################################################################
# New fits?
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(13)
plt.clf()

subp = 3, 5


flag2 = (f2.nSolutions__s > 0) & (utils.ind2flag(S.tabData, f2.sourcesRowsFitted))
print flag2.sum()

ftype = 'jpdf'

ax = plt.subplot2grid(subp, (0, 0)) 
f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2Ha': [-1.3, 'low']})
plt.sca(ax)
pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = flag2, addToTitle = 'N2Ha')
nplot.fix_ticks(ax)
#plt.xlim(6.3, 9.7)
#plt.ylim(-2.2, 0.4)

ax = plt.subplot2grid(subp, (0, 1)) 
f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2O2': [-1.0, 'low']})
plt.sca(ax)
pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = flag2, addToTitle = 'N2O2')
nplot.fix_ticks(ax)
#plt.xlim(6.3, 9.7)
#plt.ylim(-2.2, 0.4)

ax = plt.subplot2grid(subp, (0, 2)) 
f = (S.tabData['F5876'] > -999) & (S.tabData['F7135'] > -999) & (S.tabData['F3869'] > -999) & (S.tabData['F6584'] > -999)& (S.tabData['F3727'] > -999)& (S.tabData['F5007'] > -999)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = f, colours = 'k', addToTitle = 'Te | ')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 3)) 
f = (S.tabData['flagOk_ab']) & (S.tabData['F5876'] > -999) & (S.tabData['F7135'] > -999) & (S.tabData['F3869'] > -999) & (S.tabData['F6584'] > -999)& (S.tabData['F3727'] > -999)& (S.tabData['F5007'] > -999)
f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'N2O2': [-1.0, 'low']})
pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = f, addToTitle = 'N2O2')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)


#+=ax = plt.subplot2grid(subp, (1, 1)) 
#+=G.tabData['Ar3Ne3_vs_O3O2'] = S.tabData['Ar3Ne3'] + 0.4 * S.tabData['O3O2'] + 0.4
#+=f2.chooseBranch(temp_sumType=ftype, recalc=True, temp_based = [], astroCritLims = {'Ar3Ne3_vs_O3O2': [0.0, 'low']})
#+=plt.sca(ax)
#+=pl.plot_table_xy(f2.PDFSummaries__tcps[ftype]['sol'], xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = flag2, addToTitle = 'N2Ha')
#+=nplot.fix_ticks(ax)
#plt.xlim(6.3, 9.7)
#plt.ylim(-2.2, 0.4)



####################################################################################################
# After new fits
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(14)
plt.clf()

subp = 3, 5

Gs = slr.read_OurGrids('orig', None, forceRead = False)
G = slr.f[gScenario]

aux = utils.calcLineSum(G.tabGrid['logQ1'], G.tabGrid['logQ2'], fluxInLog = True, returnAll = False)
logQHe = aux[0]
aux = utils.calcLineSum(G.tabGrid['logQ0'], logQHe, fluxInLog = True, returnAll = False)
logQ = aux[0]
G.tabGrid['QHe_QH'] = 10**(logQHe - G.tabGrid['logQ'])


ax = plt.subplot2grid(subp, (0, 0), colspan=2)

QHeQH = G.tabGrid['QHe_QH']
OH = 12 + G.tabGrid['OXYGEN']
F = G.tabGrid['F5876']

plt.scatter(10**F, QHeQH, c=OH, lw=0, s=5, alpha=0.5)

cb = plt.colorbar()
cb.set_label('12+log O/H')
plt.xlabel('F5876/Hb')
plt.ylabel(r'Q$_\mathrm{He}$/Q$_\mathrm{H}$')
nplot.fix_ticks(ax, 3, 3)



####################################################################################################
# Ar3Ne3 - again
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(15)
plt.clf()

subp = 3, 6

fig.set_tight_layout(True) 

Gs = slr.read_OurGrids('orig', 'all', forceRead = True)

ax = plt.subplot2grid(subp, (0, 0))
ip = 1

for fr in [0.03, 3.00]:
    for age in range(1, 7):

        Gscen = 'Sage%.2ffr%.2f' % (age, fr)
        G = Gs[Gscen]
        
        print G, Gscen

        ax = plt.subplot(subp[0], subp[1], ip, sharey=ax, sharex=ax)
        ip += 1

        f = G.branches['low']
        #plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5, alpha=0.5)
        plt.scatter(10**G.tabGrid['F5876'][f], G.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5, alpha=0.5)

        f = G.branches['upp']
        #plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5, alpha=0.5)
        plt.scatter(10**G.tabGrid['F5876'][f], G.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5, alpha=0.5)

        plt.xlabel('F5876')
        plt.ylabel('Ar3Ne3')
        nplot.fix_ticks(ax)

        plt.xlim(0, 0.2)
        plt.ylim(-2, 6)

        #x = np.array([-9.0, 2.4])
        #y = -0.4 * x - 0.4
        #plt.plot(x, y, 'k', lw=1)

        plt.title(Gscen)
        nplot.fix_ticks(ax, 2, 2)

nplot.title_date()
#nplot.save(fig, 'grid_QHe.png')


psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(16)
plt.clf()

subp = 1, 1

fig.set_tight_layout(True) 

Gs = slr.read_OurGrids('orig', 'all', forceRead = True)

ax = plt.subplot2grid(subp, (0, 0))
ip = 1

for fr in [0.03, 3.00]:
    for age in range(1, 7):

        Gscen = 'Sage%.2ffr%.2f' % (age, fr)
        G = Gs[Gscen]
        
        print G, Gscen

        ip += 1

        f = G.branches['low']
        #plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5, alpha=0.5)
        plt.scatter(10**G.tabGrid['F5876'][f], G.tabGrid['Ar3Ne3'][f], c='b', lw=0, s=5, alpha=0.5)

        f = G.branches['upp']
        #plt.scatter(G.tabGrid['O3O2'][f], G.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5, alpha=0.5)
        plt.scatter(10**G.tabGrid['F5876'][f], G.tabGrid['Ar3Ne3'][f], c='r', lw=0, s=5, alpha=0.5)

        plt.xlabel('F5876')
        plt.ylabel('Ar3Ne3')
        nplot.fix_ticks(ax)

        plt.xlim(0, 0.2)
        plt.ylim(-2, 6)

        #x = np.array([-9.0, 2.4])
        #y = -0.4 * x - 0.4
        #plt.plot(x, y, 'k', lw=1)

        nplot.fix_ticks(ax, 2, 2)


ff = (S.tabData['F6584'] > -999) & (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['Ar3Ne3'] > -999) & (S.tabData['F5876'] > -999)
plt.scatter(10**S.tabData['F5876'][ff], S.tabData['Ar3Ne3'][ff], c='k', marker='*', lw=0, s=100, alpha=0.5)
        
nplot.title_date()
#nplot.save(fig, 'grid_QHe.png')
