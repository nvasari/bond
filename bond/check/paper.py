def plot_grid_sources(x, y, ax, flag = None, pltSources=True):

    # Plot lines
    nU  = len(G.inputVars__u['logUin'])
    nOH = len(G.inputVars__u['OXYGEN'])
    nNO = len(G.inputVars__u['logN2O'])
    colours = sns.color_palette("hls", nNO)[::-1]
    for NO, colour in zip(G.inputVars__u['logN2O'], colours):
        for OH in G.inputVars__u['OXYGEN']:
            newGrid = G.maskGrid(inputVars = {'OXYGEN': OH, 'logN2O': NO}, flag=flag)
            if (newGrid is not None):
                plt.plot(newGrid.tabGrid[x], newGrid.tabGrid[y], color=colour, lw=1)
        for U in G.inputVars__u['logUin']:
            newGrid = G.maskGrid(inputVars = {'logUin': U,  'logN2O': NO}, flag=flag)
            if (newGrid is not None):
                plt.plot(newGrid.tabGrid[x], newGrid.tabGrid[y], color=colour, lw=0.1)
                
    if pltSources:
        f = (S.tabData[x] > -999) & (S.tabData[y] > -999)
        plt.scatter(S.tabData[x][f], S.tabData[y][f], color='#999999', marker=',', s=1, zorder=0, lw=1)
    
    plt.xlabel(x)
    plt.ylabel(y)
    nplot.fix_ticks(ax)



Gs = slr.read_OurGrids('orig', scen, forceRead = False)
G = Gs['Sage2.00fr0.03']

psetup(fig_width_pt=screenwidth, aspect=0.3, lw=3., fontsize=25, override_params = {'figure.subplot.bottom': 0.18})
fig = plt.figure(1)
plt.clf()

ax1 = plt.subplot(131)
plot_grid_sources('F6584', 'F5007', ax1)

ax2 = plt.subplot(132, sharey=ax1)
plot_grid_sources('F3727', 'F5007', ax2)
plt.setp(ax2.get_yticklabels(), visible=False)

ax3 = plt.subplot(133, sharey=ax1)
plot_grid_sources('O23', 'O3O2', ax3)
plt.setp(ax3.get_yticklabels(), visible=False)



#Gs = slr.read_OurGrids('full', scen, forceRead = False)
Gs = slr.read_OurGrids('orig', scen, forceRead = False)
G = Gs['Sage2.00fr0.03']

#G.findBimodality()
f_low = G.branch['low']
f_upp = G.branch['upp']

psetup(fig_width_pt=screenwidth, aspect=0.3, lw=3., fontsize=25, override_params = {'figure.subplot.bottom': 0.18})
fig = plt.figure(2)
plt.clf()

ax1 = plt.subplot(131)
plot_grid_sources('OXYGEN', 'O23', ax1, pltSources = False)

ax2 = plt.subplot(132, sharey=ax1, sharex=ax1)
plot_grid_sources('OXYGEN', 'O23', ax2, flag=f_low, pltSources = False)
plt.setp(ax2.get_yticklabels(), visible=False)

ax3 = plt.subplot(133, sharey=ax1, sharex=ax1)
plot_grid_sources('OXYGEN', 'O23', ax3, flag=f_upp, pltSources = False)
plt.setp(ax3.get_yticklabels(), visible=False)



#Gs = slr.read_OurGrids('full', scen, forceRead = False)
Gs = slr.read_OurGrids('orig', scen, forceRead = False)
G = Gs['Sage2.00fr0.03']

#G.findBimodality()
f_low = G.branch['low']
f_upp = G.branch['upp']

psetup(fig_width_pt=screenwidth, aspect=0.3, lw=3., fontsize=25, override_params = {'figure.subplot.bottom': 0.18})
fig = plt.figure(2)
plt.clf()

ax1 = plt.subplot(131)
plot_grid_sources('OXYGEN', 'F5876', ax1, pltSources = False)

ax2 = plt.subplot(132, sharey=ax1, sharex=ax1)
plot_grid_sources('OXYGEN', 'O23', ax2, flag=f_low, pltSources = False)
plt.setp(ax2.get_yticklabels(), visible=False)

ax3 = plt.subplot(133, sharey=ax1, sharex=ax1)
plot_grid_sources('OXYGEN', 'O23', ax3, flag=f_upp, pltSources = False)
plt.setp(ax3.get_yticklabels(), visible=False)



