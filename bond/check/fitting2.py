'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots

Fs = {}

for sScenario in sScenarios:
  for gScenario in gScenarios:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
      dirName = 'fits/'
      baseName = 'fit_%s_%s' % (Fscenario, suffix)
          
      # ===> Read fit
      fitFile = os.path.join(dirName, '%s_summary' % baseName)
      f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)
      f.dirName = dirName
      f.baseName = baseName

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[Fscenario] = f


ff = (S.tabData['F6584'] > -999) & (S.tabData['F3727'] > -999) & (S.tabData['F5007'] > -999) & (S.tabData['Ar3Ne3'] > -999)
ff2 = (S.tabData['limF4363'] > -999).data | (S.tabData['limF5755'] > -999).data | (S.tabData['limF6312'] > -999).data | (S.tabData['limF7135'] > -999).data | (S.tabData['limF7325'] > -999).data
ff3 = ff & ff2

sourcesRowsFitted = deepcopy(f.sourcesRowsFitted)
f.sourcesRowsFitted = np.where(ff3)[0]

# Refit
f.loadProbData()

# Apply step prior
f.stepPrior()

# Recalc 
f.priorSolutions()
f.calcPosterior()
f.calcPDFsAll()
f.calcPDFSummaries()

# Save
f.baseName = baseName + '_upperLims'
overwrite = True
f.sourcesRowsFitted = sourcesRowsFitted

f.saveToHDF5(overwrite = overwrite)
tab = f.saveToTxt(overwrite = overwrite)
f.saveFittedSourcesToHDF5(overwrite = overwrite)
f.saveProbToHDF5(overwrite = overwrite)
