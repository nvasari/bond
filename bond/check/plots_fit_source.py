'''
Plot PDFs

Natalia@UFSC - 22/May/2015

Running:
%time %run plots_fit_source.py lines      orig all  HIIBCD
%time %run plots_fit_source.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen)
S = slr.readSources(sources, debug = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

# Read lines, all_lines and all_ratios
obsFit1__f, grid, scen, sources = slr.options('', 'lines', *sys.argv[2:])
suffix1 = '_'.join(obsFit1__f)
obsFit2__f, grid, scen, sources = slr.options('', 'all_lines', *sys.argv[2:])
suffix2 = '_'.join(obsFit2__f)
obsFit3__f, grid, scen, sources = slr.options('', 'all_ratios', *sys.argv[2:])
suffix3 = '_'.join(obsFit3__f)
suffixes = [suffix1, suffix2, suffix3]

errCookingFactors = [0.1, 10., 50., 100.]
fakeErrs = [0.01, 0.1, 1.0]
for errCookingFactor in errCookingFactors:
    suffix = "%s_errC%05.1f" % (suffix1, errCookingFactor)
    suffixes.append(suffix)
for fakeErr in fakeErrs:
    suffix = "%s_fErr%4.2f" % (suffix1, fakeErr)
    suffixes.append(suffix)

#=========================================================================
# ===> Read fits & do plots
gScenario = 'Sage2.00fr0.03'
sScenario = sources

xsize = 9.
nplot.plotSetup(300, lw = 2., fontsize=15, override_params={'text.usetex': False, 'font.weight': 'normal'})

for suffix in suffixes[3:]:

    Fscenario = 's%s_g%s' % (sScenario, gScenario)
    dirName = 'fits/'
    baseName = 'fit_grid%s_%s_%s' % (grid, Fscenario, suffix)
    #baseName = 'false_fit_grid%s_%s_%s' % (grid, Fscenario, suffix)
    
    # ===> Read fit
    fitFile = os.path.join(dirName, '%s_summary' % baseName)
    f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fitFile = '%s.hdf5' % fitFile)
    G = Gs[gScenario]
    f.sources = S
    f.grid = G

    # Load data of all sources into a single fit
    iSources = [675]
    #iSources = f.sourcesRowsFitted[80:81]
    #iSources = f.sourcesRowsFitted
    f.debug = True
    f.loadDataSources(baseName, iSources, nChars = 4, dirName = dirName, debug = True)
    f.chooseBranch()

    # Plots
    for iSource in iSources:
    #for iSource in iSources[:1]:
        plt.figure(21, figsize=(14,10))
        plt.clf()
        pl.plot_PDFs(f, iSource)
        plt.savefig('plots/fitting_PDFs_%s_s%04i.png' % (baseName, iSource))
    
        fig = plt.figure(22, figsize=(14,10))
        plt.clf()
        pl.allPlots_gridSourceFit(f, iSource, psize = 3)
        fig.set_tight_layout(True)
        plt.savefig('plots/fitting_chi2_%s_s%04i.png' % (baseName, iSource))

        
'''
'''
