'''
Plots for paper1

Natalia@Meudon - 31/Jul/2015

Running:
%time %run plots_fit_sources_scenarios.py lines      orig all  HIIBCD
%time %run plots_fit_sources_scenarios.py all_ratios full main SDSS
'''

import os
import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt
import astropy.table
import seaborn as sns

import strongLinesHelper as sl

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
Gs = slr.read_OurGrids(grid, scen, forceRead = False)
S = slr.readSources(sources, debug = False, forceRead = False)

suffix = '_'.join(obsFit__f)
gScenarios = Gs.keys()
sScenarios = [sources]

#=========================================================================
# ===> Read fits & do plots

Fs = {}

for sScenario in sScenarios:
  for gScenario in gScenarios:
       
      Fscenario = 's%s_g%s' % (S.tabDataName, gScenario)
      dirName = 'fits/'
      baseName = 'fit_%s_%s' % (Fscenario, suffix)

      ## ===> Read fit
      #fitFile = os.path.join(dirName, '%s_summary' % baseName)
      #f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      # New fits with upper limits
      #baseName = baseName + '_upperLims'
      #baseName = baseName + '_upperLims2'
      baseName = baseName + '_upperLims3'
      #baseName = baseName + '_chebyshev'
          
      # ===> Read fit
      fitFile = os.path.join(dirName, '%s_summary' % baseName)
      #f = slr.readFit('grid%s_%s_%s_chebyshev' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)
      #f = slr.readFit('grid%s_%s_%s_upperLims2' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)
      #f = slr.readFit('grid%s_%s_%s_upperLims3' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)
      f = slr.readFit('grid%s_%s_%s_upperLims' % (grid, Fscenario, suffix), fileName = '%s.hdf5' % fitFile, forceRead = False)

      '''
      f.dirName = dirName
      f.baseName = baseName
      f.loadProbData()
      f.boxPrior(nSigma = 2.)
      f.priorSolutions()
      '''

      G = Gs[gScenario]
      f.sources = S
      f.grid = G
      Fs[Fscenario] = f
  
#=========================================================================
# ===> Set up plots

columnwidth = 240.
textwidth = 504.
screenwidth = 1024.
psetup = nplot.plotSetupMinion
#psetup = nplot.plotSetup

##########################################################################
# N/O vs O/H
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=15)
fig = plt.figure(1)
plt.clf()
nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))


subp = 4, 5


#print ff1.sum(), ff2.sum()

ff = S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 
#pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (_f), colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'Te | ', colours='b')
plt.xlim(6.9, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

# N/O vs O/H

xfit = f.PDFSummaries__tcps['ave']['all']['OXYGEN'] + 12
yfit = f.PDFSummaries__tcps['ave']['all']['logN2O']
_f = (xfit > 8.7) & (yfit < -1.5) & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0)
print np.where(_f)
_f = utils.ind2flag(S.tabData, [269])

ff2 = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
#ff2 = utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (1, 0)) 
#pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (_f), colours = 'g', marker = 'x', s = 50., lw = 2, pltLegend=False)
#pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff), addToTitle = 'BOND ave | ', pltLegend=False, colours='b')
#pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (~ff&ff2),  addToTitle = 'BOND ave | ', pltLegend=False, colours='r')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff2),  addToTitle = 'BOND ave | ', pltLegend=False, colours='r')
plt.xlim(6.9, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)


'''
iScrs = [18]
_f = utils.ind2flag(S.tabData, iScrs)

ff = S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 
#pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (_f), colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff&f1), addToTitle = 'Te | ', colours='b')
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff&f2), addToTitle = 'Te | ', colours='r')
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff&f3), addToTitle = 'Te | ', colours='y')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

# N/O vs O/H

#ff = utils.ind2flag(S.tabData, f.sourcesRowsFitted)
ax = plt.subplot2grid(subp, (1, 0)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (_f), pltLegend=False, colours = 'g', marker = 'x', s = 50., lw = 2)
#pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff&f1), addToTitle = 'BOND ave | ', pltLegend=False, pltUncer=True, colours='b')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff&f1), addToTitle = 'BOND ave | ', pltLegend=False, colours='b')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff&f2), addToTitle = 'BOND ave | ', pltLegend=False, colours='r')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff&f3), addToTitle = 'BOND ave | ', pltLegend=False, colours='y')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 1)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'med', flag = ff, addToTitle = 'BOND med | ', pltLegend=False, pltUncer=True)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'jpdf', flag = ff, addToTitle = 'BOND jpdf | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 3)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'peak', flag = ff, addToTitle = 'BOND peak | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 4)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'best', flag = ff, addToTitle = 'BOND best | ', pltLegend=False)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

# Fit vs observed

ax = plt.subplot2grid(subp, (2, 0)) 
pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, diff_y = False, flag = (_f), pltLegend=False, colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'OXYGEN', dx = 12, dy = 12, diff_y = False, flag = ff, addToTitle = 'Te vs BOND ave | ', pltLegend=False, colours=colours)
nplot.plot_identity(np.array([6.3, 9.7]), np.array([6.3, 9.7]), color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(6.3, 9.7)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 1)) 
pl.plot_fit_obsAndFit(f, xlab = 'logN2O', ylab = 'logN2O', diff_y = False, flag = (_f), pltLegend=False, colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_fit_obsAndFit(f, xlab = 'logN2O', ylab = 'logN2O', diff_y = False, flag = ff, addToTitle = 'Te vs BOND ave | ', pltLegend=False, colours=colours)
nplot.plot_identity(np.array([-2.2, 0.4]), np.array([-2.2, 0.4]), color = '#666666', lw = 1)
plt.xlim(-2.2, 0.4)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 2)) 
pl.plot_fit_obsAndFit(f, xlab = 'Ar3Ne3', ylab = 'Ar3Ne3', diff_y = False, flag = (_f), pltLegend=False, colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_fit_obsAndFit(f, xlab = 'Ar3Ne3', ylab = 'Ar3Ne3', diff_y = False, flag = ff, addToTitle = 'Obs vs BOND ave | ', pltLegend=False, colours=colours)
nplot.plot_identity(np.array([-1.7, 1.5]), np.array([-1.7, 1.5]), color = '#666666', lw = 1)
plt.xlim(-1.7, 1.5)
plt.ylim(-1.7, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (2, 3)) 
pl.plot_fit_obsAndFit(f, xlab = 'F5876', ylab = 'F5876', diff_y = False, flag = (_f), pltLegend=False, colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_fit_obsAndFit(f, xlab = 'F5876', ylab = 'F5876', diff_y = False, flag = ff, addToTitle = 'Obs vs BOND ave | ', pltLegend=False, colours=colours)
nplot.plot_identity(np.array([-1.3, -0.8]), np.array([-1.3, -0.8]), color = '#666666', lw = 1)
plt.xlim(-1.3, -0.8)
plt.ylim(-1.3, -0.8)
nplot.fix_ticks(ax, 3, 3)



ax = plt.subplot2grid(subp, (2, 4)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = f2, addToTitle = 'BOND ave | ', pltLegend=False, colours='b')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = f1, addToTitle = 'BOND ave | ', pltLegend=False, colours='r')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, 0.4)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 4)) 
pl.plot_fit_obsAndFit(f, xlab = 'age', ylab = 'OXYGEN', x_data = False,  diff_y = True, flag = f2, addToTitle = 'Te vs BOND ave | ', pltLegend=False, colours='b')
pl.plot_fit_obsAndFit(f, xlab = 'age', ylab = 'OXYGEN', x_data = False,  diff_y = True, flag = f1, addToTitle = 'Te vs BOND ave | ', pltLegend=False, colours='r')
plt.xlim(0, 7)
plt.ylim(-2, 2)
nplot.fix_ticks(ax)

# Ar3Ne3 vs O/H

a, b = 0.984, -7.63
x = np.array([6.3, 9.7])
y = a * x + b

ff = S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
x, y = S.tabData['OXYGEN'][ff], S.tabData['Ar3Ne3'][ff]
z = np.polyfit(x, y, 2)
#z[0] *= 1.1
p = np.poly1d(z)
r_x = np.linspace(6.3, 9.7, 20) - 12
r_y = p(r_x) # * regression[0] + regression[1]

ax = plt.subplot2grid(subp, (3, 0)) 
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, flag = (_f), colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, flag = ff, addToTitle = 'Te | ', colours=colours)
#plt.plot(12+r_x, r_y, color = '#666666', lw = 1)
plt.plot(12+r_x, r_y+0.3, color = '#666666', lw = 1)
#plt.plot(12+r_x, r_y-0.3, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1.7, 1.5)
nplot.fix_ticks(ax)


ff = utils.ind2flag(S.tabData, f.sourcesRowsFitted)
ax = plt.subplot2grid(subp, (3, 1)) 
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, ftype = 'ave', flag = (_f), pltLegend=False, colours = 'g', marker = 'x', s = 50., lw = 2)
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, ftype = 'ave', flag = (ff&f1), addToTitle = 'BOND ave | ', pltLegend=False, colours='b')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, ftype = 'ave', flag = (ff&f2), addToTitle = 'BOND ave | ', pltLegend=False, colours='r')
pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'Ar3Ne3', dx = 12, ftype = 'ave', flag = (ff&f3), addToTitle = 'BOND ave | ', pltLegend=False, colours='y')
plt.plot(x, y, color = '#666666', lw = 1)
#plt.plot(12+r_x, r_y, color = '#666666', lw = 1)
plt.plot(12+r_x, r_y+0.3, color = '#666666', lw = 1)
#plt.plot(12+r_x, r_y-0.3, color = '#666666', lw = 1)
plt.xlim(6.3, 9.7)
plt.ylim(-1.7, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 2)) 
pl.plot_xy(S.tabData['OXYGEN'], 10**S.tabData['Ar3Ne3'], xlab = 'OXYGEN', ylab = 'Ar3Ne3 lin', dx = 12, flag = ff, addToTitle = 'Te | ', colours=colours)
plt.xlim(6.3, 9.7)
plt.ylim(-0.2, 1.5)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 3)) 
pl.plot_xy(f.PDFSummaries__tcps['ave']['all']['OXYGEN'], 10**f.PDFSummaries__tcps['ave']['all']['Ar3Ne3'], xlab = 'OXYGEN', ylab = 'Ar3Ne3 lin', dx = 12, flag = ff, addToTitle = 'BOND | ', colours=colours)
plt.xlim(6.3, 9.7)
plt.ylim(-0.2, 1.5)
nplot.fix_ticks(ax)

fig.set_tight_layout(True)
#nplot.save(fig, 'Pasadena_HEBCD_WHb_dOH_2_6Myr.png')
'''

fig.set_tight_layout(True)
#nplot.save(fig, 'Pasadena_HEBCD_WHb_dOH_2_6Myr.png')


##########################################################################
# N/O vs O/H
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=15)
fig = plt.figure(2)
plt.clf()
nplot.title_date('Spirals+HEBCD %s' % (suffix.replace('_', ' ')))

dRO3 = f.PDFSummaries__tcps['ave']['all']['RO3'] - S.tabData['RO3']

subp = 1, 1

import matplotlib.colors as mplcolors
colours = sns.hls_palette(len(f.sourcesRowsFitted), l=.5, s=.8)
cmap = mplcolors.ListedColormap(colours)

ff = S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, f.sourcesRowsFitted) & (f.nSolutions__s > 0) & (S.tabData['F5876'] > -999)
ax = plt.subplot2grid(subp, (0, 0)) 

xobs = S.tabData['OXYGEN'][ff] + 12
yobs = S.tabData['logN2O'][ff]
xfit = f.PDFSummaries__tcps['ave']['all']['OXYGEN'][ff] + 12
yfit = f.PDFSummaries__tcps['ave']['all']['logN2O'][ff]

pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'Te | ', colours='k', s=10)
#pl.plot_fit(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = 'ave', flag = (ff),  addToTitle = 'BOND ave | ', pltLegend=False, s=10, marker = 'o', c=dRO3)
plt.scatter(xfit, yfit, s=40, marker = 'o', c=dRO3[ff], cmap=cmap, lw=0)
cb = plt.colorbar()
cb.set_label(r'log [OIII]4363/[OIII]5007$_\mathrm{fit}$ - log [OIII]4363/[OIII]5007$_\mathrm{obs}$')

for xo, yo, xf, yf in zip(xobs, yobs, xfit, yfit):
    plt.plot([xo, xf], [yo, yf], color='#999999', lw = 1)

#plt.xlim(6.9, 9.7)
#plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

nplot.save(fig, 'compare_Te_BOND.png')
fig.set_tight_layout(True)


####################################################################################################
# Compare to Perez-Montero 2014
psetup(fig_width_pt=screenwidth, aspect=0.6, lw=3., fontsize=17)
fig = plt.figure(3)
plt.clf()

subp = 4, 5

p1 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_noF4363_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p2 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_v01.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p3 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_noF4363_v01.2.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p4 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_SPIRALS-HEBCD_v01.2.txt", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')
p5 = astropy.table.Table.read("/Users/natalia/projects/strong_lines/HII-CHI-mistry_v01/output_muestra_4363_v01.2.dat", names=('F3727', 'F4363', 'F5007', 'F6584', 'F6724', 'OXYGENTe', 'logN2OTe', 'grid', 'OXYGEN', 'eOXYGEN', 'logN2O', 'elogN2O', 'logUin', 'elogUin'), format='ascii.fast_no_header')

p1['OXYGEN'] -= 12.
p2['OXYGEN'] -= 12.
p3['OXYGEN'] -= 12.
p4['OXYGEN'] -= 12.
p5['OXYGEN'] -= 12.


ff = S.tabData['flagOk_ab']
ax = plt.subplot2grid(subp, (0, 0)) 
#pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (_f), colours = 'b', marker = 'x', s = 50., lw = 2)
pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'Te | ', colours='b')
plt.xlim(6.9, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)


ax = plt.subplot2grid(subp, (0, 1)) 
pl.plot_table_xy(p1, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PMv1-noF4363 | ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 2)) 
pl.plot_table_xy(p1, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1-noF4363 | ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 3)) 
pl.plot_table_xy(p2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PMv1-F4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (0, 4)) 
pl.plot_table_xy(p2, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1-F4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)
fig.set_tight_layout(True)


ax = plt.subplot2grid(subp, (1, 1)) 
pl.plot_table_xy(p3, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PMv1.2-noF4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 2)) 
pl.plot_table_xy(p3, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1.2-noF4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 3)) 
pl.plot_table_xy(p4, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (ff), addToTitle = 'PMv1.2-F4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (1, 4)) 
pl.plot_table_xy(p4, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'PMv1.2-F4363 ', zorder=10)
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)
fig.set_tight_layout(True)


ax = plt.subplot2grid(subp, (3, 0)) 
pl.plot_table_xy(p5, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, addToTitle = 'muestra4363', zorder=10, facecolor='r')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)

ax = plt.subplot2grid(subp, (3, 1)) 
pl.plot_table_xy(p5, xlab = 'OXYGENTe', ylab = 'logN2OTe', addToTitle = 'muestra4363 Te', zorder=10, facecolor='r')
plt.xlim(6.3, 9.7)
plt.ylim(-2.2, -0.2)
nplot.fix_ticks(ax)


nplot.save(fig, 'compare_Te_PM14_check.png')

'''
'''
