'''
Check fit with and without 4363

Natalia@Corrego - 08/Jun/2015

%time %run plots_4363.py lines full main
'''

import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt
import astropy.table

# Reader
import strongLinesHelper as sl
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot
import utils
import plotHelpers as pl

#=========================================================================
# ==> Read sources & grid
obsFit__f, grid, scen, sources = slr.options(*sys.argv)
S = slr.readSources(sources, debug = False)
Gs = slr.read_OurGrids(grid, scen)

# Read lines, all_lines and all_ratios
obsFit1__f, grid, scen, sources = slr.options('', 'lines', *sys.argv[2:])
suffix1 = '_'.join(obsFit1__f)
obsFit2__f, grid, scen, sources = slr.options('', 'all_lines', *sys.argv[2:])
suffix2 = '_'.join(obsFit2__f)
obsFit3__f, grid, scen, sources = slr.options('', 'all_ratios', *sys.argv[2:])
suffix3 = '_'.join(obsFit3__f)
suffixes = [suffix1, suffix2, suffix3]

#=========================================================================
# ===> Read fits & do plots
gScenario = 'Sage2.00fr0.03'
sScenario = sources

#for i in range(20):
#    plt.close()

# Save figures for sources
#pl.allPlotsSources(S, figNumber = 101, saveFigs = True)

xsize = 9.
nplot.plotSetup(300, lw = 2., fontsize=15, override_params={'text.usetex': False, 'font.weight': 'normal'})

dfig = 0

for suffix in suffixes:

    Fscenario = 's%s_g%s' % (sScenario, gScenario)
    dirName = 'fits/'
    baseName = 'fit_grid%s_%s_%s' % (grid, Fscenario, suffix)
    
    # ===> Read fit
    fitFile = os.path.join(dirName, '%s_summary' % baseName)
    f = slr.readFit('grid%s_%s_%s' % (grid, Fscenario, suffix), fitFile = '%s.hdf5' % fitFile)
    G = Gs[gScenario]
    f.sources = S
    f.grid = G

    # Check results for average 
    ftype = 'ave'
    fresult = f.PDFSummaries__tcps[ftype] 
    

    #### Choosing branches
    f.chooseBranch(force_sumType=ftype)
    ####################################################################################################
    # ===> Plots
    subp = (3, 4)
    

    ####################################################################################################
    # Check O/H vs N/O
    fig = plt.figure(dfig+1, figsize=(xsize*np.sqrt(2), xsize))
    plt.clf()
    nplot.title_date('%s %s' % (Fscenario, suffix))

    iScrs = [675]
    #iScrs = np.where(S.tabData['flagOk_ab'])

    # Checking sources
    #print np.where((S.tabData['flagOk_ab']) & (fresult['sol']['OXYGEN'] < (7.5 - 12)) & (fresult['sol']['logN2O'] < 7.5))
    #iScrs = [62]

    ax = plt.subplot2grid(subp, (0, 0)) 
    pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = S.tabData['flagOk_ab'], colours = 'k', addToTitle = 'Te | ')
    pl.plot_table_xy(S.tabData, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, flag = (S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, iScrs)), colours = 'r', marker = 'x', s = 100., lw = 2, pltTitle = False)
    plt.xlim(6.3, 9.7)
    plt.ylim(-2, 1)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (0, 1)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'OXYGEN', diff_y = False, dx = 12, dy = 12, color = 'k', flag = (S.tabData['flagOk_OH']), branch = 'sol', pltLegend = False)
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'OXYGEN', diff_y = False, dx = 12, dy = 12, flag = (S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, iScrs)), colours = 'r', marker = 'x', s = 100., lw = 2, pltTitle = False, pltLegend = False)
    plt.xlim(6.3, 9.7)
    plt.ylim(6.3, 9.7)
    plt.plot([6.3, 9.7], [6.3, 9.7])
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (1, 0)) 
    plt.title('BOND')
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = S.tabData['flagOk_ab'], addToTitle = 'BOND chosen | ')
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, iScrs)), colours = 'r', marker = 'x', s = 100., lw = 2, pltTitle = False, pltLegend = False)
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, flag = (S.tabData['flagOk_ab'] & utils.ind2flag(S.tabData, iScrs)), colours = 'r', marker = 'x', s = 100., lw = 2, pltTitle = False, pltLegend = False, branch = 'wro', alpha = 0.4)
    plt.xlim(6.3, 9.7)
    plt.ylim(-2.2, 0.4)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (1, 1)) 
    plt.title('BOND')
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'wro', flag = S.tabData['flagOk_ab'], addToTitle = 'BOND wrong | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2.2, 0.4)
    nplot.fix_ticks(ax)
    
    ax = plt.subplot2grid(subp, (2, 0)) 
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, addToTitle = 'BOND chosen | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2.2, 0.4)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (2, 1)) 
    pl.plot_fit_chooseBranch(f, xlab = 'OXYGEN', ylab = 'logN2O', dx = 12, ftype = ftype, branch = 'wro', addToTitle = 'BOND wrong | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2.2, 0.4)
    nplot.fix_ticks(ax)
    

    
    ####################################################################################################
    # Checking 4363 obs / grid
    
    #fig = plt.figure(dfig+2, figsize=(xsize*np.sqrt(2), xsize))
    #plt.clf()
    #nplot.title_date('%s %s' % (Fscenario, suffix))
    
    ax = plt.subplot2grid(subp, (0, 2)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'RO3', dx = 12, flag = (S.tabData['flagOk_OH']) )
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)
    
    ax = plt.subplot2grid(subp, (0, 3)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'RO3', dx = 12, flag = (S.tabData['flagOk_OH']), branch = 'wro', addToTitle = 'wrong | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)
    
    ax = plt.subplot2grid(subp, (1, 2)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'RN2', dx = 12, flag = (S.tabData['flagOk_OH']) )
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (1, 3)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'RN2', dx = 12, flag = (S.tabData['flagOk_OH']), branch = 'wro', addToTitle = 'wrong | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (2, 2)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'Ar3O3', dx = 12, flag = (S.tabData['flagOk_OH']) )
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)

    ax = plt.subplot2grid(subp, (2, 3)) 
    pl.plot_fit_obsAndFit(f, xlab = 'OXYGEN', ylab = 'Ar3O3', dx = 12, flag = (S.tabData['flagOk_OH']), branch = 'wro', addToTitle = 'wrong | ')
    plt.xlim(6.3, 9.7)
    plt.ylim(-2., 2)
    nplot.fix_ticks(ax)

    fig.set_tight_layout(True)

    
    ####################################################################################################
    dfig += 10

    
'''
'''
