'''
Plots to compare grid N/O

Natalia@Meudon - 25/Feb/2015
'''

import sys
import time
import re

import numpy as np
import matplotlib.pyplot as plt

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot

#=========================================================================
# ==> Fit lines or line ratios?
try:
    sys.argv[1]
    fit = sys.argv[1]
except:
    fit = 'lines'

if fit == 'lines':
    obsFit__f = ['F3727', 'F5007', 'F6584']
else:
    obsFit__f = ['O23', 'O3O2', 'N2O2']

suffix = '_'.join(obsFit__f)
print "@@ Reading fits for %s." % obsFit__f

#=========================================================================
# ===> Define grid, scenarios
# Read original grid file
Gorigin = slr.init('Gorigin', gridFile = 'grids/tab_HII_15_All.hdf5', whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
scenVars = {'age': [2], 'fr': [0.03]}
GOs = slr.gridScenarios('Gorigin', scenVars = scenVars)

sScenarios = ['Sage2.00fr0.03']

#=========================================================================
# ===> Read fits & sources; do plots

for sScenario in sScenarios:
    for gScenario in GOs.keys():

        if sScenario == gScenario:

             # ===> Read fits & sources
             Fscenario = 's%s_g%s' % (sScenario, gScenario)
             fitFile = 'fits/compare_gridNO_fit_%s_%s.hdf5' % (Fscenario, suffix)
             f = slr.readFit('gridNO_%s_%s' % (Fscenario, suffix), fitFile = fitFile)
             S = f.sources
     
             print "@@> Checking sources %s with grid %s for parameters %s" % (sScenario, gScenario, suffix)
     
             # Save source data
             PO = 'OXYGEN'
             PN = 'logN2O'
             PU = 'logUin'
             inpO__s = S.tabData[PO]
             inpN__s = S.tabData[PN]
             inpU__s = S.tabData[PU]
         
             # Check results for average and median, peak (marginalized PDF), and peak (full PDF).
             fresults_keys = ['ave', 'med', 'peak', 'best']
             fresults = f.zoom_summaries 
         
             xsize = 9.
             nplot.plotSetup(300, lw = 2., fontsize=15, override_params={'text.usetex': True, 'font.weight': 'normal'})
             
             fig = plt.figure(Fscenario, figsize=(xsize, xsize*np.sqrt(2)))
             plt.clf()
             plt.suptitle(r'\verb|%s %s %s|' % (Fscenario, suffix, time.strftime("%d/%b/%Y %H:%M:%S")), y = 1)
         
             subp = (5, 3)
         
             # Check results for all PDF summaries
             for i_type, ftype in enumerate(fresults_keys):
         
                 fresult = fresults[ftype]
     
                 # Get fitted O/H and N/O
                 outO__s = np.array(fresult['sol'][PO])
                 outN__s = np.array(fresult['sol'][PN])
                 outU__s = np.array(fresult['sol'][PU])
     
                 # Get the result for the other (wrong) branch
                 outO_wrong__s = fresult['wro'][PO]
                 outN_wrong__s = fresult['wro'][PN]
                 outU_wrong__s = fresult['wro'][PU]
     
     
                 # Plots
                 ax = plt.subplot2grid(subp, (i_type, 0)) 
                 plt.title(ftype)
                 ax.errorbar(12+inpO__s, (outO__s - inpO__s), fmt = 'r.', markersize = 3.)
                 plt.xlabel("12 + log O/H in")
                 plt.ylabel("$\Delta$ log O/H")
                 ax.axhline(0., color = 'g')
                 plt.ylim(-1., 1.)
                 nplot.fix_ticks(ax, 2, 3)
                 
                 ax = plt.subplot2grid(subp, (i_type, 1)) 
                 plt.errorbar(inpN__s, (outN__s - inpN__s), fmt = 'r.', markersize = 3.)
                 plt.xlabel("log N/O in")
                 plt.ylabel("$\Delta$ log N/O")
                 plt.axhline(0., color = 'g')
                 plt.ylim(-1., 1.)
                 nplot.fix_ticks(ax, 2, 3)
         
                 ax = plt.subplot2grid(subp, (i_type, 2)) 
                 plt.errorbar(inpU__s, (outU__s - inpU__s), fmt = 'r.', markersize = 3.)
                 plt.xlabel("log U in")
                 plt.ylabel("$\Delta$ log U")
                 plt.axhline(0., color = 'g')
                 plt.ylim(-1., 1.)
                 nplot.fix_ticks(ax, 2, 3)
         
         
         
             fig.set_tight_layout(True)
             fig.savefig("p1f03_plots_fit_gridNO_%s_%s.pdf" % (Fscenario, suffix))
        
'''
'''
