'''
Choosing branch -- branched from plots_compare_scenarios.py

Natalia@UFSC - 26/Nov/2014
'''

import sys
import time
import re
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot

#=========================================================================
# ==> Fit lines or line ratios?
try:
    sys.argv[1]
    fit = sys.argv[1]
except:
    fit = 'lines'

if fit == 'lines':
    obsFit__f = ['F3727', 'F5007', 'F6584']
else:
    obsFit__f = ['O23', 'O3O2', 'N2O2']

suffix = '_'.join(obsFit__f)
print "@@ Reading fits for %s." % obsFit__f

#=========================================================================
# ===> Define grid, scenarios
# Read original grid file
Gorigin = slr.init('Gorigin', gridFile = 'grids/tab_HII_15_All.hdf5', whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
scenVars = {'age': [2, 4], 'fr': [0.03]}
GOs = slr.gridScenarios('Gorigin', scenVars = scenVars)

sScenarios = ['Sage2.00fr0.03']

#=========================================================================
# ===> Read fits & sources; do plots

for sScenario in sScenarios:
    for gScenario in GOs.keys():

        if sScenario == gScenario:

            # ===> Read fits & sources
            Fscenario = 's%s_g%s' % (sScenario, gScenario)
            fitFile = 'fits/compare_scenarios_fit_%s_%s.hdf5' % (Fscenario, suffix)
            # ++ Force re-reading file
            #slr.fit['%s_%s' % (Fscenario, suffix)] = False
            f = slr.readFit('%s_%s' % (Fscenario, suffix), fitFile = fitFile)
            S = f.sources
    
            print "@@> Checking sources %s with grid %s for parameters %s" % (sScenario, gScenario, suffix)
    
            # Save source data
            PO = 'OXYGEN'
            PN = 'logN2O'
            PU = 'logUin'
            inpO__s = S.tabData[PO]
            inpN__s = S.tabData[PN]
            inpU__s = S.tabData[PU]
        
            # Check results for average and median, peak (marginalized PDF), and peak (full PDF).
            fresults_keys = ['ave', 'med', 'peak', 'best']
            fresults = f.zoom_summaries 
        
            xsize = 9.
            nplot.plotSetup(300, lw = 2., fontsize=15, override_params={'text.usetex': True, 'font.weight': 'normal'})
    

            
            #### Choosing branches
            temp_upperLims = [0.001, 0.01, 0.10]

            fig = plt.figure(Fscenario, figsize=(xsize, xsize*np.sqrt(2.)))
            plt.clf()
            plt.suptitle(r'\verb|%s %s %s|' % (Fscenario, suffix, time.strftime("%d/%b/%Y %H:%M:%S")), y = 1)
            subp = (5, 3)
    
        
            for ilim, temp_upperLim in enumerate(temp_upperLims):
            
                # Save observations
                tabData = deepcopy(f.sources.tabData)
                for line in ['F4363', 'F5755']:
                    flag_noObs = (f.sources.tabData[line] < np.log10(temp_upperLim))
                    f.sources.tabData[line][flag_noObs] = -999.
                    #print f.sources.tabData[line], tabData[line]
        
                # Choosing branches - forcing solutions
                f.chooseBranch(forceVars=['OXYGEN'])
                # Get fitted O/H and N/O
                outO_forced__s = np.array(f.zoom_summaries['ave']['sol'][PO]).copy()
                outN_forced__s = np.array(f.zoom_summaries['ave']['sol'][PN]).copy()
                outU_forced__s = np.array(f.zoom_summaries['ave']['sol'][PU]).copy()
        
                # Choosing branches - like in real life
                f.chooseBranch(temp_upperLim = temp_upperLim, astroCrit = True, debug = True)
                f.sources.tabData = deepcopy(tabData)
                outO_test__s = np.array(f.zoom_summaries['ave']['sol'][PO]).copy()
                outN_test__s = np.array(f.zoom_summaries['ave']['sol'][PN]).copy()
                outU_test__s = np.array(f.zoom_summaries['ave']['sol'][PU]).copy()
                f_noBd = f.branchCrit__s == 'no_bimodal'
                f_N2O2 = f.branchCrit__s == 'N2O2'
                f_4363lim = (f.branchCrit__s == 'F4363') & (f.branchType__s == 'temp_lim')
                f_5755lim = (f.branchCrit__s == 'F5755') & (f.branchType__s == 'temp_lim')
                f_4363obs = (f.branchCrit__s == 'F4363') & (f.branchType__s == 'temp_obs')
                f_5755obs = (f.branchCrit__s == 'F5755') & (f.branchType__s == 'temp_obs')
        
        
                ax = plt.subplot2grid(subp, (ilim, 0)) 
                ax.errorbar(12+inpO__s[f_noBd], 12+outO_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
                ax.errorbar(12+inpO__s[f_N2O2], 12+outO_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
                ax.errorbar(12+inpO__s[f_4363obs], 12+outO_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
                ax.errorbar(12+inpO__s[f_5755obs], 12+outO_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
                ax.errorbar(12+inpO__s[f_4363lim], 12+outO_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
                ax.errorbar(12+inpO__s[f_5755lim], 12+outO_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
                plt.xlabel("12 + log O/H input")
                plt.ylabel("12 + log O/H in chosen")
                nplot.fix_ticks(ax, 2, 3)
                plt.title(r'lim = %.3f H$\beta$' % temp_upperLim)
                if ilim == 0:
                    plt.legend(loc = 'upper left', numpoints = 1, frameon = False, bbox_to_anchor=(0.05, 1.01), labelspacing = 0.05, handlelength = 0, borderaxespad = 0.01, fontsize = 'x-small')

    
        
                ax = plt.subplot2grid(subp, (ilim, 1)) 
                ax.errorbar(inpN__s[f_noBd], outN_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
                ax.errorbar(inpN__s[f_N2O2], outN_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
                ax.errorbar(inpN__s[f_4363obs], outN_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
                ax.errorbar(inpN__s[f_5755obs], outN_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
                ax.errorbar(inpN__s[f_4363lim], outN_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
                ax.errorbar(inpN__s[f_5755lim], outN_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
                plt.xlabel("log N/O input")
                plt.ylabel("log N/O in chosen")
                nplot.fix_ticks(ax, 2, 3)
        
                ax = plt.subplot2grid(subp, (ilim, 2)) 
                ax.errorbar(inpU__s[f_noBd], outU_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
                ax.errorbar(inpU__s[f_N2O2], outU_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
                ax.errorbar(inpU__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
                ax.errorbar(inpU__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
                ax.errorbar(inpU__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'bx', markersize = 4., label = '4363obs')
                ax.errorbar(inpU__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'mx', markersize = 4., label = '5755obs')
                plt.xlabel("log U input")
                plt.ylabel("log U in chosen")
                nplot.fix_ticks(ax, 2, 3)
    
            
            
            fig.set_tight_layout(True)
            fig.savefig("p1f05_plots_choose_branch_gridVsGrid_%s_%s.pdf" % (Fscenario, suffix))

'''

'''
