'''
Plots to compare scenarios

Natalia@UFSC - 26/Nov/2014
'''

import sys
import time
import re
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt

# Clever reader
import strongLinesReader as slr

# Some useful functions
import plotutils as nplot

#=========================================================================
# ==> Fit lines or line ratios?
try:
    sys.argv[1]
    fit = sys.argv[1]
except:
    fit = 'lines'

if fit == 'lines':
    obsFit__f = ['F3727', 'F5007', 'F6584']
else:
    obsFit__f = ['O23', 'O3O2', 'N2O2']

suffix = '_'.join(obsFit__f)
print "@@ Reading fits for %s." % obsFit__f

#=========================================================================
# ===> Define grid, scenarios
# Read original grid file
Gorigin = slr.init('Gorigin', gridFile = 'grids/tab_HII_15_All.hdf5', whoDidTable = 'Natalia', useLog = True, tableIsInLog = True)
scenVars = {'age': [2, 4], 'fr': [0.03]}
GOs = slr.gridScenarios('Gorigin', scenVars = scenVars)

sScenarios = ['Sage2.00fr0.03']

#=========================================================================
# ===> Read fits & sources; do plots

for sScenario in sScenarios:
    for gScenario in GOs.keys()[1:]:
        #++for gScenario in GOs.keys():

        # ===> Read fits & sources
        Fscenario = 's%s_g%s' % (sScenario, gScenario)
        fitFile = 'fits/compare_scenarios_fit_%s_%s.hdf5' % (Fscenario, suffix)
        # ++ Force re-reading file
        #slr.fit['%s_%s' % (Fscenario, suffix)] = False
        f = slr.readFit('%s_%s' % (Fscenario, suffix), fitFile = fitFile)
        S = f.sources

        print "@@> Checking sources %s with grid %s for parameters %s" % (sScenario, gScenario, suffix)

        # Save source data
        PO = 'OXYGEN'
        PN = 'logN2O'
        PU = 'logUin'
        inpO__s = S.tabData[PO]
        inpN__s = S.tabData[PN]
        inpU__s = S.tabData[PU]
    
        # Check results for average and median, peak (marginalized PDF), and peak (full PDF).
        fresults_keys = ['ave', 'med', 'peak', 'best']
        fresults = f.zoom_summaries 
    
        xsize = 9.
        nplot.plotSetup(300, lw = 2., fontsize=15, override_params={'text.usetex': True, 'font.weight': 'normal'})

        
        '''
        fig = plt.figure(Fscenario, figsize=(np.sqrt(2)*xsize, xsize))
        plt.clf()
        plt.suptitle(r'\verb|%s %s %s|' % (Fscenario, suffix, time.strftime("%d/%b/%Y %H:%M:%S")))
        subp = (4, 5)
    
        # Check results for all PDF summaries
        for i_type, ftype in enumerate(fresults_keys):
    
            fresult = fresults[ftype]

            # Get fitted O/H and N/O
            outO__s = np.array(fresult['sol'][PO])
            outN__s = np.array(fresult['sol'][PN])
            outU__s = np.array(fresult['sol'][PU])

<<<<<<< local
            # Get the result for the other (wrong) branch
            outO_wrong__s = fresult['wro'][PO]
            outN_wrong__s = fresult['wro'][PN]
            outU_wrong__s = fresult['wro'][PU]
=======
    # Check results for average and median, peak (marginalized PDF), and peak (full PDF).
    fresults_keys = ['ave', 'med', 'peak', 'best']
    fresults = { 'ave': Fs[Fscenario].zoom_parAve__cps, 
                 'med': Fs[Fscenario].zoom_parMed__cps,
                 'peak': Fs[Fscenario].zoom_parPeak__cps, 
                 'best': Fs[Fscenario].zoom_parBest__cps }
    #print Fs[Fscenario].zoom_parAve__cps['low'][iP_O]
>>>>>>> other


            # Plots
            ax = plt.subplot2grid(subp, (i_type, 0)) 
            plt.title(ftype)
            ax.errorbar(12+inpO__s, (outO__s - inpO__s), fmt = 'r.', markersize = 3.)
            plt.xlabel("12 + log O/H in")
            plt.ylabel("$\Delta$ log O/H")
            ax.axhline(0., color = 'g')
            plt.ylim(-1., 1.)
            nplot.fix_ticks(ax, 2, 3)
            
            ax = plt.subplot2grid(subp, (i_type, 1)) 
            plt.errorbar(inpN__s, (outN__s - inpN__s), fmt = 'r.', markersize = 3.)
            plt.xlabel("log N/O in")
            plt.ylabel("$\Delta$ log N/O")
            plt.axhline(0., color = 'g')
            plt.ylim(-1., 1.)
            nplot.fix_ticks(ax, 2, 3)
    
            ax = plt.subplot2grid(subp, (i_type, 2)) 
            plt.errorbar(inpU__s, (outU__s - inpU__s), fmt = 'r.', markersize = 3.)
            plt.xlabel("log U in")
            plt.ylabel("$\Delta$ log U")
            plt.axhline(0., color = 'g')
            plt.ylim(-1., 1.)
            nplot.fix_ticks(ax, 2, 3)
    
            ax = plt.subplot2grid(subp, (i_type, 3)) 
            ax.plot(12+inpO__s, inpN__s, 'bx', label = 'in')
            ax.plot(12+outO__s, outN__s, 'r.', label = 'out', markersize = 3.)
            #ax.plot(12+outO_wrong__s, outN_wrong__s, 'g.', alpha = 0.5, label = 'out wrong')
            #plots=[ax.plot([outO__s[i], outO_wrong__s[i]], [outN__s[i], outN_wrong__s[i]], 'g', alpha = 0.5) for i in range(len(outO_wrong__s))]
            #ax.legend(loc = 'upper left')
            #plt.title('%s %s' % (Fscenario, ftype))
            plt.xlim(6.3, 9.7)
            plt.ylim(-2.2, 0.4)
            plt.xlabel("12 + log O/H")
            plt.ylabel("log N/O")
            nplot.fix_ticks(ax)
    
            ax = plt.subplot2grid(subp, (i_type, 4)) 
            plt.plot(12+inpO__s, inpU__s, 'bx', label = 'in')
            plt.plot(12+outO__s, outU__s, 'r.', label = 'out', markersize = 3.)
            #plt.plot(outO_wrong__s, outU_wrong__s, 'g.', alpha = 0.5, label = 'out wrong')
            #plots=[plt.plot([outO__s[i], outO_wrong__s[i]], [outU__s[i], outU_wrong__s[i]], 'g', alpha = 0.5) for i in range(len(outO_wrong__s))]
            #plt.legend(loc = 'upper left')
            #plt.title('%s %s' % (Fscenario, ftype))
            plt.xlim(6.3, 9.7)
            plt.ylim(-4.5, -0.5)
            plt.xlabel("12 + log O/H")
            plt.ylabel("log U")
            nplot.fix_ticks(ax)
    
    
    
        fig.set_tight_layout(True)
        fig.savefig("plots_compare_scenarios_%s_%s.pdf" % (Fscenario, suffix))
        '''


        #### Choosing branches
        temp_upperLim = 0.01
        # Save observations
        tabData = deepcopy(f.sources.tabData)
        for line in ['F4363', 'F5755']:
            flag_noObs = (f.sources.tabData[line] < np.log10(temp_upperLim))
            f.sources.tabData[line][flag_noObs] = -999.
            #print f.sources.tabData[line], tabData[line]

        fig = plt.figure(Fscenario, figsize=(np.sqrt(2)*xsize, xsize))
        plt.clf()
        plt.suptitle(r'\verb|%s %s %s|' % (Fscenario, suffix, time.strftime("%d/%b/%Y %H:%M:%S")))
        subp = (3, 4)

        # Choosing branches - forcing solutions
        f.chooseBranch(forceVars=['OXYGEN'])
        # Get fitted O/H and N/O
        outO_forced__s = np.array(f.zoom_summaries['ave']['sol'][PO]).copy()
        outN_forced__s = np.array(f.zoom_summaries['ave']['sol'][PN]).copy()
        outU_forced__s = np.array(f.zoom_summaries['ave']['sol'][PU]).copy()

        # Choosing branches - like in real life
        f.chooseBranch(temp_upperLim = temp_upperLim, astroCrit = True, debug = True)
        f.sources.tabData = deepcopy(tabData)
        outO_test__s = np.array(f.zoom_summaries['ave']['sol'][PO]).copy()
        outN_test__s = np.array(f.zoom_summaries['ave']['sol'][PN]).copy()
        outU_test__s = np.array(f.zoom_summaries['ave']['sol'][PU]).copy()
        f_noBd = f.branchCrit__s == 'no_bimodal'
        f_N2O2 = f.branchCrit__s == 'N2O2'
        f_4363lim = (f.branchCrit__s == 'F4363') & (f.branchType__s == 'temp_lim')
        f_5755lim = (f.branchCrit__s == 'F5755') & (f.branchType__s == 'temp_lim')
        f_4363obs = (f.branchCrit__s == 'F4363') & (f.branchType__s == 'temp_obs')
        f_5755obs = (f.branchCrit__s == 'F5755') & (f.branchType__s == 'temp_obs')

        
        #f.chooseBranch(temp_upperLim = 0.1, debug = True)
        #tab = f.saveToTxt()
        #print tab[['sourceName', 'branch', 'branchType', 'branchCrit', 'sol_OXYGEN_ave', 'low_OXYGEN_ave', 'upp_OXYGEN_ave', 'branchTempUndef', 'obs_F4363']][200:210]

        ax = plt.subplot2grid(subp, (0, 0)) 
        #ax.errorbar(12+inpO__s, (outO__s - inpO__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(12+inpO__s[f_noBd], 12+outO_forced__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(12+inpO__s[f_N2O2], 12+outO_forced__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(12+inpO__s[f_4363obs], 12+outO_forced__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(12+inpO__s[f_5755obs], 12+outO_forced__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(12+inpO__s[f_4363lim], 12+outO_forced__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(12+inpO__s[f_5755lim], 12+outO_forced__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("12 + log O/H input")
        plt.ylabel("12 + log O/H correct branch")
        plt.legend(loc = 'upper left')
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (1, 0)) 
        #ax.errorbar(12+inpO__s, (outO__s - inpO__s), fmt = 'r.', markersize = 3.)
        #ax.errorbar(12+inpO__s, 12+outO_test__s, fmt = ',')
        ax.errorbar(12+inpO__s[f_noBd], 12+outO_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(12+inpO__s[f_N2O2], 12+outO_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(12+inpO__s[f_4363obs], 12+outO_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(12+inpO__s[f_5755obs], 12+outO_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(12+inpO__s[f_4363lim], 12+outO_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(12+inpO__s[f_5755lim], 12+outO_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("12 + log O/H input")
        plt.ylabel("12 + log O/H in chosen")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (2, 0)) 
        #ax.errorbar(inpO__s, (outN__s - inpO__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(12+outO_forced__s[f_noBd], 12+outO_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(12+outO_forced__s[f_N2O2], 12+outO_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(12+outO_forced__s[f_4363obs], 12+outO_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(12+outO_forced__s[f_5755obs], 12+outO_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(12+outO_forced__s[f_4363lim], 12+outO_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(12+outO_forced__s[f_5755lim], 12+outO_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("12 + log O/H in correct branch")
        plt.ylabel("12 + log O/H in chosen")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (0, 1)) 
        #ax.errorbar(inpN__s, (outN__s - inpN__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(inpN__s[f_noBd], outN_forced__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(inpN__s[f_N2O2], outN_forced__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(inpN__s[f_4363obs], outN_forced__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(inpN__s[f_5755obs], outN_forced__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(inpN__s[f_4363lim], outN_forced__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(inpN__s[f_5755lim], outN_forced__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("log N/O input")
        plt.ylabel("log N/O correct branch")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (1, 1)) 
        #ax.errorbar(inpN__s, (outN__s - inpN__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(inpN__s[f_noBd], outN_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(inpN__s[f_N2O2], outN_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(inpN__s[f_4363obs], outN_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(inpN__s[f_5755obs], outN_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(inpN__s[f_4363lim], outN_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(inpN__s[f_5755lim], outN_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("log N/O input")
        plt.ylabel("log N/O in chosen")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (2, 1)) 
        #ax.errorbar(inpN__s, (outN__s - inpN__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(outN_forced__s[f_noBd], outN_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(outN_forced__s[f_N2O2], outN_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(outN_forced__s[f_4363obs], outN_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(outN_forced__s[f_5755obs], outN_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(outN_forced__s[f_4363lim], outN_test__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(outN_forced__s[f_5755lim], outN_test__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("log N/O in correct branch")
        plt.ylabel("log N/O in chosen")
        nplot.fix_ticks(ax, 2, 3)
        
        ax = plt.subplot2grid(subp, (0, 2)) 
        #ax.errorbar(inpU__s, (outU__s - inpU__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(inpU__s[f_noBd], outU_forced__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(inpU__s[f_N2O2], outU_forced__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(inpU__s[f_4363obs], outU_forced__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(inpU__s[f_5755obs], outU_forced__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(inpU__s[f_4363lim], outU_forced__s[f_4363lim], fmt = 'bx', markersize = 4., label = '4363lim')
        ax.errorbar(inpU__s[f_5755lim], outU_forced__s[f_5755lim], fmt = 'mx', markersize = 4., label = '5755lim')
        plt.xlabel("log U input")
        plt.ylabel("log U correct branch")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (1, 2)) 
        #ax.errorbar(inpU__s, (outU__s - inpU__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(inpU__s[f_noBd], outU_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(inpU__s[f_N2O2], outU_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(inpU__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(inpU__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(inpU__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'bx', markersize = 4., label = '4363obs')
        ax.errorbar(inpU__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'mx', markersize = 4., label = '5755obs')
        plt.xlabel("log U input")
        plt.ylabel("log U in chosen")
        nplot.fix_ticks(ax, 2, 3)

        ax = plt.subplot2grid(subp, (2, 2)) 
        #ax.errorbar(inpU__s, (outU__s - inpU__s), fmt = 'r.', markersize = 3.)
        ax.errorbar(outU_forced__s[f_noBd], outU_test__s[f_noBd], fmt = 'r.', markersize = 3., label = 'no bimod')
        ax.errorbar(outU_forced__s[f_N2O2], outU_test__s[f_N2O2], fmt = 'g.', markersize = 3., label = 'N2O2')
        ax.errorbar(outU_forced__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'b.', markersize = 4., label = '4363obs')
        ax.errorbar(outU_forced__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'm.', markersize = 4., label = '5755obs')
        ax.errorbar(outU_forced__s[f_4363obs], outU_test__s[f_4363obs], fmt = 'bx', markersize = 4., label = '4363obs')
        ax.errorbar(outU_forced__s[f_5755obs], outU_test__s[f_5755obs], fmt = 'mx', markersize = 4., label = '5755obs')
        plt.xlabel("log U in correct branch")
        plt.ylabel("log U in chosen")

        ax = plt.subplot2grid(subp, (0, 3)) 
        plt.errorbar(12+inpO__s, (outU_forced__s - inpU__s), fmt = 'r.', markersize = 3.)
        plt.xlabel("12 + log O/H in")
        plt.ylabel("$\Delta$ log U")
        plt.axhline(0., color = 'g')
        plt.ylim(-1., 1.)
        nplot.fix_ticks(ax, 2, 3)
        
        nplot.fix_ticks(ax, 2, 3)
        fig.set_tight_layout(True)
        
'''

'''
