from distutils.core import setup

setup(
    name='bond',
    version='0.1',
    packages=['bond'],
    url='http://bond.ufsc.br',
    license='GPL',
    author='Natalia Vale Asari',
    author_email='natalia@astro.ufsc.br',
    install_requires=['numpy >= 1.10.1',
                      'scipy >= 0.16.1',
                      'astropy >= 1.0.dev11618',
                      'matplotlib >= 1.4.3',
                      'seaborn >= 0.6.0',
                      'h5py >= 2.4.0',
                      ],
    description='BOND: Bayesian Oxygen and Nitrogen abundance Determinations'
)
