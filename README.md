# BOND

BOND infers oxygen and nitrogen abundances in giant HII regions by
comparison with a large grid of photoionization models using strong and
semi-strong lines.

Website: [bond.ufsc.br](http://bond.ufsc.br)

Natalia Vale Asari  
natalia@astro.ufsc.br  
04/May/2016

## Quick start

1) Downloading

Download the code:
[bond v1.1](https://bitbucket.org/nvasari/bond/get/v1.1.zip)

Download the initial octree grid and an example data table:

[emission line table](http://bond.ufsc.br/data/BOND_sourcesSPIRALS-HEBCD.txt)

[grid](http://bond.ufsc.br/data/tab_HII_15_All_interpOctree.hdf5)

2) Installing

Unzip the code to a directory of yout choice.

[You also likely need to add bond/bond/ to your python path.]

Install these python libraries:

    numpy (>= 1.10.1)
    scipy (>= 0.16.1)
    astropy (>= 1.0.dev11618)
    matplotlib (>= 1.4.3)
    seaborn (>= 0.6.0)
    h5py (>= 2.4.0)
    
[Note: setup.py being tested]

3) Running the example

Go to the directory where you unzipped the bond code.

    cd bond/example

Edit the configuration file (bond.cfg) to match your local directories,
especially for the grid and sources. Try to run:

    python fitting.py

4) Checking the results

You will find a few output files in your `outDir':

- A directory with an individual fit for each source
  (./out/fit_sSPIRALS-HEBCD_gGoctree/*.hdf5 if you have not edited
  bond.cfg).

- A .hdf5 file with the results for all sources combined
  (./out/fit_sSPIRALS-HEBCD_gGoctree.hdf5 if you have not edited
  bond.cfg).

- A .txt file with a summary of the PDFs for all objects
  (./out/fit_sSPIRALS-HEBCD_gGoctree.txt if you have not edited
  bond.cfg).

- A plot showing the N/O vs O/H PDFs for all sources in the current
  directory (NO_Vs_OH_BOND.pdf).

## Improvements and fixes to come

1) What summaries from the output file should you use?

2) A better python installer.

3) Checking the PDFs object by object.

4) Using your own data tables

BOND needs the following data columns:

    name      -- meaning
    F3727     -- ([O II] 3726 + 3729)/Hb
    F5007     -- [O III] 5007/Hb (not the sum of 4959 and 5007)
    F6584     -- [N II] 6584/Hb (not the sum of 6548 and 6584)
    F7135     -- [Ar III] 7135/Hb
    F3869     -- [Ne III] 3869/Hb
    F5876     -- He I 5876/Hb
    eF3727    -- uncertainty in F3727 = ([O II] 3726 + 3729)/Hb
    eF*       -- uncertainty in all the other emission line rations above
    limF4363  -- * Detection limit for [O III] 4363/Hb, if 4363 has not been detected
    limF5755  -- * Detection limit for [N II]  5755/Hb, if 5755 has not been detected
    F4363     -- * [O III] 4363/Hb (this will not be used in the fit, but will be used to calculate an upper limit for 4363)
    F5755     -- * [N II]  5755/Hb (this will not be used in the fit, but will be used to calculate an upper limit for 5755)

* Highly recommended, but not essential.